let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/scripts/app.js', 'public/js')
   .sass('resources/styles/app.scss', 'public/css')
   .sass('resources/atrium-assets/styles/atrium.scss', 'public/atrium-assets/css')
  .version();

mix.sourceMaps();

// mix.copyDirectory('resources/assets/images', 'public/images', false);

mix.browserSync({
  proxy: 'vulcan.test',
  files: [
    'resources/atrium-assets/styles/**/*.scss',
    'resources/styles/**/*.scss',
    'resources/views/**/*.php',
    'routes/**/*.php',
    'config/**/*.php'
  ]
});
