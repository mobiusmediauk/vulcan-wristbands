<?php

namespace App;

use Cart;
use App\CartItems;
use App\UserAddresses;
use App\CartCustomStrap;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user orders
     * @method orders
     * @return [type] [description]
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'userId');
    }

    /**
     * Get user addresses
     * @method addresses
     * @return [type]    [description]
     */
    public function addresses()
    {
        return $this->hasMany(UserAddresses::class, 'userId');
    }

    public function saveAddress($type, $request)
    {
        // dd($request->all(), $type, $this->id);
        UserAddresses::updateOrCreate(['userId' => $this->id, 'type' => $type], [
          'name' => $request[$type.'_name'],
          'address1' => $request[$type.'_address1'],
          'address2' => $request[$type.'_address2'],
          'city' => $request[$type.'_city'],
          'province' => $request[$type.'_province'],
          'postalCode' => $request[$type.'_postalCode'],
          'countryCode' => $request[$type.'_countryCode'],
        ]);

        return true;
    }

    /**
     * Get all user saved cart items
     *
     * @method savedCartItems
     */
    public function savedCartItems()
    {
        return $this->hasMany(CartItems::class, 'userId');
    }

    /**
     * Get all user saved custom straps
     *
     * @method savedCustomStraps
     */
    public function savedCustomStraps()
    {
        return $this->hasMany(CartCustomStrap::class, 'userId');
    }

    /**
     * //TODO: FINISH
    * Function which syncs two carts into one if user is
    * loged in and visits `/cart` page
    *
    * @method syncCarts
    */
    public function syncCarts()
    {
        $cart = Cart::content();
        $dbCart = $this->savedCartItems;
        $dbCustomStraps = $this->savedCustomStraps;
        // If user have cart items saved in database, create new `Cart::` instance
        // and populate products from db. Else if user don't have any saved cart
        // item in db, add it from `Cart::` instance
        if (count($dbCart) > 0) {
            Cart::destroy();
            foreach ($dbCart as $item) {
                if ($item->customStrap && count($dbCustomStraps) > 0) {
                    // Customiser::reasemble($dbCustomStraps);
                } else {
                    $options = [
                      'type' => $item->product->type->name,
                      'stockLevel' => $item->product->stockCount,
                      'image' => $item->product->thumb(),
                      'description' => $item->product->description
                    ];
                    Cart::add($item->product->id, $item->product->name, $item->qty, $item->product->price(), $options)->associate('App\Product\Product');
                }
            }
        } else {
            foreach ($cart as $item) {
                CartItems::create(['userId' => $this->id, 'productId' => $item->id, 'qty' => $item->qty]);
            }
        }
        return true;
    }
}
