<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['ordersEmail', 'infoEmail', 'enquiryEmail', 'GAtrackingCode', 'sendEnquiries'];
    protected $cast = [
      'sendEnquiries' => 'boolean',
    ];
}
