<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
      'leadConversionTypes' => 'array'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'orderNumber';
    }

    /**
     * Get all items for this order
     *
     * @method products
     */
    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'orderId');
    }

    /**
     * Get all generated vouchers
     *
     * @method vouchers
     */
    public function vouchers()
    {
        return $this->hasMany(Voucher::class, 'orderId');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucherId');
    }

    /**
     * Get all addresses for this order
     *
     * @method address
     */
    public function addresses()
    {
        return $this->hasMany(Address::class, 'orderId');
    }

    /**
     * Get user info by email
     *
     * @method user
     */
    public function user()
    {
        return $this->hasOne(User::class, 'email', 'email');
    }

    /**
     * Remove this order with all components
     *
     * @method delete
     */
    public function delete()
    {
        $this->addresses()->delete();
        $this->products()->delete();
        parent::delete();
    }
}
