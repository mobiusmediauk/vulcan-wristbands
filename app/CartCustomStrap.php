<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartCustomStrap extends Model
{
    protected $table = 'cartCustomStraps';
    protected $fillable = ['productId', 'userId', 'strapId', 'bandId', 'buckleId'];
}
