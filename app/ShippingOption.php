<?php

namespace App;

use File;
use App\Country;
use App\MainPage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShippingOption extends Model
{
    protected $table = "shippingOptions";

    protected $fillable = ['name', 'description', 'flatFee', 'country', 'groupedBy'];

    /**
     * Display only available options for selected country
     *
     * @method displayOptions
     */
    public static function countries($options = null)
    {
        $countries = \DB::table('countries')->get();

        if (!is_null($options)) {
            $options; #FREE
            $shippingOptions = $countries->map(function ($item, $key) use ($options) {
                $item->shippingOptions = [];
                array_push($item->shippingOptions, $options->first());
                return $item;
            });
        } else {
            $options = ShippingOption::all();
            $shippingOptions = $countries->map(function ($item, $key) use ($options) {
                $item->shippingOptions = [];
                foreach ($options as $option) {
                    if ($option->groupedBy == $item->groupedBy) {
                        array_push($item->shippingOptions, $option);
                    }
                }
                return $item;
            });
        }
        
        return $shippingOptions;
    }

    public function getCountry()
    {
        return $this->hasOne('App\Country', 'groupedBy', 'groupedBy');
    }
}
