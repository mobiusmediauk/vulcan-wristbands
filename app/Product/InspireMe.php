<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class InspireMe extends Model
{
    protected $table = 'inspireMe';

    protected $fillable = ['watchId', 'strapId', 'bandId', 'buckleId'];

    public function getWatch()
    {
        return $this->hasOne(Product::class, 'id', 'watchId');
    }

    public function getStrap()
    {
        return $this->hasOne(Product::class, 'id', 'strapId');
    }

    public function getBand()
    {
        return $this->hasOne(Product::class, 'id', 'bandId');
    }

    public function getBuckle()
    {
        return $this->hasOne(Product::class, 'id', 'buckleId');
    }
}
