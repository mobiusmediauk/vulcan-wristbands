<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'productTypes';

    /**
     * Get the comments for the blog post.
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'typeId')->orderBy('orderId', 'ASC');
    }
}
