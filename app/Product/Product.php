<?php

namespace App\Product;

use Image;
use Input;
use Currency;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

  /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
      'inStock' => 'boolean',
      'availableToBuy' => 'boolean'
    ];

    protected $fillable = ['name', 'typeId', 'stockCount', 'description', 'weight', 'rgb', 'availableToBuy'];

    /**
    * Get the Type associated with the Product.
    */
    public function type()
    {
        return $this->belongsTo(Type::class, 'typeId');
    }

    public function thumb($productId=null, $pos=null)
    {
        if ($productId == 5 || $productId == 6 || $productId == null) {
            return asset('/images/products/' . $this->thumbnail);
        } else {
            if ($productId == 2 && $pos == 'strap') {
                $thumb = $this->strapImage;
            } elseif ($productId == 2 && $pos == 'top') {
                $thumb = $this->strapAppliedTopImage;
            } elseif ($productId == 2 && $pos == 'bottom') {
                $thumb = $this->strapAppliedBottomImage;
            } else {
                $thumb = $this->thumbnail;
            }
            return asset('images/customiser/' . strtolower(str_plural($this->type->name, 2)) . '/' . $thumb);
        }
    }

    /**
     * Get product prices in different currency
     *
     * @method prices
     */
    public function prices()
    {
        return $this->hasMany(ProductPrices::class, 'productId');
    }

    /**
     * Display only price of the model without currency symbol
     * @method priceAmount
     * @return [type]      [description]
     */
    public function price($quantity=null)
    {
        return Currency::decimal($this->modelPrices($quantity), app('currency')->get(), 2);
    }

    /**
     * Get product price by currency
     *
     * @method getPriceByCurrency
     * @param  string $currency (gbp, eur, usd etc..)
     */
    public function getPriceByCurrency($currency=null)
    {
        $currency = ($currency) ? $currency : app('currency')->get();
        return Currency::decimal($this->modelPrices(), $currency, 2);
    }

    /**
     * Simple display from table (qiockfix for admin/producs not displaying large numbers)
     *
     * @method displayPriceValue
     */
    public function displayPriceValue($currency)
    {
        return number_format($this->modelPrices()[$currency], 2, '.', '');
    }

    /**
     * Price formatted with the currency symbol.
     *
     * @return string
     */
    public function priceDisplay()
    {
        return Currency::withPrefix($this->modelPrices(), null, 2);
    }

    /**
     * Construct and display all model prices
     *
     * @method modelPrices
     * @param integer $quantity Quantity
     */
    public function modelPrices($quantity=null)
    {
        return array_collapse($this->prices->map(function ($price) use ($quantity) {
            if ($quantity) {
                $list[$price->currency] = ($quantity * $price->amount);
            } else {
                $list[$price->currency] = $price->amount;
            }
            return $list;
        }));
    }

    /**
     * Get product color and convert to hex
     * @method hexColor
     */
    public function hexColor()
    {
        if ($this->rgb) {
            $c = explode(',', $this->rgb);
            $hex = "#";
            $hex .= str_pad(dechex($c[0]), 2, "0", STR_PAD_LEFT);
            $hex .= str_pad(dechex($c[1]), 2, "0", STR_PAD_LEFT);
            $hex .= str_pad(dechex($c[2]), 2, "0", STR_PAD_LEFT);
        } else {
            $hex = '#ffffff';
        }
        return $hex;
    }

    /**
     * Image uploader for products and customiser parts
     *
     * @method saveImages
     */
    public function saveImages(Request $request)
    {
        if ($request->typeId == 1) {
            if ($request->file('image')) {
                $this->customiserImage = sha1($this->slug) . '.png';
                $this->thumbnail = sha1($this->slug) . '.png';
                Image::make($request->file('image'))->save('images/customiser/watches/' . sha1($this->slug) . '.png', 60);
            }
        } elseif ($request->typeId == 2) {
            foreach ($request->files as $key => $file) {
                if ($key == 'strap') {
                    $this->strapImage = sha1($key . $this->slug) . '.png';
                } elseif ($key == 'strapTop') {
                    $this->strapAppliedTopImage =  sha1($key . $this->slug) . '.png';
                } elseif ($key == 'strapBottom') {
                    $this->strapAppliedBottomImage = sha1($key . $this->slug) . '.png';
                }
                Image::make($request->file($key))->save('images/customiser/straps/' . sha1($key . $this->slug) . '.png', 60);
            }
        } elseif ($request->typeId == 3) {
            foreach ($request->files as $key => $file) {
                if ($key == 'image') {
                    $this->customiserImage = sha1($this->slug) . '.png';
                    Image::make($request->file('image'))->save('images/customiser/bands/' . sha1($this->slug) . '.png', 60);
                } elseif ($key == 'shopImage') {
                    $this->thumbnail = sha1($this->slug) . '.png';
                    Image::make($request->file('shopImage'))->save('images/products/' . sha1($this->slug) . '.png', 60);
                }
            }
            // if ($request->file('image')) {
            //     $this->customiserImage = sha1($this->slug) . '.png';
            //     Image::make($request->file('image'))->save('images/customiser/bands/' . sha1($this->slug) . '.png', 60);
            // }
        } elseif ($request->typeId == 4) {
            if ($request->file('image')) {
                $this->customiserImage = sha1($this->slug) . '.png';
                $this->thumbnail = sha1($this->slug) . '.png';
                Image::make($request->file('image'))->save('images/customiser/buckles/' . sha1($this->slug) . '.png', 60);
            }
        } else {
            if ($request->file('image')) {
                $this->thumbnail = sha1($this->slug) . '.png';
                Image::make($request->file('image'))->save('images/products/' . sha1($this->slug) . '.png', 60);
            }
        }
        $this->save();
    }
}
