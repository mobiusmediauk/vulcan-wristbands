<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductPrices extends Model
{
    protected $table = 'productPrices';
    protected $fillable = ['productId', 'currency', 'amount'];
}
