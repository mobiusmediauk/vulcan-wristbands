<?php

namespace App\Media;

use Illuminate\Database\Eloquent\Model;

use Image;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class SliderItem extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'sliderItems';
    protected $guarded = [
      'background',
      'image',
    ];

    /**
     * Save image
     */
    public function saveMedia($mediaFile, $type)
    {
        switch ($type) {
          case 'item':
              if ($this->getItemMedia()) {
                  $this->getItemMedia()->delete();
              }
            break;

          case 'background':
          if ($this->getBackgroundMedia()) {
              $this->getBackgroundMedia()->delete();
          }
            break;
        }

        $this->addMedia($mediaFile)
              ->toMediaCollection('slider-' . $type, 'public');
    }

    /**
     * Get slider
     */
    public function slider()
    {
        return $this->belongsTo(Slider::class, 'sliderId');
    }

    /**
     * Get item media
     */
    public function getItemMedia()
    {
        return $this->media->where('collection_name', 'slider-item')->first();
    }

    /**
     * Get media item
     */
    public function getBackgroundMedia()
    {
        return $this->media->where('collection_name', 'slider-background')->first();
    }
}
