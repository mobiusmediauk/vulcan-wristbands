<?php

namespace App\Media;

use App\MainPage;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $guarded = [];
    /**
     * Get slider items
     */
    public function items()
    {
        return $this->hasMany(SliderItem::class, 'sliderId')->orderBy('id', 'asc');
    }

    /**
     * Slider page relation
     */
    public function page()
    {
        return $this->hasOne(MainPage::class, 'pageId');
    }
}
