<?php

namespace App\Mail;

use App\Order;
use App\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $output = $this->replaceVariables();
        return $this->markdown('emails.orders.shipped', compact('output'));
    }

    /**
     * Used to replace admin defined variables to real variables
     *
     * {customer.name}
     * {order.number}
     * {order.tracking_number}
     * {order.vouchers}
     *
     * @method replaceVariables
     */
    private function replaceVariables()
    {
        $template = EmailTemplate::find(1);

        $text = [
          '{customer.name}',
          '{order.number}',
          '{order.tracking_number}',
          '{order.vouchers}'
        ];

        # If user's not registered, use billing address name
        if (isset($this->order->user->name)) {
            $customerName = $this->order->user->name;
        } elseif (isset($this->order->addresses->where('type', 'billing')->first()->name) && $this->order->addresses->where('type', 'billing')->first()->name !== '') {
            $customerName = $this->order->addresses->where('type', 'billing')->first()->name;
        } else {
            $customerName = 'Customer';
        }

        $variables = [
          $customerName,
          $this->order->orderNumber,
          $this->order->trackingNumber,
          $this->vouchersHtmlOutput(),
        ];

        return str_replace($text, $variables, $template->body);
    }

    /**
     * Generate vouchers html list
     * @method vouchersOutput
     * @return [type]         [description]
     */
    private function vouchersHtmlOutput()
    {
        $output = '';
        if (count($this->order->vouchers) > 0) {
            $output .= 'You also bought ' . str_plural('voucher', count($this->order->vouchers)) . ':<br/>';
            foreach ($this->order->vouchers as $voucher) {
                $output .=  '<li>' . $voucher->name . '</li>';
            }
        }
        return $output;
    }
}
