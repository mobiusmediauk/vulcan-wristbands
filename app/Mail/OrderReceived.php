<?php

namespace App\Mail;

use Currency;
use App\Order;
use App\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance
     *
     * @method __construct
     * @param  string      $orderNumber
     * @param  boolean     $sendToAdmin Set true if you want to send order email copy to admin email
     */
    public function __construct($orderNumber, $sendToAdmin=false)
    {
        $this->sendToAdmin = $sendToAdmin;

        $this->order = Order::where('orderNumber', $orderNumber)
              ->with('products')
              ->with('vouchers')
              ->with('addresses')
              ->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->sendToAdmin) {
            $output = $this->replaceVariablesByTemplateId(3);
            return $this->subject('Thanks for your order')->markdown('emails.orders.admin.received', compact('output'));
        } else {
            $output = $this->replaceVariablesByTemplateId(2);
            return $this->subject('Thanks for your order')->markdown('emails.orders.received', compact('output'));
        }
    }

    /**
     * Used to replace admin defined variables to real variables
     *
     * @method replaceVariablesByTemplateId
     */
    private function replaceVariablesByTemplateId($templateId)
    {
        $template = EmailTemplate::find($templateId);

        $text = [
          '{customer.name}',
          '{customer.email}',
          '{order.number}',
          '{order.products}',// Simple list with products
          '{order.products.withImages}',// List with products + images
          '{order.products.details}',// Detailed list of products for admins
          '{order.products.details.withImages}',// Detailed list of products for admins
          '{order.addresses}',
        ];

        $variables = [
          $this->order->user->name ?? '',
          $this->order->email,
          $this->order->orderNumber,
          $this->productsHtmlOutput(['details' => false, 'images' => false]),
          $this->productsHtmlOutput(['details' => false, 'images' => true]),
          $this->productsHtmlOutput(['details' => true, 'images' => false]),
          $this->productsHtmlOutput(['details' => true, 'images' => true]),
          $this->addressesHtmlOutput(),
        ];

        return str_replace($text, $variables, $template->body);
    }

    /**
     * Html output of products bought
     *
     * @method productsHtmlOutput
     */
    private function productsHtmlOutput($options)
    {
        $output = '<div class="table"><table>
                    <thead>
                    <tr>
                      ' . (($options['images']) ? '<th></th>' : '') .'
                      <th>Item</th>
                      <th style="text-align:center">Quantity</th>
                      <th style="text-align:right">Unit Price</th>
                      <th style="text-align:right">Price</th>
                    </tr>
                    </thead>
                    <tbody>';

        $output .= $this->generateHtmlProductsList($options);

        if ($this->order->voucherId) {
            $subtotal = Currency::withPrefix(($this->order->subtotal - $this->order->discount), $this->order->currency, 2);
            $total = Currency::withPrefix(($this->order->total - $this->order->discount), $this->order->currency, 2);
            $output .= '<tr>
                        <td>Discount (' . $this->order->voucher->name . ')</td>
                        <td style="text-align:center"></td>
                        <td style="text-align:right"></td>
                        <td style="text-align:right"><strong>' . $this->order->discount . '</strong></td>
                      </tr>';
        } else {
            $subtotal = Currency::withPrefix($this->order->subtotal, $this->order->currency, 2);
            $total = Currency::withPrefix($this->order->total, $this->order->currency, 2);
        }

        $output .= '<tr>
                  <td></td>
                  <td style="text-align:center"></td>
                  <td style="text-align:right">Subtotal:</td>
                  <td style="text-align:right">' . $subtotal . '</td>
                </tr>
                <tr>
                  <td></td>
                  <td style="text-align:center"></td>
                  <td style="text-align:right">Shipping:</td>
                  <td style="text-align:right">' . Currency::withPrefix($this->order->shippingPrice, $this->order->currency, 2) . '</td>
                </tr>
                <tr>
                  <td></td>
                  <td style="text-align:center"></td>
                  <td style="text-align:right"><strong>Total:</strong></td>
                  <td style="text-align:right">' . $total . '</td>
                </tr>
                </tbody>
              </table></div>';

        return $output;
    }

    /**
     * Generate products html list with parameters
     *
     * @method generateHtmlProductsList
     */
    private function generateHtmlProductsList($options)
    {
        $output = '';
        if ($options['details']) {
            foreach ($this->order->products as $item) {
                $productName = $item->product->name;
                $productType = $item->product->type->name;
                $productImagePath = $item->product->thumb($item->product->type->id, ($item->product->type->id == 2) ? 'strap' : null);
                $productQty = $item->qty;
                $orderCurrency = $this->order->currency;
                $itemPriceByCurrency = $item->product->getPriceByCurrency($orderCurrency);

                if ($item->product->type->id == 2) {
                    $imgWidth = '64';
                    $imgHeight = 'auto';
                } else {
                    $imgWidth = '64';
                    $imgHeight = '64';
                }

                $output .= '<tr>
                  ' . (($options['images']) ? '<td><img src="'.$productImagePath.'" height="'.$imgHeight.'" width="'.$imgWidth.'" style="height:'.$imgHeight.'px;widht:'.$imgWidth.';"></img></td>' : '') .'
                  <td style="vertical-align: middle;">' . $productName . '<br/>(' . $productType . ')</td>
                  <td style="text-align:center">' . $productQty . '</td>
                  <td style="text-align:right">' . Currency::withPrefix($itemPriceByCurrency, $orderCurrency, 2) . '</td>
                  <td style="text-align:right">' . Currency::withPrefix(($itemPriceByCurrency * $productQty), $orderCurrency, 2) . '</td>
                  </tr>';
            }
        } else {
            $items = $this->order->products->each(function ($product) {
                if ($product->orderCustomStrapId) {
                    $product->isCustomStrap = true;
                } else {
                    $product->orderCustomStrapId = sha1(rand(111, 999).time());
                    $product->isCustomStrap = false;
                }
            })->unique('orderCustomStrapId');

            foreach ($items as $item) {
                if ($item->isCustomStrap === true && strlen($item->orderCustomStrapId) <= 10 && $item->orderCustomStrapId) {
                    $productName = $item->customStrap->name;
                    $productType = $item->customStrap->color;
                    $productImagePath = $item->customStrap->thumb();
                    $productQty = $item->customStrap->qty;
                    $orderCurrency = $this->order->currency;
                    $itemPriceByCurrency = $item->customStrap->unitPrice;
                } else {
                    $productName = $item->product->name;
                    $productType = $item->product->type->name;
                    $productImagePath = $item->product->thumb();
                    $productQty = $item->qty;
                    $orderCurrency = $this->order->currency;
                    $itemPriceByCurrency = $item->product->getPriceByCurrency($orderCurrency);
                }

                $output .= '<tr>
                    ' . (($options['images']) ? '<td><img src="'.$productImagePath.'" height="64" width="auto" style="height:64px;widht:100%;"></img></td>' : '') .'
                    <td style="vertical-align: middle;">' . $productName . '<br/>(' . $productType . ')</td>
                    <td style="text-align:center">' . $productQty . '</td>
                    <td style="text-align:right">' . Currency::withPrefix($itemPriceByCurrency, $orderCurrency, 2) . '</td>
                    <td style="text-align:right">' . Currency::withPrefix(($itemPriceByCurrency * $productQty), $orderCurrency, 2) . '</td>
                    </tr>';
            }
        }

        return $output;
    }

    /**
     * Html output of addresses
     *
     * @method addressesHtmlOutput
     */
    private function addressesHtmlOutput()
    {
        return '<div class="table"><table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Billing Address:</th>
                            <th>Shipping Address:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>Name</td>
                          <td><strong>' . $this->order->addresses->first()->name . '</strong></td>
                          <td><strong>' . $this->order->addresses[1]->name . '</strong></td>
                        </tr>
                        <tr>
                          <td>Address 1</td>
                          <td>' . $this->order->addresses->first()->address1 . '</td>
                          <td>' . $this->order->addresses[1]->address1 . '</td>
                        </tr>
                        <tr>
                          <td>Address 2</td>
                          <td>' . $this->order->addresses->first()->address2 . '</td>
                          <td>' . $this->order->addresses[1]->address2 . '</td>
                        </tr>
                        <tr>
                          <td>City</td>
                          <td>' . $this->order->addresses->first()->city . '</td>
                          <td>' . $this->order->addresses[1]->city . '</td>
                        </tr>
                        <tr>
                          <td>Province</td>
                          <td>' . $this->order->addresses->first()->province . '</td>
                          <td>' . $this->order->addresses[1]->province . '</td>
                        </tr>
                        <tr>
                          <td>Postal Code</td>
                          <td>' . $this->order->addresses->first()->postalCode . '</td>
                          <td>' . $this->order->addresses[1]->postalCode . '</td>
                        </tr>
                        <tr>
                          <td>Country Code</td>
                          <td>' . $this->order->addresses->first()->countryCode . '</td>
                          <td>' . $this->order->addresses[1]->countryCode . '</td>
                        </tr>
                        <tr>
                          <td>Mobile</td>
                          <td>' . $this->order->addresses->first()->mobile . '</td>
                          <td>' . $this->order->addresses[1]->mobile . '</td>
                        </tr>
                        <tr>
                          <td>SMS</td>
                          <td>' . $this->order->addresses->first()->sms . '</td>
                          <td>' . $this->order->addresses[1]->sms . '</td>
                        </tr>
                    </tbody>
                </table>
            </div>';
    }
}
