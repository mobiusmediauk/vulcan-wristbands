<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table = 'socialMedia';

    protected $fillable = ['name', 'link'];

    protected $casts = [
      'show' => 'boolean',
    ];
}
