<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareCounter extends Model
{
    protected $table = 'shareCounter';
}
