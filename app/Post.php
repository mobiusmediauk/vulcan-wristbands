<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Post extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function excerpt()
    {
        $body = $this->body;
        $body = str_replace('<strong>', '', $body);
        $body = str_replace('</strong>', '', $body);
        $body = str_replace('{!! $imageBundle !!}', '', $body);
        $body = str_replace('{{ $images[0] }}', '', $body);
        $body = str_replace('{{ $images[1] }}', '', $body);
        $body = str_replace('{{ $images[2] }}', '', $body);
        return $body;
    }

    /**
     * Show post cover image
     *
     * @method coverImage
     */
    public function coverImage()
    {
        return asset('/images/article/' . $this->image);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('images');
    }

    /*
    * Get the images uploaded for this post
    */
    public function images()
    {
        return $this->getMedia('images');
    }

    /**
     * Get the media items absolute paths
     */
    public function imagePaths()
    {
        $mediaItemPaths = [];
        foreach ($this->images() as $item) {
            array_push($mediaItemPaths, $item->getFullUrl());
        }
        return $mediaItemPaths;
    }

    public function outputImage()
    {
        $images = [];
        foreach ($this->imagePaths() as $image) {
            $images[] = '<li><img src=' . $image . '></li>' ;
        }
        return '<ul class="blog-body-images-group">' . implode($images) . '</ul>';
    }

    /**
     * Slugify
     *
     * @method makeSlug
     */
    public static function makeSlug($string)
    {
        $string = str_replace(array('[\', \']'), '', $string);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
        $string = preg_replace(array(
            '/[^a-z0-9]/i',
            '/[-]+/'
        ), '-', $string);
        return strtolower(trim($string, '-'));
    }
}
