<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsletterList extends Model
{
    protected $table = 'newsletters';
    protected $fillable = ['email'];
}
