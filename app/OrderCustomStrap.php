<?php

namespace App;

use Image;
use Illuminate\Database\Eloquent\Model;

class OrderCustomStrap extends Model
{
    protected $table = 'orderCustomStraps';
    protected $fillable = [
      'orderId',
      'name',
      'color',
      'image',
      'qty',
      'unitPrice',
      'bandId',
      'strapId',
      'buckleId',
    ];

    /**
     * Get custom strap image
     *
     * @method thumb
     */
    public function thumb()
    {
        return asset('images/custom-straps/'.$this->image);
    }

    /**
     * Save new generated custom strap for order
     *
     * @method saveAndGenerateImage
     */
    public static function saveAndGenerateImage($settings)
    {
        $settings['image'] =  OrderCustomStrap::saveImage($settings['orderId'], $settings['image']);
        $new = OrderCustomStrap::create($settings);
        return $new->id;
    }

    /**
     * Save new image with this custom strap and return
     * image path
     *
     * @method saveImage
     */
    private static function saveImage($cartItemId, $encodedImage)
    {
        $imagePath = sha1($cartItemId).time().'.jpg';
        Image::make($encodedImage)->save('images/custom-straps/' . $imagePath, 60);

        return $imagePath;
    }
}
