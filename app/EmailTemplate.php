<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = 'emailTemplates';

    protected $fillable = [
      'body',
      'ccCopy',
      'ccTo'
    ];

    protected $casts = [
      'ccCopy' => 'boolean'
    ];
}
