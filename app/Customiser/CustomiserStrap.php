<?php

namespace App\Customiser;

use Illuminate\Database\Eloquent\Model;

class CustomiserStrap extends Model
{
    protected $table = 'CustomiserStraps';
}
