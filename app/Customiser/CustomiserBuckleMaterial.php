<?php

namespace App\Customiser;

use Illuminate\Database\Eloquent\Model;

class CustomiserBuckleMaterial extends Model
{
    protected $table = 'CustomiserBuckleMaterials';
}
