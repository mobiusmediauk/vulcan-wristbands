<?php

namespace App\Customiser;

use Illuminate\Database\Eloquent\Model;

class CustomiserBand extends Model
{
    protected $table = 'CustomiserBands';

    /**
     * @method loadAll
     */
    public static function loadAll()
    {
        $load = CustomiserBand::all();
        //TODO: create and add prices model

        return $load;
    }
}
