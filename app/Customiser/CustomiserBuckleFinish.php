<?php

namespace App\Customiser;

use Illuminate\Database\Eloquent\Model;

class CustomiserBuckleFinish extends Model
{
    protected $table = 'CustomiserBuckleFinishes';
}
