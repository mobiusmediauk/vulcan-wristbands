<?php

namespace App;

use Auth;
use App\Product\Product;
use Illuminate\Database\Eloquent\Model;

class CartItems extends Model
{
    protected $table = 'cartItems';
    protected $fillable = ['userId', 'productId', 'qty'];
    protected $casts = [
      'customStrap' => 'boolean',
    ];

    /**
     * Get product model
     *
     * @method product
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'productId');
    }
}
