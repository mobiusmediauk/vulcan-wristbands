<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['orderId', 'type', 'address1', 'address2', 'city', 'province', 'postalCode', 'countryCode'];
}
