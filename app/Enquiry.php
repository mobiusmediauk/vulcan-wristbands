<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $fillable = ['topic', 'firstName', 'lastName', 'email', 'orderNumber', 'message'];
}
