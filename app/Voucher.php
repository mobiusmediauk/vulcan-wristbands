<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    // /**
    // * Get the route key for the model.
    // *
    // * @return string
    // */
    // public function getRouteKeyName()
    // {
    //     return 'name';
    // }

    protected $guarded = [];

    /**
    * Voucher use function
    *
    * @method use
    */
    public function use()
    {
        $this->timesUsed = ($this->timesUsed+1);
        return $this->save();
    }

    /**
     * Voucher count function
     *
     * @method discount
     */
    public function discount($total)
    {
        $total = str_replace(',', '', $total);
        if ($this->type === 'percent') {
            $t = ($total / 100) * (100 - $this->amount);
            $t = $total - $t;
        } else {
            $t = $this->amount;
        }
        return round($t);
    }

    /**
     * When user buys new Gift Coupon, it'll generate new voucher in Database
     * with amount of price of the voucher, fixed amount, one time use, without
     * expiry date
     *
     * @method generateNewVoucher
     */
    public static function generateNewVoucher($price, $qty, $orderId)
    {
        for ($i = 1; $i <= $qty; $i++) {
            Voucher::create([
              'name' => self::generateVoucherCode(),
              'amount' => ($price/$qty),
              'currency' => app('currency')->get(),
              'quantity' => 1,
              'type' => 'fixed',
              'orderId' => $orderId,
            ]);
        }
    }

    /**
     * Voucher Code generator
     *
     * @method generateVoucherCode
     */
    private static function generateVoucherCode()
    {
        return 'VWS'.rand(111111, 999999).strtoupper(app('currency')->get());
    }

    /**
     * Voucher count function but static
     *
     * @method getDiscountAmount
     */
    public static function getDiscountAmount($voucherId, $total)
    {
        $voucher = Voucher::where('id', $voucherId)->first();
        if (isset($voucher->name)) {
            $total = str_replace(',', '', $total);
            if ($voucher->type === 'percent') {
                $t = ($total / 100) * (100 - $voucher->amount);
                $t = $total - $t;
            } else {
                $t = $voucher->amount;
            }
            return round($t);
        } else {
            return 0;
        }
    }
}
