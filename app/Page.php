<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    // protected $fillable = ['title', 'header', 'slug', 'image', 'body', 'template', 'position', 'linkName'];

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

    protected $casts = [
      'position' => 'boolean'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
