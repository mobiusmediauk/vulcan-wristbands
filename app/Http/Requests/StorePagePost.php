<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class StorePagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'title' => 'required', Rule::unique('pages')->ignore($this->slug),
          'header'  => 'sometimes|required',
          'slug'  => 'required', Rule::unique('pages')->ignore($this->slug),
          'body'  => 'sometimes|nullable',
          'linkName' => 'required',
          'titleTag' => 'sometimes|required',
          'altTag' => 'sometimes|required',
          'metaDescription' => 'sometimes|required',
      ];
    }
}
