<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class StoreVoucherPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Voucher check if there's same voucher with the same name and currency:
        $id = $this->id ?: 'NULL';
        $rule = 'unique:vouchers,name,' . $id . ',id,currency,' . $this->currency;

        return [
          'name' => 'required|' . $rule,
          'type' => 'required|sometimes',
          'amount' => 'required|sometimes',
          'currency' => 'required|sometimes',
        ];
    }
}
