<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'title' => 'required', Rule::unique('posts')->ignore($this->id),
          'slug'  => 'required', Rule::unique('posts')->ignore($this->id),
          'titleTag' => 'sometimes|required',
          'altTag' => 'sometimes|required',
          'metaDescription' => 'sometimes|required',
          'type'  => 'sometimes|required',
          'body'  => 'sometimes|nullable',
          'video' => 'sometimes|required',
          'image' => 'image',
      ];
    }
}
