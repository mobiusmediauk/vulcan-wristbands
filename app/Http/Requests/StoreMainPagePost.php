<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

use Illuminate\Foundation\Http\FormRequest;

class StoreMainPagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'body1' => 'sometimes|required',
          'body2' => 'sometimes|required',
          'body3' => 'sometimes|required',
          'body4' => 'sometimes|required',

          'text1' => 'sometimes|required',
          'text2' => 'sometimes|required',

          'caption1' => 'sometimes|required',
          'caption2' => 'sometimes|required',
          'caption3' => 'sometimes|required',
          'caption4' => 'sometimes|required',
          'caption5' => 'sometimes|required',
          'caption6' => 'sometimes|required',

          // 'altTag1' => 'sometimes|required',
          // 'altTag2' => 'sometimes|required',
          // 'altTag3' => 'sometimes|required',
          // 'altTag4' => 'sometimes|required',
          // 'altTag5' => 'sometimes|required',
          // 'altTag6' => 'sometimes|required',
          // 'altTag7' => 'sometimes|required',

          'titleTag' => 'required',
          'metaDescription' => 'required',

          'slide.*' => 'sometimes|required',
          'slide.new.background' => 'required_with:slide.new.body',
          'slideTransitionSpeed' => 'sometimes|required',
        ];

        // Those were in update function on MainPageController:
        // if ($mainpage->id == 1 || $mainpage->id == 2) {
        //     $validated = $request->validate([
        //       'body1' => 'required',
        //       'body2' => 'required',
        //       'body3' => 'required',
        //       'titleTag' => 'required',
        //       'metaDescription' => 'required',
        //     ]);
        // } elseif ($mainpage->id == 5) {
        //     $validated = $request->validate([
        //     'body1' => 'required',
        //     'body2' => 'required',
        //     'body3' => 'required',
        //     'body4' => 'required',
        //     'titleTag' => 'required',
        //     'metaDescription' => 'required',
        //   ]);
        // } else {
        //     $validated = $request->validate([
        //       'body1' => 'required',
        //       'titleTag' => 'required',
        //       'metaDescription' => 'required',
        //     ]);
        // }
        //
        //
        // $validated['text1'] = (isset($request->text1)) ? $request->text1 : null;
        // $validated['text2'] = (isset($request->text2)) ? $request->text2 : null;
        //
        // $validated['caption1'] = (isset($request->caption1)) ? $request->caption1 : null;
        // $validated['caption2'] = (isset($request->caption2)) ? $request->caption2 : null;
        // $validated['caption3'] = (isset($request->caption3)) ? $request->caption3 : null;
        // $validated['caption4'] = (isset($request->caption4)) ? $request->caption4 : null;
        // $validated['caption5'] = (isset($request->caption5)) ? $request->caption5 : null;
        // $validated['caption6'] = (isset($request->caption6)) ? $request->caption6 : null;
    }

    public function messages()
    {
        return [
        'slideTransitionSpeed.required' => 'Please set slider transition speed.',
        'slide.new.background.required_with' => 'Please set a background image or video for new slide.',
      ];
    }
}
