<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailTemplatePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required',
            'ccCopy' => 'required|sometimes',
            'ccTo' => 'required_with:ccCopy',
        ];
    }

    public function messages()
    {
        return [
          'ccTo.required_with' => 'Please enter email address'
        ];
    }
}
