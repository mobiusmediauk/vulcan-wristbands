<?php

namespace App\Http\Middleware;

use Closure;
use Currency;

class CheckIp
{
    /**
     * Handle an incoming request.
     * 37.123.118.81 GB IP ADDRESS
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $getCurrencyUrl = "http://country.io/currency.json";
        $getIPdetailsUrl = "https://www.iplocate.io/api/lookup/". $_SERVER['REMOTE_ADDR'];
        $defaultCurrency = 'gbp';

        if (@file_get_contents($getIPdetailsUrl) && @file_get_contents($getCurrencyUrl)) {
            $details = json_decode(file_get_contents($getIPdetailsUrl));
            $currency = (array) json_decode(file_get_contents($getCurrencyUrl));

            $getCurrency = strtolower($currency[$details->country_code ?? 'GB']);

            // If there's no currency in array, set default:
            $setCurrency = (in_array($getCurrency, ['gbp', 'usd', 'eur', 'hkd'])) ? $getCurrency : $defaultCurrency ;
        } else {
            $setCurrency = $defaultCurrency;
        }

        session()->flash('selectedCurrency', $setCurrency);

        Currency::set($setCurrency);

        return $next($request);
    }
}
