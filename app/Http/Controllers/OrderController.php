<?php

namespace App\Http\Controllers;

use App\Order;
use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::all();
        foreach ($data as $order) {
            $order->order_number = $order->orderNumber;
            $order->tracking_number = $order->trackingNumber;
            $total = ($order->total > $order->discount) ? $order->total - $order->discount : 0 ;
            $order->price_total = ($order->voucherId) ? $total : $order->total ;
            $order->last_updated = $order->updated_at->format('Y-m-d H:i');
            
            $order_address = $order->addresses;
            if( isset( $order->addresses[0]->name ) ) {
                $order->name = $order->addresses[0]->name;
            } else {
                $order->name = '';
            }

        }
        $keys = ['order_number', 'status', 'tracking_number', 'price_total', 'email', 'name', 'last_updated'];
        return view('atrium.pages.orders-index', compact('data', 'keys'));
    }

    public function orderSuccess() {
        return view('pages.order-success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('atrium.pages.orders-show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if ($request->status == 'dispatched') {
            $validate = $request->validate([
              'trackingNumber' => 'required'
            ]);
            $order->trackingNumber = $validate['trackingNumber'];
            Mail::to($order->email)->send(new OrderShipped($order));
        }
        $order->status = $request->status;
        $order->save();

        session()->flash('notification', 'Order was successfuly updated!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();

        session()->flash('notification', 'Order removed!');
        session()->flash('type', 'positive');

        // return redirect('admin/order');
        return redirect()->back();
    }
    
    /**
     * Invoice
     */
    public function invoice(Order $order)
    {
        return view('atrium.pages.invoice', compact('order'));
    }

}
