<?php

namespace App\Http\Controllers;

use Image;
use Input;
use App\Product\Type;
use App\Product\ProductPrices;
use App\Product\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Get all accessories
     *
   * @method indexAccessories
     */
    public function indexAccessories()
    {
        $accessories = Product::where('typeId', 5)->where('availableToBuy', 1)->where('stockCount', '>', 0)->get();
        $vouchers = Product::where('typeId', 6)->where('availableToBuy', 1)->where('stockCount', '>', 0)->get();
        $bands = Product::where('typeId', 3)->where('availableToBuy', 1)->where('stockCount', '>', 0)->get();
        // dd($bands);
        return view('pages.accessories', compact('accessories', 'bands', 'vouchers'));
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Type $type=null)
    {
        if ($type) {
            $data = Product::where('typeId', $type->id)->with('type')->with('prices')->orderBy('orderId', 'ASC')->get();
            $typeName = str_plural($type->name, 2);
        } else {
            $data = Product::with('type')->with('prices')->orderBy('typeId', 'desc')->get();
            $typeName = 'Products';
        }

        foreach ($data as $product) {
            $product->name = $product->name;
            $product->product_type = $product->type->name;
            $product->unit_price = $product->getPriceByCurrency('gbp');
            $product->stock = $product->stockCount;
        }
        $keys = ['name', 'product_type', 'unit_price', 'stock'];

        return ($type && $type->id == 1) ? view('atrium.pages.reorderable-index', compact('keys', 'data', 'typeName')) : view('atrium.pages.products-index', compact('keys', 'data', 'typeName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        return view('atrium.pages.products-create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
          'name' => 'required',
          'typeId' => 'required',
          'description' => 'required',
          'stockCount'   => 'sometimes|nullable',
          'image'        => 'required_if:typeId,1,4,5',
          'strap'        => 'required_if:typeId,2',
          'strapTop'     => 'required_if:typeId,2',
          'strapBottom'  => 'required_if:typeId,2',
          'shopImage'    => 'required_if:typeId,3',
        ]);

        // Save new product:
        $product = new Product;
        $product->name = $validated['name'];
        $product->typeId = $validated['typeId'];
        $product->description = $validated['description'];
        $product->weight = (isset($request->weight)) ? $request->weight : null ;
        $product->stockCount = $validated['stockCount'];
        $product->availableToBuy = ($request->availableToBuy) ? 1 : 0;
        $product->rgb = $this->convertToRgb($request->color);
        $product->slug = $this->makeSlug($request->name . '-' . $request->type);
        $product->save();

        // Save product image:
        if (null !== $request->file('strap') || null !== $request->file('strapTop') || null !== $request->file('strapBottom') || null !== $request->file('image') || null !== $request->file('shopImage')) {
            $product->saveImages($request);
        }

        // Save prices:
        foreach (app('currency')->options() as $option) {
            $amount = ($request['price_' . $option->value]) ? $request['price_' . $option->value] : '0.00';
            ProductPrices::insert(['productId' => $product->id, 'currency' => $option->value, 'amount' => $amount]);
        }

        session()->flash('notification', 'Product created successfuly!');
        session()->flash('type', 'positive');

        return redirect('admin/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $types = Type::all();
        return view('atrium.pages.products-edit', compact('product', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $request->validate([
          'name' => 'required',
          'typeId' => 'required',
          'description' => 'required',
          'stockCount'   => 'sometimes|nullable',
          'image'        => 'sometimes|required_if:typeId,1,4,5',
          'strap'        => 'sometimes|required_if:typeId,2',
          'strapTop'     => 'sometimes|required_if:typeId,2',
          'strapBottom'  => 'sometimes|required_if:typeId,2',
          'shopImage'    => 'sometimes|required_if:typeId,3',
        ]);

        $productId = $product->id;

        // Update product image:
        if ($request->file('strap') || $request->file('strapTop') || $request->file('strapBottom') || $request->file('image') || $request->file('shopImage')) {
            $product->saveImages($request);
        }

        // Save product:
        $validated['availableToBuy'] = ($request->availableToBuy) ? 1 : 0;
        $validated['rgb'] = $this->convertToRgb($request->color);
        $product->update($validated);

        // Save prices:
        foreach (app('currency')->options() as $option) {
            $amount = ($request['price_' . $option->value]) ? $request['price_' . $option->value] : '0.00';
            $product->prices()->updateOrCreate(['productId' => $productId, 'currency' => $option->value], ['amount' => $amount]);
        }


        session()->flash('notification', 'Product updated successfuly!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Reorder Products
     *
     * @method reorder
     */
    public function reorder(Request $request)
    {
        foreach ($request->all() as $key => $item) {
            Product::whereId($item['id'])->where('typeId', $item['typeId'])->update(['orderId' => $key]);
        }

        return ['success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->prices()->delete();
        $product->delete();

        session()->flash('notification', 'Product deleted successfuly!');
        session()->flash('type', 'positive');

        return redirect('admin/product');
    }

    /**
     * Converts color to rbg
     *
     * @method convertToRgb
     */
    private function convertToRgb($hex)
    {
        $hex = str_replace("#", "", $hex);

        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1).substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1).substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1).substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        return $r . ',' . $g . ',' . $b;
    }

    public function getProductsJson() {
       return Type::with('products')->get()->each(function ($type) {
            foreach ($type->products as $product) {
                if (!in_array($product->id, [1,2])) {
                    $product['price'] = $product->price();
                }
            }
        });   
    }

    /**
     * Slugify
     * @method makeSlug
     */
    private function makeSlug($string)
    {
        $string = str_replace(array('[\', \']'), '', $string);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string);
        $string = preg_replace(array(
            '/[^a-z0-9]/i',
            '/[-]+/'
        ), '-', $string);
        return strtolower(trim($string, '-'));
    }
}
