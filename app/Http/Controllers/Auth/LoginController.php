<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'signed-in/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function signedIn() {
        if (Auth::user()->hasRole('admin')) {
            // If admin, redirect to admin dashboard
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('home');
        }    
    }

    public function join() {
        return view('auth.login');
    }

    public function logOut() {
        Auth::logout();
        return redirect('/');
    }
}
