<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keys = ['id', 'name', 'email'];
        $data = User::select($keys)->get();
        $count = User::count();
        return view('atrium.pages.user-index', compact('keys', 'data', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $orders = $user->orders;
        $addresses = $user->addresses;
        return view('atrium.pages.user-edit', compact('user', 'orders', 'addresses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validated = $request->validate([
          'name' => 'required',
          'email' => 'required',
          'shipping_name' => 'required',
          'shipping_address1' => 'required',
          'billing_name' => 'required',
          'billing_address1' => 'required',
        ]);

        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->save();

        $user->saveAddress('shipping', $request);
        $user->saveAddress('billing', $request);


        session()->flash('notification', 'User settings saved!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->addresses()->delete();
        // $user->orders->delete();
        $user->delete();

        session()->flash('notification', 'User removed!');
        session()->flash('type', 'positive');

        return redirect('admin/user');
    }

    /**
     * Update profile information
     *
     * @method adminProfileUpdate
     */
    public function adminProfileUpdate(Request $request, User $user)
    {
        $validated = $request->validate([
          'name' => 'required',
          'email' => 'sometimes|required|email|unique:users,email,' . $user->id,
        ]);

        $user->update($validated);

        session()->flash('notification', 'your profile was updated successfuly!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Display admin profile
     *
     * @method adminProfileIndex
     */
    public function adminProfileIndex()
    {
        $user = Auth::user();
        return view('atrium.pages.admin-profile', compact('user'));
    }
}
