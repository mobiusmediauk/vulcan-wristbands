<?php

namespace App\Http\Controllers;

use Auth;
use Cart;
use Currency;
use App\MainPage;
use App\CartItems;
use App\ShippingOption;
use App\Product\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{

    /**
    * User's cart with all items
    *
    * @method cart
    */
    public function cart()
    {
        // if (Auth::check()) {
        //     Auth::user()->syncCarts();
        // }

        $cartItems = Cart::content()->each(function ($item) {
            if ($item->model) {
                if ($item->options->isVoucher) {
                    $item->price = ($item->options->amount * $item->qty);
                } else {
                    $item->price = $item->model->price($item->qty);
                }
            } elseif ($item->name == 'Custom Strap') {
                $price = 0;
                if ($item->name == 'Custom Strap') {
                    $price += (int)$item->options->band->price();
                    $price += (int)$item->options->buckle->price();
                    $price += (int)$item->options->strap->price();
                }
                $item->price = $price;
            } else {
                $item->price = $item->options->singleUnitPrice * $item->qty;
            }
        });

        # Cart only has accessories, FREE shipping
        $hasAccessoriesOnly = $cartItems->search(function ($cartItem, $rowId) {
            if (!is_null($cartItem->options->strap)) {
                return true;
            }
        });

        if ($hasAccessoriesOnly == false) {
            $freeShipping = ShippingOption::where('name', 'Free Shipping')->get();
            $shippingOptions = ShippingOption::countries($freeShipping);
        } else {
            $shippingOptions = ShippingOption::countries();
        };


        $accessories = Product::where('typeId', 5)->where('availableToBuy', 1)->where('stockCount', '>', 0)->get();

        $ipDetails = MainPage::ipData();

        $doCartHaveStraps = $this->checkIfCustomStrapExists(collect($cartItems)->toArray());

        return (Cart::count() > 0) ? view('pages.cart', compact('cartItems', 'shippingOptions', 'accessories', 'ipDetails', 'doCartHaveStraps')) : view('pages.cart-empty') ;
    }

    /**
     * Add new item to cart
     *
     * @method add
     */
    public function add(Product $product)
    {
        $options = [  'type' => $product->type->name,
                      'stockLevel' => $product->stockCount,
                      'image' => $product->thumb(),
                      'description' => $product->description,
                      'isVoucher' => (isset(request()->amount)) ? true : false ,
                      'amount' => (isset(request()->amount)) ? round((request()->amount > 2000) ? 2000 : request()->amount) : $product->price() ,
                    ];

        Cart::add($product->id, $product->name, 1, $product->price(), $options)->associate('App\Product\Product');

        // if (Auth::check()) {
        //     CartItems::create(['userId' => Auth::user()->id, 'productId' => $product->id, 'qty' => 1]);
        // }

        session()->flash('status', 'Added to cart!');

        return redirect()->back();
    }

    /**
     * Increase item Quantity
     *
     * @method qty
     */
    public function qty()
    {
        Cart::update(request()->cartItemId, request()->newQty);
        return ['status' => 'Cart Updated!'];
    }

    /**
     * Destroy user's cart
     *
     * @method destroy
     */
    public function destroy()
    {
        Cart::destroy();

        // if (Auth::check()) {
        //     Auth::user()->savedCartItems->delete();
        // }

        session()->flash('status', 'Cart destroyed!');

        return redirect()->back();
    }

    /**
     * Removes item from cart
     *
     * @method remove
     */
    public function remove()
    {
        Cart::remove(request()->cartItemId);
        return ['status' => 'Item removed from cart!'];
    }

    /**
     * Set currency cookie.
     *
     * @param $id
     */
    public function setCurrency($id)
    {
        Currency::set($id);

        session()->flash('status', 'Currency changed!');
        session()->forget('voucherId');

        return redirect()->back();
    }

    public function checkIfCustomStrapExists($array)
    {
        foreach ($array as $key => $val) {
            if ($val['name'] === "Custom Strap") {
                return 1;
            }
        }
        return 0;
    }
}
