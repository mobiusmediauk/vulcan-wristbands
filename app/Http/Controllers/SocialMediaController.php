<?php

namespace App\Http\Controllers;

use App\Notifications\ShareImage;
use App\ShareCounter;
use App\SocialMedia;
use Illuminate\Http\Request;

class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socialMedia = SocialMedia::all();
        return view('atrium.pages.social-media', compact('socialMedia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function incrementShareCount() {
        ShareCounter::where('socialMedia', request('socialMedia'))
        ->increment('count');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SocialMedia $social)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  SocialMedia $social
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialMedia $social)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  SocialMedia $social
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialMedia $social)
    {
        $social->link = $request->link;
        $social->token = $request->token;
        $social->username = $request->username;
        $social->show = (isset($request->show)) ? true : false;
        $social->count = $request->count;
        $social->save();

        session()->flash('notification', 'Updated!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Share image on email
     *
     * @method emailShare
     */
    public function emailShare(Request $request)
    {
        $request->validate([
          'name' => 'required',
          'email' => 'required',
          'subject' => 'required',
        ]);

        \Notification::route('mail', $request->email)
                        ->notify(new ShareImage($request));

        ShareCounter::where('socialMedia', 'email')
                        ->increment('count');

        session()->flash('status', 'Email successfuly sent.');
        return redirect()->route('customise');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  SocialMedia $social
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialMedia $social)
    {
        //
    }
}
