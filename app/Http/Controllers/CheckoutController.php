<?php

namespace App\Http\Controllers;

use Auth;
use App\Settings;
use App\Checkout;
use App\LeadConversionType;
use App\Mail\OrderReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function __construct()
    {
        $this->settings = Settings::first();
    }

    /**
     * Show page with credit card input
     *
     * @method post
     */
    public function post(Request $request)
    {
        $request->validate([
          'shipping.name' => 'required',
          'billing.name' => 'required',
          'shipping.address1' => 'required',
          'billing.address1' => 'required',
          'deliveryOption' => 'required'
        ]);

        $addresses = ['billing' => $request['billing'], 'shipping' => $request['shipping']];

        $convTypes = LeadConversionType::all();

        session()->forget('addresses');
        session()->forget('shippingOptionId');

     // session(['addresses' => $addresses, 'shippingOptionId' => $request['deliveryOption']]);
        session(['addresses' => $addresses, 'shippingOptionId' => $request['deliveryOption'], 'rolex' => $request['rolex']]);

        if (Auth::check()) {
            return view('pages.checkout', compact('convTypes'));
        } else {
            return view('pages.checkout-login');
        }
    }

    /**
     * Adds guest email to array
     *
     * @method guest
     */
    public function guest(Request $request)
    {
        $request->validate([
          'email' => 'required',
        ]);

        $convTypes = LeadConversionType::all();

        session(['email' => $request['email']]);

        return view('pages.checkout', compact('convTypes'));
    }


    public function checkoutFailed() {
        return view('pages.failed');
    }
    /**
     * Run checkout method for worldpay
     *
     * @method checkout
     */
    public function checkout(Request $request)
    {
        if (Auth::check()) {
            $sendMailTo = Auth::user()->email;
        } else {
            $sendMailTo = session()->get('email');
        }

        $checkout = new Checkout($request->token, $request->name, $sendMailTo, (isset($request->leadConvType)) ? $request->leadConvType : null);
        $orderNumber = $checkout->checkoutAndCreateOrder();

        if ($orderNumber) {
            Mail::to($sendMailTo)->send(new OrderReceived($orderNumber));

            // Send email of order to admin:
            if (isset($this->settings->ordersEmail)) {
                Mail::to($this->settings->ordersEmail)->send(new OrderReceived($orderNumber, true));
            }

            session()->flash('orderNumber', $orderNumber);

            return redirect()->route('checkout.completed');
        } else {
            // Redirect when something bad happens with the card:
            return redirect()->route('checkout.failed');
        }
    }

    public function checkoutLogin() {
        // This page only shows when visitor is not logged in
        // otherwise jump directly to route 'checkout'
        return view('pages.checkout-login');
    }


    public function mailTest($order) {
        return new OrderReceived($order, false);
    }

    public function adminMailTest($order) {
        return new OrderReceived($order, true);
    }

    /**
     * Completed order
     *
     * @method completed
     */
    public function completed()
    {
        if (null != session('orderNumber')) {
            $orderNumber = session()->get('orderNumber');
            // return view('pages.completed', compact('orderNumber'));
            return view('pages.order-success');
        } else {
            return redirect()->route('home');
        }
    }
}
