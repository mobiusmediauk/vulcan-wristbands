<?php

namespace App\Http\Controllers;

use App\Product\Type;
use App\Product\Product;
use App\Product\InspireMe;
use Illuminate\Http\Request;

class InspireMeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = InspireMe::all();
        $error_message = '## Not found ##';
        foreach ($data as $item) {
            if( is_object($item->getWatch)  ) { $item->watch_face = $item->getWatch->name; } else { $item->watch_face = $error_message; }
            if( is_object($item->getStrap)  ) { $item->strap = $item->getStrap->name;      } else { $item->strap = $error_message;      }
            if( is_object($item->getBand)   ) { $item->band = $item->getBand->name;        } else { $item->band = $error_message;       }
            if( is_object($item->getBuckle) ) { $item->buckle = $item->getBuckle->name;    } else { $item->buckle = $error_message;     }
        }
        $keys = ['id', 'watch_face', 'strap', 'band', 'buckle'];
        return view('atrium.pages.inspireme-index', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $watchfaces = Product::where('typeId', 1)->get();
        $straps = Product::where('typeId', 2)->get();
        $bands = Product::where('typeId', 3)->get();
        $buckles = Product::where('typeId', 4)->get();

        $data = Type::with('products')->get()->each(function ($type) {
            foreach ($type->products as $product) {
                if (!in_array($product->id, [1,2])) {
                    $product['price'] = $product->price();
                }
                $product['description'] = '';
            }
        })->toJson();

        return view('atrium.pages.inspireme-create', compact('watchfaces', 'straps', 'bands', 'buckles', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validated = $request->validate([
        //   'name' => 'required',
        //   'watchId' => 'required',
        //   'strapId' => 'required',
        //   'bandId' => 'required',
        //   'buckleId' => 'required',
        // ]);

        $preset = new InspireMe;
        $preset->watchId = $request['selected']['watch'];
        $preset->strapId = $request['selected']['strap'];
        $preset->bandId = $request['selected']['band'];
        $preset->buckleId = $request['selected']['buckle'];
        $preset->save();

        return ['success'];

        // session()->flash('notification', 'Combination created!');
        // session()->flash('type', 'positive');
        //
        // return redirect()->route('inspireme.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product\InspireMe  $inspireMe
     * @return \Illuminate\Http\Response
     */
    public function show(InspireMe $inspireme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product\InspireMe  $inspireMe
     * @return \Illuminate\Http\Response
     */
    public function edit(InspireMe $inspireme)
    {
        $data = Type::with('products')->get()->each(function ($type) {
            foreach ($type->products as $product) {
                if (!in_array($product->id, [1,2])) {
                    $product['price'] = $product->price();
                }
                $product['description'] = '';
            }
        })->toJson();
        $preset = $inspireme->find($inspireme->id)->toJson();

        return view('atrium.pages.inspireme-edit', compact('preset', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product\InspireMe  $inspireMe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InspireMe $inspireme)
    {
        $inspireme->watchId = $request['selected']['watch'];
        $inspireme->strapId = $request['selected']['strap'];
        $inspireme->bandId = $request['selected']['band'];
        $inspireme->buckleId = $request['selected']['buckle'];
        $inspireme->save();

        return ['success'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product\InspireMe  $inspireMe
     * @return \Illuminate\Http\Response
     */
    public function destroy(InspireMe $inspireme)
    {
        $inspireme->delete();

        session()->flash('notification', 'Removed!');
        session()->flash('type', 'positive');

        return redirect('admin/inspireme');
    }
}
