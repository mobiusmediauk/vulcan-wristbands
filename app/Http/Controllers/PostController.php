<?php

namespace App\Http\Controllers;

use Image;
use App\Post;
use App\MainPage;

use App\Http\Requests\StoreBlogPost;

use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Post::where('type', 'article')->get();

        foreach ($data as $post) {
            $post->last_updated = $post->updated_at->format('Y-m-d H:i');
        }

        $keys = ['title', 'slug', 'last_updated'];

        return view('atrium.pages.post-index', compact('data', 'keys'));
    }

    public function vlogIndex()
    {
        $data = Post::where('type', 'video')->orderBy('id', 'DESC')->get();

        foreach ($data as $post) {
            $post->last_updated = $post->updated_at->format('Y-m-d H:i');
        }

        $keys = ['title', 'last_updated'];

        return view('atrium.pages.video-index', compact('data', 'keys'));
    }

    /**
     * Display all posts in front-end
     *
     * @method allPosts
     */
    public function allPosts()
    {
        $videos = Post::where('type', 'video')->orderBy('id', 'DESC')->limit(3)->get();
        $posts = Post::where('type', 'article')->orderBy('id', 'DESC')->get();

        return view('blog.index', compact('posts', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('atrium.pages.post-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeVlog(Request $request) {
        

        $vlogArray = [];
        $vlogArray['title'] = $_POST['title'];
        $vlogArray['type'] = 'video';
        $vlogArray['video'] = $_POST['video'];
        $vlogArray['image'] = '';
        $vlogArray['body'] = '';

        $vlogArray['slug'] = $this->generateRandomString();
//        $vlogArray['body'] = 'vlog';
        $post = Post::create($vlogArray);
        return redirect('admin/video-post') ;
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogPost $StoreBlogPost)
    {   
        
        $validatedPost = $StoreBlogPost->validated();

        if ($StoreBlogPost->has('image')) {
            $validatedPost['type'] = 'article';
        } else {
            $validatedPost['type'] = 'video';
        }

        $validatedPost['slug']  = Post::makeSlug(request('slug'));
        $validatedPost['image'] = $validatedPost['slug'].'.jpg' ?? null;

        if ($StoreBlogPost->has('image')) {
            Image::make($StoreBlogPost->file('image'))
                                ->save('images/article/' . $validatedPost['slug'] . '.jpg', 60);
        }

        $post = Post::create($validatedPost);

        if (request()->hasFile('images')) {
            $fileAdders =  $post
                            ->addMultipleMediaFromRequest(['images'])
                            ->each(function ($fileAdder) {
                                $fileAdder->toMediaCollection('images');
                            });
        }

        session()->flash('notification', 'Post created successfuly!');
        session()->flash('type', 'positive');

        return ($post['type'] == 'article') ? redirect('admin/post') : redirect('admin/video-post') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $posts = Post::where('type', 'article')->where('id', '>', $post->id)->limit(2)->get();

        if ($posts->count() == 0) {
            $posts = Post::where('type', 'article')->limit(2)->get();
        }

        //Seo:
        $post->seo = ['titleTag' => $post->titleTag, 'metaDescription' => $post->metaDescription];

        $mainpage = MainPage::find(7);

        return view('blog.show', compact('post', 'posts', 'mainpage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('atrium.pages.post-edit', compact('post'));
    }

    /**
     * Edit vlog post
     * @method editVlog
     */
    public function editVlog($postId)
    {
        $post = Post::whereId($postId)->first();
        return view('atrium.pages.postvideo-edit', compact('post'));
    }


    public function updateVlog(Request $request) {
        
        $post = Post::whereId($_POST['id'])->first();
        $updatedPost = $post->getAttributes();
        $updatedPost['title'] = $_POST['title'];
        $updatedPost['video'] = $_POST['video'];
        $updatedPost['image'] = '';
        $post->update($updatedPost);
        return redirect('admin/video-post');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBlogPost $request, Post $post)
    {
        $validatedPost = $request->validated();

        $validatedPost['slug']  = Post::makeSlug(request('slug'));
        $validatedPost['image'] = $validatedPost['slug'].'.jpg' ?? null;

        if ($request->has('image')) {
            Image::make($request->file('image'))
                                  ->save('images/article/' . $validatedPost['slug'] . '.jpg', 60);
        }

        if (request()->hasFile('images')) {
            $post->clearMediaCollection('images');
            $fileAdders =  $post
                            ->addMultipleMediaFromRequest(['images'])
                            ->each(function ($fileAdder) {
                                $fileAdder->toMediaCollection('images');
                            });
        }

        $post->update($validatedPost);

        session()->flash('notification', 'Post updated successfuly!');
        session()->flash('type', 'positive');

        return ($post->type == 'article') ? redirect('admin/post') : redirect('admin/video-post') ;
    }

    /**
     * Share post for selected social media
     *
     * @method share
     */
    public function share(Post $post, $socialMedia)
    {
        switch ($socialMedia) {
          case 'facebook':
            $link = 'https://www.facebook.com/sharer/sharer.php?u=';
            $share = route('blog.show', $post);
          break;

          case 'pinterest':
            $link = 'https://pinterest.com/pin/create/button/?url=';
            $share = route('blog.show', $post).'&media='.$post->coverImage().'&description='.$post->title;
          break;

          case 'twitter':
            $link = 'https://twitter.com/home?status=';
            $share = $post->title . ' ' . route('blog.show', $post);
          break;
        }

        return redirect($link.$share);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        session()->flash('notification', 'Post removed!');
        session()->flash('type', 'positive');

        return redirect('admin/post');
    }

    public function createVlog() {
        return view('atrium.pages.postvideo-create');
    }


    public function destroyVlog($postId)
    {   
        $post = Post::whereId($postId)->get()->first();

        $post->delete();

        session()->flash('notification', 'Post removed!');
        session()->flash('type', 'positive');

        return redirect('admin/video-post');
    }
}
