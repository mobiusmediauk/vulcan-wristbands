<?php

namespace App\Http\Controllers;

use App\Media\SliderItem;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Remove slide
     */
    public function removeSlide(SliderItem $slide)
    {
        if ($slide->getBackgroundMedia()) {
            $slide->getBackgroundMedia()->delete();
        }
        if ($slide->getItemMedia()) {
            $slide->getItemMedia()->delete();
        }

        $slide->delete();

        session()->flash('notification', 'Slide removed.');
        session()->flash('type', 'positive');
        return redirect()->back();
    }

    /**
     * Remove slide background
     */
    public function removeBackground(SliderItem $slide)
    {
        if ($slide->getBackgroundMedia()) {
            $slide->getBackgroundMedia()->delete();

            session()->flash('notification', 'Slide background removed.');
            session()->flash('type', 'positive');
        }
        return redirect()->back();
    }

    /**
     * Remove slide item
     */
    public function removeItem(SliderItem $slide)
    {
        if ($slide->getItemMedia()) {
            $slide->getItemMedia()->delete();

            session()->flash('notification', 'Slide image removed.');
            session()->flash('type', 'positive');
        }
        return redirect()->back();
    }
}
