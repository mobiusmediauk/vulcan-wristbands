<?php

namespace App\Http\Controllers;

use App\LeadConversionType;
use Illuminate\Http\Request;
use App\Http\Requests\PutLeadConvType;

class LeadConversionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = LeadConversionType::all();
        foreach ($data as $enquiry) {
            $enquiry->last_modified = $enquiry->updated_at->format('Y-m-d H:i');
        }
        $keys = ['name', 'last_modified'];
        return view('atrium.pages.leadconvtypes-index', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('atrium.pages.leadconvtypes-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PutLeadConvType $request)
    {
        $newType = $request->validated();
        $newType['slug'] = str_slug($request->name);

        LeadConversionType::create($newType);

        session()->flash('notification', 'New type created.');
        session()->flash('type', 'positive');

        return redirect()->route('lead-conversion-types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeadConversionType  $leadConversionType
     * @return \Illuminate\Http\Response
     */
    public function show(LeadConversionType $leadConversionType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeadConversionType  $leadConversionType
     * @return \Illuminate\Http\Response
     */
    public function edit(LeadConversionType $leadConversionType)
    {
        return view('atrium.pages.leadconvtypes-edit', ['type' => $leadConversionType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeadConversionType  $leadConversionType
     * @return \Illuminate\Http\Response
     */
    public function update(PutLeadConvType $request, LeadConversionType $leadConversionType)
    {
        $leadConversionType->update($request->validated());

        session()->flash('notification', 'Type updated successfuly.');
        session()->flash('type', 'positive');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeadConversionType  $leadConversionType
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeadConversionType $leadConversionType)
    {
        $leadConversionType->delete();

        session()->flash('notification', 'Type removed.');
        session()->flash('type', 'positive');

        return redirect()->route('lead-conversion-types.index');
    }
}
