<?php

namespace App\Http\Controllers;

use Image;
use App\MainPage;
use App\Media\Slider;
use App\Media\SliderItem;
use Illuminate\Http\Request;
use App\Http\Requests\StoreMainPagePost;

class MainPageController extends Controller
{   

    public function home() {
      
        $page = MainPage::find(5);
        // App\MainPage::ipCurrencyCheck();
    
        $slider = $page->slider ?? null;
        $sliderTransitionSpeed = ($slider) ? $slider->slideTransitionSpeed : 4000;
        return view('pages.home', compact('page', 'slider', 'sliderTransitionSpeed'));
    
    }

    public function homeRedirect() {
        return redirect('/');
    }

    public function aboutUs() {
        $page = MainPage::find(1);
        return view('pages.aboutus', compact('page'));
    }

    public function differences() {
        $page = MainPage::find(2);
        return view('pages.differences', compact('page'));     
    }

    public function contact() {
        $page = MainPage::find(3);
        return view('pages.contact', compact('page'));
        
    }

    public function contactSuccess() {
        return view('pages.contact-success');
    }

    public function share($id) {
        $page = MainPage::find(4);
        return view('pages.share', compact('id', 'page')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MainPage::all();
        $keys = ['name'];
        return view('atrium.pages.mainpages-index', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainPage  $mainPage
     * @return \Illuminate\Http\Response
     */
    public function show(MainPage $mainpage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainPage  $mainpage
     * @return \Illuminate\Http\Response
     */
    public function edit(MainPage $mainpage)
    {
        return view('atrium.pages.mainpages-edit', compact('mainpage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainPage  $mainpage
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMainPagePost $request, MainPage $mainpage)
    {
        $validated = $request->all();

        foreach (range(1, 7) as $num) {
            $validated['altTag' . $num] = $request['altTag' . $num];
        }

        if (request()->has('slide')) {
            $slider = Slider::where('pageId', $mainpage->id)->first();
            $slider->update(['slideTransitionSpeed' => $request->slideTransitionSpeed]);

            foreach (request('slide') as $id => $slide) {
                if ($id == 'new') {

                    // Only create when `image` or `background` is set
                    if (isset($slide['image']) || isset($slide['background'])) {
                        $slide['sliderId'] = 1;
                        $item = SliderItem::create($slide);
                    }
                } else {

                    // Else update item:
                    $item = SliderItem::find($id);
                    $item->update($slide);
                }

                if (isset($slide['image'])) {
                    $item->saveMedia($slide['image'], 'item');
                }

                if (isset($slide['background'])) {
                    $item->saveMedia($slide['background'], 'background');
                }
            }
        }


        $mainpage->update($validated);
        $mainpage->saveImages($request);

        session()->flash('notification', 'Page saved!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainPage  $mainpage
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainPage $mainpage)
    {
        //
    }
}
