<?php

namespace App\Http\Controllers;

use File;
use App\Country;
use App\ShippingOption;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ShippingOption::all();
        foreach ($data as $option) {
            $option->flat_fee = $option->flatFee;
            $option->last_updated = (isset($option->updated_at)) ? $option->updated_at->format('Y-m-d H:i') : '' ;
            $option->country = strtoupper($option->groupedBy);
        }
        $keys = ['name', 'country', 'flat_fee', 'last_updated'];
        return view('atrium.pages.shipping-index', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = json_decode(File::get('../resources/scripts/data/countryList.json'))->countries;

        return view('atrium.pages.shipping-create', compact('countries'));
    }

    public function getCountryJson() {
            return ShippingOption::countries();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
          'name' => 'required',
          'flatFee' => 'required',
          'description' => 'required',
          'groupedBy' => 'required',
        ]);

        $shippingOption = new ShippingOption;
        $shippingOption->groupedBy = strtolower($validate['groupedBy']);
        $shippingOption->name = $validate['name'];
        $shippingOption->flatFee = $validate['flatFee'];
        $shippingOption->description = $validate['description'];
        $shippingOption->save();

        Country::where('abbreviation', strtolower($validate['groupedBy']))
                  ->update(['groupedBy' => strtolower($validate['groupedBy'])]);

        session()->flash('notification', 'New shipping option added!');
        session()->flash('type', 'positive');

        return redirect('admin/shipping');
    }

    /**
     * Display the specified resource.
     *
     * @param  int
     * @return \Illuminate\Http\Response
     */
    public function show($optionId)
    {
        $option = ShippingOption::find($optionId);
        $countries = json_decode(File::get('../resources/scripts/data/countryList.json'))->countries;
        return view('atrium.pages.shipping-edit', compact('option', 'countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function edit($optionId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $optionId)
    {
        $option = ShippingOption::find($optionId);

        $validatedFields = $request->validate([
          'name' => 'required',
          'flatFee' => 'required',
          'description' => 'required',
          'groupedBy' => 'required',
        ]);

        $option->update($validatedFields);

        $option->getCountry()->update(['groupedBy' => strtolower($validatedFields['groupedBy'])]);

        session()->flash('notification', 'Shipping option saved!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $shippingOption
     * @return \Illuminate\Http\Response
     */
    public function destroy($optionId)
    {
        $option = ShippingOption::find($optionId);
        $option->delete();

        session()->flash('notification', 'Shipping Option removed!');
        session()->flash('type', 'positive');

        return redirect('admin/shipping');
    }
}
