<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests\StorePagePost;

class PageController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Page::all();
        $keys = ['title', 'slug'];
        return view('atrium.pages.pages-index', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('atrium.pages.pages-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePagePost $request)
    {
        $validated = $request->all();

        $validated['position'] = (isset($request->position)) ? true : false;
        Page::create($validated);

        session()->flash('notification', 'Page created!');
        session()->flash('type', 'positive');
        return redirect('admin/page/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show($page)
    {
        $page = Page::where('slug', $page)->first();
        
        if ($page) {
            $page->seo = ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription];

            return view('pages.default', compact('page'));
        } else {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('atrium.pages.pages-edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(StorePagePost $request, Page $page)
    {
        $validated = $request->all();

        $validated['position'] = (isset($request->position)) ? true : false;
        $page->update($validated);

        session()->flash('notification', 'Page content updated!');
        session()->flash('type', 'positive');

        return redirect('admin/page/' . $validated['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        session()->flash('notification', 'Page removed!');
        session()->flash('type', 'positive');
        return redirect('admin/page/');
    }
}
