<?php

namespace App\Http\Controllers;

use App\Settings;
use App\Mail\SendEnquiry;
use Newsletter;
use App\NewsletterList;
use App\Enquiry;
use Illuminate\Http\Request;
use ReCaptcha;

class EnquiryController extends Controller
{
    public function index()
    {
        $data = Enquiry::all();
        foreach ($data as $enquiry) {
            $enquiry->order_number = $enquiry->orderNumber;
            $enquiry->date = $enquiry->created_at->format('Y-m-d H:i');
        }
        $keys = ['topic', 'order_number', 'date'];
        return view('atrium.pages.enquiry-index', compact('data', 'keys'));
    }

    public function mailingListIndex()
    {
        $data = NewsletterList::all();
        $keys = ['id', 'email'];
        return view('atrium.pages.mailinglist', compact('data', 'keys'));
    }

    public function show(Enquiry $enquiry)
    {
        return view('atrium.pages.enquiry-show', compact('enquiry'));
    }

    public function destroy(Enquiry $enquiry)
    {
        $enquiry->delete();

        session()->flash('notification', 'Enquiry removed!');
        session()->flash('type', 'positive');

        return redirect('admin/enquiry');
    }
    /**
     * Send user enquiry
     *
     * @method send
     */
    public function send(Request $request)
    {
        $form = $request->validate([
          'firstName' => 'required',
          'email' => 'required',
          'message' => 'required'
        ]);
            
        $secret = "6Lca4JYUAAAAAGVBxgnbykndvMYQHY-dL7bRh2e3";
        // empty response
        $response = null;
 
        // check secret key
        $reCaptcha = new ReCaptcha($secret);

        if ($_POST["g-recaptcha-response"]) {
            $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]
            );
        }

        if ($response != null && $response->success) {

        $form['topic'] = $request->topic;
        $form['lastName'] = $request->lastName;
        $form['orderNumber'] = $request->orderNumber;

        $enquiry = new Enquiry;
        $enquiry->topic = $form['topic'];
        $enquiry->firstName = $form['firstName'];
        $enquiry->lastName = $form['lastName'];
        $enquiry->orderNumber = $form['orderNumber'];
        $enquiry->email = $form['email'];
        $enquiry->message = $form['message'];
        $enquiry->save();

        // Sending enquiry from contact form to the email saved in enquiry
        if(Settings::first()->sendEnquiries){
            \Mail::to(Settings::first()->enquiryEmail)
                ->send(new SendEnquiry($enquiry));
        }

        session()->flash('status', 'Your message was sent!');

        return redirect()->back();
        } else {
            session()->flash('status', 'Error sending message!');

        return redirect()->back();
        }

    }

    /**
     * Subscribe to Newsletter
     *
     * @method signupForNewsletter
     */
    public function signupForNewsletter(Request $request)
    {
        $form = $request->validate([
          'email' => 'required'
        ]);

        Newsletter::subscribe($form['email']);

        NewsletterList::insert($form);

        session()->flash('status', 'You have successfuly subscribed to our newsletter!');

        return redirect()->back();
    }

    /**
     * Unsubscribe from newsletters
     * @method unsubscribeFromNewsletter
     */
    public function unsubscribeFromNewsletter($email)
    {
        Newsletter::unsubscribe($form['email']);

        session()->flash('status', 'You have successfuly unsubscribed from our newsletters!');

        return redirect()->back();
    }
}
