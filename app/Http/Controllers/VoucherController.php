<?php

namespace App\Http\Controllers;

use Cart;
use Carbon\Carbon;
use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Requests\StoreVoucherPost;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Voucher::all();
        foreach ($data as $voucher) {
            $voucher->expiry_date = $voucher->expiryDate;
            $voucher->discount = $voucher->amount . ' (' . $voucher->currency . ')';
            $voucher->used = $voucher->timesUsed;
        }
        $keys = ['name', 'type', 'discount', 'quantity', 'used', 'expiry_date'];

        return view('atrium.pages.voucher-index', compact('keys', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('atrium.pages.voucher-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVoucherPost $request)
    {
        $validated = $request->except(['_token']);

        $validated['quantity'] = $request->quantity;
        $validated['expiryDate'] = $request->expiryDate;

        Voucher::insert($validated);

        // Voucher::updateOrCreate(
        //   [
        //     'name' => $request->name,
        //     'currency' => $request->currency
        //   ],
        //   [
        //     'type' => $request->type,
        //     'amount' => $request->amount,
        //     'quantity' => $request->quantity,
        //     'expiryDate' => $request->expiryDate
        //   ]
        // );

        session()->flash('notification', 'Voucher created!');
        session()->flash('type', 'positive');

        return redirect('admin/voucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        return view('atrium.pages.voucher-edit', compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVoucherPost $request, Voucher $voucher)
    {
        $validated = $request->except(['_token']);

        $validated['quantity'] = $request->quantity;
        $validated['expiryDate'] = $request->expiryDate;

        $validated['currency'] = $voucher->currency;

        $voucher->update($validated);

        session()->flash('notification', 'Voucher updated!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
          * Apply the Voucher to the Users Cart
          */
    public function apply(Request $request)
    {
        $currency = app('currency')->get();
        
        if (!Voucher::where('name', $request->name)->whereIn('currency', [$currency, 'all'])->exists()) {
            session()->flash('status', "The voucher you entered was not found or is invalid.");
            session()->flash('type', "warning");

            return back();
        }

        $voucher =  Voucher::where('name', $request->name)
                              ->whereIn('currency', [$currency, 'all'])
                              ->orderBy('currency', 'DESC')
                              ->first();

        //Voucher checks:
        if ($voucher->timesUsed >= $voucher->quantity && $voucher->quantity != 0) {
            session()->flash('status', "Your voucher is no longer valid.");
        } elseif (Carbon::today() > $voucher->expiryDate && $voucher->expiryDate) {
            session()->flash('status', "Your voucher is expired.");
            // } elseif ($voucher->currency !== app('currency')->get()) {
        //     session()->flash('status', "You can't use this voucher with this currency! Switch to " . strtoupper($voucher->currency) . ".");
        } else {
            session()->put('voucherId', $voucher->id);
            session()->flash('status', "Congratulations! The voucher has been applied to your cart.");
        }

        return redirect()->back();
    }

    /**
     * Remove voucher
     */
    public function remove()
    {
        session()->forget('voucherId');
        session()->flash('status', "The voucher was removed.");

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        $voucher->delete();

        session()->flash('notification', 'Voucher removed!');
        session()->flash('type', 'positive');

        return redirect('admin/voucher');
    }
}
