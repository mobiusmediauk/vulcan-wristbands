<?php

namespace App\Http\Controllers;

use Image;

class ImageController extends Controller
{   
   public function imageGenerate() {
        $now = str_replace(' ', '', now());
        
        Image::make(request('sharedImageBase64'))
                      ->resize(640, 400)
                      ->save('images/shared/' . $now . '.jpg', 60);
        return 'images/shared/' . $now . '.jpg';
    }
}
