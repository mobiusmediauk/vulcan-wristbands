<?php

namespace App\Http\Controllers;

use App\Product\InspireMe;
use App\Product\Type;
use App\Customiser;
use Illuminate\Http\Request;

class CustomiserController extends Controller
{
    public function index()
    {
        $sourceData = Type::with('products')->get()->each(function ($type) {
            foreach ($type->products as $product) {
                if (!in_array($product->id, [1,2])) {
                    $product['price'] = $product->price();
                }
            }
        })->toJson();

        $presets = InspireMe::get()->toJson();

        return view('pages.customise', compact('sourceData', 'presets'));
    }

    public function assemble()
    {
        $strap = new Customiser(request()->all());
        $strap->assemble();

        return ['success', 'Added to cart!'];
    }
}
