<?php

namespace App\Http\Controllers;

use Auth;
use App\ShareCounter;
use App\LeadConversionType;

class StatisticsController extends Controller
{   

    public function getStatistics() {
        $shareCounter = ShareCounter::all();
        $leadConversionStats = LeadConversionType::all();
        return view('atrium.pages.statistics', compact('shareCounter', 'leadConversionStats'));
    }
}
