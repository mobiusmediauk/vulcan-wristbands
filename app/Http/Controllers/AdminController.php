<?php

namespace App\Http\Controllers;

use Auth;

class AdminController extends Controller
{   

    public function home() {
        if (Auth::check() && Auth::user()->hasRole('admin')) {
            return redirect('admin/order');
        } else {
            return view('atrium.login');
        }
    }

   public function moduleSheet() {
        return view('atrium.pages.module-sheet');
    }
}
