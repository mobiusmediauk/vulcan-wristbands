<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::first();

        return view('atrium.pages.settings', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function show(Settings $settings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function edit(Settings $settings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
          'ordersEmail' => 'required',
          'infoEmail' => 'required',
          'enquiryEmail' => 'required_if:sendEnquiries,1',
          'sendEnquiries' => 'required',
        ], [
          'enquiryEmail.required_if' => 'The Enquiry Email field is required if the Send Enquiry Email checkbox is checked.'
        ]);

        $sendEnquiries = (isset($request->sendEnquiries[0]) == '1' ? '1' : '0');

        $validated['GAtrackingCode'] = $request->GAtrackingCode ?? null;

        $settings = Settings::first();
        $settings->update($validated);

        session()->flash('notification', 'Settings updated!');
        session()->flash('type', 'positive');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Settings  $settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Settings $settings)
    {
        //
    }
}
