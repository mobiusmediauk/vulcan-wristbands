<?php

namespace App\Http\Controllers;

use App\EmailTemplate;
use App\Http\Requests\EmailTemplatePost;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EmailTemplate::all();
        foreach ($data as $post) {
            $post->last_updated = $post->updated_at->format('Y-m-d H:i');
        }

        $keys = ['name', 'last_updated'];

        return view('atrium.pages.email-templates', compact('data', 'keys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(EmailTemplate $emailTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailTemplate $emailTemplate)
    {
        return view('atrium.pages.email-template-edit', compact('emailTemplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(EmailTemplatePost $request, EmailTemplate $emailTemplate)
    {
        $validated = $request->validated();
        $validated['ccCopy'] = (isset($request->ccCopy)) ? true : false;

        $emailTemplate->update($validated);

        session()->flash('notification', 'Email template updated.');
        session()->flash('type', 'positive');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailTemplate $emailTemplate)
    {
        //
    }
}
