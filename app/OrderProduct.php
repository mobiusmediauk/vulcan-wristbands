<?php

namespace App;

use App\Voucher;
use App\Product\Product;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'orderProducts';
    protected $fillable = ['orderId', 'typeId', 'productId', 'qty', 'orderCustomStrapId'];

    /**
     * Get product
     *
     * @method product
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'productId');
    }

    /**
     * Get Custom strap
     *
     * @method customStrap
     */
    public function customStrap()
    {
        return $this->hasOne(OrderCustomStrap::class, 'id', 'orderCustomStrapId');
    }

    /**
     * Used to save custom strap
     *
     * @method convertAndSave
     */
    public static function convertAndSave($product, $qty, $orderId, $orderCustomStrapId=null)
    {
        if (isset($product->id)) {
            $orderProduct = new OrderProduct;
            $orderProduct->orderId = $orderId;
            $orderProduct->typeId = (isset($product->typeId)) ? $product->typeId : 5 ; // If type id is missing, set as accessory(5)
            $orderProduct->productId = $product->id;
            $orderProduct->qty = $qty;
            $orderProduct->orderCustomStrapId = $orderCustomStrapId;
            $orderProduct->save();

            if (isset($product->options) && $product->options->type == 'Voucher') {
                Voucher::generateNewVoucher($product->price, $qty, $orderId);
            }
        }
    }
}
