<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadConversionType extends Model
{
    protected $table = 'leadConversionTypes';
    protected $guarded = [];
}
