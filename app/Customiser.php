<?php

namespace App;

use Auth;
use Cart;
use Image;
use App\CartItems;
use App\CartCustomStrap;
use App\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Customiser extends Model
{
    public function __construct($request=null)
    {
        if ($request) {
            $this->parts = $request['selected'];
            $this->image = $request['renderedImage64'];
        }
    }

    /**
     * Assemble all parts and generate cart item
     *
     * @method assemble
     */
    public function assemble()
    {
        $strap = Product::find($this->parts['strap']);
        $band = Product::find($this->parts['band']);
        $buckle = Product::find($this->parts['buckle']);
        // $buckleMaterial = Product::find($this->parts['buckleMaterial']);

        $price = array_sum([$strap->price(), $band->price(), $buckle->price()]);

        $options = [
          'strap' => $strap,
          'band' => $band,
          'buckle' => $buckle,
          // 'buckleMaterial' => $buckleMaterial,
          'description' => $strap->name,
          'image' => $this->image,
          'singleUnitPrice' => $price,
        ];

        $productId = rand(1111, 9999);

        Cart::add($productId, 'Custom Strap', 1, $price, $options);
        // CartItems::create(['userId' => Auth::user()->id, 'productId' => $productId, 'qty' => 1]);
        // CartCustomStrap::create(['productId' => $productId, 'userId' => Auth::user()->id, 'strapId' => $strap->id, 'bandId' => $band->id, 'buckleId' => $buckleMaterial->id]);

        return ['success', 'Custom Watch Strap added to cart!'];
    }

    /**
     * Reasemble function from given `strap`, `band` and `buckleMaterial`
     *
     * @method reasemble
     */
    public static function reasemble($strapId, $bandId, $buckleId, $addToCart=false)
    {
        $strap = Product::find($strapId);
        $band = Product::find($bandId);
        $buckleMaterial = Product::find($buckleId);

        $price = array_sum([$strap->price(), $band->price(), $buckleMaterial->price()]);

        $options = [
          'strap' => $strap,
          'band' => $band,
          'buckleMaterial' => $buckleMaterial,
          'description' => $strap->name,
          // 'image' => $base64,
          'singleUnitPrice' => $price,
        ];
    }
}
