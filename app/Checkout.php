<?php

namespace App;

use Auth;
use Cart;
use App\Order;
use App\OrderProduct;
use App\OrderCustomStrap;
use App\UserAddresses;
use App\ShippingOption;
use Worldpay\Worldpay;
// use Worldpay\WorldpayException;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    public function __construct($orderToken, $ccHolderName, $userMailAddress, $leadConvTypes)
    {
        $this->billingAddress = session()->get('billing');
        $this->orderToken = $orderToken;
        $this->worldPay = new Worldpay(config('services.worldpay.service_key'));
        $this->ccHolderName = $ccHolderName;
        $this->userMailAddress = $userMailAddress; // TODO: do something with this
        $this->voucher = Voucher::where('id', session()->get('voucherId'))->first();
        $this->leadConvTypes = $leadConvTypes;
    }

    /**
     * Function to run all order process. If successful, returns
     * generated unique order number and writes to `orders` table
     *
     * @method checkoutAndCreateOrder
     */
    public function checkoutAndCreateOrder()
    {
        $shippingOption = ShippingOption::find(session()->get('shippingOptionId'));
        $shippingPrice = (isset($shippingOption->flatFee)) ? $shippingOption->flatFee : 0 ;

        $discount = (isset($this->voucher)) ? round($this->voucher->discount($this->cartSubtotal())) : 0 ;

        $orderNumber = 'VWS'.rand(1111111111, 9999999999);

        // If user buy item with 100% discount and selects `Click & Collect` option
        // so he don't have to pay and order becomes 'FREE'
        if (($this->cartSubtotal() + $shippingPrice) <= $discount) {
            $orderCode = 'free';
        } else {
            $orderCode = $this->createOrder($orderNumber, $shippingPrice);
        }

        // Increment selected lead convertion types
        if ($this->leadConvTypes) {
            foreach ($this->leadConvTypes as $id => $value) {
                LeadConversionType::whereId($id)->increment('timesSelected');
            }
        }

        if ($orderCode) {
            $order = new Order;
            $order->orderCode = $orderCode;
            $order->orderNumber = $orderNumber;
            $order->shippingOption = session()->get('shippingOptionId');
            $order->shippingPrice = $shippingPrice;
            $order->subtotal = $this->cartSubtotal();
            $order->discount = $discount;
            $order->total = ($this->cartSubtotal() + $shippingPrice);
            $order->currency = app('currency')->get();
            $order->voucherId = (isset($this->voucher)) ? $this->voucher->id : null ;
            $order->leadConversionTypes = $this->leadConvTypes ?? null;
            $order->email = $this->userMailAddress;
            $order->userId = (Auth::check()) ? Auth::user()->id : null ;
            $order->rolex = session()->get('rolex');
            $order->save();

            // Save products to `orderProducts` table:
            foreach (Cart::content() as $item) {
                if ($item->name == "Custom Strap") {

                    // Save new custom strap with image for this order to database:
                    $newCustomStrapId = OrderCustomStrap::saveAndGenerateImage([
                      'orderId' => $order->id,
                      'name' => $item->name,
                      'color' => $item->options->description,
                      'qty' => $item->qty,
                      'unitPrice' => $item->options->singleUnitPrice,
                      'bandId' => $item->options->band->id,
                      'strapId' => $item->options->strap->id,
                      'buckleId' => $item->options->buckle->id,
                      'image' => $item->options->image,
                    ]);

                    OrderProduct::convertAndSave($item->options->band, $item->qty, $order->id, $newCustomStrapId);
                    OrderProduct::convertAndSave($item->options->strap, $item->qty, $order->id, $newCustomStrapId);
                    OrderProduct::convertAndSave($item->options->buckle, $item->qty, $order->id, $newCustomStrapId);
                } else {
                    OrderProduct::convertAndSave($item, $item->qty, $order->id);
                }
            }

            foreach (session()->get('addresses') as $key => $address) {
                $newAddress = new Address;
                $newAddress->orderId = $order->id;
                $newAddress->type = $key;
                $newAddress->name = $address['name'];
                $newAddress->address1 = $address['address1'];
                $newAddress->address2 = $address['address2'];
                $newAddress->city = $address['city'];
                $newAddress->province = $address['province'];
                $newAddress->postalCode = $address['postalCode'];
                $newAddress->countryCode = $address['countryCode'];
                $newAddress->mobile = (isset($address['mobile'])) ? $address['mobile'] : 'NA' ;
                $newAddress->sms =  (isset($address['sms'])) ? $address['sms'] : 'On' ;
                $newAddress->save();

                if (Auth::check()) {
                    UserAddresses::updateOrCreate(['userId' => Auth::user()->id, 'type' => $key], [
                      'name' => $address['name'],
                      'address1' => $address['address1'],
                      'address2' => $address['address2'],
                      'city' => $address['city'],
                      'province' => $address['province'],
                      'postalCode' => $address['postalCode'],
                      'countryCode' => $address['countryCode'],
                      'mobile' => (isset($address['mobile'])) ? $address['mobile'] : 'NA',
                      'sms' => (isset($address['sms'])) ? $address['sms'] : 'On',
                    ]);
                }
            }

            $order->addressId = $newAddress->id;
            $order->save();

            if (isset($this->voucher)) {
                $this->voucher->use();
                session()->forget('voucherId');
            }

            // Remove cart and other checkout stuff from session:
            Cart::destroy();
            session()->forget('addresses');
            session()->forget('shippingOptionId');
            session()->forget('email');
        } else {
            $orderNumber = null;
        }
        return $orderNumber;
    }

    /**
     * Send user order to `Worldpay`
     *
     * @method createOrder
     */
    public function createOrder($orderNumber, $shippingPrice)
    {
        try {
            if (isset($this->voucher)) {
                $amount = $this->cartSubtotal() - $this->voucher->discount($this->cartSubtotal());
            } else {
                $amount = $this->cartSubtotal();
            }
            $amount = ($amount + $shippingPrice)*100;

            // Send array with parameters to world pay. Here world pay creates an
            // array from variables we send. We shold send correct variables or
            // else JSON would fail.
            $response = $this->worldPay->createOrder([
              "token" => "{$this->orderToken}",
              "amount" => floatval($amount),
              "currencyCode" => strtoupper(app('currency')->get()),
              "name" => "{$this->ccHolderName}",
              "orderDescription" => "New Order",
              "customerOrderCode" => "{$orderNumber}",
            ]);

            if ($response['paymentStatus'] === 'SUCCESS') {
                return $response['orderCode'];
            } else {
                return false;
            }
        } catch (\Exception $e) {
            // Mail developer failed worldPay exception:
            \Mail::raw('Failed VULCAN transaction: ' . $e, function ($message) {
                $message->to('robert@viralbamboo.com');
            });
            return false;
        }
    }

    /**
     * `Cart::subtotal()` quickfix
     *
     * @method cartSubtotal
     */
    public static function cartSubtotal()
    {
        foreach (Cart::content() as $item) {
            $price[] = $item->price;
        }
        return (Cart::count() > 0) ? array_sum($price) : 0 ;
    }
}
