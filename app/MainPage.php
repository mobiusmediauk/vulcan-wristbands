<?php

namespace App;

use Currency;
use Image;
use App\Media\Slider;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
    protected $table = 'mainPages';

    protected $fillable = ['text1', 'text2',
      'body1', 'body2', 'body3', 'body4',
      'image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7',
      'caption1', 'caption2', 'caption3', 'caption4', 'caption5', 'caption6',
      'altTag1', 'altTag2', 'altTag3', 'altTag4', 'altTag5', 'altTag6', 'altTag7',
      'titleTag', 'metaDescription',
    ];

    /**
     * Save images
     *
     * @method saveImages
     */
    public function saveImages(Request $request)
    {
        foreach ($request->files as $key => $file) {
            if ($key !== 'slide') {
                Image::make($request->file($key))->save('images/' . $file->getClientOriginalName());
                $this->update([$key => $file->getClientOriginalName()]);
            }
        }
    }

    /**
     * Get this page slider
     */
    public function slider()
    {
        return $this->hasOne(Slider::class, 'pageId');
    }

    /**
     * Gets data based on IP
     * @method ipData
     * @return [type] [description]
     */
    public static function ipData()
    {
        $getIPdetailsUrl = "http://ipinfo.io/json";
        if (@file_get_contents($getIPdetailsUrl)) {
            $data = json_decode(file_get_contents($getIPdetailsUrl));
        }

        return $data ?? null;
    }

    /**
     * Currency change by ip
     *
     * @method ipCurrencyCheck
     */
    public static function ipCurrencyCheck()
    {
        // $getIPdetailsUrl = "http://ipinfo.io/json";
        // $getCurrencyUrl = "http://country.io/currency.json";
        // $defaultCurrency = 'gbp';
        //
        // // Prevent from failed url:
        // if (@file_get_contents($getIPdetailsUrl) && @file_get_contents($getCurrencyUrl)) {
        //     $details = json_decode(file_get_contents($getIPdetailsUrl));
        //     $currency = (array) json_decode(file_get_contents($getCurrencyUrl));
        //     $getCurrency = strtolower($currency[$details->country]);
        //
        //     // If there's no currency in array, set default:
        //     $setCurrency = (in_array($getCurrency, ['gbp', 'usd', 'eur', 'hkd'])) ? $getCurrency : $defaultCurrency ;
        // } else {
        //     $setCurrency = $defaultCurrency;
        // }
        //
        // Currency::set($setCurrency);
    }
}
