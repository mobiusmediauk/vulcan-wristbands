<?php

namespace App\Currencies;

use SSD\Currency\Currencies\BaseCurrency;

class HKD extends BaseCurrency
{
    /**
     * @var string
     */
    protected $prefix = '$';

    /**
     * @var string
     */
    protected $postfix = 'HKD';
}
