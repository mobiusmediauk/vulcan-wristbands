<?php

namespace App\Currencies;

use SSD\Currency\Currencies\BaseCurrency;

class AED extends BaseCurrency
{
    /**
     * @var string
     */
    protected $prefix = 'د.إ';

    /**
     * @var string
     */
    protected $postfix = 'AED';
}
