
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Uncomment below when compiling to production
 */
Vue.config.devtools = true
Vue.config.debug = true
Vue.config.silent = true

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('strap-customiser', require('./components/StrapCustomiser/StrapCustomiser.vue'));
Vue.component('shopping-cart', require('./components/ShoppingCart/ShoppingCart.vue'));
Vue.component('reorderable-index', require('./components/ReorderableIndex/ReorderableIndex.vue'));

import navEnhancer from './mixins/navEnhancer.js'

import { EventBus } from './eventBus.js';
if (document.getElementById('app')) {

  const app = new Vue({
    el: '#app',
    data: {
    },
    mixins: [navEnhancer],
    methods: {
      removeLoader() {
        let loader = document.getElementById('loading-notice');
        if (loader) {
          loader.parentNode.removeChild(loader);
        }
      }
    },
    mounted() {
      this.navEnhancer();
      EventBus.$on('initiated',this.removeLoader());
    }
  });

}

// Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    if (isIE) {
      document.querySelector('body').classList.add('isIE');
      document.querySelector('body').classList.add('isMS');
      // s.browser.isMS = true;
    }
    if (isEdge) {
      document.querySelector('body').classList.add('isEdge');
      document.querySelector('body').classList.add('isMS');
      // s.browser.isMS = true;
    }

    if (document.getElementById('homepage-content-slider')) {
      var mySwiper = new Swiper ('#homepage-content-slider', {
        // Optional parameters
        // autoHeight: true,
        direction: 'horizontal',
        loop: true,
        autoplay: {
          delay: sliderTransitionSpeed,
          disableOnInteraction: false
        },
        // direction: 'horizontal',
        // Navigation arrows
        paginationType: 'bullets',
        // If we need pagination
        pagination: { el: '.swiper-pagination' },
          });
      }

      if($(".swiper-slide").length == 3) {
        $('.swiper-wrapper').addClass( "disabled" );
        $('.swiper-pagination').addClass( "disabled" );
      }

      if ( $('#slide_image').length == 0 ) {
        $('.slides-image__display').addClass( "no-img" );
      }

      $(function() {
        $('summary').click(function(e) {
          e.preventDefault();
          var parent = $(this).parent('div');
          parent.toggleClass('open');
        })
        $('.join-newsletter').click(function(e) {
          e.preventDefault();
          var parent = $(this).parent('div');
          parent.toggleClass('is_open');
          parent.removeClass('join-newsletter-swipe');
          parent.removeClass('join-newsletter-swipe-out');
        })
        $('.close-newsletter-button').click(function() {
          $(".site-nav-join").removeClass('is_open');
          $(".site-nav-join").removeClass('join-newsletter-swipe');
          $(".site-nav-join").removeClass('join-newsletter-swipe-out');
          localStorage.setItem('close-newsletter', 'True');
          localStorage.setItem('close-newsletterOn', new Date().getTime());
          create_cookie('close_newsletter', 'true', 20);
        })
        setTimeout(function() {
          $(window).resize();
        },3000);
        setTimeout(function() {
          $(window).resize();
        },10000);
      })
      if (localStorage.getItem('newsletter-signup') !== 'True' && localStorage.getItem('close-newsletter') !== 'True' && get_cookie('join_newsletter') != 'true' ) {
        if (get_cookie('close_newsletter') != 'true' ) {
        setTimeout(function(){
        $(".site-nav-join").addClass('join-newsletter-swipe');

          setTimeout(function(){
             $(".site-nav-join").addClass('join-newsletter-swipe-out');
          }, 15000);
        }, 7000);
      } else {
        console.log('Cookies are still active, signup form will not automatically pop up while above cookies are still active.')
      }
    } else {
      console.log('Join Newsletter cookie or localStorage items are still active, signup form will not automatically pop up while these are still active.')
    }

    $(function() {
      $('.join-newsletter-button').click(function(e) {
        localStorage.setItem('newsletter-signup', 'True');
      })
    })

    if (localStorage.getItem('close-newsletter') === 'True') {
        if (new Date() - localStorage.getItem('close-newsletterOn') > 86400000) {
          localStorage.removeItem('close-newsletter');
          localStorage.removeItem('close-newsletterOn');
        }
      }

      function create_cookie(name, value, days) {
          var expires = "";
          if (days) {
              var date = new Date();
              date.setTime( date.getTime() + (days*24*60*60*1000) );
              expires = "; expires=" + date.toGMTString();
          }
          document.cookie = name + "=" + value + expires + "; path=/";
      }

      function get_cookie(name) {
          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for (var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) === ' ') {
                  c = c.substring(1, c.length);
              }
              if (c.indexOf(nameEQ) === 0) {
                  return c.substring(nameEQ.length, c.length);
              }
          }
          return null;
      }

      jQuery(document).ready(function($) {
      $('.join-newsletter-button').click(function(e) {
        // Create cookie so that the user is no longer redirected
        create_cookie('join_newsletter', 'true', 150);
      });
    });
