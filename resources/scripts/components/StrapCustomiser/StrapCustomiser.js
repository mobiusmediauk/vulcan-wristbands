import '../../vendors/canvg/rgbcolor.js';
import '../../vendors/canvg/StackBlur.js';
import '../../vendors/canvg/canvg.js';

import { EventBus } from '../../eventBus.js';

function getRandomInt(max) {
  // return Math.floor(Math.random() * Math.floor(max));
  var random;
    do {
        random = Math.floor(Math.random() * Math.floor(max));
    } while (random === getRandomInt.last);
    getRandomInt.last = random;
    return random;
  // return (getRandomInt.number = Math.floor(Math.random() * Math.floor(max))) === getRandomInt.lastNumber ? getRandomInt() : getRandomInt.lastNumber = getRandomInt.number;
}

export default {
  name: 'strap-customiser',
  props: ['sourceData', 'customText', 'mode', 'preset', 'presets'],
  data() {
    return {
      csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
      meta: {
        currency: null,
      },
      isAdminMode: this.mode === 'admin',
      selected: {
        watch: 0,
        strap: 0,
        band: 0,
        buckle: 0,
        // buckleFinish: 0,
      },
      renderedImage64: '',
      options: {
        watches: [],
        straps: [],
        bands: [],
        buckles: [],
        // buckleFinishes: [],
      },
      imageUrlToShare: null,
      imgurUrlToShare: null,
      imagePageUrlToShare: null,
      showShare: false,
      sharingToTwitter: false,
      sharingToFacebook: false,
      lastSharedSelection: {
        watch: null,
        strap: null,
        band: null,
        buckle: null,
      },
      sharingUrlPartrials: {
        url: encodeURIComponent('https://vulcanwatchstraps.com/'),
        title: encodeURIComponent('Beautiful customised Vulcan watch straps for your Rollex at vulcanwatchstraps.com.'),
        description: encodeURIComponent('Check out this design I created using the Vulcan Watch Straps customisation tool. What do you think? Customise your Rolex in your own personal style with these luxury vulcanised rubber watch straps. #rolex #vulcanwatchstraps #watchstraps'),
      },
      form: {
        image: null,
        name: null,
        email: null,
        subject: null,
        message: null,
      },
    }
  },

  computed: {
    price() {
      let price = 0;
      if (this.meta.currency) {
        price = parseFloat(this.options.straps.find(i => i.id === this.selected.strap).price) +
          parseFloat(this.options.bands.find(i => i.id === this.selected.band).price) +
          parseFloat(this.options.buckles.find(i => i.id === this.selected.buckle).price)
          // parseFloat(this.options.buckleFinishes.find(i => i.id === this.selected.buckleFinish).price);
      }
      return price;
    },
    facebookUrl() {
      return 'https://www.facebook.com/sharer/sharer.php?u=' + this.sharingUrlPartrials.url + '?img='
      // return 'http://www.facebook.com/sharer.php?s=100&title=' +
      // this.sharingUrlPartrials.title +
      // '&summary=' +
      // this.sharingUrlPartrials.description +
      // '&url=' +
      // this.sharingUrlPartrials.url
      //       FB.ui({
      //   method: 'feed',
      //   link: 'https://www.vulcanwatchstraps.com/our-straps',
      //   caption: 'An example caption',
      // }, function(response){});
    },
    twitterUrl() {
      return 'https://twitter.com/intent/tweet?text=' + this.sharingUrlPartrials.description + '&url=' + 'https://vulcanwatchstraps.com/share' + this.imageUrlToShare.replace(/images\/shared\//,'').replace(/.jpg$/, '');
    },
    pinterestUrl() {
      return 'https://pinterest.com/pin/create/button/?url=' +
      'https://vulcanwatchstraps.com/share' + this.imageUrlToShare.replace(/images\/shared\//,'').replace(/.jpg$/, '') +
      '&media=' + 'https://vulcanwatchstraps.com'
      + this.imageUrlToShare +
      '&description=' +
      this.sharingUrlPartrials.description;
    },
    email() {
      return 'mailto:?subject=Vulcan%20Watch%20Straps' + '&body=' + encodeURIComponent('This is a test email') + encodeURIComponent('img src="" /&gt;')
    },
    isComplete() {
      return this.form.name && this.form.email && this.form.subject;
    }
  },

  methods: {
    panelsController() {
      let summariesDOM = document.querySelectorAll('.customiser-option-item > summary');
      let submenusDOM = document.querySelectorAll('.customiser-option-sublist');

      [].forEach.call(summariesDOM, function(summary) {
        summary.addEventListener('click', function(e) {
          e.preventDefault();
          [].forEach.call(summariesDOM, function(s) {
            if (s !== summary) setTimeout(() => {
              s.parentNode.classList.remove('open');
            });
          })
        });
      });

      [].forEach.call(submenusDOM, function(submenu) {
        submenu.addEventListener('click', function(e) {
          // e.preventDefault();
          setTimeout(() => {
            submenu.parentNode.classList.remove('open');
          });
        })
      });

    },
    randomPreset() {
      this.showShare = false;
      let presets = this.options.watches.find(w => w.id == this.selected.watch).presets;
      if (!presets) {
        this.randomiseOptions();
        return;
      }
      let randomPreset = presets[getRandomInt(presets.length)];
      this.selected.strap = randomPreset[0];
      this.selected.band = randomPreset[1];
      this.selected.buckle = randomPreset[2];
    },
    randomiseOptions() {
      this.showShare = false;
      // this.selected.watch = this.options.watches[getRandomInt(this.options.watches.length)].id;
      this.selected.strap = this.options.straps[getRandomInt(this.options.straps.length)].id;
      this.selected.band = this.options.bands[getRandomInt(this.options.bands.length)].id;
      this.selected.buckle = this.options.buckles[getRandomInt(this.options.buckles.length)].id;
      // this.selected.buckleFinish = this.options.buckleFinishes[getRandomInt(this.options.buckleFinishes.length)].id;
    },
    renderImage() {
      let customiserCanvas = document.getElementById('customiser-canvas');
      if (customiserCanvas) {
        canvg(customiserCanvas, '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' + document.getElementById('customiser-viewbox-svg-artboard').innerHTML + '</svg>', { ignoreMouse: true, ignoreAnimation: true });
      }
    },

    share(showShare=true) {
      if (this.showShare) {
        this.showShare = false;
        return;
      }
      this.showShare = showShare;
      if (JSON.stringify(this.selected) === JSON.stringify(this.lastSharedSelection)) {
        console.log("option hasn't changed");
      } else {
        this.imageUrlToShare = null;
        this.renderImage();
        setTimeout(() => {
          let imgBase64 = document.getElementById('customiser-canvas').toDataURL("image/png");
          // console.log(imgBase64);
          this.renderedImage64 = imgBase64;
          document.getElementById('customiser-img-renderer').setAttribute('src', imgBase64);
          // let linkEl = document.createElement('link');
          // linkEl.setAttribute('id', 'link');
          // linkEl.setAttribute('id', 'link');
          axios.post('/image-generate', {sharedImageBase64: imgBase64}).then(response => {
            console.log(response.data);
            this.imageUrlToShare = '/' + response.data;
            // this.sharingUrlPartrials.image = encodeURIComponent('http://vulcanwatchstraps.com/' + response.data);
            let encodedImageUrl = encodeURIComponent('https://vulcanwatchstraps.com/' + response.data);
            this.sharingUrlPartrials.image = encodedImageUrl;
            this.form.image = 'https://vulcanwatchstraps.com/' + response.data;
            document.getElementById('facebook-image').setAttribute('content', 'https://vulcanwatchstraps.com/' + response.data);
            document.getElementById('twitter-image').setAttribute('content', 'https://vulcanwatchstraps.com/' + response.data);
            // document.getElementById('email-image').setAttribute('content', 'https://vulcanwatchstraps.com/' + response.data);
            console.log(this.imageUrlToShare);
            this.requestImgur();
          });
        },500);
      }
    },
    shareLinkClicked(socialMedia) {
      this.showShare = false;
      // Count share clicks:
      axios.post('/share-count', {socialMedia: socialMedia});
    },
    requestImgur(target) {
      if (target === 'twitter') {
        this.sharingToTwitter = true;
      }
      if (target === 'facebook'){
        this.sharingToFacebook = true;
      }
      console.log('https://www.vulcanwatchstraps.com' + this.imageUrlToShare);
      axios.post('https://imgur-apiv3.p.mashape.com/3/image', {
        image: 'https://www.vulcanwatchstraps.com/' + this.imageUrlToShare,
        title: 'Vulcan Watch Strap',
        description: 'https://www.vulcanwatchstraps.com/'
      }, {
        headers: {
          'Authorization': 'Client-ID 17742f41e2cad71',
          'X-Mashape-Key': 'iRxRhqTlNKmshNnXPQOIU3UKBduIp1NDC6TjsnlJ6wL6CGPjBD',
        }
      }).then(response => {
        console.log(response);
        this.imgurUrlToShare = 'https://imgur.com/' + response.data.data.id;
        // if (target === 'twitter') this.shareToTwitter();
      }).catch(error => {
        console.log(error);
        this.imgurUrlToShare = 'https://www.vulcanwatchstraps.com/';
        // if (target === 'twitter') this.shareToTwitter();
      })
    },
    shareWithImgur(target) {
      if (target === 'twitter') {
        var newTab = window.open('https://twitter.com/home?status=' +
        this.sharingUrlPartrials.description +
        encodeURIComponent(' ' + this.imgurUrlToShare));
        this.sharingToTwitter = false;
      };
      if (target === 'facebook') {
        var newTab = window.open('https://www.facebook.com/sharer/sharer.php?u=' +
        encodeURIComponent(' ' + this.imgurUrlToShare));
        this.sharingToFacebook = false;
      }
    },

    sumbitEmailShareForm() {
      axios.post('/email-share', this.form)
        .then(function(){
          document.getElementById("sharing-modal").classList.remove("is-open");
        })
    },

    onSubmit() {
      if (!this.isAdminMode) this.renderImage();
      setTimeout(()=> {
        if (!this.isAdminMode) {
          let imgBase64 = document.getElementById('customiser-canvas').toDataURL("image/png");
          this.renderedImage64 = imgBase64;
        }
        document.getElementById("customiser-form").classList.add('submitting');
        let url = this.isAdminMode ? '/admin/inspireme' : '/customiser/assemble';
        let formModel = {
          selected: {
            watch: this.selected.watch,
            strap: this.selected.strap,
            band: this.selected.band,
            buckle: this.selected.buckle,
            // buckleFinish: this.selected.buckleFinish,
          },
        }
        if (!this.isAdminMode) formModel.renderedImage64 = this.renderedImage64;

        if (this.isAdminMode) {
          if (this.preset) {
            // editing a preset
            axios.put(url + '/' + this.preset.id, formModel).then(() => window.location.href = '/admin/inspireme');
          } else {
            // creating a preset
            axios.post(url, formModel).then(() => window.location.href = '/admin/inspireme');
          }
        } else {
          // front end customiser
          axios.post(url, formModel).then(() => window.location.href = '/cart');
        }
      },1000);
    }
  },

  beforeMount() {
    let productTypeMap = {
      'Watch': 'watches',
      'Strap': 'straps',
      'Band': 'bands',
      'Buckle': 'buckles',
      // 'Buckle Finish': 'buckleFinishes',
    };
    this.sourceData.forEach(key => {
      if (productTypeMap[key.name]) this.options[productTypeMap[key.name]] = key.products;
    })
    if (this.presets) this.presets.forEach(preset => {
      let presetParentWatch = this.options.watches.find(w => w.id === parseInt(preset.watchId));
      if (presetParentWatch) {
        if (!presetParentWatch.presets) presetParentWatch.presets = [];
        presetParentWatch.presets.push([parseInt(preset.strapId),parseInt(preset.bandId),parseInt(preset.buckleId)]);
      }
    })
    // this.randomiseOptions();
    if (this.preset) {
      this.selected.watch = parseInt(this.preset.watchId);
      this.selected.strap = parseInt(this.preset.strapId);
      this.selected.band = parseInt(this.preset.bandId);
      this.selected.buckle = parseInt(this.preset.buckleId);
    } else {
      this.selected.watch = 13;
      this.selected.strap = 39;
      this.selected.band = 46;
      this.selected.buckle = 54;
    }
  },

  mounted() {
    if (!this.isAdminMode) this.meta.currency = document.getElementById('currency-value-holder').getAttribute('data-currency');
    this.panelsController();
    EventBus.$emit('initiated');

  }
}
