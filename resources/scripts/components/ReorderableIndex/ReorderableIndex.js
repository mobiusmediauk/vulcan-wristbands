import draggable from 'vuedraggable';

export default {
  props: {
    data: Array,
    columns: Array,
  },
  components: {
    draggable,
  },
  data() {
    return {
      init: false,
      rows: this.data.map(r => r),
    }
  },
  filters: {
    tableHeadPrepper(str) {
      return (str.charAt(0).toUpperCase() + str.slice(1)).replace(/_/g, ' ')
    }
  },
  methods: {
    goToPermalink(row) {
      if (row.link !== undefined) {
        window.location.href = row.link;
      }
    },
    onDragStart() {
      // this.searchQuery = '';
    },
    onDragEnd() {
      this.assignOrderIds();
    },
    assignOrderIds() {
      this.rows.forEach((row, rowIndex) => row.orderId = rowIndex);
      if (this.init) axios.post('/admin/product/reorder', this.rows).then();
    }
  },
  beforeMount() {
    console.log(this.rows);
    this.assignOrderIds();
    this.init = true;
  },
}
