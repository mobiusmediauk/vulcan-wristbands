import { EventBus } from '../../eventBus.js';


export default {
  props: ['shippingOptions', 'cartItems', 'accessories', 'discount', 'ipDetails', 'doCartHaveStraps'],
  data() {
    return {
      csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
      meta: {
        curreny: null,
        currencySymbol: '£',
        showAccessories: true,
        countryList: require('../../data/countryList.json'),
      },
      voucher: {
        name: '',
        discount: 0,
      },
      addresses: {
        billing: {
          name: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          postalCode: '',
          countryCode: 'GB',
          mobile: '',
          sms: '',
        },
        shipping: {
          name: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          postalCode: '',
          countryCode: 'GB',
        },
      },
      payment: {
        subtotal: 0,
        total: 0,
        discount: 0,
      },
      selectedShippingOption: '',
      selected: {
      },
      // cartItems: [
      //   {
      //     id: 35,
      //     name: 'Vulcan Watchstrap',
      //     type: 'strap',
      //     quantity: 1,
      //     stockLevel: 100,
      //     price: 216,
      //     image: '',
      //     description: 'Blue Strap, Red Loops, Gold, Polished Finish',
      //   },
      //   {
      //     id: 2,
      //     name: 'Vulcan Cleaning Cloth',
      //     type: 'accessory',
      //     quantity: 1,
      //     stockLevel: 100,
      //     price: 12,
      //     image: 'vulcan-cleaning-cloth.jpg',
      //     description: '',
      //   }
      // ],
      // shippingOptions: [
      //   {
      //     id: 1,
      //     name: 'Standard Delivery',
      //     isFree: false,
      //     isUKOnly: true,
      //     flatFee: 2.5,
      //   },
      //   {
      //     id: 2,
      //     name: 'Tracked Delivery',
      //     isFree: false,
      //     isUKOnly: true,
      //     flatFee: 3.5,
      //   },
      //   {
      //     id: 3,
      //     name: 'International Delivery',
      //     isFree: false,
      //     isUKOnly: false,
      //     flatFee: 10,
      //   },
      //   {
      //     id: 4,
      //     name: 'Click & Collect',
      //     isFree: true,
      //     isUKOnly: false,
      //     flatFee: 0,
      //   },
      // ],
      // accessories: [
      //   {
      //     id: 1,
      //     name: 'Vulcan Watch Roll',
      //     price: 50,
      //     stockLevel: 50,
      //     image: 'vulcan-watch-roll.jpg',
      //   },
      //   {
      //     id: 2,
      //     name: 'Vulcan Cleaning Cloth',
      //     price: 50,
      //     stockLevel: 50,
      //     image: 'vulcan-cleaning-cloth.jpg',
      //   },
      //   {
      //     id: 3,
      //     name: 'Vulcan Bar Tool',
      //     price: 50,
      //     stockLevel: 50,
      //     image: 'vulcan-bar-tool.jpg',
      //   },
      //   // {
      //   //   id: 4,
      //   //   name: 'Vulcan Bar Tool',
      //   //   price: 50,
      //   //   stockLevel: 50,
      //   //   image: 'vulcan-bar-tool.jpg',
      //   // },
      //   // {
      //   //   id: 5,
      //   //   name: 'Vulcan Bar Tool',
      //   //   price: 50,
      //   //   stockLevel: 50,
      //   //   image: 'vulcan-bar-tool.jpg',
      //   // },
      // ]
    }
  },
  computed: {
    selectedDeliveryCountry() {
      return this.addresses.shipping.countryCode;
    },
    validConditionB() {
      return this.selectedShippingOption; // check if there's a shipping option selected
    },
    formIsValid() {
      let conditionA = this.addresses.shipping.countryCode; // check if there's a country to ship to
      let conditionB = this.validConditionB;
      let conditionC = Object.keys(this.cartItems).length > 0
      return conditionA && conditionB && conditionC;
    }
  },
  watch: {
    selectedDeliveryCountry() {
      console.log('adf')
     if (this.selectedDeliveryCountry !== 'GB') {
       this.selectedShippingOption = null;
     }
    },
    selectedShippingOption() {
      this.updateTotal();
    }
  },
  methods: {
    setCurrency() {
      let currency = document.getElementById('currency-value-holder').getAttribute('data-currency');
      let currencySymbol = '';
      switch(currency) {
        case 'usd':
          currencySymbol = '$';
          break;
        case 'eur':
          currencySymbol = '€';
          break;
        case 'gbp':
          currencySymbol = '£';
          break;
        case 'hkd':
          currencySymbol = 'HKD';
          break;
        case 'aed':
          currencySymbol = 'د.إ';
          break;
        default:
          currencySymbol = '£';
          break;
      }
      this.meta.currency = currency;
      this.meta.currencySymbol = currencySymbol;
    },
    removeItem(item) {
        axios.post('/cart/remove', {
          cartItemId: item.rowId
        })
        .then((response) => {
          location.reload();
          console.log(response.data);
        })
        .catch(function(error) {
          console.log(error);
        });
      // this.cartItems = this.cartItems.filter(i => i !== item);
    },
    increment(item, amount) {
      if ((item.qty >= item.stockLevel && amount > 0) || (item.qty === 1 && amount < 0)) {
        return;
      }
      item.qty = parseInt(item.qty) + parseInt(amount);
      this.updateTotal();

      axios.post('/cart/qty', {
        cartItemId: item.rowId,
        newQty: item.qty
      })
      .then((response) => {
        location.reload();
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
    },
    addAccessory(accessory) {
      axios.post('/cart/' + accessory.id)
      .then((response) => {
        location.reload();
      })
      .catch(function(error) {
        console.log(error);
      });
      // let id = accessory.id;
      // let targetItem = this.cartItems.find(i => i.id === id);
      // if (targetItem) {
      //   this.increment(targetItem, 1);
      // } else {
      //   let newItem = accessory;
      //   newItem.qty = 1;
      //   this.cartItems.push(newItem);
      //   this.updateTotal();
      // }
    },
    copyToShipping() {
      this.addresses.shipping.name = this.addresses.billing.name;
      this.addresses.shipping.address1 = this.addresses.billing.address1;
      this.addresses.shipping.address2 = this.addresses.billing.address2;
      this.addresses.shipping.city = this.addresses.billing.city;
      this.addresses.shipping.province = this.addresses.billing.province;
      this.addresses.shipping.countryCode = this.addresses.billing.countryCode;
      this.addresses.shipping.postalCode = this.addresses.billing.postalCode;
    },
    updateTotal() {
      let subtotal = 0;
      for (let cartItem in this.cartItems) {
        console.log(this.cartItems[cartItem]);
        let price = parseFloat(this.cartItems[cartItem].price);
        subtotal += price;
      }
      console.log(subtotal);

      if(this.discount > 0){
        subtotal = subtotal - this.discount;
      }

      this.payment.subtotal = subtotal;
      this.payment.total = subtotal;

      if(this.payment.subtotal <= 0){
        this.payment.subtotal = 0;
        this.payment.total = 0;
      }

      if (this.selectedShippingOption) this.payment.total += parseFloat(this.shippingOptions.find(i => i.abbreviation.toUpperCase() === this.addresses.shipping.countryCode).shippingOptions.find(o => o.id === this.selectedShippingOption).flatFee);
    },
    onSubmit() {
      return;
    }
  },
  mounted() {
    this.updateTotal();
    this.setCurrency();
    EventBus.$emit('initiated');
  }
}
