var navEnhancer = {
  methods: {
    navEnhancer() {

      let summariesDOM = document.querySelectorAll('.details > summary');
      let submenusDOM = document.querySelectorAll('.site-nav-join > summary');
      [].forEach.call(summariesDOM, function(summary) {
        summary.addEventListener('click', function(e) {
          // e.preventDefault();
          [].forEach.call(summariesDOM, function(s) {
            if (s !== summary) setTimeout(() => {
              s.parentNode.classList.remove('open');
            });
          })
        });
      });

      [].forEach.call(submenusDOM, function(submenu) {
        submenu.addEventListener('click', function(e) {
          e.preventDefault();
          setTimeout(() => {
            submenu.parentNode.classList.remove('open');
          });
        })
      });

    },

    // dismissStatusNotice() {
    //   let noticeDOM = document.getElementById('session-status-notice');
    //   noticeDOM.parentNode.removeChild(noticeDOM);
    // }

  }

}

export default navEnhancer;
