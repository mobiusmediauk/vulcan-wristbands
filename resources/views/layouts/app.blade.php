<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
  <script src="https://www.google.com/recaptcha/api.js" async defer></script>

  <title>{{ (isset($seo['titleTag'])) ? $seo['titleTag'] . ' | ' : '' }}Vulcan Watch Straps Official</title>


  <meta property="og:url" content="@php echo Request::url(); @endphp" />
  <meta property="og:title" content="{{ (isset($seo['titleTag'])) ? $seo['titleTag'] . ' | ' : '' }}Vulcan Watch Straps Official" />
  <meta property="og:description" content="{{ $seo['metaDescription'] ?? '' }}" />
  <meta id="facebook-image" property="og:image" content="{{ (isset($imageToShare)) ? $imageToShare : asset('images/vulcan-homepage-1.jpg')  }}"/>
  <meta property="og:image:width" content="640" />
  <meta property="og:image:height" content="400" />
  <meta property="og:updated_time" content="{{time()}}" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@VulcanStraps">
  <meta name="twitter:title" content="{{ (isset($seo['titleTag'])) ? $seo['titleTag'] . ' | ' : '' }}Vulcan Watch Straps Official">
  <meta name="twitter:description" content="{{ $seo['metaDescription'] ?? '' }}">
  <meta name="twitter:creator" content="@VulcanStraps">
  {{-- <meta name="twitter:image" content="{{ asset('images/vulcan-homepage-1.jpg') }}"/> --}}
  <meta name="twitter:domain" content="http://vulcanwatchstraps.com/">
  <meta id="twitter-image" name="twitter:image" content="{{ (isset($imageToShare)) ? $imageToShare : asset('images/vulcan-homepage-1.jpg') }}">



  @include('includes.head')
  @php
    $settings = (isset($settings)) ? $settings : App\Settings::first();
  @endphp

  @isset($seo['metaDescription'])
    <meta name="description" content="{{ $seo['metaDescription'] }}">
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  @endisset

  @if ($settings->GAtrackingCode)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ $settings->GAtrackingCode }}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '{{ $settings->GAtrackingCode }}');
    </script>
  @endif


</head>
<body class="index" style="height:100%">

  @include('includes.backstage')

  @if (Route::currentRouteName() === 'customise' ||
    Route::currentRouteName() === 'checkout' ||
    Route::currentRouteName() === 'cart')
    <div id="app">
  @endif

    @include('includes.header')


    <main class="view-main">

      @yield('content')

    </main>

    @if (Route::currentRouteName() === 'customise' ||
      Route::currentRouteName() === 'checkout' ||
      Route::currentRouteName() === 'cart')
      {{-- ends vue #app container --}}
      </div>
    @endif

  @include('includes.footer')
  @include('includes.scripts')

</body>
</html>
