<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>
  @include('includes.head')
</head>
<body class="index">

  @include('includes.backstage')

  {{-- @include('includes.header') --}}


  <main class="view-main">

    @yield('content')

  </main>

  {{-- @include('includes.footer') --}}
  @include('includes.scripts')

</body>
</html>
