@extends('layouts.app', ['seo' => $post->seo, 'imageToShare' => $post->coverImage()])
@section('content')

  <header class="page-section section-simple-header has-background blog-single-header" style="background-image: url('/images/article/{{ $post->image }}')">
    <div class="section-container">
      <h1>{{ $post->title }}</h1>
    </div>
  </header>

  <div class="page-paper-wrapper blog-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <article class="page-section blog-article-section">
        <div class="section-container">
          <figure class="blog-article__featured-image">
            <img alt="{{ $post->altTag }}" title="{{ $post->altTag }}" src="{{ $post->coverImage() }}" />
          </figure>
          <div class="blog-article__body">
            @php
              echo \DbView::make($post)->field('body')->with([
                'post' => $post,
                'imageBundle' => $post->outputImage(),
                'images' => $post->images(),
                ])->render();
            @endphp
          </div>
        </div>
        <footer class="blog-article__footer">
          <ul class="social-links-list">
            <li>Share</li>
            @include('includes.social-share')
          </ul>
        </footer>
      </article>

      <section class="page-section read-next-section">
        <div class="section-container">
          <h2>Read Next</h2>
          <ul class="blog-read-next-list">
            @foreach ($posts as $key => $nextPost)
              <li>
                <a class="blog-single-thumb" href="{{ url('blog', $nextPost->slug) }}">
                  <figure style="background-image: url('{{ $nextPost->coverImage() }}')">
                    {{-- <img src="/images/article/{{ $nextPost->image }}" /> --}}
                    <img src="{{ $nextPost->coverImage() }}" alt="{{ $nextPost->altTag }}"/>
                  </figure>
                  <h3>{{ $nextPost->title }}</h3>
                </a>
              </li>
            @endforeach
          </ul>
        </div>
      </section>

      {{-- <section class="page-section section-generic-banner-with-text" style="background-image: url('images/ship-globally-background.jpg')">
        <div class="section-container section-container__advertising">
          <h2>Full width banner for advertising vulcan products</h2>


        </div>
      </section> --}}

    </div>
    @isset($mainpage->image1)
      @if ($mainpage->altTag7)
        <section class="page-section products-banner" style="background-image: url({{asset('images/' . $mainpage->image1)}})">
          <div class="section-container">
            <h2>{{ $mainpage->caption1 }}</h2>
            <a href="{{ route('customise') }}" class="button btn-customise">Our Straps</a>
          </div>
          {{-- <figure >
            <img src="{{asset('images/' . $mainpage->image1)}}" alt="{{ $mainpage->altText2 }}"/>
          </figure> --}}
        </section>
      @endif
    @endisset
  </div>

@endsection
