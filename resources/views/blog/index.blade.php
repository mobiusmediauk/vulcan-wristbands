@php
  $page = App\MainPage::find(7);
@endphp

@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')

  <section class="page-section section-vlog">
    <div class="section-container">
      <header class="section-vlog-header">
        <h1>News from the vlog</h1>
        <p>If you love luxury innovations and style as much as we do then this is the place for you. Keep up to date with the latest in British made luxury and watch industry news, get inspired by our engineers and find out more about Vulcan’s designs and what we are working on right now.</p>
      </header>
      <div class="vlog-video-list">
        <div class="vlog-featured-video-column">
          @if(null !== $videos->first())
            <figure>
              <div>
                <iframe width="560" height="315" src="{{ $videos->first()->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              <figcaption>{{ $videos->first()->title }}</figcaption>
            </figure>
          @endif
        </div>
        <div class="vlog-secondary-videos-column">
        @forelse ($videos as $video)
          @if ($video->id != $videos->first()->id)
            <figure>
              <div>
                <iframe width="560" height="315" src="{{ $video->video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              <figcaption>{{ $video->title }}</figcaption>
            </figure>
          @endif
        @empty
        </div>
          <p>No videos yet...</p>
        @endforelse
        {{-- <div class="vlog-featured-video-column">
          <figure>
            <div>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/Pg8hrWT8cxs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <figcaption>Match your style</figcaption>
          </figure>
        </div>
        <div class="vlog-secondary-videos-column">
          <figure>
            <div>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/Pg8hrWT8cxs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <figcaption>Match your style</figcaption>
          </figure>
          <figure>
            <div>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/Pg8hrWT8cxs?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <figcaption>Match your style</figcaption>
          </figure>
        </div> --}}
      </div>
    </div>
  </section>

  <div class="page-section blog-index-section">
    <div class="section-container">
      <h2 class="section-heading">Latest Stories</h2>
      <ul class="blog-index-list">
        @forelse ($posts as $post)
          <li>
            <a class="blog-single-thumb" href="{{ url('blog/' . $post->slug)}}">
              <figure style="background-image: url('/images/article/{{ $post->image }}')">
                <img src="/images/article/{{ $post->image }}" alt="{{ $post->altTag }}"/>
              </figure>
              <article class="page-section__copy-article">
                <h2>{{ $post->title }}</h2>
                <div class="">
                  {!! str_limit(preg_replace('/<a.*?>|<a.*?>|<\/a>/', '', $post->excerpt()), $limit = 100, $end = '...' ) !!}
                </div>
                <span class="button pseudo-read-story-button">Read Story</span>
              </article>
            </a>
          </li>
        @empty
          <li>No Stories...</li>
        @endforelse
      </ul>
      {{-- <div class="blog-index-load-more-wrapper">
        <button class="button">Load more</button>
      </div> --}}
    </div>
  </div>

@endsection
