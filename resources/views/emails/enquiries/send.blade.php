@component('mail::message')
# New {{ $enquiry->topic }} Enquiry from {{ $enquiry->firstName }} {{ $enquiry->lastName }}
Email: {{ $enquiry->email }}

@if($enquiry->orderNumber )Order reference No: <br>{{ $enquiry->orderNumber }}<br>@endif

<br>Message: <br>{{ $enquiry->message }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
