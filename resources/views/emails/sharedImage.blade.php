@component('mail::message')
# Hello {{ $name }},

{{ $message }}

<img src="{{ $image }}">

Thanks,<br>
{{ config('app.name') }}
@endcomponent
