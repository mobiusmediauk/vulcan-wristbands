@extends('templates.default')

@section('title', 'Page Not Found')

@section('header')
  Page not found...
@endsection

@section('body')
  <p>We could not find the page you are looking for.</p>
  <a class="button" href="{{ url('/') }}">Go back to homepage</a>
@endsection
