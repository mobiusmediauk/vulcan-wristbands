@extends('layouts.app')
@section('content')

  {{-- This page only shows when visitor is not logged in --}}

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>Check out</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section login-forms-section">
        <div class="section-container">

          @include('includes.login-form')

          <form action="{{ route('checkout.guest') }}" method="post" class="shaded-form padded-form" id="shop-as-guest-form">

            <h3>Shop as Guest</h3>
            {{ csrf_field() }}
            <div class="input--text">
              <input name="email" placeholder="Email" type="email" required/>
            </div>
            <h4>Newsletter</h4>
            <div class="checkbox">
              <input type="checkbox" name="receiveNewsletter" id="receiveNewsletter" checked/>
              <label for="receiveNewsletter">I want to receive the Vulcan Newsletter</label>
            </div>
            <p><small>Your personal data will be used by Vulcan to provide the Newsletter service expressly requested by you. Please consult the <a href="{{ url('/') }}">Privacy Information Notice</a> for further information.</small></p>
            <div class="shaded-form-submit-row">
              <button type="submit" class="button button-inverted">Proceed</button>
            </div>
          </form>

        </div>
      </section>

    </div>
  </div>

@endsection
