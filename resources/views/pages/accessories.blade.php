@php
  $page = App\MainPage::find(6);
@endphp

@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-image-accessories.jpg')">
    <div class="section-container">
      <h1>Accessories</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section product-list-section">
        <div class="section-container accessories-container">
          <div class="product-list-item">
            <figure class="product-list-item__figure" id="bands-thumb" style="background-image: url({{ asset('images/products/bands-emerald-green.png') }})">
            </figure>
            <article class="page-section__copy-article">
              {{-- <div> --}}
                <h2>Vulcan Watch Bands (pair)</h2>
                <div id="bands-description" class="product-description">{!! $bands->first()->description !!}</div>
              {{-- </div> --}}
              <form action="{{ route('cart.add', $bands->first()->id) }}" id="bands-form" method="post">
                {{ csrf_field() }}
                <ul class="accessory-bands-color-selector">
                  @foreach ($bands as $band)
                    <li class="accessory-bands-color-selector-radio">
                      <input type="radio" name="bands-color"
                        id="accessory-bands-color-{{ $band->id }}" value="{{ $band->id }}"
                        onclick="document.getElementById('bands-thumb').style.backgroundImage = 'url({{ asset('images/products/' . $band->thumbnail) }})';
                          document.getElementById('bands-form').setAttribute('action', '{{ route('cart.add', $band) }}');
                          document.getElementById('bands-description').innerHTML='{{ $band->description }}'"
                        @if ($band->id === $bands->first()->id) checked @endif
                        />
                      <label for="accessory-bands-color-{{ $band->id }}" style="background-color: rgb({{ $band->rgb }})"></label>
                    </li>
                  @endforeach
                </ul>
                <div class="accessory-add-to-cart-form">
                  <button class="button">Add to cart</button>
                  <span class="accessory-price-tag">{{ $band->priceDisplay() }}</span>
                </div>
              </form>
            </article>
          </div>
          @foreach ($accessories as $product)
            <div class="product-list-item">
              <figure class="product-list-item__figure" style="background-image: url({{ $product->thumb() }})">
                <img src="{{ $product->thumb() }}" alt="{{ $product->name }}"/>
              </figure>
              <article class="page-section__copy-article">
                {{-- <div> --}}
                  <h2>{{ $product->name }}</h2>
                  <div class="product-description">{!! $product->description !!}</div>
                {{-- </div> --}}
                <form class="accessory-add-to-cart-form" action="{{ route('cart.add', $product) }}" method="post">
                  {{ csrf_field() }}
                  <button class="button">Add to cart</button>
                  <span class="accessory-price-tag">{{ $product->priceDisplay() }}</span>
                </form>
              </article>
            </div>
          @endforeach
          @foreach ($vouchers as $product)
            @if($product->price() > 0)
              <div class="product-list-item">
                <figure class="product-list-item__figure" style="background-image: url({{ $product->thumb() }})">
                  <img src="{{ $product->thumb() }}" />
                </figure>
                <article class="page-section__copy-article">
                  {{-- <div class="product-description"> --}}
                    <h2>{{ $product->name }}</h2>
                    <div>{!! $product->description !!}</div>
                  {{-- </div> --}}
                  <form class="accessory-add-to-cart-form vouchers-form" action="{{ route('cart.add', $product) }}" method="post">
                    {{ csrf_field() }}
                    <div class="voucher-input" data-currency="{{ app('currency')->get() }}">
                      {{-- ({{ app('currency')->get() }}) --}}
                      <input type="number" name="amount" min="35" max="2000" placeholder="Enter amount">
                    </div>
                    <button class="button">Add to cart</button>
                    {{-- <span class="accessory-price-tag">{{ $product->priceDisplay() }}</span> --}}
                  </form>
                </article>
              </div>
            @endif
          @endforeach
        </div>
      </section>

      {{-- TODO: finish pagination: --}}
      {{-- <section class="page-section pagination-section">
        <ul class="section-container"> --}}
          {{-- <li><a class="button">Back</a></li> --}}
          {{-- <li><a>1</a></li>
          <li><a>2</a></li>
          <li><a>3</a></li> --}}
          {{-- {{ $accessories->links() }} --}}
          {{-- <li><a class="button">Next</a></li> --}}
        {{-- </ul>
      </section> --}}

    </div>
  </div>

@endsection
