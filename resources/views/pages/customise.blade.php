@php
  $page = App\MainPage::find(4);
@endphp

@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')
<section class="page-section customise-now-section">
  <div class="section-container">
    <div class="loading-notice" id="loading-notice"><span><svg width="38" height="38" viewBox="0 0 38 38"><use xlink:href="#vulcan-logo-icon"></use></svg><p>Loading...</p></span></div>
    <strap-customiser :source-data='{{ $sourceData }}' :custom-text='{{ json_encode($page->body1) }}' :presets='{!! $presets !!}'></strap-customiser>
  </div>
</section>
@endsection
