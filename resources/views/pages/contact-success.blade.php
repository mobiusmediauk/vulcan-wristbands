@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>Message sent!</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section order-success-section">
        <form class="section-container">
          <h2>Thanks for contacting Vulcan Watch Strap.</h2>
          <p>We will get back to you as soon as possible.</p>
          <a href="{{ url('customise')}}" class="button">Create your own watch strap</a>
          <a href="{{ url('/')}}" class="button button-inverted">Go back to homepage</a>
        </form>
      </section>

    </div>
  </div>

@endsection
