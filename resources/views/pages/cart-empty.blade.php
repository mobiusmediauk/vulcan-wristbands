@extends('templates.default')

@section('title', 'Empty cart')

@section('header')
  Your cart is empty
@endsection

@section('body')
  <p>We can't show your cart, because it's empty.</p>
  <a href="{{ route('customise')}}" class="button">Create a new watch strap</a>
  <a href="{{ route('accessories') }}" class="button">Browse our accessories</a>
  <a class="button button-inverted" href="{{ url('/') }}">Go back to homepage</a>
@endsection
