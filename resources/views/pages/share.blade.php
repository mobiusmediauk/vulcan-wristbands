<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">
<head>

  <title>Vulcan Watch Straps Official</title>

  <meta property="og:url" content="https://vulcanwatchstraps.com/share/{{ $id }}" />
  <meta property="og:title" content="{{ $page->body2 ?? 'Vulcan Watch Straps Official' }}" />
  <meta property="og:description" content="{{ $page->body3 ?? 'Check out this design I created using the Vulcan Watch Straps customisation tool. What do you think? Customise your Rolex in your own personal style with these luxury vulcanised rubber watch straps. #rolex #vulcanwatchstraps #watchstraps' }}" />
  <meta id="facebook-image" property="og:image" content="http://vulcanwatchstraps.com/images/shared/{{ $id }}.jpg"/>
  <meta property="og:image:width" content="640" />
  <meta property="og:image:height" content="400" />
  <meta property="og:updated_time" content="{{time()}}" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@VulcanStraps">
  <meta name="twitter:title" content="Vulcan Watch Straps Official">
  <meta name="twitter:description" content="Premium Meets Personality: Stand out from the crowd with Vulcan’s customisable rubber watch straps. We use the most advanced materials to combine science and style because we understand your watch should be as unique as the life you lead. We’ve engineered it, you customise it.">
  <meta name="twitter:creator" content="@VulcanStraps">
  <meta name="twitter:image" content="http://vulcanwatchstraps.com/images/shared/{{ $id }}.jpg">
  <meta name="twitter:domain" content="http://vulcanwatchstraps.com/share/{{ $id }}">
  <meta id="twitter-image" name="twitter:image" content="http://vulcanwatchstraps.com/images/shared/{{ $id }}.jpg">

</head>

<body>

  <script>
    window.location.href = '{{ url('/our-straps') }}';
  </script>

</body>

</html>
