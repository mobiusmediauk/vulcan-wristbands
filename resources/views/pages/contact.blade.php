@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-contact-us-banner.jpg')">
    <div class="section-container">
      <h1>Contact us</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section contact-form-section">

        <div class="section-container">
          <article class="page-section__copy-article">
            {!! $page->body1 !!}
            <div class="contact-page-social-list">
              <h3>Follow us</h3>
              <ul class="social-links-list">
                @include('includes.social-media-list-partial')
              </ul>
            </div>
          </article>
          <form action="{{ route('enquiry.send') }}" method="post" class="contact-form shaded-form padded-form" id="contact-form">
            <h3>Send us a message</h3>
            {{ csrf_field() }}
            <div class="select">
              <select name="topic">
                <option value="No topic" selected>Select a topic</option>
                <option value="Sales">Sales</option>
                <option value="Product Information">Product Information</option>
                <option value="Shipping">Shipping</option>
                <option value="Cancellation, return and refund">Cancellation, return and refund</option>
                <option value="Retailers">Retailers</option>
              </select>
              <svg width="12" height="12" viewBox="0 0 12 12"><use xlink:href="#icon-triangle-southeast"></use></svg>
            </div>
            <div class="input--text">
              <input name="firstName" placeholder="First Name (Required)" type="text" required/>
            </div>
            <div class="input--text">
              <input name="lastName" placeholder="Last Name" type="text"/>
            </div>
            <div class="input--text">
              <input name="email" placeholder="Email (Required)" type="email" required/>
            </div>
            <div class="input--text">
              <input name="orderNumber" placeholder="Order Number" type="text"/>
            </div>
            <div class="textarea">
              <textarea name="message" placeholder="Message (Required)" required></textarea>
            </div>
            <div class="g-recaptcha" data-sitekey="6Lca4JYUAAAAAHkBvwHMOe-S-Vucxd6gj9BiJ7CX"></div>
            <div class="shaded-form-submit-row">
              <button type="submit" class="button">Send</button>
            </div>
          </form>
        </div>

      </section>

    </div>
  </div>

@endsection
