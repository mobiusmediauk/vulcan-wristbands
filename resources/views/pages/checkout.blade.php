@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url({{asset('images/vulcan-banner-people.jpg')}})">
    <div class="section-container">
      <h1>Check out</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section checkout-section">

        <div class="section-container">

          <span id="paymentErrors" class="payment-errors"></span>

          <form class="shaded-form payment-form" action="{{ route('checkout.complete') }}" id="paymentForm" method="post">
            {{ csrf_field() }}

            <div class="grid">

              <div class="input--text grid-12-col">
                <label>Card Number</label>
                <input data-worldpay="number" size="20" type="text" placeholder="Card Number"/>
              </div>
              <div class="input--text grid-12-col">
                <label>Name on Card</label>
                <input data-worldpay="name" name="name" type="text" placeholder="Name on Card" />
              </div>
              <div class="input--text payment-expiration-form-fields grid-8-col">
                <label>Expiration (MM/YYYY)</label>
                <div>
                  <input data-worldpay="exp-month" size="2" type="text" placeholder="MM"/><span> / </span><input data-worldpay="exp-year" size="4" type="text" placeholder="YYYY"/>
                </div>
              </div>
              <div class="input--text grid-4-col">
                <label>CVC</label>
                <input data-worldpay="cvc" size="4" type="text" />
              </div>

              @if($convTypes->count() > 0)
                <div class="input--checkbox checkout-survey">
                  <h2>How did you hear about us?</h2>
                  <ul class="checkout-survey__list">
                    @foreach ($convTypes as $key => $type)
                      <li class="checkbox">
                        <input type="checkbox" name="leadConvType[{{ $type->id }}]" value="{{ $type->name }}">
                        <label for="">{{ $type->name }}</label>
                      </li>
                    @endforeach
                  </ul>
                </div>
              @endif

              <div class="input--submit grid-12-col">
                <input class="button button-inverted button--big" type="submit" value="Place Order" />
              </div>

            </div>

          </form>

          <div class="checkout-form-world-pay-logo-col">
            <a class="payment-icon-worldpay" href="https://www.worldpay.com/" target="_blank"><img src="{{ asset('images/payment-icons/powered-by-worldpay.png') }}" /></a>
            <div class="payment-methods-icon-list">
              <img src="{{ asset('images/payment-icons/card-visa.png') }}"/>
              <img src="{{ asset('images/payment-icons/card-mastercard.png') }}"/>
              <img src="{{ asset('images/payment-icons/amex.png') }}"/>
              {{-- <img src="{{ asset('images/payment-icons/card-maestro.png') }}"/>
              <img src="{{ asset('images/payment-icons/card-jcb.gif') }}"/> --}}
            </div>
          </div>

        </div>

      </section>

    </div>
  </div>

@endsection
