@extends('templates.default')

@section('title', 'Something went wrong!')

@section('header')
  Something went wrong!
@endsection

@section('body')
  <h2>Your Credit Card was declined!</h2>

  <a class="button button-inverted" href="{{ url('/') }}">Go back to homepage</a>
@endsection
