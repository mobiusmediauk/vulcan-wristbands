@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>Thank you!</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section checkout-section">
        <h2>We received your order!</h2>
        <p>Order number <b>{{ $orderNumber }}</b></p>
      </section>

    </div>
  </div>

@endsection
