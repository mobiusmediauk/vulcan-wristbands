@extends('layouts.app')
@section('content')
  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>Cart</h1>
    </div>
  </header>

  <div class="page-paper-wrapper cart-wrapper">
    <div class="page-paper-wrapper__container">
        <section class="page-section shopping-cart-section">
          <div class="section-container">
            {{-- @if(Cart::count() > 0) --}}
              <div class="loading-notice" id="loading-notice"><span><svg width="38" height="38" viewBox="0 0 38 38"><use xlink:href="#vulcan-logo-icon"></use></svg><p>Loading...</p></span></div>
                <shopping-cart
                :shipping-options="{{ json_encode($shippingOptions) }}"
                :accessories="{{ $accessories }}"
                :cart-items="{{ $cartItems }}"
                :discount="{{ App\Voucher::getDiscountAmount(session()->get('voucherId'), App\Checkout::cartSubtotal()) }}"
                :ip-details="{{ json_encode($ipDetails) }}"
                :do-cart-have-straps="{{ $doCartHaveStraps }}"
                ></shopping-cart>
              </div>
            {{-- @else
              Your cart is empty!
            @endif --}}
        </section>
    </div>
  </div>

@endsection
