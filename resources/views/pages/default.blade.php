@extends('templates.' . $page->template, ['seo' => $page->seo])

@section('title', $page->title)

@section('header')
  {{ $page->header }}
@endsection

@section('body')
  {!! $page->body !!}
@endsection
