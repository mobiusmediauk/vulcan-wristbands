@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription, 'sliderTransitionSpeed' => $sliderTransitionSpeed]])
@section('content')
  <script type="text/javascript">
    var sliderTransitionSpeed =  @php echo ($sliderTransitionSpeed * 1000) @endphp
  </script>
  <section class="page-section homepage-banner-section has-video-background swiper-container" id="homepage-content-slider">
    @if($slider)
      <div class="swiper-wrapper">
        @foreach ($slider->items as $key => $item)
          <div class="swiper-slide">

            @if($item->getBackgroundMedia())
              @if($item->getBackgroundMedia()->mime_type == 'video/mp4')
                <video class="background-video" src="{{ asset($item->getBackgroundMedia()->getUrl()) }}" playsinline autoplay muted loop></video>
              @else
                <div class="background-video__image" style="background-image: url('{{ asset($item->getBackgroundMedia()->getUrl()) }}');"></div>
              @endif
            @endif

            <div class="section-container">
              <div class="homepage-banner-copy">
                {!! $item->body !!}
                @if($item->buttonLink && $item->buttonText)
                  <a href="{{ $item->buttonLink }}" class="button">{{ $item->buttonText }}</a>
                @endif
              </div>

              @if($item->getItemMedia())
                <div class="homepage-banner-figure-slides">
                  <figure>
                    <img data-src="{{ asset($item->getItemMedia()->getUrl()) }}" alt="slider-item"/>
                    <figcaption></figcaption>
                  </figure>
                </div>
              @endif

            </div>
          </div>
        @endforeach
      </div>
      <div class="swiper-pagination"></div>
      <div class="homepage-banner-social-links">
        <ul class="social-links-list">
          <li><h3>Follow us</h3></li>
          @include('includes.social-media-list-partial')
        </ul>
      </div>
    @endif
  </section>

  <section class="page-section feature-showcase-section">
    <div class="section-container">
      <div class="feature-showcase-copy-col">
        <figure class="feature-showcase-figure-watch">
          <img class="lazy" data-src="images/{{ $page->image2 }}" alt="{{ $page->altTag2 }}"/>
          <figcaption>{{ $page->caption2 }}</figcaption>
        </figure>
        <article class="page-section__copy-article">
          {!! $page->body2 !!}
          <a href="{{ url('our-straps')}}" class="button">Buy Now</a>
        </article>
      </div>
      <div class="feature-showcase-lifestyle-col">
        {{-- <figure class="feature-showcase-figure-lifestyle" style="background-image: url({{asset('images/final-images/vulcan-strap-blue.jpg')}})">
          <img src="{{asset('images/final-images/vulcan-strap-blue.jpg')}}" /> --}}
        <figure class="feature-showcase-figure-lifestyle lazy" data-bg="url({{asset('images/' . $page->image3)}})">
          <img class="lazy" data-src="{{asset('images/' . $page->image3)}}" alt="{{ $page->altTag3 }}"/>
          <figcaption>{{ $page->caption3 }}</figcaption>
        </figure>
        <div class="feature-showcase-figure-straps strap-image-container" >
          <figure class="lazy" data-bg="url({{asset('images/' . $page->image4)}})">
            <img class="lazy" data-src="{{asset('images/' . $page->image4)}}" alt="{{ $page->altTag4 }}"/>
          </figure>
        </div>
      </div>
    </div>
  </section>

  <section class="page-section feature-showcase-section is-mirrored">
    <div class="section-container">
      <div class="feature-showcase-copy-col">
        <figure class="feature-showcase-figure-watch">
          <img class="lazy" data-src="images/{{ $page->image5 }}" alt="{{ $page->altTag5 }}"/>
          <figcaption>{{ $page->caption4 }}</figcaption>
        </figure>
        <article class="page-section__copy-article">
          {!! $page->body3 !!}
          <a href="{{ url('our-straps')}}" class="button">Buy Now</a>
        </article>
      </div>
      <div class="feature-showcase-lifestyle-col">
        {{-- <figure class="feature-showcase-figure-lifestyle" style="background-image: url('{{asset('images/final-images/vulcan-strap-green.jpg')}}')">
          <img src="{{asset('images/final-images/vulcan-strap-green.jpg')}}" /> --}}
        <figure class="feature-showcase-figure-lifestyle lazy" data-bg="url('{{asset('images/' . $page->image6)}}')">
          <img class="lazy" data-src="{{asset('images/final-images/' . $page->image6)}}" alt="{{ $page->altTag6 }}"/>
          <figcaption>{{ $page->caption5 }}</figcaption>
        </figure>
        <div class="feature-showcase-figure-straps strap-image-container" >
          <figure class="lazy" data-bg="url({{asset('images/' . $page->image7)}})">
            <img class="lazy" data-src="images/{{ $page->image7 }}" alt="{{ $page->altTag7 }}"/>
          </figure>
        </div>
      </div>
    </div>
  </section>

  <section class="page-section our-story-section">
    <div class="section-container">
      <article class="page-section__copy-article">
        {!! $page->body4 !!}
      </article>
      <div class="video-block">
        <div class="video-player-wrapper">
          <iframe width="560" height="315" class="lazy" data-src="{{ $page->caption6 }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>
@endsection
