@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-difference-banner.jpg')">
    <div class="section-container">
      <h1>Why are we different</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section section-with-quote">
        <div class="section-container">
          <figure class="quote-figure">
            <blockquote>
              {{-- <p><span>“You Will Never</span> <span>Influence The World</span> <span>By Trying To Be Like It</span>.”</p> --}}
              <p><span>“{{ $page->text1 }}”</p>
            </blockquote>
            <figcaption>- {{ $page->text2 }}</figcaption>
          </figure>
          <article class="page-section__copy-article">
            {!! $page->body1 !!}
          </article>
        </div>
      </section>

    </div>
  </div>

  <section class="page-section section-figure-outstretcher-right">
    <figure class="section-figure-outstretcher-right--75__figure">
      <img src="images/{{ $page->image1 }}" alt="{{ $page->altTag1 }}"/>
    </figure>
    <div class="section-container">
      <article class="page-section__copy-article">
        {!! $page->body2 !!}
      </article>
    </div>
  </section>

  <section class="page-section section-figure-outstretcher-right">
    <figure class="section-figure-outstretcher-right--82-5__figure">
      <img src="images/{{ $page->image2 }}" alt="{{ $page->altTag2 }}"/>
      {{-- <img src="images/vulcan-graph-placeholder-2.png" /> --}}
    </figure>
    <div class="section-container">
      <article class="page-section__copy-article">
        {!! $page->body3 !!}
      </article>
    </div>
  </section>



@endsection
