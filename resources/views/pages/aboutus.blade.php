@extends('layouts.app', ['seo' => ['titleTag' => $page->titleTag, 'metaDescription' => $page->metaDescription]])
@section('content')
  <header class="page-section section-simple-header">
      <div class="section-container">
        <h1>About Us</h1>
      </div>
    </header>

    <section class="page-section section-generic">
      <div class="section-container section-container__ie">
        <div class="section-generic__figure-column-with-backdrop">
          <figure style="background-image: url(images/{{ $page->image1 }})">
            {{-- <img src="images/our-straps-top.jpg" /> --}}
          </figure>
        </div>
        <div class="section-generic__article-column">
          <article class="page-section__copy-article">
            {!! $page->body1 !!}
            <img class="strap-page-featured-strap-image" src="images/{{ $page->image2 }}"/>
            <a href="{{ route('customise') }}" class="button btn-customise">Our Straps</a>
          </article>

        </div>
      </div>
    </section>

    <section class="page-section section-generic-banner-with-text" style="background-image: url('images/{{ $page->image3 }}'); background-position: right;">
      <div class="section-container">
        <div class="page-section__copy-article">
          {!! $page->body2 !!}
        </div>
      </div>
    </section>

    <section class="page-section section-generic">
      <div class="section-container section-container__ie">
        <div class="section-generic__figure-column-with-backdrop">
          <figure style="background-image: url(images/{{ $page->image4 }})">
            {{-- <img src="images/our-strap-bottom.jpg" /> --}}
          </figure>
        </div>
        <div class="section-generic__article-column">
          <article class="page-section__copy-article">
            {!! $page->body3 !!}
          </article>
        </div>
      </div>
    </section>

    @include('includes.instagram-feed')

@endsection
