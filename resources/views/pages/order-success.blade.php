@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>Thank you</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section order-success-section">
        <form class="section-container">
          <h2>Thanks for your order.</h2>
          <p>Please check your inbox for an email confirmation of your order.<br />We will notify you once your package is on the way!</p>
          <a href="{{ route('customise')}}" class="button">Create a new watch strap</a>
          <a href="{{ url('/')}}" class="button button-inverted">Go back to homepage</a>
        </form>

      </section>
      {{-- <section class="page-section share-order">
        <div class="section-container share-order-image">
          <figure class="order-success-image">
            <img src="{{asset('images/shared/1.jpg')}}" />
            <figcaption>
              Watch details
            </figcaption>
          </figure>
        </div>
        <div class="page-section share-order-list">
          <h3>Share your Purchase:</h3>
          <a target="_blank" :href="facebookUrl">
            <svg width="24" height="24" viewBox="0 0 24 24"><use xlink:href="#icon-facebook"></use></svg> Share to Facebook
          </a>
          <a target="_blank" :href="pinterestUrl">
            <svg width="24" height="24" viewBox="0 0 24 24"><use xlink:href="#icon-pinterest"></use></svg> Share to Pinterest
          </a>
          <a target="_blank" :href="twitterUrl">
            <svg width="24" height="24" viewBox="0 0 24 24"><use xlink:href="#icon-twitter"></use></svg> Share to Twitter
          </a>
        </div>
      </section> --}}

    </div>
  </div>

@endsection
