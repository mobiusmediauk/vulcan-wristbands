@extends('layouts.app')
@section('content')
<header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
   <div class="section-container">
      <h1>Admin login</h1>
   </div>
</header>
<div class="page-paper-wrapper">
   <div class="page-paper-wrapper__container">
      <section class="page-section login-forms-section">
         <div class="section-container">
            <form class="shaded-form padded-form" role="form" method="POST" action="{{ route('login') }}">
               {{ csrf_field() }}
               <div class="input--text{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>E-mail Address</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                  <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="input--text{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label>Password</label>
                  <input id="password" type="password" class="form-control" name="password" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
               </div>
               <div class="checkbox">
                  <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label for="remember">Remember Me</label>
               </div>
               <div class="form-group">
                  <button type="submit" class="button">
                  Login
                  </button>
                  <br />
                  <br />
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                  Forgot Your Password?
                  </a>
               </div>
            </form>
         </div>
      </section>
   </div>
</div>
@endsection
