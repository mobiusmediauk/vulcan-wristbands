<header class="view-header">
  <div class="view-header__logo-container">
    <a href="{{ url('')}}" title="Vulcan Home" class="view-header-logo__permalink" target="_blank">
      <svg width="120" height="60">
        <use xlink:href="#{{ $siteNameStr }}-logo"></use>
      </svg>
      <span class="view-header-logo__label">{{ config('app.name', 'Atrium') }}</span>
    </a>
  </div>
  <div class="view-header__main">
    <div class="view-header__main__container">
      @yield('view-header__content')
      {{-- {!! Breadcrumbs::renderIfExists() !!} --}}
    </div>
  </div>
    <div class="view-header__controls">
      {{-- <a class="view-header__control u-desktop" href="/admin/home" title="Atrium Home">
        @component('atrium.modules.atrium-dotted-icon', [
          'icon' => 'home',
          'title' => 'Home',
          'alert' => false,
        ])
        @endcomponent
      </a> --}}
      {{-- <a class="view-header__control">
        @component('atrium.modules.atrium-dotted-icon', [
          'icon' => 'notification',
          'title' => 'Notification',
          'alert' => true,
          'count' => 4
        ])
        @endcomponent
      </a>
      <a class="view-header__control">
        @component('atrium.modules.atrium-dotted-icon', [
          'icon' => 'message',
          'title' => 'Messages',
          'alert' => true,
          // if alert is set to true, a red dot will show up.
          'count' => 889
          // count is the number to show in the bubble when hovered.
        ])
      @endcomponent --}}
    </a>
    <div class="view-header__user-icon-button">
      <a class="user-icon-link">
        @component('atrium.modules.user-icon', [
          'iconShape' => 'circle'
        ])
        @slot('userName') {{ Auth::user()->name }} @endslot
        @endcomponent
        <span class="user-name-tag" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</span>
      </a>
      <ul class="view-header__user-menu">
        <li><a href="{{ route('admin.profile') }}">View your profile</a></li>
        <li><a href="{{ env('APP_URL') }}" target="_blank">Go to Vulcan Homepage</a></li>
        <li><a title="Log out" href="{{ url('admin/logout')}}">Log out</a></li>
      </ul>
    </div>


  </div>

</header>
