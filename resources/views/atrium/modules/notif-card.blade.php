<section
  class="notif-card"
  data-notif-type="{{ $type }}">
  @if (!isset($notifIcon))
    @php
      if ($type === 'attention') {
        $notifIcon = 'attention';
      } elseif ($type === 'alert') {
        $notifIcon = 'warning';
      } else {
        $notifIcon = 'info';
      }
    @endphp
  @endif
  <header class="notif-card__header">
    <svg class="notif-card__icon" width="18" height="18" viewbox="0 0 18 18"><use xlink:href="#icon-{{ $notifIcon }}"></use></svg>
    <h3 class="notif-card__title">
      @if ( !empty($title) )
        {{ $title }}
      @else
        Notification
      @endif
    </h3>
    <button type="button" data-action="dismiss-notif" class="module-close-button">
      <svg width="14" height="14" viewbox="0 0 14 14"><use xlink:href="#icon-close"></use></svg>
    </button>
  </header>

  @if ( !empty($message) )
    <div class="notif-card__body">
      {{ $message }}
    </div>
  @endif

  <footer class="notif-card__footer">
    <div class="button-group--pill">
      <button class="btn--pill--primary" type="button" data-action="dismiss-notif">OK</button>
      @if ( !empty($undoable) )
        @if ($undoable === true)
          <button class="btn--pill" type="button">Undo</button>
        @endif
      @endif
    </div>
      @if ( !empty($customActions) )
        {{ $customActions }}
      @endif
  </footer>

</section>
