<li class="statistics-card" data-stat-type="{{ $type }}">
  <h3 class="statistics-card__title">{{ $title }}</h3>
  <em class="statistics-card__stat">{{ $stat }}</em>
</li>
