<section id="widget-panel__search-bar" class="widget-panel__section is-search" data-section-name="search">
  <form class="widget-panel__search-bar">
    @if ( $target === 'account' )
      <input type="search" placeholder="Search for an account" />
    @endif
    <button type="submit"/>
      <svg width="18" height="18" viewbox="0 0 18 18"><use xlink:href="#icon-search"></use></svg>
    </button>
  </form>
</section>
