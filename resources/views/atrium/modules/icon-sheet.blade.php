<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="554" height="504" viewBox="0 0 554 504">
  <defs>
    <symbol id="icon-grid" data-name="icon-grid" viewBox="0 0 24.25 24.25">
      <rect x="0.13" y="0.13" width="24" height="24" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <rect x="2.13" y="2.13" width="20" height="20" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <circle cx="12.13" cy="12.13" r="10" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <line x1="0.13" y1="12.13" x2="24.13" y2="12.13" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <line x1="12.13" y1="0.13" x2="12.13" y2="24.13" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <line x1="24.13" y1="24.13" x2="0.13" y2="0.13" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
      <line x1="24.13" y1="0.13" x2="0.13" y2="24.13" style="fill: none;stroke: #ddefeb;stroke-miterlimit: 10;stroke-width: 0.25px"/>
    </symbol>
    <symbol id="icon-caret" data-name="icon-caret" viewBox="0 0 24 24">
      <polygon points="17.16 12 9.02 6.16 9.02 17.84 17.16 12"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-select" data-name="icon-select" viewBox="0 0 24 24">
      <g>
        <polygon points="12.09 4.06 8.18 10 16 10 12.09 4.06"/>
        <polygon points="12.09 19.94 8.18 14 16 14 12.09 19.94"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-notification" data-name="icon-notification" viewBox="0 0 24 24">
      <g>
        <polygon points="12 1.57 3.29 17 20.71 17 12 1.57"/>
        <circle cx="12" cy="20.14" r="2.42"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-arrow" data-name="icon-arrow" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="13.71 3.29 12.29 4.71 18.59 11 3 11 3 13 18.59 13 12.29 19.29 13.71 20.71 22.41 12 13.71 3.29"/>
    </symbol>
    <symbol id="icon-back" data-name="icon-back" viewBox="0 0 24 24">
      <g id="icon-arrow-4" data-name="icon-arrow">
        <rect width="24" height="24" style="fill: none"/>
        <polygon points="10.29 3.29 11.71 4.71 5.41 11 21 11 21 13 5.41 13 11.71 19.29 10.29 20.71 1.59 12 10.29 3.29"/>
      </g>
    </symbol>
    <symbol id="icon-circle-outlined" data-name="icon-circle-outlined" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,19a7,7,0,1,1,7-7A7,7,0,0,1,12,19ZM12,7a5,5,0,1,0,5,5A5,5,0,0,0,12,7Z"/>
    </symbol>
    <symbol id="icon-check" data-name="icon-check" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="10 17.48 3.26 10.74 4.67 9.33 10 14.66 19.33 5.33 20.74 6.74 10 17.48"/>
    </symbol>
    <symbol id="icon-message" data-name="icon-message" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="21 4 3 4 3 17 8 17 8 22 13 17 21 17 21 4"/>
    </symbol>
    <symbol id="icon-wrench" data-name="icon-wrench" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M22.56,20.44,13.16,11A6,6,0,0,0,5,2.84l4.1,4.1L6.94,9.06,2.84,5A6,6,0,0,0,11,13.16l9.4,9.4Z"/>
    </symbol>
    <symbol id="icon-more" data-name="icon-more" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <circle cx="12" cy="12" r="2"/>
        <circle cx="19" cy="12" r="2"/>
        <circle cx="5" cy="12" r="2"/>
      </g>
    </symbol>
    <symbol id="icon-zoom" data-name="icon-zoom" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="9.71 8.29 6.71 5.29 9 3 3 3 3 9 5.29 6.71 8.29 9.71 9.71 8.29"/>
      <polygon points="15.71 9.71 18.71 6.71 21 9 21 3 15 3 17.29 5.29 14.29 8.29 15.71 9.71"/>
      <polygon points="9.71 15.71 6.71 18.71 9 21 3 21 3 15 5.29 17.29 8.29 14.29 9.71 15.71"/>
      <polygon points="15.71 14.29 18.71 17.29 21 15 21 21 15 21 17.29 18.71 14.29 15.71 15.71 14.29"/>
    </symbol>
    <symbol id="icon-close" data-name="icon-close" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="19.71 5.71 18.29 4.29 12 10.59 5.71 4.29 4.29 5.71 10.59 12 4.29 18.29 5.71 19.71 12 13.41 18.29 19.71 19.71 18.29 13.41 12 19.71 5.71"/>
    </symbol>
    <symbol id="icon-menu" data-name="icon-menu" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <rect x="5" y="11" width="14" height="2"/>
      <rect x="5" y="5" width="14" height="2"/>
      <rect x="5" y="17" width="14" height="2"/>
    </symbol>
    <symbol id="icon-info" data-name="icon-info" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm1,16H11V11h2ZM12,8.69A1.19,1.19,0,1,1,13.19,7.5,1.19,1.19,0,0,1,12,8.69Z"/>
    </symbol>
    <symbol id="icon-attention" data-name="icon-attention" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2ZM11,6h2v7H11Zm1,11.69a1.19,1.19,0,1,1,1.19-1.19A1.19,1.19,0,0,1,12,17.69Z"/>
    </symbol>
    <symbol id="icon-warning" data-name="icon-warning" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2,1,21H23ZM11,8h2v6H11Zm1,10.69a1.19,1.19,0,1,1,1.19-1.19A1.19,1.19,0,0,1,12,18.69Z"/>
    </symbol>
    <symbol id="icon-error" data-name="icon-error" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm4.71,13.29-1.41,1.41L12,13.41,8.71,16.71,7.29,15.29,10.59,12,7.29,8.71,8.71,7.29,12,10.59l3.29-3.29,1.41,1.41L13.41,12Z"/>
    </symbol>
    <symbol id="icon-add" data-name="icon-add" viewBox="0 0 24 24">
      <g id="icon-error-4" data-name="icon-error">
        <rect width="24" height="24" style="fill: none"/>
        <path d="M4.93,4.93a10,10,0,1,0,14.14,0A10,10,0,0,0,4.93,4.93ZM17.66,11v2H13v4.66H11V13H6.34V11H11V6.34h2V11Z"/>
      </g>
    </symbol>
    <symbol id="icon-home" data-name="icon-home" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="12 2 3 11 3 21 9 21 9 15 15 15 15 21 21 21 21 11 12 2"/>
    </symbol>
    <symbol id="icon-statistics" data-name="icon-statistics" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <rect x="4" y="9" width="4" height="11"/>
        <rect x="10" y="4" width="4" height="16"/>
        <rect x="16" y="7" width="4" height="13"/>
      </g>
    </symbol>
    <symbol id="icon-person" data-name="icon-person" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <path d="M21,21A9,9,0,0,0,3,21Z"/>
        <circle cx="12" cy="6.31" r="3.68"/>
      </g>
    </symbol>
    <symbol id="icon-person-small" data-name="icon-person-small" viewBox="0 0 24 24">
      <g id="icon-person-4" data-name="icon-person">
        <rect width="24" height="24" style="fill: none"/>
        <g>
          <path d="M20,21A8,8,0,0,0,4,21Z"/>
          <circle cx="12" cy="7" r="4"/>
        </g>
      </g>
    </symbol>
    <symbol id="icon-pin" data-name="icon-pin" viewBox="0 0 24 24">
      <polygon points="16.49 11.5 14.96 10.65 14.56 5.5 15.49 5.5 15.99 3.5 7.99 3.5 8.49 5.5 9.41 5.5 9.01 10.65 7.49 11.5 7.49 13 11.22 13 12.04 22.5 12.78 13 16.49 13 16.49 11.5"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-zoom-in" data-name="icon-zoom-in" viewBox="0 0 24 24">
      <g id="icon-zoom-in-3" data-name="icon-zoom-in">
        <rect width="24" height="24" style="fill: none"/>
        <path d="M15.28,12.87a6.83,6.83,0,1,0-1.41,1.41L21.59,22,23,20.59ZM13,9.5H10.5V12h-1V9.5H7v-1H9.5V6h1V8.5H13Z"/>
      </g>
    </symbol>
    <symbol id="icon-search" data-name="icon-search" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M22.87,21.13l-8-8a7.26,7.26,0,1,0-1.75,1.75l8,8ZM4.24,9A4.76,4.76,0,1,1,9,13.76,4.77,4.77,0,0,1,4.24,9Z"/>
    </symbol>
    <symbol id="icon-reblog" data-name="icon-reblog" viewBox="0 0 24 24">
      <g>
        <polygon points="20.49 11.4 20.49 6.4 9.31 6.4 9.31 8.4 18.49 8.4 18.49 11.4 16.28 11.4 19.49 14.6 22.69 11.4 20.49 11.4"/>
        <polygon points="5.51 12.6 7.72 12.6 4.51 9.4 1.31 12.6 3.51 12.6 3.51 17.6 14.69 17.6 14.69 15.6 5.51 15.6 5.51 12.6"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-facebook" data-name="icon-facebook" viewBox="0 0 24 24">
      <path d="M3.54,3.56H20.46V20.44H3.54ZM17.93,6.93H15.79a2.8,2.8,0,0,0-2.08.87,2.9,2.9,0,0,0-.85,2.1V12h-1.7v2.54h1.7v5.91h2.54V14.54h2.54V12H15.39V10.29a.83.83,0,0,1,.24-.57.78.78,0,0,1,.59-.26h1.71Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-twitter" data-name="icon-twitter" viewBox="0 0 24 24">
      <path d="M19.37,8.22V8.7a10.76,10.76,0,0,1-.72,3.85,11.78,11.78,0,0,1-2,3.45,9.86,9.86,0,0,1-3.43,2.54,10.87,10.87,0,0,1-4.63,1,10.59,10.59,0,0,1-5.82-1.69q.43,0,.91,0a7.56,7.56,0,0,0,4.74-1.61,3.84,3.84,0,0,1-2.24-.78,3.63,3.63,0,0,1-1.33-1.87,2.8,2.8,0,0,0,.69.09,2.91,2.91,0,0,0,1-.17,3.7,3.7,0,0,1-2.17-1.3,3.63,3.63,0,0,1-.87-2.39v0a4.11,4.11,0,0,0,1.74.48A3.67,3.67,0,0,1,3.5,7.09,3.65,3.65,0,0,1,4,5.18,10.92,10.92,0,0,0,7.48,8a10.72,10.72,0,0,0,4.37,1.17,4.43,4.43,0,0,1-.09-.87,3.78,3.78,0,0,1,3.78-3.78,3.71,3.71,0,0,1,2.78,1.17,8,8,0,0,0,2.43-.91,3.66,3.66,0,0,1-1.69,2.09,8,8,0,0,0,2.22-.57A8.17,8.17,0,0,1,19.37,8.22Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-pinterest" data-name="icon-pinterest" viewBox="0 0 24 24">
      <path d="M9,10.82a3.17,3.17,0,0,1,.59-1.94A1.74,1.74,0,0,1,11,8.07a1.28,1.28,0,0,1,1.08.46,1.81,1.81,0,0,1,.36,1.14,5.49,5.49,0,0,1-.13,1.05q-.17.59-.42,1.43t-.38,1.31a1.53,1.53,0,0,0,.32,1.46,1.72,1.72,0,0,0,1.37.61,2.78,2.78,0,0,0,2.43-1.67,8,8,0,0,0,1-4,4,4,0,0,0-1.16-3,4.47,4.47,0,0,0-3.27-1.14,5.09,5.09,0,0,0-3.82,1.5,5,5,0,0,0-1.46,3.61A3.25,3.25,0,0,0,7.59,13a.59.59,0,0,1,.17.59,5.66,5.66,0,0,0-.21.84.47.47,0,0,1-.23.27.49.49,0,0,1-.32,0,3.17,3.17,0,0,1-1.65-1.56,5.59,5.59,0,0,1-.55-2.53,6.11,6.11,0,0,1,.3-1.86A6.23,6.23,0,0,1,6,7,7.5,7.5,0,0,1,7.55,5.42,7,7,0,0,1,9.7,4.34a9.06,9.06,0,0,1,2.74-.4,7.49,7.49,0,0,1,2.76.51A6.39,6.39,0,0,1,17.36,5.8a6.15,6.15,0,0,1,1.35,2,5.73,5.73,0,0,1,.49,2.3,8.29,8.29,0,0,1-1.6,5.23,5,5,0,0,1-4.14,2.07A3.41,3.41,0,0,1,11.87,17a2.32,2.32,0,0,1-1-.93q-.63,2.45-.76,2.91a9.84,9.84,0,0,1-1.52,3H7.84a10.29,10.29,0,0,1,.08-3.54l1.39-5.82A4,4,0,0,1,9,10.82Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-email" data-name="icon-email" viewBox="0 0 24 24">
      <polygon points="3 10.35 3 19 21 19 21 10.35 12 15.19 3 10.35"/>
      <polygon points="3 5 3 8.35 12 13.19 21 8.35 21 5 3 5"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-download" data-name="icon-download" viewBox="0 0 24 24">
      <path d="M18.15,9.59a6.24,6.24,0,0,0-12-1A5,5,0,0,0,7,18.5H17.51a4.48,4.48,0,0,0,.63-8.91ZM12,16.25,7.82,12.07h2.5V8.75h3.36v3.32h2.5Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-upload" data-name="icon-upload" viewBox="0 0 24 24">
      <g id="icon-download-4" data-name="icon-download">
        <path d="M18.15,9.59a6.24,6.24,0,0,0-12-1A5,5,0,0,0,7,18.5H17.51a4.48,4.48,0,0,0,.63-8.91Zm-2,2.84h-2.5v3.32H10.32V12.43H7.82L12,8.25Z"/>
        <rect width="24" height="24" style="fill: none"/>
      </g>
    </symbol>
    <symbol id="icon-link" data-name="icon-link" viewBox="0 0 24 24">
      <path d="M5.39,15.49a3.1,3.1,0,0,0,3.07,3.07,2.94,2.94,0,0,0,2.2-.91l2.81-2.81,1.35,1.35L12,19a4.81,4.81,0,0,1-3.54,1.45A4.81,4.81,0,0,1,4.95,19a4.81,4.81,0,0,1-1.45-3.54,4.81,4.81,0,0,1,1.45-3.54L7.75,9.13l1.35,1.35L6.3,13.3A3,3,0,0,0,5.39,15.49Zm4.48,0L8.48,14.09l5.65-5.65,1.39,1.39ZM12,4.91a4.81,4.81,0,0,1,3.54-1.45,4.81,4.81,0,0,1,3.54,1.45,4.81,4.81,0,0,1,1.45,3.54A4.81,4.81,0,0,1,19.05,12l-2.81,2.81-1.35-1.35,2.81-2.81a3,3,0,0,0,.89-2.18,3,3,0,0,0-3.07-3.07,3,3,0,0,0-2.18.89L10.53,9.07,9.17,7.71Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-google-plus" data-name="icon-google-plus" viewBox="0 0 24 24">
      <path d="M8.62,10.9h5.65a6.83,6.83,0,0,1-.13,3.18,5.44,5.44,0,0,1-1.55,2.52,5.53,5.53,0,0,1-2.47,1.28,6.45,6.45,0,0,1-3.09,0,5.59,5.59,0,0,1-2.16-1.1,5.85,5.85,0,0,1-1.63-2,5.68,5.68,0,0,1-.53-3.93,4.56,4.56,0,0,1,.53-1.5,5.64,5.64,0,0,1,3.4-3,5.85,5.85,0,0,1,4.06,0,6.1,6.1,0,0,1,1.94,1.19,2,2,0,0,1-.31.33,1.29,1.29,0,0,0-.27.29,6.18,6.18,0,0,0-.55.51,7.65,7.65,0,0,0-.55.6,3.11,3.11,0,0,0-1.33-.8,3.28,3.28,0,0,0-1.77,0,3.52,3.52,0,0,0-1.81,1,4.3,4.3,0,0,0-.88,1.46,3.55,3.55,0,0,0,0,2.34A3.67,3.67,0,0,0,6.59,15a3.31,3.31,0,0,0,1.33.57,4,4,0,0,0,1.46,0,3.31,3.31,0,0,0,1.33-.53,2.57,2.57,0,0,0,1.19-1.86H8.62V10.9ZM21.43,11v1.5H19.36v2h-1.5v-2H15.78V11h2.08V9h1.5V11Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-delicious" data-name="icon-delicious" viewBox="0 0 24 24">
      <path d="M20.41,12H11.92v8.44H3.48V12h8.44V3.56h8.48Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-apple" data-name="icon-apple" viewBox="0 0 24 24">
      <path d="M19,9a2.72,2.72,0,0,0-1.48,1.37,4.41,4.41,0,0,0-.59,2.14,4.62,4.62,0,0,0,.68,2.43,3,3,0,0,0,1.82,1.42,9.11,9.11,0,0,1-1.12,2.35,6.83,6.83,0,0,1-1.59,1.8A2.92,2.92,0,0,1,15,21a4.49,4.49,0,0,1-1.57-.34,3,3,0,0,0-1.31-.38,6,6,0,0,0-1.69.51,4.72,4.72,0,0,1-1.1.21,3.34,3.34,0,0,1-2.07-.85,10.08,10.08,0,0,1-2.37-3.43,10.48,10.48,0,0,1-.85-4.15,6.4,6.4,0,0,1,1.29-4A3.94,3.94,0,0,1,8.63,6.86a4.48,4.48,0,0,1,2,.47,2.63,2.63,0,0,0,1.44.47,3.5,3.5,0,0,0,1.31-.25,7.65,7.65,0,0,1,2.2-.68,3.77,3.77,0,0,1,2.58,1A4,4,0,0,1,19,9ZM11.64,7A4.22,4.22,0,0,1,12.7,4.32a4.42,4.42,0,0,1,2.58-1.4,4.31,4.31,0,0,1-1,2.83A3.49,3.49,0,0,1,11.64,7Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-hexagon" data-name="icon-hexagon" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,21.3,3.95,16.65V7.35L12,2.7l8.05,4.65v9.3ZM5.95,15.5,12,19l6.05-3.5v-7L12,5,5.95,8.5Z"/>
    </symbol>
    <symbol id="icon-card" data-name="icon-card" viewBox="0 0 24 24">
      <g>
        <g>
          <rect x="3" y="5.5" width="18" height="2"/>
          <rect x="3" y="7.5" width="18" height="3.5" style="opacity: 0.66"/>
        </g>
        <g>
          <path d="M3,11v7H21V11Zm1,3H8v1H4Zm6,3H4V16h6Zm8.5,0a1.49,1.49,0,0,1-1-.39,1.5,1.5,0,1,1,0-2.22,1.5,1.5,0,1,1,1,2.61Z"/>
          <rect x="4" y="16" width="6" height="1" style="opacity: 0.66"/>
          <rect x="4" y="14" width="4" height="1" style="opacity: 0.66"/>
          <path d="M17,15.5a1.49,1.49,0,0,1,.5-1.11,1.5,1.5,0,1,0,0,2.22A1.49,1.49,0,0,1,17,15.5Z" style="opacity: 0.66"/>
          <path d="M18.5,14a1.49,1.49,0,0,0-1,.39,1.48,1.48,0,0,1,0,2.22,1.5,1.5,0,1,0,1-2.61Z" style="opacity: 0.66"/>
          <path d="M18,15.5a1.49,1.49,0,0,0-.5-1.11,1.48,1.48,0,0,0,0,2.22A1.49,1.49,0,0,0,18,15.5Z"/>
        </g>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-folder" data-name="icon-folder" viewBox="0 0 24 24">
      <polygon points="11 7 9 5 4 5 4 7.28 4 18 20 18 20 7 11 7"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-dna" data-name="icon-dna" viewBox="0 0 24 24">
      <path d="M11,12.17c-.64-3.7.69-7,4.17-10.52L16.6,3.06c-3,3-4.14,5.73-3.61,8.77Z"/>
      <path d="M20.94,7.4c-3,3-5.72,4.14-8.77,3.61-3.69-.64-7,.69-10.52,4.17L3.06,16.6c3-3,5.73-4.13,8.77-3.61a10.3,10.3,0,0,0,1.76.15c3,0,5.84-1.4,8.76-4.32Z"/>
      <polygon points="9.53 20.73 3.58 14.78 4.29 14.07 10.24 20.02 9.53 20.73"/>
      <polygon points="11.15 17.81 6.04 12.7 6.75 11.99 11.86 17.1 11.15 17.81"/>
      <polygon points="16.81 11.76 11.7 6.66 12.41 5.95 17.51 11.06 16.81 11.76"/>
      <polygon points="19.02 9.55 13.91 4.44 14.62 3.73 19.73 8.84 19.02 9.55"/>
      <path d="M11.66,14c-.18,0-.35,0-.53-.05-.09,2.33-1.28,4.57-3.73,7l1.41,1.41c2.79-2.79,4.15-5.41,4.3-8.22A11.35,11.35,0,0,1,11.66,14Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-multimedia" data-name="icon-multimedia" viewBox="0 0 24 24">
      <path d="M22,6,15,3.62V15.05a2.5,2.5,0,1,0,2,2.45V8h5Z"/>
      <g>
        <path d="M9.38,13.62l1.5-1.5,2.19,2.19a3.47,3.47,0,0,1,.93-.26V8H2v6l3.88-3.88Z"/>
        <polygon points="5.88 10.12 2 14 2 17 6 17 9.38 13.62 5.88 10.12" style="opacity: 0.77"/>
        <path d="M13.07,14.31l-2.19-2.19-1.5,1.5,2.11,2.11A3.5,3.5,0,0,1,13.07,14.31Z" style="opacity: 0.86"/>
        <path d="M9.38,13.62,6,17h5.05a3.46,3.46,0,0,1,.44-1.26Z"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-gallery" data-name="icon-gallery" viewBox="0 0 24 24">
      <g>
        <rect x="4" y="16.01" width="4" height="4"/>
        <rect x="10" y="16" width="4" height="4"/>
        <rect x="16" y="16" width="4" height="4"/>
        <rect x="4" y="10.01" width="4" height="4"/>
        <rect x="10" y="10" width="4" height="4"/>
        <rect x="16" y="10" width="4" height="4"/>
        <rect x="4" y="4.01" width="4" height="4"/>
        <rect x="10" y="4" width="4" height="4"/>
        <rect x="16" y="4" width="4" height="4"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-calendar" data-name="icon-calendar" viewBox="0 0 24 24">
      <g>
        <rect x="6" y="4" width="2" height="4" rx="0.97" ry="0.97"/>
        <rect x="16" y="4" width="2" height="4" rx="0.97" ry="0.97"/>
        <path d="M4,9V20H20V9Zm14,9H15V15h3Z"/>
        <path d="M18,6V7a1,1,0,0,1-1,1H17a1,1,0,0,1-1-1V6H8V7A1,1,0,0,1,7,8H7A1,1,0,0,1,6,7V6H4V9H20V6Z" style="opacity: 0.67"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-newspaper" data-name="icon-newspaper" viewBox="0 0 24 24">
      <g>
        <path d="M5,7V18H21V7Zm7,7H7V9h5Zm7-4H13V9h6Zm0,2H13V11h6Zm0,2H13V13h6Z"/>
        <rect x="7" y="9" width="5" height="5" style="opacity: 0.58"/>
        <polygon points="5 18 5 18 3 9 5 9 5 18" style="opacity: 0.58"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-capped-person" data-name="icon-capped-person" viewBox="0 0 24 24">
      <g id="icon-person-5" data-name="icon-person">
        <g>
          <path d="M12,12a9,9,0,0,0-9,9H21A9,9,0,0,0,12,12Z"/>
          <path d="M12,10a3.68,3.68,0,0,0,3.68-3.68v-1c0-.1,0-.21,0-.31H20V4H15.44A3.68,3.68,0,0,0,8.32,5.31v1A3.68,3.68,0,0,0,12,10Z"/>
        </g>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-filter" data-name="icon-filter" viewBox="0 0 24 24">
      <polygon points="5 6 5 7 11 13 11 21 13 19 13 13 19 7 19 6 5 6"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-chats" data-name="icon-chats" viewBox="0 0 24 24">
      <g id="icon-message-4" data-name="icon-message">
        <rect width="24" height="24" style="fill: none"/>
        <g>
          <polygon points="9 9 22 9 22 18.39 18.39 18.39 18.39 22 14.78 18.39 9 18.39 9 9"/>
          <polygon points="15 4 2 4 2 13.39 5.61 13.39 5.61 17 9.22 13.39 15 13.39 15 4" style="opacity: 0.66"/>
        </g>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-send" data-name="icon-send" viewBox="0 0 24 24">
      <polygon points="4 4 4 10 16 12 4 14 4 20 22 12 4 4"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-folder-person" data-name="icon-folder-person" viewBox="0 0 24 24">
      <g id="icon-folder-3" data-name="icon-folder">
        <rect width="24" height="24" style="fill: none"/>
        <path d="M11,7,9,5H4V19H20V7Zm1,2a1.53,1.53,0,1,1-1.53,1.53A1.53,1.53,0,0,1,12,9ZM8.26,17a3.74,3.74,0,0,1,7.47,0Z"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-palette" data-name="icon-palette" viewBox="0 0 24 24">
      <path d="M11.94,3a9.24,9.24,0,0,1,6.38,2.35A7.34,7.34,0,0,1,20.95,11a4.76,4.76,0,0,1-1.48,3.52A4.87,4.87,0,0,1,15.93,16H14.19a1.42,1.42,0,0,0-1.08.45,1.48,1.48,0,0,0-.42,1.06,1.44,1.44,0,0,0,.38,1,1.61,1.61,0,0,1,.38,1,1.52,1.52,0,0,1-1.5,1.5,8.69,8.69,0,0,1-6.38-2.63A8.69,8.69,0,0,1,2.93,12,8.69,8.69,0,0,1,5.56,5.63,8.69,8.69,0,0,1,11.94,3ZM5.37,11.57A1.39,1.39,0,0,0,6.43,12a1.47,1.47,0,0,0,1.08-.45,1.47,1.47,0,0,0,0-2.11A1.47,1.47,0,0,0,6.43,9a1.39,1.39,0,0,0-1.06.45,1.53,1.53,0,0,0,0,2.11Zm3-4A1.39,1.39,0,0,0,9.43,8,1.52,1.52,0,0,0,11,6.5a1.39,1.39,0,0,0-.45-1.06A1.51,1.51,0,0,0,9.43,5,1.44,1.44,0,0,0,8,6.5,1.51,1.51,0,0,0,8.38,7.58Zm5,0A1.47,1.47,0,0,0,14.45,8a1.39,1.39,0,0,0,1.06-.45,1.51,1.51,0,0,0,.42-1.08A1.44,1.44,0,0,0,14.45,5a1.51,1.51,0,0,0-1.08.42,1.39,1.39,0,0,0-.45,1.06A1.47,1.47,0,0,0,13.37,7.58Zm3,4a1.47,1.47,0,0,0,1.08.45,1.39,1.39,0,0,0,1.06-.45,1.53,1.53,0,0,0,0-2.11A1.39,1.39,0,0,0,17.45,9a1.47,1.47,0,0,0-1.08.45,1.47,1.47,0,0,0,0,2.11Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-group" data-name="icon-group" viewBox="0 0 24 24">
      <g id="icon-person-6" data-name="icon-person">
        <rect width="24" height="24" style="fill: none"/>
        <circle cx="12" cy="6.31" r="3.68"/>
        <circle cx="6" cy="16.31" r="3.68"/>
        <circle cx="18" cy="16.31" r="3.68"/>
      </g>
    </symbol>
    <symbol id="icon-instagram" data-name="icon-instagram" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2.94c-2.44,0-2.75,0-3.71.05a6.6,6.6,0,0,0-2.18.42A4.61,4.61,0,0,0,3.47,6a6.61,6.61,0,0,0-.42,2.18C3,9.19,3,9.5,3,11.94s0,2.75.05,3.71a6.61,6.61,0,0,0,.42,2.18A4.61,4.61,0,0,0,6.1,20.47a6.62,6.62,0,0,0,2.18.42c1,0,1.27.05,3.71.05s2.75,0,3.71-.05a6.62,6.62,0,0,0,2.18-.42,4.6,4.6,0,0,0,2.63-2.63,6.6,6.6,0,0,0,.42-2.18c0-1,.05-1.27.05-3.71s0-2.75-.05-3.71A6.59,6.59,0,0,0,20.53,6,4.6,4.6,0,0,0,17.9,3.41,6.6,6.6,0,0,0,15.71,3c-1,0-1.27-.05-3.71-.05m0,1.62c2.4,0,2.69,0,3.64.05a5,5,0,0,1,1.67.31A3,3,0,0,1,19,6.63a5,5,0,0,1,.31,1.67c0,.95.05,1.23.05,3.64s0,2.69-.05,3.64A5,5,0,0,1,19,17.25,3,3,0,0,1,17.31,19a5,5,0,0,1-1.67.31c-.95,0-1.23.05-3.64.05s-2.69,0-3.64-.05A5,5,0,0,1,6.69,19,3,3,0,0,1,5,17.25a5,5,0,0,1-.31-1.67c0-.95-.05-1.23-.05-3.64s0-2.69.05-3.64A5,5,0,0,1,5,6.63,3,3,0,0,1,6.69,4.92a5,5,0,0,1,1.67-.31c.95,0,1.23-.05,3.64-.05"/>
      <path d="M12,14.94a3,3,0,1,1,3-3,3,3,0,0,1-3,3m0-7.62a4.62,4.62,0,1,0,4.62,4.62A4.62,4.62,0,0,0,12,7.32"/>
      <path d="M17.88,7.14A1.08,1.08,0,1,1,16.8,6.06a1.08,1.08,0,0,1,1.08,1.08"/>
    </symbol>
    <symbol id="icon-help" data-name="icon-help" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm-.06,16.74a1.18,1.18,0,1,1,1.18-1.18A1.18,1.18,0,0,1,11.94,18.74Zm3.13-7A3.76,3.76,0,0,1,13,13.27L12.87,15H11V13.31a1.91,1.91,0,0,1,1.44-1.83,1.91,1.91,0,0,0,1.07-2.92,2,2,0,0,0-1.78-.83A3.09,3.09,0,0,0,9.26,9V7l0,0a3.75,3.75,0,0,1,3-1.09,3.77,3.77,0,0,1,2.8,5.86Z"/>
    </symbol>
    <symbol id="icon-location" data-name="icon-location" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M17.41,5.24A7.66,7.66,0,0,0,6.59,16.07L12,21.49l5.41-5.41A7.66,7.66,0,0,0,17.41,5.24Zm-5.41,8a2.55,2.55,0,1,1,2.55-2.55A2.55,2.55,0,0,1,12,13.21Z"/>
    </symbol>
    <symbol id="icon-globe" data-name="icon-globe" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <path d="M2,12a10,10,0,0,0,.6,3.4L4,14Z"/>
        <path d="M21,15l-1,1v2a9.94,9.94,0,0,0,1.73-3.71Z"/>
        <path d="M16,18l-1-1H14l-1-1V14l1-1,1-1,1,1h2.19L19,14V12H17l-1-1-1,1H14V10h2V8h1l2.55-2.55a10,10,0,0,0-5.72-3.28L12,4V2a10,10,0,0,0-2.27.27L8,4V7L9,6V5l2,2L8,10H7L6,12H5v2l3,3H8L6,19v1a9.94,9.94,0,0,0,10.11,1.12L16,21ZM15,6h2L15,8Z"/>
        <polygon points="15 8 17 6 15 6 15 8" style="opacity: 0.45"/>
        <path d="M16,8v2H14v2h1l1-1,1,1h2v2l-.81-1H16l-1-1-2,2v2l1,1h1l1,1v3l.11.11A10,10,0,0,0,20,18V16l1-1,.73-.73a9.91,9.91,0,0,0-2.18-8.82L17,8Z" style="opacity: 0.45"/>
        <path d="M8,17H8L6,15,5,14V12H6l1-2H8l3-3L9,5V6L8,7V4L9.73,2.27A10,10,0,0,0,2,12l2,2L2.6,15.4A10,10,0,0,0,6,20V19Z" style="opacity: 0.45"/>
        <path d="M13.83,2.17A10,10,0,0,0,12,2V4Z" style="opacity: 0.45"/>
      </g>
    </symbol>
    <symbol id="icon-google-play" data-name="icon-google-play" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M3.32,20.44V3.56a1.36,1.36,0,0,1,.84-1.35L14,12,4.16,21.79A1.41,1.41,0,0,1,3.32,20.44Zm3-17.72L17.08,8.88,14.8,11.16ZM17.08,15.12,6.36,21.28l8.44-8.44Zm3.31-4.29A1.42,1.42,0,0,1,21,12a1.46,1.46,0,0,1-.56,1.17l-2.29,1.31L15.64,12l2.52-2.47Z"/>
    </symbol>
    <symbol id="icon-off" data-name="icon-off" viewBox="0 0 24 24">
      <path d="M16.95,7.05a7,7,0,1,1-9.89,0L5.64,5.64a9,9,0,1,0,12.73,0Z"/>
      <rect x="11" y="2" width="2" height="10"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-flag" data-name="icon-flag" viewBox="0 0 24 24">
      <polygon points="21 9 6 2 6 21 8 21 8 15.07 21 9"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-mug" data-name="icon-mug" viewBox="0 0 24 24">
      <g>
        <path d="M19.34,4.06H3.06A1.06,1.06,0,0,0,2,5.3L3.93,16.81H15.62l.74-4.47,5-2a1.06,1.06,0,0,0,.67-1V6.72A2.66,2.66,0,0,0,19.34,4.06Zm.2,4.73L16.78,9.89l.62-3.71h1.42a1.06,1.06,0,0,1,1.06,1.06v1A.53.53,0,0,1,19.54,8.79Z"/>
        <g style="opacity: 0.77">
          <polygon points="3.93 16.81 4.46 20 15.09 20 15.62 16.81 3.93 16.81"/>
        </g>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-cursor" data-name="icon-cursor" viewBox="0 0 24 24">
      <polygon points="6 1.71 6 21.71 11.88 16 19.94 16 6 1.71"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-quaver" data-name="icon-quaver" viewBox="0 0 24 24">
      <path d="M21,6,9,2V14.55A4,4,0,0,0,7,14a4,4,0,1,0,4,4V9H21Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-file" data-name="icon-file" viewBox="0 0 24 24">
      <polygon points="14 8 14 3 14 3 5 3 5 21 19 21 19 8 14 8"/>
      <polygon points="19 8 14 3 14 8 19 8" style="opacity: 0.5"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-cart" data-name="icon-cart" viewBox="0 0 24 24">
      <polygon points="17.07 2 16.92 4 2.89 4 4.51 13.04 16.16 13.94 16.07 15 4.86 15 5.21 17 17.93 17 18.93 4 22 4 22 2 17.07 2"/>
      <circle cx="6" cy="20" r="2"/>
      <circle cx="18" cy="20" r="2"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-support-group" data-name="icon-support-group" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="10 11 10 20 17 20 20 23 20 20 22 20 22 11 10 11" style="opacity: 0.55"/>
      <path d="M14.14,4.05A7,7,0,0,0,3,12.33L2,16.19l3.85-1a7,7,0,0,0,8.28-11.1ZM9.69,9v3.3a1.44,1.44,0,1,1-2.88,0,.5.5,0,0,1,1,0,.44.44,0,1,0,.88,0V9H5.14A4,4,0,0,1,8.69,5V5a.5.5,0,1,1,1,0V5a4,4,0,0,1,3.55,4Z"/>
    </symbol>
    <symbol id="icon-info-centre" data-name="icon-info-centre" viewBox="0 0 24 24">
      <circle cx="9.44" cy="12.35" r="0.65"/>
      <rect width="24" height="24" style="fill: none"/>
      <polygon points="21.81 19.34 19.6 20 16.69 10.28 18.9 9.62 21.81 19.34"/>
      <polygon points="10 14 8 14 8 15 9 15 9 17 8 17 8 18 11 18 11 17 10 17 10 14"/>
      <path d="M13.47,20H6.14a4,4,0,0,1-.42-7.88,4,4,0,0,1,7.68-.76h.07a4.32,4.32,0,1,1,0,8.65Zm-7.3-6.9a3,3,0,1,0,0,5.9h7.33a3.32,3.32,0,1,0,0-6.65l-.26,0-.58.07L12.54,12a3,3,0,0,0-5.84.6v.51H6.17Z"/>
      <g>
        <path d="M12.69,9.35V5h-2V8.28A4.43,4.43,0,0,1,12.69,9.35Z" style="opacity: 0.54"/>
        <path d="M13.75,10.87a5,5,0,0,1,.94.16V4h-2V9.35A4.44,4.44,0,0,1,13.75,10.87Z"/>
        <path d="M16.69,12.12V5h-2v6A4.87,4.87,0,0,1,16.69,12.12Z" style="opacity: 0.54"/>
      </g>
    </symbol>
    <symbol id="icon-laptop" data-name="icon-laptop" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M20,6V16H4V6H20m0-1H4A1,1,0,0,0,3,6V17H21V6a1,1,0,0,0-1-1Z"/>
      <path d="M15,18v.5H9V18H1a2,2,0,0,0,2,2H21a2,2,0,0,0,2-2Z"/>
    </symbol>
    <symbol id="icon-kite" data-name="icon-kite" viewBox="0 0 24.04 24">
      <path d="M23.79,22.25a.25.25,0,0,1-.17-.07c-1.33-1.28-6.72-8-8-14a.25.25,0,0,1,.19-.3.25.25,0,0,1,.3.19c1.25,5.87,6.54,12.48,7.85,13.74a.25.25,0,0,1-.17.43Z"/>
      <path d="M7.79,16.75a.54.54,0,0,0-.54-.54.53.53,0,0,0-.33.12h0a7.17,7.17,0,0,1-5,2v1.08a7,7,0,0,0,4.64-2A7.53,7.53,0,0,1,.79,20.52V21.6c3.5,0,5.23-2.3,6.89-4.52h0A.53.53,0,0,0,7.79,16.75Z"/>
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <polygon points="21 3 10.73 3 15.87 8.13 21 3" style="opacity: 0.48"/>
        <polygon points="10.73 3 7.25 16.75 15.87 8.13 10.73 3" style="opacity: 0.7000000000000001"/>
        <polygon points="15.87 8.13 21 13.27 21 3 15.87 8.13" style="opacity: 0.7000000000000001"/>
        <polygon points="7.25 16.75 21 13.27 15.87 8.13 7.25 16.75"/>
      </g>
    </symbol>
    <symbol id="icon-mood-skills" data-name="icon-mood-skills" viewBox="0 0 24 24">
      <g>
        <path d="M21.5,11l-1.36.25a.76.76,0,0,0-.43.25H18.12A6.11,6.11,0,0,0,16.68,8l1.13-1.13a.75.75,0,0,0,.47-.12L19.43,6a1,1,0,0,0,.26-.26A1,1,0,0,0,18,4.57l-.79,1.14a.76.76,0,0,0-.13.48L16,7.32A6.11,6.11,0,0,0,12.5,5.88V4.28a.75.75,0,0,0,.25-.42L13,2.5a1,1,0,0,0,0-.37,1,1,0,1,0-2,.37l.25,1.36a.76.76,0,0,0,.25.43V5.88A6.11,6.11,0,0,0,8,7.32L6.89,6.19a.75.75,0,0,0-.12-.47L6,4.57a1,1,0,0,0-.26-.26A1,1,0,1,0,4.57,6l1.14.79a.76.76,0,0,0,.48.13L7.32,8A6.11,6.11,0,0,0,5.88,11.5H4.28a.75.75,0,0,0-.42-.25L2.5,11a1,1,0,0,0-.37,0,1,1,0,1,0,.37,2l1.36-.25a.76.76,0,0,0,.43-.25H5.88A6.11,6.11,0,0,0,7.32,16L6.19,17.11a.75.75,0,0,0-.47.12L4.57,18a1,1,0,0,0-.26.26A1,1,0,1,0,6,19.43l.79-1.14a.76.76,0,0,0,.13-.48L8,16.68a6.11,6.11,0,0,0,3.47,1.44v1.6a.75.75,0,0,0-.25.42L11,21.5a1,1,0,0,0,0,.37,1,1,0,1,0,2-.37l-.25-1.36a.76.76,0,0,0-.25-.43V18.12A6.11,6.11,0,0,0,16,16.68l1.13,1.13a.75.75,0,0,0,.12.47L18,19.43a1,1,0,0,0,.26.26A1,1,0,0,0,19.43,18l-1.14-.79a.76.76,0,0,0-.48-.13L16.68,16a6.11,6.11,0,0,0,1.44-3.47h1.6a.75.75,0,0,0,.42.25L21.5,13a1,1,0,0,0,.37,0,1,1,0,0,0-.37-2ZM12,17.15A5.15,5.15,0,1,1,17.15,12,5.15,5.15,0,0,1,12,17.15Z"/>
        <circle cx="9.5" cy="10.5" r="0.6"/>
        <circle cx="14.5" cy="10.5" r="0.6"/>
        <path d="M12,15a3,3,0,0,0,3-3H9A3,3,0,0,0,12,15Z"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-mood-graph" data-name="icon-mood-graph" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <path d="M8.38,10.46c-.55-.8-1.25-.69-1.76.34a3.78,3.78,0,0,1-.29.47H7.46C7.77,11.27,8.1,11,8.38,10.46Z"/>
        <path d="M5.72,10.36A2.22,2.22,0,0,1,7.6,8.89,1.83,1.83,0,0,1,9,9.6a1.83,1.83,0,0,1,1.36-.71,2.22,2.22,0,0,1,1.88,1.47l1.25,2.53,0,0,0,0a2.22,2.22,0,0,1,1.88-1.47h5.58A4.84,4.84,0,1,0,12.16,7.6a.17.17,0,0,1-.32,0A4.84,4.84,0,1,0,3,11.27H4.74C5.07,11.27,5.44,10.93,5.72,10.36Z"/>
        <path d="M14.39,13.33a5,5,0,0,1-.28.48,1.1,1.1,0,0,0,.72.32c.49,0,1-.45,1.39-1.25a3.78,3.78,0,0,1,.29-.47H15.37C15,12.41,14.68,12.76,14.39,13.33Z"/>
        <path d="M18.09,12.41c-.33,0-.7.34-1,.91a2.72,2.72,0,0,1-2.28,1.8h0a2.05,2.05,0,0,1-1.36-.56,2.05,2.05,0,0,1-1.36.56,2.72,2.72,0,0,1-2.28-1.8l-.92-1.86a1.87,1.87,0,0,1-1.45.8H3.73L12,20.54l8.12-8.12h-2Z"/>
        <path d="M11.3,10.8c-.51-1-1.21-1.14-1.77-.33l1.2,2.41c.39.79.9,1.25,1.39,1.25a1.1,1.1,0,0,0,.72-.32,5,5,0,0,1-.28-.48Z"/>
      </g>
    </symbol>
    <symbol id="icon-online-counselling" data-name="icon-online-counselling" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <g>
        <path d="M15,19.5H9V19H1a2,2,0,0,0,2,2H21a2,2,0,0,0,2-2H15Z"/>
        <path d="M12.59,9.16A2,2,0,0,0,12,10.57,2,2,0,1,0,8.59,12L12,15.43,15.41,12a2,2,0,1,0-2.83-2.83Z"/>
        <path d="M22,4.48a3.37,3.37,0,0,0-6.45-.81,3.4,3.4,0,0,0-2.66,1.91L12.65,6H4A1,1,0,0,0,3,7V18H21V10.4a3,3,0,0,0,1-5.93ZM20,17H4V7H20V17Zm1-7.6V7a1,1,0,0,0-1-1H13.75a2.3,2.3,0,0,1,2.51-1.32A2.38,2.38,0,0,1,21,5a2.36,2.36,0,0,1,0,.34h0A2,2,0,0,1,21,9.4Z"/>
      </g>
    </symbol>
    <symbol id="icon-cloud" data-name="icon-cloud" viewBox="0 0 24 24">
      <rect width="24" height="24" style="fill: none"/>
      <path d="M17.56,10.08h-.07a4.83,4.83,0,0,0,.07-.7,4.85,4.85,0,0,0-9.67-.62,4.83,4.83,0,0,0-.76-.08,4.87,4.87,0,0,0,0,9.73H17.56a4.17,4.17,0,0,0,0-8.34Z"/>
    </symbol>
    <symbol id="icon-feeling-great" data-name="icon-feeling-great" viewBox="0 0 24 24">
      <path d="M12,17.74a6.37,6.37,0,0,0,6.37-6.37H5.63A6.37,6.37,0,0,0,12,17.74Z"/>
      <path d="M12,18.17a6.94,6.94,0,0,0,7-6.85H5A6.94,6.94,0,0,0,12,18.17Z"/>
      <polygon points="5.9 9.48 5.36 8.63 6.88 7.66 5.36 6.68 5.9 5.84 8.73 7.66 5.9 9.48"/>
      <polygon points="18.43 9.48 15.6 7.66 18.43 5.84 18.98 6.68 17.45 7.66 18.98 8.63 18.43 9.48"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-feeling-good" data-name="icon-feeling-good" viewBox="0 0 24 24">
      <circle cx="6.65" cy="8.29" r="1.28"/>
      <circle cx="17.35" cy="8.29" r="1.28"/>
      <path d="M12,17.86a5.35,5.35,0,0,0,5.35-5.35H6.65A5.35,5.35,0,0,0,12,17.86Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-feeling-bad" data-name="icon-feeling-bad" viewBox="0 0 24 24">
      <circle cx="6.65" cy="13.21" r="1.04"/>
      <circle cx="17.35" cy="13.21" r="1.04"/>
      <path d="M14.19,18.49a5,5,0,0,0-4.37,0l-.51-.86a5.91,5.91,0,0,1,5.4,0Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-feeling-okay" data-name="icon-feeling-okay" viewBox="0 0 24 24">
      <circle cx="6.65" cy="9.73" r="1.16"/>
      <circle cx="17.35" cy="9.73" r="1.16"/>
      <path d="M12,17.77a4.82,4.82,0,0,1-3.27-1.24l.67-.74a4,4,0,0,0,5.19,0l.67.74A4.82,4.82,0,0,1,12,17.77Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-feeling-terrible" data-name="icon-feeling-terrible" viewBox="0 0 24 24">
      <circle cx="6.65" cy="13.78" r="1.04"/>
      <circle cx="17.35" cy="13.78" r="1.04"/>
      <path d="M4.13,13.43v-1A4.85,4.85,0,0,0,7.58,11l.71.71A5.84,5.84,0,0,1,4.13,13.43Z"/>
      <path d="M9.75,18.16l.37.93a4.85,4.85,0,0,1,3.73.06l.4-.92A5.84,5.84,0,0,0,9.75,18.16Z"/>
      <path d="M19.87,13.43a5.84,5.84,0,0,1-4.16-1.72l.71-.71a4.85,4.85,0,0,0,3.45,1.43Z"/>
      <rect width="24" height="24" style="fill: none"/>
      <path d="M17.35,15.95l-1,1.74a1.16,1.16,0,0,0,1,1.74h0a1.16,1.16,0,0,0,1-1.74Z"/>
    </symbol>
    <symbol id="icon-like" data-name="icon-like" viewBox="0 0 24 24">
      <path d="M13.32,6.6A4.49,4.49,0,0,0,12,9.78,4.5,4.5,0,1,0,4.32,13l7.61,7.75L19.68,13A4.5,4.5,0,0,0,13.32,6.6Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-lock" data-name="icon-lock" viewBox="0 0 24 24">
      <path d="M16,10V6A4,4,0,0,0,8,6v4H5V22H19V10Zm-3,8H11V14h2Zm-3-8V6a2,2,0,0,1,4,0v4Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-youtube" data-name="icon-youtube" viewBox="0 0 24 24">
      <path d="M11.72,10.5l2.7,0q1.85,0,2.79.09a4.67,4.67,0,0,1,1.22.14A2.05,2.05,0,0,1,20,12.66a20.15,20.15,0,0,1,.23,2.77q0,1.83-.14,4.08a3.5,3.5,0,0,1-.33,1.17,2.08,2.08,0,0,1-2,1.17q-4.83.14-7.13.14-.8,0-2.23,0l-1.9,0a3.3,3.3,0,0,1-1-.19,2,2,0,0,1-1.36-1.22A9.47,9.47,0,0,1,3.75,18q0-1.92.09-4.41a8.21,8.21,0,0,1,.23-1.45,2,2,0,0,1,2-1.55Q7.5,10.64,11.72,10.5Zm-3.14,3V12.47H5.11v1.08H6.24v6.33H7.41V13.55ZM6.85,2h1c.06,0,.12.06.19.19q.47,1.59.66,2.39s0,.08.09.14a12.53,12.53,0,0,1,.42-1.5q0-.14.14-.49l.14-.54A.28.28,0,0,1,9.71,2h1a.78.78,0,0,1,0,.19.14.14,0,0,0,0,.09q-.19.66-.61,2t-.61,2a1.53,1.53,0,0,0-.09.47V9.47H8.25q0-.14,0-.4c0-.17,0-.32,0-.45a1.54,1.54,0,0,1,0-.38,9.36,9.36,0,0,0-.61-3.66Q7.22,3.32,6.85,2Zm4.5,17.87V14.34h-1v3.89a.63.63,0,0,1-.28.56.34.34,0,0,1-.42.14c-.09,0-.14-.14-.14-.33V14.34h-1V19a2.8,2.8,0,0,0,0,.42.65.65,0,0,0,.94.52A2.39,2.39,0,0,0,10,19.6a1.12,1.12,0,0,0,.28-.28v.56ZM13.51,6.75v.52q0,.33,0,.61a4.74,4.74,0,0,1-.07.52,1.24,1.24,0,0,1-.47.87,1.49,1.49,0,0,1-1,.35A1.32,1.32,0,0,1,11,9.26a1.82,1.82,0,0,1-.52-.87,7.54,7.54,0,0,1,0-1q0-1.78,0-2.3A1.32,1.32,0,0,1,12,3.84a1.36,1.36,0,0,1,1.45,1.27.37.37,0,0,0,0,.12.39.39,0,0,1,0,.12q0,.24,0,.7T13.51,6.75Zm-1.92,0V8a1,1,0,0,0,0,.38.42.42,0,0,0,.75,0,1,1,0,0,0,0-.38V5.15A.37.37,0,0,0,12,4.83a.41.41,0,0,0-.42.33.65.65,0,0,0,0,.28c0,.13,0,.33,0,.61S11.58,6.54,11.58,6.7Zm1.64,12.76.61.42a.85.85,0,0,0,.82,0,.89.89,0,0,0,.49-.7,3.26,3.26,0,0,0,.09-.66V15.75a1.77,1.77,0,0,0-.14-.7,1,1,0,0,0-.56-.77,1,1,0,0,0-.94.21.86.86,0,0,0-.21.16l-.16.16V12.47h-1v7.41h1A1,1,0,0,1,13.23,19.46Zm.19-2.34V15.66q0-.42.52-.42t.38.33v3.14a.34.34,0,0,1-.38.33q-.52,0-.52-.42A12,12,0,0,1,13.41,17.11ZM15.34,3.93V8.3c0,.19.06.3.19.33a.36.36,0,0,0,.33-.09.76.76,0,0,0,.33-.66V3.93h1V9.51h-1V9l-.23.23a2.42,2.42,0,0,1-.52.33.7.7,0,0,1-.68,0,.8.8,0,0,1-.4-.54,1.37,1.37,0,0,1,0-.33V4A8.15,8.15,0,0,1,15.34,3.93ZM18.9,17.3c0-.12,0-.3,0-.52s0-.41,0-.59a2.54,2.54,0,0,0,0-.49,1.56,1.56,0,0,0-.42-1,1.51,1.51,0,0,0-1-.42,1.54,1.54,0,0,0-1.1.3,1.34,1.34,0,0,0-.49,1,17.58,17.58,0,0,0,0,3.14,1.39,1.39,0,0,0,.66,1.06,1.63,1.63,0,0,0,1.31.16,1.33,1.33,0,0,0,.91-.73A1.63,1.63,0,0,0,18.9,18h-1a3.13,3.13,0,0,1,0,.66q0,.42-.42.42a.38.38,0,0,1-.42-.38q0-.42-.09-1.41Q17.63,17.3,18.9,17.3Zm-1.13-.84h-.94c0-.12,0-.29,0-.49s0-.37,0-.49.17-.28.42-.28a.33.33,0,0,1,.38.28A5,5,0,0,1,17.77,16.46Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-phone" data-name="icon-phone" viewBox="0 0 24 24">
      <path d="M6.61,10.78a14.71,14.71,0,0,0,6.61,6.61l2.2-2.2a.86.86,0,0,1,1-.23,11.2,11.2,0,0,0,3.56.56,1,1,0,0,1,.68.28.92.92,0,0,1,.3.7V20a1,1,0,0,1-1,1,16.73,16.73,0,0,1-8.55-2.27,16.78,16.78,0,0,1-6.19-6.19A16.73,16.73,0,0,1,3,4a.94.94,0,0,1,.3-.68A.94.94,0,0,1,4,3H7.5a.92.92,0,0,1,.7.3A1,1,0,0,1,8.48,4,11.2,11.2,0,0,0,9,7.55a1,1,0,0,1-.23,1Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-edit" data-name="icon-edit" viewBox="0 0 24 24">
      <polygon points="20 20 10 20 12.5 18 20 18 20 20" style="opacity: 0.62"/>
      <g>
        <polygon points="13.79 6.79 4 16.57 4 20 7.43 20 17.21 10.21 13.79 6.79"/>
        <polygon points="20.71 6.71 17.29 3.29 13.79 6.79 17.21 10.21 20.71 6.71" style="opacity: 0.618"/>
      </g>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-legal" data-name="icon-legal" viewBox="0 0 24 24">
      <g>
        <path d="M16,11a4,4,0,1,0,4,4A4,4,0,0,0,16,11Zm0,6a2,2,0,1,1,2-2A2,2,0,0,1,16,17Z" style="opacity: 0.5700000000000001"/>
        <circle cx="16" cy="15" r="2"/>
      </g>
      <g>
        <path d="M18.42,18.17a4,4,0,0,1-2.11.8l2.11,3.66L19,21.45l1.38.06Z"/>
        <path d="M13.87,18.37l-1.81,3.14,1.38-.06L14,22.63,16.09,19H16A4,4,0,0,1,13.87,18.37Z"/>
      </g>
      <path d="M4,2V18h9.38A4,4,0,0,1,18,11.55V2Zm7,5H6V6h5Zm4-2H6V4h9Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-folded" data-name="icon-folded" viewBox="0 0 25 25">
      <g>
        <polygon points="21.5 9.79 5.62 9.79 4.56 4.5 20.44 4.5 21.5 9.79"/>
        <polygon points="19.38 15.09 3.5 15.09 5.62 9.79 21.5 9.79 19.38 15.09" style="opacity: 0.618"/>
        <polygon points="20.44 20.38 4.56 20.38 3.5 15.09 19.38 15.09 20.44 20.38"/>
      </g>
      <rect x="0.5" y="0.5" width="24" height="24" style="fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-opacity: 0"/>
    </symbol>
    <symbol id="icon-delete" data-name="icon-delete" viewBox="0 0 25 25">
      <path d="M19.27,4.85v2H5.56v-2H9l1-1h4.91l1,1ZM6.52,19.53V7.79H18.26V19.53a1.9,1.9,0,0,1-.57,1.4,1.85,1.85,0,0,1-1.35.57H8.49a2,2,0,0,1-2-2Z"/>
      <rect x="0.5" y="0.5" width="24" height="24" style="fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-opacity: 0"/>
    </symbol>
    <symbol id="icon-document" data-name="icon-document" viewBox="0 0 24 24">
      <path d="M5.06,2.5V22h14V2.5Zm1.5,2h4.5V5H6.56Zm0,2h8V7h-8Zm4,2.5h-4V8.5h4Zm5.5,11.5h-4V20h4Zm1.5-1h-5.5V19h5.5Zm0-1h-5.5V18h5.5Zm0-1h-5.5V17h5.5Z"/>
      <rect width="24" height="24" style="fill: none"/>
    </symbol>
    <symbol id="icon-graph" data-name="icon-graph" viewBox="0 0 24 24">
      <g id="icon-statistics-4" data-name="icon-statistics">
        <rect width="24" height="24" style="fill: none"/>
      </g>
      <polygon points="6 20 4 20 4 16 6 14 6 20"/>
      <polygon points="10.67 20 8.67 20 8.67 12 10.67 14 10.67 20"/>
      <polygon points="20 20 18 20 18 9 20 11 20 20"/>
      <polygon points="15.33 20 13.33 20 13.33 14 15.33 12 15.33 20"/>
      <polygon points="20 4 15 4 16.79 5.79 12 10.59 8.5 7.09 4 11.59 4 14.41 8.5 9.91 12 13.41 18.21 7.21 20 9 20 4" style="opacity: 0.618"/>
    </symbol>
    <symbol id="icon-star" data-name="icon-star" viewBox="0 0 25 25">
      <polygon points="12.47 18.39 6.28 21.44 7.26 14.61 2.45 9.65 9.25 8.48 12.47 2.37 15.7 8.48 22.5 9.65 17.69 14.61 18.67 21.44 12.47 18.39"/>
      <rect x="0.5" y="0.5" width="24" height="24" style="fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-opacity: 0"/>
    </symbol>
  </defs>
  <title>atrium-icon-sheet</title>
  <g id="ref-grid">
    <use width="24.25" height="24.25" transform="translate(23.88 23.88)" xlink:href="#icon-grid"/>
    <use id="icon-grid-5" data-name="icon-grid" width="24.25" height="24.25" transform="translate(71.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(71.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(23.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(167.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(119.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(263.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(215.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(359.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(311.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(455.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 23.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 71.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 119.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 167.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 215.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 263.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 311.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 359.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 407.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(503.88 455.88)" xlink:href="#icon-grid"/>
    <use width="24.25" height="24.25" transform="translate(407.88 455.88)" xlink:href="#icon-grid"/>
  </g>
  <g id="icons">
    <use width="24" height="24" transform="translate(120 24)" xlink:href="#icon-caret"/>
    <use width="24" height="24" transform="translate(168 24)" xlink:href="#icon-select"/>
    <use id="icon-notification-3" data-name="icon-notification" width="24" height="24" transform="translate(24 120)" xlink:href="#icon-notification"/>
    <use id="icon-arrow-3" data-name="icon-arrow" width="24" height="24" transform="translate(24 24)" xlink:href="#icon-arrow"/>
    <use width="24" height="24" transform="translate(72 24)" xlink:href="#icon-back"/>
    <use width="24" height="24" transform="translate(216 24)" xlink:href="#icon-circle-outlined"/>
    <use width="24" height="24" transform="translate(264 24)" xlink:href="#icon-check"/>
    <use id="icon-message-3" data-name="icon-message" width="24" height="24" transform="translate(72 120)" xlink:href="#icon-message"/>
    <use id="icon-wrench-3" data-name="icon-wrench" width="24" height="24" transform="translate(120 120)" xlink:href="#icon-wrench"/>
    <use width="24" height="24" transform="translate(360 24)" xlink:href="#icon-more"/>
    <use width="24" height="24" transform="translate(408 24)" xlink:href="#icon-zoom"/>
    <use id="icon-close-3" data-name="icon-close" width="24" height="24" transform="translate(312 24)" xlink:href="#icon-close"/>
    <use id="icon-menu-3" data-name="icon-menu" width="24" height="24" transform="translate(456 24)" xlink:href="#icon-menu"/>
    <use id="icon-info-3" data-name="icon-info" width="24" height="24" transform="translate(24 72)" xlink:href="#icon-info"/>
    <use id="icon-attention-3" data-name="icon-attention" width="24" height="24" transform="translate(72 72)" xlink:href="#icon-attention"/>
    <use width="24" height="24" transform="translate(120 72)" xlink:href="#icon-warning"/>
    <use id="icon-error-3" data-name="icon-error" width="24" height="24" transform="translate(168 72)" xlink:href="#icon-error"/>
    <use width="24" height="24" transform="translate(504 264)" xlink:href="#icon-add"/>
    <use id="icon-home-3" data-name="icon-home" width="24" height="24" transform="translate(216 120)" xlink:href="#icon-home"/>
    <use id="icon-statistics-3" data-name="icon-statistics" width="24" height="24" transform="translate(264 120)" xlink:href="#icon-statistics"/>
    <use id="icon-person-3" data-name="icon-person" width="24" height="24" transform="translate(168 120)" xlink:href="#icon-person"/>
    <use id="icon-person-small-3" data-name="icon-person-small" width="24" height="24" transform="translate(408 264)" xlink:href="#icon-person-small"/>
    <use id="icon-pin-3" data-name="icon-pin" width="24" height="24" transform="translate(24 168)" xlink:href="#icon-pin"/>
    <use width="24" height="24" transform="translate(360 264)" xlink:href="#icon-zoom-in"/>
    <use id="icon-search-3" data-name="icon-search" width="24" height="24" transform="translate(72 168)" xlink:href="#icon-search"/>
    <use width="24" height="24" transform="translate(120 168)" xlink:href="#icon-reblog"/>
    <use width="24" height="24" transform="translate(24 216)" xlink:href="#icon-facebook"/>
    <use width="24" height="24" transform="translate(72 216)" xlink:href="#icon-twitter"/>
    <use width="24" height="24" transform="translate(120 216)" xlink:href="#icon-pinterest"/>
    <use id="icon-email-3" data-name="icon-email" width="24" height="24" transform="translate(24 264)" xlink:href="#icon-email"/>
    <use id="icon-download-3" data-name="icon-download" width="24" height="24" transform="translate(72 264)" xlink:href="#icon-download"/>
    <use id="icon-upload-3" data-name="icon-upload" width="24" height="24" transform="translate(120 264)" xlink:href="#icon-upload"/>
    <use width="24" height="24" transform="translate(312 120)" xlink:href="#icon-link"/>
    <use width="24" height="24" transform="translate(168 216)" xlink:href="#icon-google-plus"/>
    <use id="icon-delicious-3" data-name="icon-delicious" width="24" height="24" transform="translate(216 216)" xlink:href="#icon-delicious"/>
    <use width="24" height="24" transform="translate(312 216)" xlink:href="#icon-apple"/>
    <use width="24" height="24" transform="translate(504 24)" xlink:href="#icon-hexagon"/>
    <use width="24" height="24" transform="translate(360 120)" xlink:href="#icon-card"/>
    <use width="24" height="24" transform="translate(408 120)" xlink:href="#icon-folder"/>
    <use width="24" height="24" transform="translate(456 120)" xlink:href="#icon-dna"/>
    <use id="icon-multimedia-3" data-name="icon-multimedia" width="24" height="24" transform="translate(168 168)" xlink:href="#icon-multimedia"/>
    <use width="24" height="24" transform="translate(264 168)" xlink:href="#icon-gallery"/>
    <use id="icon-calendar-3" data-name="icon-calendar" width="24" height="24" transform="translate(312 168)" xlink:href="#icon-calendar"/>
    <use width="24" height="24" transform="translate(360 216)" xlink:href="#icon-newspaper"/>
    <use width="24" height="24" transform="translate(360 168)" xlink:href="#icon-capped-person"/>
    <use width="24" height="24" transform="translate(408 216)" xlink:href="#icon-filter"/>
    <use width="24" height="24" transform="translate(408 168)" xlink:href="#icon-chats"/>
    <use width="24" height="24" transform="translate(456 216)" xlink:href="#icon-send"/>
    <use width="24" height="24" transform="translate(456 168)" xlink:href="#icon-folder-person"/>
    <use width="24" height="24" transform="translate(216 168)" xlink:href="#icon-palette"/>
    <use width="24" height="24" transform="translate(216 264)" xlink:href="#icon-group"/>
    <use width="24" height="24" transform="translate(264 216)" xlink:href="#icon-instagram"/>
    <use id="icon-help-3" data-name="icon-help" width="24" height="24" transform="translate(216 72)" xlink:href="#icon-help"/>
    <use width="24" height="24" transform="translate(264 264)" xlink:href="#icon-location"/>
    <use id="icon-globe-3" data-name="icon-globe" width="24" height="24" transform="translate(312 264)" xlink:href="#icon-globe"/>
    <use width="24" height="24" transform="translate(24 312)" xlink:href="#icon-google-play"/>
    <use width="24" height="24" transform="translate(168 264)" xlink:href="#icon-off"/>
    <use id="icon-flag-3" data-name="icon-flag" width="24" height="24" transform="translate(264 72)" xlink:href="#icon-flag"/>
    <use width="24" height="24" transform="translate(312 72)" xlink:href="#icon-mug"/>
    <use width="24" height="24" transform="translate(360 72)" xlink:href="#icon-cursor"/>
    <use width="24" height="24" transform="translate(408 72)" xlink:href="#icon-quaver"/>
    <use width="24" height="24" transform="translate(456 72)" xlink:href="#icon-file"/>
    <use id="icon-cart-3" data-name="icon-cart" width="24" height="24" transform="translate(72 312)" xlink:href="#icon-cart"/>
    <use width="24" height="24" transform="translate(456 312)" xlink:href="#icon-support-group"/>
    <use width="24" height="24" transform="translate(408 312)" xlink:href="#icon-info-centre"/>
    <use width="24" height="24" transform="translate(360 312)" xlink:href="#icon-laptop"/>
    <use id="icon-kite-3" data-name="icon-kite" width="24.04" height="24" transform="translate(120 312)" xlink:href="#icon-kite"/>
    <use id="icon-mood-skills-3" data-name="icon-mood-skills" width="24" height="24" transform="translate(168 312)" xlink:href="#icon-mood-skills"/>
    <use width="24" height="24" transform="translate(216 312)" xlink:href="#icon-mood-graph"/>
    <use id="icon-online-counselling-3" data-name="icon-online-counselling" width="24" height="24" transform="translate(264 312)" xlink:href="#icon-online-counselling"/>
    <use width="24" height="24" transform="translate(312 312)" xlink:href="#icon-cloud"/>
    <use id="icon-feeling-great-3" data-name="icon-feeling-great" width="24" height="24" transform="translate(24 360)" xlink:href="#icon-feeling-great"/>
    <use id="icon-feeling-good-3" data-name="icon-feeling-good" width="24" height="24" transform="translate(72 360)" xlink:href="#icon-feeling-good"/>
    <use id="icon-feeling-bad-3" data-name="icon-feeling-bad" width="24" height="24" transform="translate(168 360)" xlink:href="#icon-feeling-bad"/>
    <use id="icon-feeling-okay-3" data-name="icon-feeling-okay" width="24" height="24" transform="translate(120 360)" xlink:href="#icon-feeling-okay"/>
    <use id="icon-feeling-terrible-3" data-name="icon-feeling-terrible" width="24" height="24" transform="translate(216 360)" xlink:href="#icon-feeling-terrible"/>
    <use width="24" height="24" transform="translate(264 360)" xlink:href="#icon-like"/>
    <use id="icon-lock-3" data-name="icon-lock" width="24" height="24" transform="translate(312 360)" xlink:href="#icon-lock"/>
    <use id="icon-youtube-3" data-name="icon-youtube" width="24" height="24" transform="translate(456 264)" xlink:href="#icon-youtube"/>
    <use id="icon-phone-3" data-name="icon-phone" width="24" height="24" transform="translate(360 360)" xlink:href="#icon-phone"/>
    <use id="icon-edit-3" data-name="icon-edit" width="24" height="24" transform="translate(504 72)" xlink:href="#icon-edit"/>
    <use id="icon-legal-3" data-name="icon-legal" width="24" height="24" transform="translate(504 120)" xlink:href="#icon-legal"/>
    <use id="icon-folded-3" data-name="icon-folded" width="25" height="25" transform="translate(407.5 359.5)" xlink:href="#icon-folded"/>
    <use width="25" height="25" transform="translate(455.5 359.5)" xlink:href="#icon-delete"/>
  </g>
  <g id="notes">
    <text transform="translate(30.09 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">arrow</text>
    <text transform="translate(79.32 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">back</text>
    <text transform="translate(31.32 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">info</text>
    <text transform="translate(73.16 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">attention</text>
    <text transform="translate(123.63 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">warning</text>
    <text transform="translate(174.09 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">error</text>
    <text transform="translate(223.32 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">help</text>
    <text transform="translate(271.32 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">flag</text>
    <text transform="translate(319.55 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">mug</text>
    <text transform="translate(364.86 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">cursor</text>
    <text transform="translate(412.86 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">quaver</text>
    <text transform="translate(463.32 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">file</text>
    <text transform="translate(511.32 100.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">edit</text>
    <text transform="translate(21.47 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">notification</text>
    <text transform="translate(75.63 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">message</text>
    <text transform="translate(124.86 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">wrench</text>
    <text transform="translate(172.86 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">person</text>
    <text transform="translate(405.47 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">person-small</text>
    <text transform="translate(459.63 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">youtube</text>
    <text transform="translate(512.55 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">add</text>
    <text transform="translate(356.23 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">capped-person</text>
    <text transform="translate(414.09 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">chats</text>
    <text transform="translate(452.23 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">folder-person</text>
    <text transform="translate(506.2 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">document</text>
    <text transform="translate(32.3 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">pin</text>
    <text transform="translate(76.61 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">search</text>
    <text transform="translate(124.61 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">reblog</text>
    <text transform="translate(167.68 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">multimedia</text>
    <text transform="translate(219.32 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">palette</text>
    <text transform="translate(26.14 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">facebook</text>
    <text transform="translate(75.38 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">twitter</text>
    <text transform="translate(121.19 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">pinterest</text>
    <text transform="translate(29.84 293)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">email</text>
    <text transform="translate(22.45 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">google-play</text>
    <text transform="translate(19.98 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">feeling-great</text>
    <text transform="translate(69.22 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">feeling-good</text>
    <text transform="translate(117.22 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">feeling-okay</text>
    <text transform="translate(166.45 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">feeling-bad</text>
    <text transform="translate(208.22 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">feeling-terrible</text>
    <text transform="translate(271 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">like</text>
    <text transform="translate(319 390.14)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">lock</text>
    <text transform="translate(79.07 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">cart</text>
    <text transform="translate(127.07 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">kite</text>
    <text transform="translate(166.45 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">mood-skills</text>
    <text transform="translate(215.61 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">mood-graph</text>
    <text transform="translate(253.77 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">online-counselling</text>
    <text transform="translate(317.84 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">cloud</text>
    <text transform="translate(364.61 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">laptop</text>
    <text transform="translate(365.84 390)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">phone</text>
    <text transform="translate(412.61 390)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">folded</text>
    <text transform="translate(460.61 390)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">delete</text>
    <text transform="translate(406.45 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">info-centre</text>
    <text transform="translate(451.68 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">support-group</text>
    <text transform="translate(510.77 342)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">star</text>
    <text transform="translate(74.14 293)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">download</text>
    <text transform="translate(124.61 293)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">upload</text>
    <text transform="translate(319.35 149)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">link</text>
    <text transform="translate(176.58 293)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">off</text>
    <text transform="translate(166.7 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">google-plus</text>
    <text transform="translate(216.85 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">delicious</text>
    <text transform="translate(264.85 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">instagram</text>
    <text transform="translate(317.78 244.96)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">apple</text>
    <text transform="translate(223.07 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">home</text>
    <text transform="translate(263.68 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">statistics</text>
    <text transform="translate(363.38 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">zoom-in</text>
    <text transform="translate(314.14 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">calendar</text>
    <text transform="translate(360.91 244.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">newspaper</text>
    <text transform="translate(412.61 244.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">filter</text>
    <text transform="translate(463.07 244.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">send</text>
    <text transform="translate(221.84 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">group</text>
    <text transform="translate(266.39 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">location</text>
    <text transform="translate(318.09 292.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">globe</text>
    <text transform="translate(267.38 196.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">gallery</text>
    <text transform="translate(367.07 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">card</text>
    <text transform="translate(412.61 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">folder</text>
    <text transform="translate(464.16 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">dna</text>
    <text transform="translate(509.7 148.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">legal</text>
    <text transform="translate(126.09 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">caret</text>
    <text transform="translate(172.86 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">select</text>
    <text transform="translate(209.77 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">circle-outlined</text>
    <text transform="translate(270.09 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">check</text>
    <text transform="translate(414.32 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">zoom</text>
    <text transform="translate(463.32 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">menu</text>
    <text transform="translate(507.63 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">hexagon</text>
    <text transform="translate(367.32 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">more</text>
    <text transform="translate(318.09 52.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">close</text>
    <use width="24" height="24" transform="translate(504 168)" xlink:href="#icon-document"/>
    <text transform="translate(509.84 244.63)" style="font-size: 4px;font-family: OverpassMono-Regular, Overpass Mono">graph</text>
    <use id="icon-graph-3" data-name="icon-graph" width="24" height="24" transform="translate(504 216)" xlink:href="#icon-graph"/>
    <use id="icon-star-3" data-name="icon-star" width="25" height="25" transform="translate(503.5 311.5)" xlink:href="#icon-star"/>
  </g>
</svg>
