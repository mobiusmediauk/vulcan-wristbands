<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>{{ $pageTitle or 'Welcome' }} | {{ config('app.name', 'Laravel') }}</title>

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- TypeKit -->

<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/vue-multiselect@2.0.0"></script>

<script src="https://use.typekit.net/cjt2wtd.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<link href="https://fonts.googleapis.com/css?family=Poppins:500,700" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{ mix('atrium-assets/css/atrium.css') }}" />
<script type="text/javascript" src="{{ asset('atrium-assets/js/vendors/jquery.js') }}"></script>
