<div class="view-nav-sidebar-wrapper">
  <span class="view-nav-sidebar__overlay"></span>
  <button class="view-nav-item hamburger-button" id="site-nav-toggle">
  @component('atrium.modules.atrium-dotted-icon', ['icon' => 'menu'])
  @endcomponent
  </button>
  <nav id="view-nav" class="view-nav">
    <ul class="view-nav__list">
      {{-- <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'home'])
        @endcomponent
        <span class="view-nav__item__label">Home</span>
        </a>
      </li> --}}
      {{-- <hr /> --}}
      <li class="view-nav__li has-submenu">
        {{-- <a class="view-nav__item is-parent" title="" href="{{ url('admin/product') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'cube'])
        @endcomponent
        <span class="view-nav__item__label">Products</span>
        </a> --}}
        <ul class="view-nav__submenu">
          <li class="view-nav__submenu__li">
            <a class="view-nav__submenu__item" title="" href="{{ url('admin/product') }}">
            <span class="view-nav__submenu__item__label">All Products</span>
            </a>
          </li>
          @foreach (App\Product\Type::all() as $type)
          <li class="view-nav__submenu__li">
            <a class="view-nav__submenu__item" title="" href="{{ url('admin/product/type/' . $type->id) }}">
            <span class="view-nav__submenu__item__label">{{ str_plural($type->name, 2) }}</span>
            </a>
          </li>
          @endforeach
          {{-- <li class="view-nav__submenu__li">
            <a class="view-nav__submenu__item" title="" href="{{ url('admin/product/create') }}">
            <span class="view-nav__submenu__item__label">Create new</span>
            </a>
          </li> --}}
        </ul>
      </li>
      <hr />
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/order') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'euro'])
        @endcomponent
        <span class="view-nav__item__label">Orders</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/voucher') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'euro'])
        @endcomponent
        <span class="view-nav__item__label">Vouchers Codes</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/shipping') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'truck'])
        @endcomponent
        <span class="view-nav__item__label">Shipping Options</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ route('lead-conversion-types.index') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'truck'])
        @endcomponent
        <span class="view-nav__item__label">Lead Conversion Types</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/user') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Users</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/inspireme') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Inspire-Me Presets</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/mainpage') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Main Pages</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/page') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Pages</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/post') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Blogs</span>
        </a>
      </li>
      <li class="view-nav__li">
        <a class="view-nav__item" title="" href="{{ url('admin/video-post') }}">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'file'])
        @endcomponent
        <span class="view-nav__item__label">Vlogs</span>
        </a>
      </li>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ url('admin/social') }}">
          <span class="view-nav__submenu__item__label">Social Media</span>
        </a>
      </li>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ url('admin/enquiry') }}">
          <span class="view-nav__submenu__item__label">Enquiries</span>
        </a>
      </li>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ url('admin/mailing-list') }}">
          <span class="view-nav__submenu__item__label">Mailing List</span>
        </a>
      </li>
      <hr>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ route('statistics') }}">
          <span class="view-nav__submenu__item__label">Statistics</span>
        </a>
      </li>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ route('emailTemplates.index') }}">
          <span class="view-nav__submenu__item__label">Email templates</span>
        </a>
      </li>
      <li class="view-nav__submenu__li">
        <a class="view-nav__submenu__item" title="" href="{{ url('admin/settings') }}">
          <span class="view-nav__submenu__item__label">Settings</span>
        </a>
      </li>
      {{-- <li class="view-nav__li">
        <a class="view-nav__item" title="" href="">
        @component('atrium.modules.atrium-dotted-icon', ['icon' => 'cog-complex'])
        @endcomponent
        <span class="view-nav__item__label">Global Settings</span>
        </a>
      </li> --}}
    </ul>
  </nav>
</div>
