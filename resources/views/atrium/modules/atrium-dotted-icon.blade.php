<svg viewbox="0 0 42 42" width="42" height="42"
  @if (!empty($alert))
    @if ( $alert === true)
      class="has-new-entry @if ($count > 99)is-big-dot @endif @if ($count > 999)is-bigger-dot @endif"
    @endif
  @endif
>
  <use xlink:href="#icon-{{ $icon }}" width="24" height="24" x="9" y="9" viewbox="0 0 24 24"/>
  @if (!empty($count))
    <circle class="alert-dot" cx="30" cy="14" r="4" />
    @if ( $count > 999 ) {{ $count = '999+' }} @endif
    <text x="30" y="17" text-anchor="middle">{{ $count }}</text>
  @endif

</svg>

@if (!empty($title))
  <span class="icon-hidden-label">{{ $title }}</span>
@endif
