  {{-- @component('atrium.modules.notif-card', [
    'type' => 'neutral',
    'context' => 'action-success',
    'notifIcon' => 'person',
    'undoable' => true
  ])
    @slot('title')
      You have successfully deactivated the account <data data-name="user-id" class="u-font-mono" value="JE9290">inkjetink</data>.
    @endslot
    @slot('message')
      <p>You can reactivate this account by going to its detail page.</p>
    @endslot
    @slot('customActions')
      <button class="button--pill" type="button">View Account Details</button>
    @endslot
    positive alert attention neutral
  @endcomponent --}}
@if(session()->has('notification'))
  <div class="view-notif-card-stack" id="view-notif-card-stack">
    @component('atrium.modules.notif-card', [
      'type' => session()->get('type'),
      'context' => 'system'
    ])
    @slot('title')
      {{ session()->get('notification') }}
    @endslot
  @endcomponent
  </div>
@endif

@if($errors->any())
  <div class="view-notif-card-stack" id="view-notif-card-stack">
    @component('atrium.modules.notif-card', [
      'type' => 'alert',
      'context' => 'system'
    ])
    @slot('title')
      Required fields!
    @endslot
    @slot('message')
      @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
      @endforeach
    @endslot
  @endcomponent
  </div>
@endif
<footer class="view-footer">

  <div class="view-footer__content">
    <span class="view-footer__content__item is-copyright">
      <small>Powered by <a href="" title="" target="_blank">Atrium</a> CMS.</small>
      <small>&copy; 2017 <a href="http://www.viralbamboo.com/" title="Viralbamboo Digital Agency" target="_blank">Viralbamboo</a> Ltd.</small>
    </span>
    {{-- <span class="view-footer__content__item">
      <svg width="14" height="14" viewbox="0 0 14 14"><use xlink:href="#icon-person"/ ></svg>
      78 Online Users
    </span> --}}
    {{-- <a class="view-footer__controls__item" href="" title=""></a> --}}
  </div>

  @yield('footer')

  <div class="view-footer-roadsigns">
    {{-- <a class="view-footer-roadsign__item" href="{{ url('admin/module-sheet') }}" title="">
      <svg width="14" height="14" viewbox="0 0 14 14"><use xlink:href="#icon-wrench"/ ></svg>
      <span>Atrium Module Sheet</span>
    </a> --}}
    <a class="view-footer-roadsign__item is-primary" href="" title="">
      <svg width="14" height="14" viewbox="0 0 14 14"><use xlink:href="#icon-arrow"/ ></svg>
      <span>View Site</span>
    </a>
  </div>

</footer>


</div>

<script type="text/javascript" src="{{ asset('atrium-assets/js/vendors/gsap/uncompressed/TweenMax.js') }}"></script>
<script type="text/javascript" src="{{ asset('atrium-assets/js/vendors/ckeditor/ckeditor.js') }}"></script>

<script>
if (document.getElementById('ckeditor')) CKEDITOR.replace('ckeditor');
if (document.getElementById('ckeditor1')) CKEDITOR.replace('ckeditor1');
if (document.getElementById('ckeditor2')) CKEDITOR.replace('ckeditor2');
if (document.getElementById('ckeditor3')) CKEDITOR.replace('ckeditor3');
if (document.getElementById('ckeditor4')) CKEDITOR.replace('ckeditor4');
</script>

<script type="text/javascript" src="{{ asset('atrium-assets/js/dist/init.js') }}"></script>
<script type="text/javascript" src="{{ asset('atrium-assets/js/dist/ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('atrium-assets/js/dist/popups.js') }}"></script>
<script type="text/javascript" src="{{ asset('atrium-assets/js/dist/notif-cards.js') }}"></script>
<script type="text/javascript" src="{{ asset('atrium-assets/js/dist/index.js') }}"></script>

<script src="{{ mix('js/app.js') }}"></script>
