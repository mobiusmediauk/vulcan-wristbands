<section class="view-backstage"
  style="position: absolute; width: 0; height: 0; overflow: hidden; margin: 0; bottom: 0;">
  <svg viewBox="0 0 24 24">

    <?php
        $colorNames = array(
          'primary' ,
          'secondary' ,
          'tertiary',
          'quaternary',
          'accent' ,
          'background',
          'foreground',
          'light' ,
          'shadow' ,
          'median',
          'positive',
          'neutral',
          'attention',
          'alert',
          'text');
        foreach( $colorNames as &$color ) {
          echo '<linearGradient id="gradient--' . $color . '" gradientTransform="rotate(50)">
            <stop offset="0.4" />
            <stop offset="1" />
          </linearGradient>';
        }
        ?>

    <clipPath id="user-icon-circle" clipPathUnits="objectBoundingBox">
      <circle cx=".5" cy=".5" r=".5" />
    </clipPath>


    @include('atrium.theming.symbols')

  </svg>

  @include('atrium.assets.icons')

</section>
