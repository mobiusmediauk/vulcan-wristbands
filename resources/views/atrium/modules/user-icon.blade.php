@if (isset($size))
  @php $iconSize = $size @endphp
@else
  @php $iconSize = 42 @endphp
@endif

<svg class="user-icon" width="{{$iconSize}}" height="{{$iconSize}}" viewbox="0 0 {{$iconSize}} {{$iconSize}}">
  <title>{{ $userName }}'s Profile Icon</title>
  @if (isset($iconPath))
    <image class="user-icon__img"
           clip-path="url(#user-icon-circle)"
           x="0" y="0"
           xlink:href="{{ asset($iconPath) }}"
           alt="{{ $userName }}'s Profile Icon"
           width="{{$iconSize}}"
           height="{{$iconSize}}"/>
  @else
    <g clip-path="url(#user-icon-{{ $iconShape or 'circle' }})">
      <rect width="{{$iconSize}}" height="{{$iconSize}}" fill="#ddd"></rect>
      <rect width="{{$iconSize}}" height="{{$iconSize}}" fill="url(#gradient--primary)"></rect>
      <use xlink:href="#icon-person" x="{{ $iconSize * 0.19 }}" y="{{ $iconSize * 0.17 }}" width="{{ $iconSize * 0.62}}" height="{{ $iconSize * 0.62}}" fill="white" fill-opacity="0.618" ></use>
    </g>
  @endif
</svg>
