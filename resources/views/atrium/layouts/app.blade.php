<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" data-js="off">

<head>
  @include('atrium.modules.head')
  @include('atrium.theming.variables')
</head>

@php $siteNameStr = 'vulcan'; @endphp

<body
  data-theme="{{ $siteNameStr }}"
  {{-- data-theme="{{ $theme or 'atrium' }}" --}}
  data-area="{{ $area or 'general' }}"
  data-page="{{ Route::currentRouteName() }}"
  >

  @include('atrium.modules.view-backstage')

  <div id="view" class="view">

    @include('atrium.modules.view-header')

    <section class="view-body">

      <aside class="view-nav-sidebar" id="view-nav-sidebar">
        @include('atrium.modules.nav-sidebar')
      </aside>

      <main role="main" class="view-main"
        data-layout-type="{{ $layoutType or 'default' }}"
        >


        <div class="view-main__body"
          @if ( !empty($tabOrder) ) data-tab-order="{{ $tabOrder }}" @endif
          >
          @hasSection('panel-tab-group')
            <section class="panel-tab-group-section">
              @yield ('panel-tab-group')
            </section>
          @endif
          <div class="view-main__body__inner">
            @yield('content')
            @yield('view-main__body')
          </div>
        </div>

        @hasSection('widget-panel__content')
          <aside class="widget-panel" id="widget-panel">
            <span class="widget-panel__overlay"></span>
            <button class="widget-panel-toggle" id="widget-panel-toggle">
              <svg width="24" height="24" viewbox="0 0 24 24">
                <use xlink:href="#icon-more"></use>
              </svg>
            </button>
            <div id="widget-panel__content" class="widget-panel__content">
              @yield('widget-panel__content')
            </div>
          </aside>
        @endif

      </main>
        
        <script>
            console.log('Delete Confirmation js');
            function confirm_delete() {
                return confirm("WARNING\nAre you sure you want to delete this item?");
            }
        </script>


    </section>

    @include('atrium.modules.view-footer')

</body>

</html>
