@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Home</h1>
@endsection

@section('view-main__body')
  
  <header class="view-main__header">
    <p>Welcome to Vulcan.</p>
  </header>
  <div class="view-main__content">

  </div>
@endsection

{{-- @section('widget-panel__content')
  <section class="widget-panel__section" data-section-name="actions">
    <p class="widget-panel__section__title">Actions</p>
    <ul class="widget-panel__link-group">
      <li><a>Send bulk emails</a></li>
      <li><a>Export as Excel Spreadsheet</a></li>
    </ul>
  </section>
  <section class="widget-panel__section" data-section-name="options">
    <p class="widget-panel__section__title">Options</p>
    <form class="site-panel__options">
      <div class="input-group input--toggle">
        <input type="checkbox" class="input--toggle__checkbox" id="panel-option__show-contact-details"/>
        <label for="panel-option__show-contact-details">Show contact details</label>
      </div>
    </form>
  </section>
@endsection --}}
