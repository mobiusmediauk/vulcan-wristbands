@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Add new shipping option</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/shipping') }}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}

<section class="view-main__section">
  {{-- <header class="view-main__section__header">
    <h3 class="view-main__section__title">Product Settings</h3>
  </header> --}}

  <div class="flex-row">
    <div class="input--text flex-2-fr {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="ef-product-name">Name</label>
      <input name="name" id="ef-product-name" type="text" placeholder="Name" value="{{ old('name') }}" required/>
    </div>

{{-- TODO: ENABLE AFTER SHIPPING OPTIONS FIXED --}}
    <div class="input--select flex-2-fr">
      <label>Country</label>
      <select name="groupedBy">
        <option value="gb">UK only</option>
        <option value="usa">US only</option>
        <option value="eu">EU only</option>
        <option value="world">World wide</option>
        {{-- <option disabled>---</option>
        @foreach ($countries as $country)
          <option value="{{ $country->code }}">{{ $country->name }} ({{ $country->code }})</option>
        @endforeach --}}
      </select>
    </div>

    <div class="input--text flex-1-fr">
      <label for="ef-number">Flat Fee</label>
      <input name="flatFee" id="ef-number" type="number" placeholder="0.00" value="{{ old('flatFee') }}" min="0.00" step="0.01" required/>
    </div>

  </div>

  <div class="flex-row">
    <div class="input--textarea flex-5-fr {{ $errors->has('description') ? ' has-error' : ''}}">
      <label for="ckeditor">Description</label>
      <textarea name="description" id="ckeditor" rows="5">{!! old('description') !!}</textarea>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/shipping') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/product', $product) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this product</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
