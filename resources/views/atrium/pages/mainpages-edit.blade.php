@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit Main Page: {{ $mainpage->name }}</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/mainpage', $mainpage) }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}
@if($mainpage->id == 1)

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Top Box Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body1') ? ' has-error' : ''}}">
        <label for="ckeditor1">Content</label>
        <textarea name="body1" id="ckeditor1" rows="10">{!! old('body1') ? old('body1') : $mainpage->body1 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image1 }}">
    </div>
    <div class="input--file flex-2-fr">
      <label>Image #1</label>
      <input type="file" name="image1">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag1">Image #1 Alt Title</label>
      <input name="altTag1" id="ef-page-altTag1" type="text" placeholder="Alt Title" value="{{ (old('altTag1')) ? old('altTag1') : $mainpage->altTag1 }}"/>
    </div>

    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image2 }}">
    </div>
    <div class="input--file flex-2-fr">
      <label>Image #2</label>
      <input type="file" name="image2">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag2">Image #2 Alt Title</label>
      <input name="altTag2" id="ef-page-altTag2" type="text" placeholder="Alt Title" value="{{ (old('altTag2')) ? old('altTag2') : $mainpage->altTag2 }}"/>
    </div>
  </div>
</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Banner Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body2') ? ' has-error' : ''}}">
        <label for="ckeditor2">Content</label>
        <textarea name="body2" id="ckeditor2" rows="10">{!! old('body2') ? old('body2') : $mainpage->body2 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-4-fr">
      <img src="/images/{{ $mainpage->image3 }}">
    </div>
    <div class="input--file flex-2-fr">
      <label>Background Image</label>
      <input type="file" name="image3">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag3">Image Alt Title</label>
      <input name="altTag3" id="ef-page-altTag3" type="text" placeholder="Alt Title" value="{{ (old('altTag3')) ? old('altTag3') : $mainpage->altTag3 }}"/>
    </div>
  </div>

</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Bottom Box Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body3') ? ' has-error' : ''}}">
        <label for="ckeditor3">Content</label>
        <textarea name="body3" id="ckeditor3" rows="10">{!! old('body3') ? old('body3') : $mainpage->body3 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image4 }}">
    </div>
    <div class="input--file flex-2-fr">
      <label>Image #1</label>
      <input type="file" name="image4">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag4">Image #1 Alt Title</label>
      <input name="altTag4" id="ef-page-altTag4" type="text" placeholder="Alt Title" value="{{ (old('altTag4')) ? old('altTag4') : $mainpage->altTag4 }}"/>
    </div>
  </div>
</section>

@elseif($mainpage->id == 2)

  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Quote</h3>
    </header>
    <div class="flex-row">
      <div class="input--text flex-4-fr">
        <label for="ef-page-text1">Text</label>
        <input name="text1" id="ef-page-text1" type="text" placeholder="Quote" value="{{ (old('text1')) ? old('text1') : $mainpage->text1 }}"/>
      </div>

      <div class="input--text flex-2-fr">
        <label for="ef-page-text2">Author</label>
        <input name="text2" id="ef-page-text2" type="text" placeholder="Quote Author" value="{{ (old('text2')) ? old('text2') : $mainpage->text2 }}"/>
      </div>
    </div>
  </section>

  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">First Box Contents</h3>
    </header>
    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('body1') ? ' has-error' : ''}}">
          <label for="ckeditor1">Content</label>
          <textarea name="body1" id="ckeditor1" rows="10">{!! old('body1') ? old('body1') : $mainpage->body1 !!}</textarea>
        </div>
      </div>
    </div>
    <div class="flex-row">
      <div class="input--file flex-3-fr">
        <img src="/images/{{ $mainpage->image1 }}">
      </div>
      <div class="input--file flex-2-fr">
        <label>Image</label>
        <input type="file" name="image1">
      </div>
      <div class="input--text flex-1-fr">
        <label for="ef-page-altTag1">Image Alt Title</label>
        <input name="altTag1" id="ef-page-altTag1" type="text" placeholder="Alt Title" value="{{ (old('altTag1')) ? old('altTag1') : $mainpage->altTag1 }}"/>
      </div>
    </div>
  </section>

  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Second Box Contents</h3>
    </header>
    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('body2') ? ' has-error' : ''}}">
          <label for="ckeditor2">Content</label>
          <textarea name="body2" id="ckeditor2" rows="10">{!! old('body2') ? old('body2') : $mainpage->body2 !!}</textarea>
        </div>
      </div>
    </div>
    <div class="flex-row">
      <div class="input--file flex-3-fr">
        <img src="/images/{{ $mainpage->image2 }}">
      </div>
      <div class="input--file flex-2-fr">
        <label>Image</label>
        <input type="file" name="image2">
      </div>
      <div class="input--text flex-3-fr">
        <label for="ef-page-altTag2">Image Alt Title</label>
        <input name="altTag2" id="ef-page-altTag2" type="text" placeholder="Alt Title" value="{{ (old('altTag2')) ? old('altTag2') : $mainpage->altTag2 }}"/>
      </div>
    </div>
  </section>

  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Third Box Contents</h3>
    </header>
    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('body3') ? ' has-error' : ''}}">
          <label for="ckeditor3">Content</label>
          <textarea name="body3" id="ckeditor3" rows="10">{!! old('body3') ? old('body3') : $mainpage->body3 !!}</textarea>
        </div>
      </div>
    </div>
    {{-- <div class="flex-row">
      <div class="input--file flex-3-fr">
        <img src="/images/{{ $mainpage->image3 }}">
      </div>
      <div class="input--file flex-2-fr">
        <label>Image</label>
        <input type="file" name="image3">
      </div>
    </div> --}}
  </section>
@elseif($mainpage->id == 4)
  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Page Box Contents</h3>
    </header>

    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('body1') ? ' has-error' : ''}}">
          <label for="ckeditor1">Content</label>
          <textarea name="body1" id="ckeditor1" rows="10">{!! old('body1') ? old('body1') : $mainpage->body1 !!}</textarea>
        </div>
      </div>
    </div>

  </section>

  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Facebook Sharing</h3>
    </header>

    <div class="flex-row">
      <div class="input--text flex-2-fr">
        <label for="ef-fb-title">Title</label>
        <input name="body2" id="ef-fb-title" type="text" placeholder="Title" value="{{ (old('body2')) ? old('body2') : $mainpage->body2 }}" required/>
      </div>

      <div class="input--text flex-4-fr">
        <label for="ef-fb-description">Description</label>
        <input name="body3" id="ef-fb-description" type="text" placeholder="Description" value="{{ (old('body3')) ? old('body3') : $mainpage->body3 }}" required/>
      </div>
    </div>

  </section>
@elseif($mainpage->id == 5)

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Homepage Slider</h3>
    {{-- <h3 class="view-main__section__title">Homepage Slider (Transition speed: {{ $mainpage->slider->slideTransitionSpeed }})</h3> --}}
    <div class="input--text input--transition">
      <label for="slideTransitionSpeed">Set Transition speed for slider (in seconds)</label>
      <input type="number" name="slideTransitionSpeed" value="{{ (old('slideTransitionSpeed')) ? old('slideTransitionSpeed') : $mainpage->slider->slideTransitionSpeed }}">
    </div>
  </header>

  <script type="text/javascript" src="{{ asset('atrium-assets/js/vendors/ckeditor/ckeditor.js') }}"></script>
  <script>
  function openTab(event, slideNumber) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(slideNumber).style.display = "flex";
    event.currentTarget.className += " active";
  }

  </script>


  <div class="tab">
    @foreach ($mainpage->slider->items as $key => $item)
      @if($key < 1)
        <a id="defaultOpen" class="tablinks active" onclick="openTab(event, 'slide-{{ ($key + 1) }}')">Slide {{ ($key + 1) }}</a>
      @else
        <a class="tablinks" onclick="openTab(event, 'slide-{{ ($key + 1) }}')">Slide {{ ($key + 1) }}</a>
      @endif
    @endforeach
      <a class="tablinks" onclick="openTab(event, 'slide-{{ ($key + 2) }}')"><svg class="icon-add" width="20" height="20" viewbox="0 0 20 20"><use xlink:href="#icon-add"></use></svg></a>
  </div>
  @foreach ($mainpage->slider->items as $key => $item)
    @if($key < 1)
      <div id="slide-{{ ($key + 1)}}" class="flex-row flex-row-wrapper tabcontent" style="display: flex;">
    @else
      <div id="slide-{{ ($key + 1)}}" class="flex-row flex-row-wrapper tabcontent">
    @endif
        <h3 class="slides-heading">Slide {{ ($key + 1) }} - <a class="btn--std--primary" href="{{ route('slider.remove-slide', $item->id) }}">Remove Slide</a></h3>
        {{-- <a class="btn--std--primary" href="{{ route('slider.remove-slide', $item->id) }}">Remove Slide</a> --}}
        <div class="flex-row">

          <div class="input--textarea flex-3-fr">
            <label for="sliderEditor{{ $item->id }}">Slide {{ ($key + 1) }} Content</label>
            <textarea name="slide[{{ $item->id }}][body]" id="sliderEditor{{ $item->id }}" rows="10">{!! $item->body !!}</textarea>
          </div>

          <div class="slides-button-group">
            <div class="input--text flex-2-fr">
              <label for="slide-button-text">Button text</label>
              <input name="slide[{{ $item->id }}][buttonText]" id="slide-button-text" type="text" placeholder="Quote" value="{{ $item->buttonText }}"/>
            </div>

            <div class="input--text flex-2-fr">
              <label for="slide-button-link">Button link</label>
              <input name="slide[{{ $item->id }}][buttonLink]" id="slide-button-link" type="text" placeholder="Quote" value="{{ $item->buttonLink }}"/>
            </div>
          </div>

        </div>

        <div class="slides-image__row">
          <div class="input--file flex-1-fr slides-image__display">
            @if($item->getItemMedia())
              <img id="slide_image" height="256" width="auto" src="{{ asset($item->getItemMedia()->getUrl()) }}">
              <a class="slide-image__delete btn--std--primary" href="{{ route('slider.remove-slide-item', $item->id) }}">Remove Image</a>
            @endif
          </div>
          <div class="input--file flex-2-fr slides-image__choose">
            <label>Slide Image</label>
            <input type="file" name="slide[{{ $item->id }}][image]">
          </div>
        </div>

        <div class="background-image__row">

          @if($item->getBackgroundMedia())
            @if($item->getBackgroundMedia()->mime_type == 'video/mp4')
              <div class="flex-1-fr background-image__display background-video__display">
                <img height="256" width="auto" src="{{ asset('images/video-thumb.jpg') }}">
                <a class="slide-image__delete btn--std--primary" href="{{ route('slider.remove-slide-background', $item->id) }}">Remove Background</a>
              </div>
            @else
              <div class="input--file flex-1-fr background-image__display">
                <img height="256" width="auto" src="{{ asset($item->getBackgroundMedia()->getUrl()) }}">
                <a class="slide-image__delete btn--std--primary" href="{{ route('slider.remove-slide-background', $item->id) }}">Remove Image</a>
              </div>
            @endif
          @endif

          <div class="input--file flex-2-fr background-image__choose">
            <label>Slide Background Image</label>
            <input type="file" name="slide[{{ $item->id }}][background]">
          </div>
        </div>
      </div>
      <script type="text/javascript">
      if (document.getElementById('sliderEditor{{ $item->id }}')) CKEDITOR.replace('sliderEditor{{ $item->id }}');
      </script>
    @endforeach

  <div id="slide-{{ ($key + 2)}}" class="flex-row flex-row-wrapper flex-row-new tabcontent">
    <h2 class="view-main__section__title slides-heading">New Slide</h2>
    {{-- <h3>New slider</h3> --}}
    <div class="flex-row">

      <div class="input--textarea flex-3-fr">
        <label for="newSliderEditor">Slide Content</label>
        <textarea name="slide[new][body]" id="newSliderEditor" rows="10"></textarea>
      </div>

      <div class="input--text flex-2-fr">
        <label for="slide-button-text">Button text</label>
        <input name="slide[new][buttonText]" id="slide-button-text" type="text" placeholder="Quote" value=""/>
      </div>

      <div class="input--text flex-2-fr">
        <label for="slide-button-link">Button link</label>
        <input name="slide[new][buttonLink]" id="slide-button-link" type="text" placeholder="Quote" value=""/>
      </div>

    </div>

    <div class="flex-row">

      <div class="input--file flex-2-fr">
        <label>Slide Image</label>
        <input type="file" name="slide[new][image]">
      </div>

      <div class="input--file flex-2-fr">
        <label>Slide Background Image</label>
        <input type="file" name="slide[new][background]">
      </div>
    </div>
  </div>
  <script type="text/javascript">
  if (document.getElementById('newSliderEditor')) CKEDITOR.replace('newSliderEditor');
  </script>

</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">First Box Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body2') ? ' has-error' : ''}}">
        <label for="ckeditor2">Content</label>
        <textarea name="body2" id="ckeditor2" rows="10">{!! old('body2') ? old('body2') : $mainpage->body2 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image2 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #1</label>
      <input type="file" name="image2">
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-caption2">Image #1 Caption</label>
      <input name="caption2" id="ef-page-caption2" type="text" placeholder="Quote" value="{{ (old('caption2')) ? old('caption2') : $mainpage->caption2 }}"/>
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag2">Image #1 Alt Title</label>
      <input name="altTag2" id="ef-page-altTag2" type="text" placeholder="Alt Title" value="{{ (old('altTag2')) ? old('altTag2') : $mainpage->altTag2 }}"/>
    </div>
  </div>


  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image3 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #2</label>
      <input type="file" name="image3">
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-caption3">Image #2 Caption</label>
      <input name="caption3" id="ef-page-caption3" type="text" placeholder="Quote" value="{{ (old('caption3')) ? old('caption3') : $mainpage->caption3 }}"/>
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag3">Image #2 Alt Title</label>
      <input name="altTag3" id="ef-page-altTag3" type="text" placeholder="Alt Title" value="{{ (old('altTag3')) ? old('altTag3') : $mainpage->altTag3 }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image4 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #3</label>
      <input type="file" name="image4">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag4">Image #3 Alt Title</label>
      <input name="altTag4" id="ef-page-altTag4" type="text" placeholder="Alt Title" value="{{ (old('altTag4')) ? old('altTag4') : $mainpage->altTag4 }}"/>
    </div>
  </div>
</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Second Box Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body3') ? ' has-error' : ''}}">
        <label for="ckeditor3">Content</label>
        <textarea name="body3" id="ckeditor3" rows="10">{!! old('body3') ? old('body3') : $mainpage->body3 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image5 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #1</label>
      <input type="file" name="image5">
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-caption4">Image #1 Caption</label>
      <input name="caption4" id="ef-page-caption4" type="text" placeholder="Quote" value="{{ (old('caption4')) ? old('caption4') : $mainpage->caption4 }}"/>
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag5">Image #1 Alt Title</label>
      <input name="altTag5" id="ef-page-altTag5" type="text" placeholder="Alt Title" value="{{ (old('altTag5')) ? old('altTag5') : $mainpage->altTag5 }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image6 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #2</label>
      <input type="file" name="image6">
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-caption5">Image #2 Caption</label>
      <input name="caption5" id="ef-page-caption5" type="text" placeholder="Quote" value="{{ (old('caption5')) ? old('caption5') : $mainpage->caption5 }}"/>
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag6">Image #2 Alt Title</label>
      <input name="altTag6" id="ef-page-altTag6" type="text" placeholder="Alt Title" value="{{ (old('altTag6')) ? old('altTag6') : $mainpage->altTag6 }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-1-fr">
      <img src="/images/{{ $mainpage->image7 }}">
    </div>
    <div class="input--file flex-1-fr">
      <label>Image #3</label>
      <input type="file" name="image7">
    </div>
    <div class="input--text flex-1-fr">
      <label for="ef-page-altTag7">Image #3 Alt Title</label>
      <input name="altTag7" id="ef-page-altTag7" type="text" placeholder="Alt Title" value="{{ (old('altTag7')) ? old('altTag7') : $mainpage->altTag7 }}"/>
    </div>
  </div>
</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Third Box Contents</h3>
  </header>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('body4') ? ' has-error' : ''}}">
        <label for="ckeditor4">Content</label>
        <textarea name="body4" id="ckeditor4" rows="10">{!! old('body4') ? old('body4') : $mainpage->body4 !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-3-fr">
      <label for="ef-page-caption6">Youtube Video Link</label>
      <input name="caption6" id="ef-page-caption6" type="text" placeholder="Quote" value="{{ (old('caption6')) ? old('caption6') : $mainpage->caption6 }}"/>
    </div>
  </div>

</section>
@elseif ($mainpage->id == 6)
  {{-- There's no edit options for these two pages --}}
  {{-- Show only seo --}}
@elseif ($mainpage->id == 7)
  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Personaliser Banner</h3>
    </header>

    <div class="flex-row">
      <div class="input--file flex-3-fr">
        <img src="/images/{{ $mainpage->image1 }}">
      </div>
      <div class="input--file flex-1-fr">
        <label>Banner Image</label>
        <input type="file" name="image1">
      </div>
    </div>

    <div class="flex-row">
      <div class="input--text flex-2-fr">
        <label for="ef-page-caption1">Banner text</label>
        <input name="caption1" id="ef-page-caption1" type="text" placeholder="Banner Text" value="{{ (old('caption1')) ? old('caption1') : $mainpage->caption1 }}"/>
      </div>
    </div>

    <div class="flex-row">
      <div class="input--toggle">
        <input name="altTag7" type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle" @if($mainpage->altTag7) checked @endif/>
        <label class="input--toggle__label" for="toggle-second-toggle">Show banner</label>
      </div>
    </div>

  </section>
@else
  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Page Box Contents</h3>
    </header>

    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('body1') ? ' has-error' : ''}}">
          <label for="ckeditor1">Content</label>
          <textarea name="body1" id="ckeditor1" rows="10">{!! old('body1') ? old('body1') : $mainpage->body1 !!}</textarea>
        </div>
      </div>
    </div>

  </section>
@endif

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Seo Options</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr">
      <label for="ef-post-title">Title Tag</label>
      <input name="titleTag" id="ef-post-titletag" type="text" placeholder="Title Tag" value="{{ (old('titleTag')) ? old('titleTag') : $mainpage->titleTag }}" required/>
    </div>

    <div class="input--text flex-4-fr">
      <label for="ef-post-metadescription">Meta Description</label>
      <input name="metaDescription" id="ef-post-metadescription" type="text" placeholder="Meta Description" value="{{ (old('metaDescription')) ? old('metaDescription') : $mainpage->metaDescription }}" required/>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/mainpage') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/page', $mainpage) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this page</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
