@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Enquiry #{{ $enquiry->id }}</h1>
@endsection

@section('view-main__body')

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Topic: {{ $enquiry->topic }}</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-3-fr {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="ef-product-name">Name</label>
      <p>{{ $enquiry->firstName }} {{ $enquiry->lastName }}</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-number">Email</label>
      <p>{{ $enquiry->email }}</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-number">Order Number</label>
      <p>{{ $enquiry->orderNumber }}</p>
    </div>

  </div>

  <div class="flex-row">
    <div class="input--textarea flex-5-fr {{ $errors->has('description') ? ' has-error' : ''}}">
      <label for="ckeditor">User Message</label>
      <p>{{ $enquiry->message }}</p>
    </div>
  </div>

</section>

<div class="button-group">
  {{-- <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button> --}}
  <a href="{{ url('admin/enquiry') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>
@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/enquiry', $enquiry) }}" method="post" onsubmit="return confirm_delete();">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this enquiry</button></li>
      </form>
    </ul>
  </section>

@endsection
