@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Settings</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/settings', $settings) }}" method="POST">
  <input type="hidden" name="_method" value="PATCH">
  {{ csrf_field() }}

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Email settings</h3>
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr {{ $errors->has('ordersEmail') ? ' has-error' : ''}}">
      <label for="change-orders-email">Orders email</label>
      <input name="ordersEmail" id="change-orders-email" type="text" placeholder="Orders Email address" value="{{ old('ordersEmail') ? old('ordersEmail') : $settings->ordersEmail }}" required/>
    </div>


    <div class="input--text flex-2-fr {{ $errors->has('infoEmail') ? ' has-error' : ''}}">
      <label for="change-user-email">Info email</label>
      <input name="infoEmail" id="change-user-email" type="text" placeholder="Info Email address" value="{{ old('infoEmail') ? old('infoEmail') : $settings->infoEmail }}" required/>
    </div>

    <div class="input--text flex-2-fr {{ $errors->has('enquiryEmail') ? ' has-error' : ''}}">
      <label for="change-user-email">Enquiry email</label>
      <input name="enquiryEmail" id="enquiry-email" type="text" placeholder="Enquiry Email address" value="{{ old('enquiryEmail') ? old('enquiryEmail') : $settings->enquiryEmail }}"/>
    </div>

    <div class="input--text flex-2-fr {{ $errors->has('sendEnquiries') ? ' has-error' : ''}}">
      <label for="change-user-email">Send Enquiry email</label>
      <input name="sendEnquiries" id="send-enquiry-email" type="hidden"  value="0" /><input name="sendEnquiries" id="send-enquiry-email" type="checkbox" @if($settings->sendEnquiries == 1 ) checked @endif value="1" />
    </div>

  </div>

</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Google Analytics</h3>
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr {{ $errors->has('GAtrackingCode') ? ' has-error' : ''}}">
      <label for="change-GAtrackingCode">Tracking ID</label>
      <input name="GAtrackingCode" id="change-GAtrackingCode" type="text" placeholder="Tracking ID" value="{{ old('GAtrackingCode') ? old('GAtrackingCode') : $settings->GAtrackingCode }}"/>
    </div>

  </div>

</section>


<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/settings') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/page', $user) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this page</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
