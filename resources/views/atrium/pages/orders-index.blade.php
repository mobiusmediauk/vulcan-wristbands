@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Orders</h1>
@endsection

@section('view-main__body')

  <section class="view-main__content">
    <div class="table-container is-tr-clickable">
      {{-- <p><span class="u-big-inline-number">{{ $count }}</span> entries in total.</p> --}}
      <script type="text/x-template" id="grid-template">
        <table class="table--standard">
          <thead>
            <tr>
              <!-- <th class="th-permalink-arrow"></th> -->
              <th class="sortable-th" v-for="key in columns"
                @click="sortBy(key)"
                :class="sortOrders[key] > 0 ? 'is-asc' : 'is-dsc'"
                >
                <span class="sorter-toggle" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'">
                </span>
                @{{ key | tableHeadPrepper }}
              </th>
              <th>
                Actions
              </th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="entry in filteredData" @click="goToPermalink(entry)">

              <template v-for="key in columns">
                <td v-if="key == 'link'" class="td-permalink-arrow">
                  <a :href='entry[key]'><svg width="12" height="12" viewbox="0 0 12 12">
                    <use xlink:href="#icon-arrow" /></svg>
                  </a>
                </td>
                <td v-else-if="key == 'last_logged_in' && key.last_logged_in != 'N/A'">
                  @{{ entry.last_logged_in.created_at }}
                </td>
                <td v-else>@{{ entry[key] }}</td>
              </template>

              <td>
                <span class="item-actions">
                  <a :href="'order/' + entry.order_number" title="">Edit</a>
                  &nbsp;
                    <a :href="'invoice/' + entry.order_number" title="">Print</a>
                  &nbsp;
                  &nbsp;
                  &nbsp;
                  &nbsp;
                  &nbsp;
                  <form class="" :action="'order/' + entry.order_number" method="post" onsubmit="return confirm_delete();">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field() }}
                    <button type="submit" class="" href="">Delete</button>
                  </form>
                </span>
              </td>
            </tr>
          </tbody>
        </table>
      </script>

      <!-- table root element -->
      <div id="table" class="sortable-table">
        <form id="search" class="table-filter-form">
          <input class="table-filter-input" type="search" name="query" placeholder="Filter results..." v-model="searchQuery" id="table-filter-input">
          
        </form>
        <table-grid
          :data="gridData"
          :columns="gridColumns"
          :filter-key="searchQuery">
        </table-grid>
      </div>

      <script>
          // register the grid component
          Vue.component('table-grid', {
            template: '#grid-template',
            props: {
              data: Array,
              columns: Array,
              filterKey: String
            },
            data: function () {
              var sortOrders = {}
              this.columns.forEach(function (key) {
                sortOrders[key] = 1
              })
              return {
                sortKey: '',
                sortOrders: sortOrders
              }
            },
            computed: {
              filteredData: function () {
                var sortKey = this.sortKey
                var filterKey = this.filterKey && this.filterKey.toLowerCase()
                var order = this.sortOrders[sortKey] || 1
                var data = this.data
                if (filterKey) {
                  data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                      return String(row[key]).toLowerCase().indexOf(filterKey) > -1
                    })
                  })
                }
                if (sortKey) {
                  data = data.slice().sort(function (a, b) {
                    a = a[sortKey]
                    b = b[sortKey]
                    return (a === b ? 0 : a > b ? 1 : -1) * order
                  })
                }
                return data
              }
            },
            filters: {
              tableHeadPrepper: function (str) {
                return (str.charAt(0).toUpperCase() + str.slice(1)).replace(/_/g, ' ')
              }
            },
            methods: {
              sortBy: function (key) {
                this.sortKey = key
                this.sortOrders[key] = this.sortOrders[key] * -1
              },
              goToPermalink: function(entry) {
                if (entry.link !== undefined) {
                  window.location.href = entry.link;
                }
              }
            },
            created: function() {
            }
          })

          // bootstrap the table
          var table = new Vue({
            el: '#table',
            data: {
              searchQuery: '',
              gridColumns: {!! json_encode( $keys ) !!},
              gridData: {!! json_encode( $data ) !!}
            }
          })

        </script>
    </div>
  </section>

@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link" href="{{url('admin/order/create')}}">Create new product</a></li>
    </ul>
  </section> --}}

@endsection
