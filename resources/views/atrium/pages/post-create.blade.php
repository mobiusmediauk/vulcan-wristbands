@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">New Post</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/post') }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="type" value="1">
  {{ csrf_field() }}
<section class="view-main__section">

  <div class="flex-row">
    <div class="input--text flex-3-fr">
      <label for="ef-post-title">Post Title</label>
      <input name="title" id="ef-post-title" type="text" placeholder="Post Title" value="{{ old('title') }}" required/>
    </div>

    <div class="input--text flex-3-fr">
      <label for="ef-post-slug">Slug</label>
      <input name="slug" id="ef-post-slug" type="text" placeholder="Post Slug" value="{{ old('slug') }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-3-fr">
      <label for="ef-post-title">Title for Social media & SEO</label>
      <input name="titleTag" id="ef-post-titletag" type="text" placeholder="Title Tag" value="{{ old('titleTag') }}" required/>
    </div>

    <div class="input--text flex-3-fr">
      <label for="ef-post-metadescription">Meta Description (SEO)</label>
      <input name="metaDescription" id="ef-post-metadescription" type="text" placeholder="Meta Description" value="{{ old('metaDescription') }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-3-fr">
      <label for="ef-post-imagealttag">Image Alt Tag</label>
      <input name="altTag" id="ef-post-imagealttag" type="text" placeholder="Image Alt Tag" value="{{ old('altTag') }}" required/>
    </div>
    <div class="input--file flex-3-fr">
      <label for="ef-post-image">Main Image</label>
      <input id="ef-post-image" type="file" name="image" required>
    </div>
  </div>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr">
        <label for="ckeditor">Content</label>
        <textarea name="body" id="ckeditor" rows="10" required>{!! old('body') !!}</textarea>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--file flex-5-fr">
      <label for="ef-post-image">Blog Images</label>
      <input id="ef-post-image" type="file" name="images[]" multiple>
    </div>
  </div>

  <div class="blog-image-uploader-previews">
    <p>
      To use the bulk media uploader in the blog post simply copy and paste <code>@{!!$imageBundle!!}</code> at where you need the images to be.
      Also you can use each image as a single object via <code>@{{$images[0]}}</code>, where the number represents the image order. You can view the images after you post the article.
    </p>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Post</span>
  </button>
  <a href="/admin/post" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link" href="">Delete this page</a></li>
    </ul>
  </section> --}}

@endsection
