@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Website statistics</h1>
@endsection

@section('view-main__body')
  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Custom Strap image shares counter</h3>
    </header>

    <div class="flex-row">
        @foreach ($shareCounter as $item)

          <div class="">
            <h1 class="view-title">{{ $item->count }}</h1>
            @if($item->id == 4)
              <p>Shared via {{ $item->socialMedia }}</p>
            @elseif ($item->id == 5)
              <p>Image downloads</p>
            @else
              <p>Shared on {{ $item->socialMedia }}</p>
            @endif
          </div>

        @endforeach
    </div>

    @if($leadConversionStats->count() > 0)
      <hr>
      <header class="view-main__section__header">
        <h3 class="view-main__section__title">Lead Conversion statistics</h3>
      </header>

      <div class="flex-row">
          @foreach ($leadConversionStats as $item)

            <div class="">
              <h1 class="view-title">{{ $item->timesSelected }}</h1>
              <p>{{ $item->name }}</p>
            </div>

          @endforeach
      </div>
    </section>
  @endif

@endsection
