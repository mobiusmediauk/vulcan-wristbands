@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Order <data>#{{ $order->orderNumber }}</data></h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/order', $order) }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}

<section class="view-main__section">
  <header class="view-main__section__header">
    {{-- <h3 class="view-main__section__title">Overview</h3> --}}
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Customer Email</label>
      <p>{{ $order->email }}</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Customer Company</label>
      <p>No Company</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Order Total</label>
      <p>{{ $order->total }} ({{ $order->currency }})</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Order Number</label>
      <p>{{ $order->orderNumber }}</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Creation Date</label>
      <p>{{ $order->created_at }}</p>
    </div>

    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Last Updated</label>
      <p>{{ $order->updated_at }}</p>
    </div>
      
      
    <div class="input--text flex-2-fr">
      <label for="ef-product-name">Rolex</label>
        <?php if( isset($order->rolex) && $order->rolex != '' ) { ?>
        <p>{{ $order->rolex }}</p>
        <?php } else { ?>
            <p>N/A</p>
        <?php } ?>
    </div>



  </div>


  <div class="flex-row">

    <div class="input--select flex-2-fr">
      <label>Status</label>
      <select name="status">
        @foreach (['pending','dispatched','completed','cancelled'] as $status)
          <option value="{{ $status }}" @if($order->status == $status) selected @endif>{{ $status }}</option>
        @endforeach
      </select>
    </div>

    <div class="input--text flex-4-fr {{ $errors->has('trackingNumber') ? ' has-error' : ''}}">
      <label for="tracking-number">Tracking Number</label>
      <input id="tracking-number" name="trackingNumber" type="text" placeholder="Tracking Number" value="{{ old('trackingNumber') ? old('trackingNumber') : $order->trackingNumber }}"/>
    </div>

  </div>
</section>

<section class="view-main__section">
{{--
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Order Products</h3>
  </header> --}}

  <div class="flex-row">

    <div class="table-container u-fill-width">
      <table class="table--standard">
        <thead>
          <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($order->products as $product)
            <tr>
              <td>{{ $product->product->name }} ({{ $product->product->type->name }})</td>
              <td>{{ $product->qty }}</td>
              <td>{{ $product->product->getPriceByCurrency($order->currency) }} ({{ $order->currency }})</td>
              <td>{{ $product->product->price($product->qty) }} ({{ $order->currency }})</td>
            </tr>
          @empty
            <tr>
              <td>No Orders..</td>
            </tr>
          @endforelse
          <tr>
            <td>Subtotal</td>
            <td></td>
            <td></td>
            <td>{{ $order->subtotal }} ({{ $order->currency }})</td>
          </tr>
          <tr>
            <td>Shipping Price</td>
            <td></td>
            <td></td>
            <td>{{ $order->shippingPrice }} ({{ $order->currency }})</td>
          </tr>
        @if($order->discount != '0.00')
            <tr>
              <td><b>Discount</b></td>
              <td></td>
              <td></td>
              <td><b>- {{ $order->discount }} ({{ $order->currency }})</b></td>
            </tr>
            <tr>
              <td>Total</td>
              <td></td>
              <td></td>
              <td>{{ ($order->total <= $order->discount) ? 0 : ($order->total - $order->discount) }} ({{ $order->currency }})</td>
            </tr>
        @else
          <tr>
            <td>Total</td>
            <td></td>
            <td></td>
            <td>{{ $order->total }} ({{ $order->currency }})</td>
          </tr>
        @endif
        </tbody>
      </table>
    </div>

  </div>

</section>

@if($order->leadConversionTypes)
  <section class="view-main__section">
    <header class="view-main__section__header">
      <h3 class="view-main__section__title">Selected Lead Conversion options</h3>
    </header>

    <div class="flex-row">
      <div class="input--checkbox-group flex-3-fr">
        @foreach ($order->leadConversionTypes as $type)
            <li>{{ $type }}</li>
        @endforeach
      </div>
    </div>

  </section>
@endif

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Addresses</h3>
  </header>

  <div class="flex-row">
    @foreach ($order->addresses as $address)
      <div class="input--checkbox-group flex-3-fr">
        <h3 style="text-transform: capitalize">{{ $address->type }} Details</h3>
        <p><strong>{{ $address->name }}</strong></p>
        <p>{{ $address->address1 }}</p>
        <p>{{ $address->address2 }}</p>
        <p>{{ $address->city }}</p>
        <p>{{ $address->province }}</p>
        <p>{{ $address->postalCode }}</p>
        <p>{{ $address->countryCode }}</p>
        <p>{{ $address->mobile }}</p>
      </div>
    @endforeach
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/order') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

<p>&nbsp;</p>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/product', $order) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this order</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
