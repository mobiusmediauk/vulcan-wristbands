@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title"><data data-name="user-id" value="NSI291">#NSI291</data> Page Title Style</h1>
  <p class="view-title-breadcrumbs"><a href="/atrium/entry-list" title="">Accounts &amp; Groups</a> <a href="/atrium/entry-list" title="">All Users</a> <a href="" title="" class="is-current">User Profile (ID <data data-name="user-id" value="NSI291">NSI291</data>)</a></p>
@endsection


@section('panel-tab-group')
  <ul class="panel-tab-group">
    <li class="panel-tab"><a href="#">Tab 1</a></li>
    <li class="panel-tab"><a href="#">Tab Anotherism 2</a></li>
    <li class="panel-tab"><a href="#">Tab 3</a></li>
    <li class="panel-tab"><a href="#">Tab with a long name 4</a></li>
    <li class="panel-tab"><a href="#">Tab 5</a></li>
    <li class="panel-tab is-current"><a href="#">Tab with a long name 6</a></li>
    <li class="panel-tab"><a href="#">Tabtress 7</a></li>
    <li class="panel-tab"><a href="#">Tablet 8</a></li>
    <li class="panel-tab"><a href="#">Tab Infinite 9</a></li>
  </ul>
@endsection

@section('view-main__body')
  <header class="view-main__header">
  </header>

  <section class="view-main__content">
    <form class="view-main__content__editor-form form--standard is-flex-grid">

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Flex form layout, common inputs, selects and tag groups</h3>
        </header>

        <div class="flex-row">

          <div class="input--text is-disabled flex-2-fr is-num">
            <label for="ef-label-sample">Label Standard</label>
            <input id="ef-label-sample" type="text" placeholder="Placeholder" value="" disabled/>
          </div>
          <div class="input--text flex-2-fr">
            <label for="ef-text-1">Text Field</label>
            <input id="ef-text-1" type="text" placeholder="Placeholder" value="Text field with value"/>
          </div>
          <div class="input--text flex-2-fr">
            <label for="ef-number">Number</label>
            <input id="ef-number" type="number" placeholder="2" value="22" min="0" max="30"/>
          </div>
          <div class="input--select flex-2-fr">
            <label>Select</label>
            <select name="ef-title">
              <optgroup label="Optgroup is optional">
                <option value='Default'>Option Value</option>
                <option value='Mrs.'>Mrs.</option>
                <option value='Miss.'>Miss.</option>
                <option value='Other'>Other</option>
              </optgroup>
            </select>
          </div>
          <div class="input--text flex-2-fr">
            <label for="ef-month">Month</label>
            <input id="ef-month" type="month" value="2017-03"/>
          </div>
          {{-- DateTime commented out as it's deprecated --}}
          {{-- <div class="input--text flex-2-fr">
            <label for="ef-datetime">DateTime</label>
            <input id="ef-datetime" type="datetime" />
          </div> --}}
          <div class="input--text flex-2-fr">
            <label for="ef-date">Date</label>
            <input id="ef-date" type="date" value="#2017-05-09"/>
          </div>
          <div class="input--text flex-2-fr">
            <label for="ef-email">Email</label>
            <input id="ef-email" type="email" placeholder="type=email will be automatically validated on submit"/>
          </div>
          <div class="input--text flex-2-fr">
            <label for="ef-color">Color</label>
            <input id="ef-color" type="color" value="#9888ae"/>
          </div>

          <div class="input-group is-tag-list flex-4-fr">
            <span>Tags</span>
            <div class="tag-list-row">
              <div class="tag-list">
                <span class="tag-label is-editable" data-tag-id="138A">Restaurant</span>
                <span class="tag-label is-editable" data-tag-id="20A8">Hotel</span>
                <span class="tag-label is-editable" data-tag-id="E93D">Bar</span>
                <span class="tag-label is-editable" data-tag-id="CC20">Food &amp; Drink</span>
              </div>
              <button class="tag-list__add-button">Add tag</button>
            </div>
          </div>

          <div class="input--range flex-2-fr">
            <label>Range (Under construction)</label>
            <input type="range" name="ef-range" min="0" max="100" step="5" value="25"/>
          </div>


        </div>

      </section>

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Section Title</h3>
        </header>

        <div class="flex-row">

          <div class="input--radio-group flex-3-fr">
            <h3>Radio Group Title</h3>
            <p>Explanations to this radio group.</p>
            <div class="input--radio">
              <input class="input--radio__radio" type="radio" id="radio-checked" name="radio-" checked/>
              <label class="input--radio__label" for="radio-checked">Checked radio</label>
            </div>
            <div class="input--radio">
              <input class="input--radio__radio" type="radio" id="radio-last-month" name="radio-"/>
              <label class="input--radio__label" for="radio-last-month">Another Radio</label>
            </div>
            <div class="input--radio is-disabled">
              <input class="input--radio__radio" type="radio" id="radio-disabled" name="radio-" disabled/>
              <label class="input--radio__label" for="radio-disabled" >Disabled</label>
            </div>
          </div>
          <div class="input--checkbox-group flex-3-fr">
            <h3>Checkbox Group Title</h3>
            <p>Explanations to this checkbox group.</p>
            <div class="input--checkbox">
              <input class="input--checkbox__checkbox" type="checkbox" id="checkbox-checked" name="checkbox-" checked/>
              <label class="input--checkbox__label" for="checkbox-checked">Checked checkbox</label>
            </div>
            <div class="input--checkbox">
              <input class="input--checkbox__checkbox" type="checkbox" id="checkbox-last-month" name="checkbox-"/>
              <label class="input--checkbox__label" for="checkbox-last-month">Checkbox Sans Check</label>
            </div>
            <div class="input--checkbox is-disabled">
              <input class="input--checkbox__checkbox" type="checkbox" id="checkbox-disabled" name="checkbox-" disabled/>
              <label class="input--checkbox__label" for="radio-disabled" >Disabled</label>
            </div>
          </div>

        </div>

        <fieldset class="flex-6-fr">
          <legend>Toggle Group Title</legend>
          <p>Explanation to this toggle group</p>
          <div class="input--toggle-group">
            <div class="input--toggle">
              <input type="checkbox" class="input--toggle__checkbox" id="toggle-"/>
              <label class="input--toggle__label" for="toggle-show-contact-details">Show more details</label>
            </div>
            <div class="input--toggle">
              <input type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle" checked/>
              <label class="input--toggle__label" for="toggle-second-toggle">Toggle the second, checked</label>
            </div>
            <div class="input--toggle is-disabled">
              <input type="checkbox" class="input--toggle__checkbox" id="toggle-disabled-toggle" disabled/>
              <label class="input--toggle__label" for="toggle-disabled-toggle">A disabled option that cannot be clicked</label>
            </div>
          </div>
        </fieldset>

      </section>

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">File Uploaders: Under construction</h3>
        </header>

        <div class="flex-row">
          <div class="input--file">
            <label>Profile Icon</label>
            <input type="file" / name="ef-profile-icon">
          </div>
        </div>

      </section>

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Textareas</h3>
        </header>

        <div class="flex-row">
          <div class="input--textarea flex-6-fr">
            <label for="ef-textarea-sample">Textara Sample Label</label>
            <textarea id="ckeditor" rows="5"></textarea>
          </div>
        </div>

      </section>

      <section class="view-main__section u-reset-display">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Buttons</h3>
        </header>


        <div class="button-group">
          <button type="submit" class="btn--std--primary">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
            <span class="button-label">Save</span>
          </button>
          <button type="submit" class="btn--std--secondary">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
            <span class="button-label">Apply</span>
          </button>
          <a href="" class="btn--std">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
            <span class="button-label">Normal Button</span>
          </a>
          <a href="" class="btn--std--alert">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
            <span class="button-label">Delete</span>
          </a>
          <a href="" class="btn--std--primary is-disabled-button">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-message"></use></svg>
            <span class="button-label">Disabled</span>
          </a>
        </div>

        <div class="button-group--pill">
          <button type="submit" class="btn--pill--primary">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-dna"></use></svg>
            <span class="button-label">DNA</span>
          </button>
          <button type="submit" class="btn--pill--secondary">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-multimedia"></use></svg>
            <span class="button-label">Multimedia</span>
          </button>
          <a href="" class="btn--pill">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-hexagon"></use></svg>
            <span class="button-label">Hexagon</span>
          </a>
          <a href="" class="btn--pill--alert">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-location"></use></svg>
            <span class="button-label">Location</span>
          </a>
          <a href="" class="btn--pill--primary is-disabled-button">
            <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-palette"></use></svg>
            <span class="button-label">Disabled</span>
          </a>
        </div>
      </section>

    </form>

    <section class="view-main__section">

      <header class="view-main__section__header">
        <h3 class="view-main__section__title">Sample Table</h3>
      </header>

      <div class="table-container">
        {{-- "table-container" wrapper is necessary for the table to be scrollable horizontally --}}
        <table class="table--standard">
          <thead>
            <tr>
              <th class="sortable-th"><span class="sorter-toggle"></span>Sortable Table Heading Cell</th>
              <th class="sortable-th is-dsc"><span class="sorter-toggle"></span>Descending</th>
              <th class="sortable-th is-asc"><span class="sorter-toggle"></span>Ascending</th>
              <th>Normal Heading</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="is-multirow" rowspan="2">Multirow table cell</td>
              <td class="is-num">Numbers Monospace</td>
              <td>Content Normal</td>
              <td>
              <span class="item-actions">
                <a href="#" title="">More</a>
                <a href="#" title="">Options</a>
              </span>
              </td>
            </tr>
            <tr>
              <td class="is-num">£88.20</td>
              <td>Content Normal</td>
              <td>
              <span class="item-actions">
                <a href="#" title="">More</a>
                <a href="#" title="">Options</a>
              </span>
              </td>
            </tr>
            <tr class="is-alert-row">
              <td>Content</td>
              <td class="is-num">12345</td>
              <td>Content Normal</td>
              <td>
              <span class="item-actions">
                <a href="#" title="">More</a>
                <a href="#" title="">Options</a>
                <a class="alert-link">
                  <span class="alert-link__label">Unflag record</span>
                  <svg class="btn-flag-icon" width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-flag"></use></svg>
                </a>
              </span>
              </td>
            </tr>
            <tr class="is-highlight-row">
              <td>Content</td>
              <td class="is-num">12345</td>
              <td>Content Normal</td>
              <td>
              <span class="item-actions">
                <a href="#" title="">More</a>
                <a href="#" title="">Options</a>
              </span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>

  </section>
@endsection

@section('widget-panel__content')

  @component('atrium.modules.search-bar', ['target' => 'account'])@endcomponent

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Account Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link">Deactivate Account</a></li>
      <li><a class="widget-panel__link is-alert-link">Delete Account</a></li>
    </ul>
  </section>
@endsection
