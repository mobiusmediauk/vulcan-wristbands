@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">All {{ $typeName }}</h1>
@endsection

@section('view-main__body')

  <section class="view-main__content" id="app">
    <reorderable-index :data='{!! $data !!}' :columns='{!! json_encode( $keys ) !!}'></reorderable-index>
  </section>

@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link" href="{{url('admin/product/create')}}">Create new product</a></li>
    </ul>
  </section>

@endsection
