@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit Inspire-Me Preset</h1>
@endsection

@section('view-main__body')

<section class="view-main__section" id="app">

  <strap-customiser :source-data='{!! $data !!}' :preset='{!! $preset !!}' custom-text="" mode="admin"></strap-customiser>

</section>

<style>
    .details:not(.open) summary~* { display: none; }
</style>

@endsection
