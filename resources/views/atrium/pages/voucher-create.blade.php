@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Voucher Creation</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/voucher') }}" method="POST" enctype="multipart/form-data">
  {{ csrf_field() }}

<section class="view-main__section">

  <div class="flex-row">
    <div class="input--text flex-6-fr {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="ef-voucher-name">Name</label>
      <input name="name" id="ef-voucher-name" type="text" placeholder="Voucher Name" value="{{ old('name') }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--select flex-1-fr">
      <label>Voucher type</label>
      <select name="type" id="voucherType">
        <option value="percent">Cart Percentage</option>
        <option value="fixed">Fixed Amount</option>
      </select>
    </div>

    <div class="input--text flex-2-fr {{ $errors->has('amount') ? ' has-error' : ''}}">
      <label for="ef-voucher-amount" id="voucherTypeTitle">Discount Percentage (%)</label>
      <input name="amount" id="ef-voucher-amount" type="number" placeholder="" value="{{ old('amount') }}" step="0.01"/>
    </div>

    <div class="input--select flex-1-fr">
      <label>Currency</label>
      <select name="currency" id="productTypeSelector">
        <option value="all">For all Currencies</option>
        @foreach(app('currency')->options() as $option)
          <option value='{{ $option->value }}'>{{ $option->label }}</option>
        @endforeach
      </select>
    </div>

    <div class="input--text flex-2-fr {{ $errors->has('quantity') ? ' has-error' : ''}}">
      <label for="ef-voucher-quantity">Quantity (integer)</label>
      <input name="quantity" id="ef-voucher-quantity" type="text" placeholder="Leave blank for unlimited" value="{{ old('quantity') }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-2-fr {{ $errors->has('expiryDate') ? ' has-error' : ''}}">
      <label for="ef-expiry-date">Expiry Date</label>
      <input name="expiryDate" id="ef-expiry-date" type="date" value="{{ old('expiryDate') }}"/>
    </div>
  </div>

  <script>
    function checkVoucherType(type) {
      if (type == 'percent') {
        jQuery('#voucherTypeTitle').text('Discount Percentage (%)');
      }
      if (type == 'fixed') {
        jQuery('#voucherTypeTitle').text('Discount Amount');
      }
    }
    jQuery('#voucherType').on('change', function(event) {
      checkVoucherType(event.target.value)
    })
    checkVoucherType(jQuery('#voucherType').value);
  </script>
</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/voucher') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>

  <br/>
  <br/>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/product', $product) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this product</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
