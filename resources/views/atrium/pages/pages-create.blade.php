@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Create a new Page</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/page') }}" method="POST">
  {{ csrf_field() }}
<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Page Contents</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr">
      <label for="ef-page-title">Page Title</label>
      <input name="title" id="ef-page-title" type="text" placeholder="Page Title" value="{{ old('title') }}" required/>
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-slug">Slug</label>
      <input name="slug" id="ef-page-slug" type="text" placeholder="Page Slug" value="{{ old('slug') }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-4-fr">
      <label for="ef-page-header">Header</label>
      <input name="header" id="ef-page-header" type="text" placeholder="Page Header" value="{{ old('header') }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr">
        <label for="ckeditor">Content</label>
        <textarea name="body" id="ckeditor" rows="10">{!! old('body') !!}</textarea>
      </div>
    </div>
  </div>

</section>

{{-- <section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Header Image</h3>
  </header>

  <div class="flex-row">
    <div class="input--file">
      <input type="file" / name="image">
    </div>
  </div>

</section> --}}

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Seo And Other Page Options</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr">
      <label for="ef-post-title">Title Tag</label>
      <input name="titleTag" id="ef-post-titletag" type="text" placeholder="Title Tag" value="{{ old('titleTag') }}" required/>
    </div>

    <div class="input--text flex-4-fr">
      <label for="ef-post-metadescription">Meta Description</label>
      <input name="metaDescription" id="ef-post-metadescription" type="text" placeholder="Meta Description" value="{{ old('metaDescription') }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-3-fr">
      <label for="ef-link-name">Link Name</label>
      <input name="linkName" id="ef-link-name" type="text" placeholder="Link Name" value="{{ old('linkName') }}"/>
    </div>

    <div class="input--toggle">
      <input name="position" type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle"/>
      <label class="input--toggle__label" for="toggle-second-toggle">Show page in footer</label>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Create</span>
  </button>
  <a href="" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link" href="">Delete this page</a></li>
    </ul>
  </section> --}}

@endsection
