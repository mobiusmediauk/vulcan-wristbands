@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Create a new lead conversion type</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ route('lead-conversion-types.update', $type) }}" method="POST">
  {{ csrf_field() }}
  <input type="hidden" name="_method" value="PUT">
<section class="view-main__section">
  {{-- <header class="view-main__section__header">
    <h3 class="view-main__section__title">Page Contents</h3>
  </header> --}}

  <div class="flex-row">
    <div class="input--text flex-2-fr">
      <label for="ef-lead-name">Name</label>
      <input name="name" id="ef-lead-name" type="text" placeholder="Page Title" value="{{ (old('name')) ? old('name') : $type->name }}" required/>
    </div>
    {{-- <div class="input--text flex-2-fr">
      <label for="ef-page-slug">Slug</label>
      <input name="slug" id="ef-page-slug" type="text" placeholder="Page Slug" value="{{ old('slug') }}" required/>
    </div> --}}
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Update</span>
  </button>
  <a href="{{ route('lead-conversion-types.index') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link" href="">Delete this page</a></li>
    </ul>
  </section> --}}

@endsection
