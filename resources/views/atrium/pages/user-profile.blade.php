@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title"><data data-name="user-id" value="NSI291">#NSI291</data> Noel Martel</h1>
@endsection


@section('panel-tab-group')
  <ul class="panel-tab-group">
    <li class="panel-tab is-current"><a href="{{url('atrium/user-profile')}}">Profile</a></li>
    <li class="panel-tab"><a href="{{url('atrium/user-account-activity')}}">Account Activity</a></li>
    {{-- <li class="panel-tab"><a href="#">Manage</a></li> --}}
  </ul>
@endsection

@section('view-main__body')
  <header class="view-main__header">
  </header>

  <section class="view-main__content">
    <form class="view-main__content__editor-form is-flex-grid" id="form-user-profile">

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Basic Information</h3>
        </header>

        <div class="flex-row">

          <div class="input--text is-disabled flex-2-fr">
            <label>Account ID</label>
            <input class="u-font-mono" type="text" placeholder="Account ID" value="NSI291" disabled/>
          </div>
          <div class="input--text is-disabled flex-4-fr">
            <label>Email</label>
            <input id="ef-email" type="text" placeholder="Email" value="noel@morganstanley.com" disabled/>
          </div>
          <div class="input--text flex-2-fr">
            <label>First Name</label>
            <input id="ef-first-name" type="text" placeholder="First Name" value="Noel"/>
          </div>
          <div class="input--text flex-2-fr">
            <label>Last Name</label>
            <input id="ef-last-name" type="text" placeholder="Last Name" value="Martel"/>
          </div>
          <div class="input--select flex-2-fr">
            <label>Title</label>
            <select name="ef-title">
              <option value='Mr.'>Mr.</option>
              <option value='Mrs.'>Mrs.</option>
              <option value='Miss.'>Miss.</option>
              <option value='Other'>Other</option>
            </select>
          </div>
          <div class="input--select flex-2-fr">
            <label>Country</label>
            <select name="ef-country">
              <option value='Ireland'>Ireland</option>
            </select>
          </div>
          <div class="input--text flex-2-fr">
            <label>Phone</label>
            <input name="ef-phone" type="text" placeholder="Phone" value="0836666666"/>
          </div>
          <div class="input--text flex-2-fr">
            <label>Address</label>
            <input name="ef-address" type="text" placeholder="Address" value="67 Central Park Ave., Co. Dublin"/>
          </div>

          {{-- <div class="input-group is-tag-list flex-6-fr">
            <span>Tags</span>
            <div class="tag-list-row">
              <div class="tag-list">
                <span class="tag-label is-editable" data-tag-id="138A">Restaurant</span>
                <span class="tag-label is-editable" data-tag-id="20A8">Hotel</span>
                <span class="tag-label is-editable" data-tag-id="E93D">Bar</span>
                <span class="tag-label is-editable" data-tag-id="CC20">Food &amp; Drink</span>
              </div>
              <button class="tag-list__add-button">Add tag</button>
            </div>
          </div>
           --}}

        </div>

      </section>

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Company Information and Access Settings</h3>
        </header>

        <div class="flex-row">

          <div class="input--select is-disabled flex-2-fr">
            <label for="ef-client">Client</label>
            <select name="ef-country" disabled>
              <option value='Morgan Stanley'>Morgan Stanley</option>
            </select>
          </div>
          <div class="input--select flex-4-fr">
            <label for="ef-client">User Access Group</label>
            <select name="ef-country">
              <option value='default'>Same as the default setting for this client</option>
              <option value='access-setting-1'>Custom 1: Bank + All Risk Matrix Reports</option>
              <option value='new-access-setting'>Set up a new access setting</option>
            </select>
          </div>

        </div>

      </section>

      <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Personalisation</h3>
        </header>

        <div class="flex-row">
          <div class="input--file">
            <label>Profile Icon</label>
            <input type="file" / name="ef-profile-icon">
          </div>
        </div>

      </section>

      {{-- <section class="view-main__section">

        <header class="view-main__section__header">
          <h3 class="view-main__section__title">Another Section</h3>
        </header>

        <div class="flex-row">
          <div class="input--textarea flex-6-fr">
            <label for="ef-textarea-sample">Textara Sample Label</label>
            <textarea id="ef-textarea-sample" rows="5"></textarea>
          </div>
        </div>

      </section> --}}

      <section class="view-main__section">
        <div class="button-group">
          <button type="submit" class="btn--std--primary">
            <svg width="16" height="16" viewbox="0 0 16 16">
              <use xlink:href="#icon-check"></use>
            </svg>
            <span class="button-label">Save Changes</span>
          </button>
          <a href="" class="btn--std">
            <svg width="16" height="16" viewbox="0 0 16 16">
              <use xlink:href="#icon-close"></use>
            </svg>
            <span class="button-label">Discard Changes</span>
          </a>
        </div>
      </section>


    </form>
  </section>
@endsection

@section('widget-panel__content')
  <section class="widget-panel__section">
    <div class="button-group">
      <button type="submit" class="btn--std--primary">
        <svg width="16" height="16" viewbox="0 0 16 16">
          <use xlink:href="#icon-check"></use>
        </svg>
        <span class="button-label">Save Changes</span>
      </button>
    </div>
  </section>
  <section class="widget-panel__section">
    <h3 class="widget-panel__section__title">Account Actions</h3>
    <ul class="widget-panel__link-group">
      <li><a class="widget-panel__link">Deactivate Account</a></li>
      <li><a class="widget-panel__link is-alert-link">Delete Account</a></li>
    </ul>
  </section>
@endsection
