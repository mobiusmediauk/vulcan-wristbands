@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit Video Post</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/video-post/edit', $post->id) }}" method="POST">
  <input type="hidden" name="type" value="2">
  <input type="hidden" name="id" value="{{ $post->id }}">
  {{ csrf_field() }}
<section class="view-main__section">

  <div class="flex-row">
    <div class="input--text flex-4-fr">
      <label for="ef-post-title">Title</label>
      <input name="title" id="ef-post-title" type="text" placeholder="Post Title" value="{{ (old('title')) ? old('title') : $post->title }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-4-fr">
      <label for="ef-post-video">Video Url</label>
      <p class="video-link-warning">You must use an <strong>embed</strong> Youtube link to display videos.</p>
      <input name="video" id="ef-post-video" type="text" placeholder="Click share, embed and copy the link from src" value="{{ (old('video')) ? old('video') : $post->video }}" required/>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/video-post') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/video-post/' . $post->id . '/delete') }}" method="post" onsubmit="return confirm_delete();">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this post</button></li>
      </form>
    </ul>
  </section>

@endsection
