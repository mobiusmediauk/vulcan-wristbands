@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">New Inspire-Me Preset</h1>
@endsection

@section('view-main__body')

<section class="view-main__section" id="app">

  <strap-customiser :source-data='{!! $data !!}' :preset='null' custom-text="" mode="admin"></strap-customiser>

</section>

@endsection


https://api.imgur.com/oauth2/authorize?client_id=8b1e81c5edfda64&response_type=JSON
