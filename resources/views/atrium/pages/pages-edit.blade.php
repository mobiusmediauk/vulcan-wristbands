@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit Page: {{ $page->title }}</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/page', $page) }}" method="POST">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}
<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Page Contents</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr {{ $errors->has('title') ? ' has-error' : ''}}">
      <label for="ef-page-title">Page Title</label>
      <input name="title" id="ef-page-title" type="text" placeholder="Page Title" value="{{ old('title') ? old('title') : $page->title }}"/>
    </div>
    <div class="input--text flex-2-fr">
      <label for="ef-page-slug">Slug</label>
      <input name="slug" id="ef-page-slug" type="text" placeholder="Page Slug" value="{{ (old('slug')) ? old('slug') : $page->slug }}"/>
    </div>
    <div class="input--text flex-2-fr {{ $errors->has('header') ? ' has-error' : ''}}">
      <label for="ef-page-header">Header</label>
      <input name="header" id="ef-page-header" type="text" placeholder="Page Header" value="{{ old('header') ? old('header') : $page->header }}"/>
    </div>
  </div>

  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('content') ? ' has-error' : ''}}">
        <label for="ckeditor">Content</label>
        <textarea name="body" id="ckeditor" rows="10">{!! old('body') ? old('body') : $page->body !!}</textarea>
      </div>
    </div>
  </div>

</section>

{{-- <section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Header Image</h3>
  </header>

  <div class="flex-row">
    <div class="input--file">
      <input type="file" / name="image">
    </div>
  </div>

</section> --}}

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Seo And Other Page Options</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr">
      <label for="ef-post-title">Title Tag</label>
      <input name="titleTag" id="ef-post-titletag" type="text" placeholder="Title Tag" value="{{ (old('titleTag')) ? old('titleTag') : $page->titleTag }}" required/>
    </div>

    <div class="input--text flex-4-fr">
      <label for="ef-post-metadescription">Meta Description</label>
      <input name="metaDescription" id="ef-post-metadescription" type="text" placeholder="Meta Description" value="{{ (old('metaDescription')) ? old('metaDescription') : $page->metaDescription }}" required/>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--text flex-3-fr {{ $errors->has('linkName') ? ' has-error' : ''}}">
      <label for="ef-link-name">Link Name</label>
      <input name="linkName" id="ef-link-name" type="text" placeholder="Link Name" value="{{ old('linkName') ? old('linkName') : $page->linkName }}"/>
    </div>

    <div class="input--toggle">
      <input name="position" type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle" @if($page->position) checked @endif/>
      <label class="input--toggle__label" for="toggle-second-toggle">Show page in footer</label>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/page') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/page', $page) }}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this page</button></li>
      </form>
    </ul>
  </section>

@endsection
