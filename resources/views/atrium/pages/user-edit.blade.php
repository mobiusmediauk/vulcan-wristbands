@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">User: {{ $user->name }}</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/user', $user) }}" method="POST">
  <input type="hidden" name="_method" value="PATCH">
  {{ csrf_field() }}

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">User Details</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-3-fr {{ $errors->has('email') ? ' has-error' : ''}}">
      <label for="ef-user-email">Email</label>
      <input name="email" id="ef-user-email" type="text" placeholder="User Email" value="{{ (old('email')) ? old('email') : $user->email }}"/>
    </div>

    <div class="input--text flex-2-fr  {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="ef-user-name">Name</label>
      <input name="name" id="ef-user-name" type="text" placeholder="User Name" value="{{ (old('name')) ? old('name') : $user->name }}"/>
    </div>

  </div>
</section>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">User Address Details</h3>
  </header>

  <div class="flex-row">

    @forelse ($addresses as $address)
      <div class="input--checkbox-group flex-3-fr">
        <h3 style="text-transform: capitalize">{{ $address->type }} Details</h3>

        <input name="{{ $address->type }}_name" type="text" placeholder="Name" value="{{ (old( $address->type . '-name')) ? old( $address->type . '-name') : $address->name }}" required/><br/>
        <input name="{{ $address->type }}_address1" type="text" placeholder="Address 1" value="{{ (old( $address->type . '-address1')) ? old( $address->type . '-address1') : $address->address1 }}" required/><br/>
        <input name="{{ $address->type }}_address2" type="text" placeholder="Address 2" value="{{ (old( $address->type . '-address2')) ? old( $address->type . '-address2') : $address->address2 }}" required/><br/>
        <input name="{{ $address->type }}_city" type="text" placeholder="City" value="{{ (old( $address->type . '-city')) ? old( $address->type . '-city') : $address->city }}" required/><br/>
        <input name="{{ $address->type }}_province" type="text" placeholder=" Province" value="{{ (old( $address->type . '-province')) ? old( $address->type . '-province') : $address->province }}" required/><br/>
        <input name="{{ $address->type }}_postalCode" type="text" placeholder="Postal Code" value="{{ (old( $address->type . '-postalCode')) ? old( $address->type . '-postalCode') : $address->postalCode }}" required/><br/>
        <input name="{{ $address->type }}_countryCode" type="text" placeholder=" Country Code" value="{{ (old( $address->type . '-countryCode')) ? old( $address->type . '-countryCode') : $address->countryCode }}" required/><br/>
      </div>
    @empty
      <div class="input--checkbox-group flex-3-fr">
        <h3 style="text-transform: capitalize">Shipping Details</h3>
        <input name="shipping_name" type="text" placeholder="Name" value="{{ old('shipping_name') }}" required/><br/>
        <input name="shipping_address1" type="text" placeholder="Address 1" value="{{ old('shipping_address1') }}" required/><br/>
        <input name="shipping_address2" type="text" placeholder="Address 2" value="{{ old('shipping_address2') }}"/><br/>
        <input name="shipping_city" type="text" placeholder="City" value="{{ old('shipping_city') }}"/><br/>
        <input name="shipping_province" type="text" placeholder=" Province" value="{{ old('shipping_province') }}"/><br/>
        <input name="shipping_postalCode" type="text" placeholder="Postal Code" value="{{ old('shipping_postalCode') }}"/><br/>
        <input name="shipping_countryCode" type="text" placeholder=" Country Code" value="{{ old('shipping_countryCode') }}"/><br/>
      </div>
      <div class="input--checkbox-group flex-3-fr">
        <h3 style="text-transform: capitalize">Billing Details</h3>
        <input name="billing_name" type="text" placeholder="Name" value="{{ old('billing_name') }}" required/><br/>
        <input name="billing_address1" type="text" placeholder="Address 1" value="{{ old('billing_address1') }}" required/><br/>
        <input name="billing_address2" type="text" placeholder="Address 2" value="{{ old('billing_address2') }}"/><br/>
        <input name="billing_city" type="text" placeholder="City" value="{{ old('billing_city') }}"/><br/>
        <input name="billing_province" type="text" placeholder=" Province" value="{{ old('billing_province') }}"/><br/>
        <input name="billing_postalCode" type="text" placeholder="Postal Code" value="{{ old('billing_postalCode') }}"/><br/>
        <input name="billing_countryCode" type="text" placeholder=" Country Code" value="{{ old('billing_countryCode') }}"/><br/>
      </div>

    @endforelse

  </div>
</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/user') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>
</form>

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">User Orders</h3>
  </header>

    <div class="table-container">
      <table class="table--standard">
        <thead>
          <tr>
            <th>Order Number</th>
            <th>Status</th>
            <th>Tracking Number</th>
            <th>Price Total</th>
            <th>Last Updated</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($orders as $order)
            <tr>
              <td>{{ $order->orderNumber }}</td>
              <td>{{ $order->status }}</td>
              <td>{{ $order->trackingNumber }}</td>
              <td>{{ $order->total }} ({{ $order->currency }})</td>
              <td>{{ $order->updated_at }}</td>
              <td>
              <span class="item-actions">
                <a href="{{ url('admin/order', $order) }}" title="">Show</a>
                &nbsp;
                <form class="" action="{{ url('admin/order', $order) }}" method="post" onsubmit="return confirm_delete();">
                  <input type="hidden" name="_method" value="DELETE">
                  {{ csrf_field() }}
                  <button type="submit" class="" href="">Delete</button>
                </form>
              </span>
              </td>
            </tr>
          @empty
            <tr>
              <td>This user doesn't have any orders.</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>

</section>
@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/user', $user) }}" method="post" onsubmit="return confirm_delete();">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Remove user</button></li>
      </form>
    </ul>
  </section>

@endsection
