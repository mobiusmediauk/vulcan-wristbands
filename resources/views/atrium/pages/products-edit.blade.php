@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit {{ $product->type->name }}</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/product', $product) }}" method="POST" enctype="multipart/form-data">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}

<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Product Settings</h3>
  </header>

  <div class="flex-row">
    <div class="input--text flex-2-fr {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="ef-product-name">Name</label>
      <input name="name" id="ef-product-name" type="text" placeholder="Product Name" value="{{ old('name') ? old('name') : $product->name }}" required/>
    </div>
    <div class="input--select flex-2-fr">
      <label>Product type</label>
      <select name="typeId" id="productTypeSelector">
        {{-- <optgroup label="Optgroup is optional"> --}}
        @foreach ($types as $type)
          <option value='{{ $type->id }}' @if($product->type->id == $type->id) selected @endif>{{ $type->name }}</option>
        @endforeach
        {{-- </optgroup> --}}
      </select>
    </div>
    <div class="input--text flex-2-fr {{ $errors->has('color') ? ' has-error' : ''}}">
      <label for="ef-color">Color</label>
      <input name="color" id="ef-color" type="color" value="{{ old('color') ? old('color') : $product->hexColor() }}" />
    </div>
  </div>

  <div class="flex-row">
    <div class="flex-row">
      <div class="flex-row">
        <div class="input--textarea flex-6-fr {{ $errors->has('description') ? ' has-error' : ''}}">
          <label for="ckeditor">Description</label>
          <textarea name="description" id="ckeditor" rows="5" required>{!! old('description') ? old('description') : $product->description !!}</textarea>
        </div>
      </div>
    </div>
  </div>

  <div class="flex-row">
    <div class="input--toggle">
      <input name="availableToBuy" type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle" @if($product->availableToBuy) checked @endif/>
      <label class="input--toggle__label" for="toggle-second-toggle">Product is available to purchase</label>
    </div>
  </div>
</section>

<section class="view-main__section">

  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Product Image</h3>
  </header>

    <div class="flex-row strap-image-inputs">
      <div class="input--file flex-1-fr">
        <img src="{{ $product->thumb($product->typeId, 'strap') }}">
      </div>
      <div class="input--file flex-4-fr">
        <label>Straps</label>
        <input type="file" name="strap">
      </div>
    </div>

    <div class="flex-row strap-image-inputs">
      <div class="input--file flex-1-fr">
        <img src="{{ $product->thumb($product->typeId, 'top') }}">
      </div>
      <div class="input--file flex-4-fr">
          <label>Top partial strap image</label>
          <input type="file" name="strapTop">
        </div>
    </div>

    <div class="flex-row strap-image-inputs">
      <div class="input--file flex-1-fr">
        <img src="{{ $product->thumb($product->typeId, 'bottom') }}">
      </div>
      <div class="input--file flex-4-fr">
        <label>Bottom partial strap image</label>
        <input type="file" name="strapBottom">
      </div>
    </div>


    <div class="flex-row product-image-inputs">
      <div class="input--file flex-1-fr">
        <img src="{{ $product->thumb($product->typeId) }}">
      </div>
      <div class="input--file flex-2-fr">
        <label>Product image</label>
        <input type="file" name="image">
      </div>
    </div>

    <div class="flex-row product-shop-image">
      <div class="input--file flex-1-fr">
        <img src="/images/products/{{ $product->thumbnail }}">
      </div>
      <div class="input--file flex-2-fr">
        <label>Accessories Page Image</label>
        <input type="file" name="shopImage">
      </div>
    </div>


    <script>
    $(function() {
      function checkProductType(typeId) {
        if (typeId == '2' || $( "select#productTypeSelector option:checked" ).val() == 2) {
          jQuery('.strap-image-inputs').show();
          jQuery('.product-image-inputs').hide();
          jQuery('.product-shop-image').hide();
        } else if(typeId == '3' || $( "select#productTypeSelector option:checked" ).val() == 3) {
          jQuery('.strap-image-inputs').hide();
          jQuery('.product-image-inputs').show();
          jQuery('.product-shop-image').show();
        } else {
          jQuery('.strap-image-inputs').hide();
          jQuery('.product-image-inputs').show();
          jQuery('.product-shop-image').hide();
        }
        if (typeId == '6' || $( "select#productTypeSelector option:checked" ).val() == 6) {
          console.log('dasfd')
          $('#voucher-notice').show();
        } else {
          $('#voucher-notice').hide();
        }
      }
      jQuery('#productTypeSelector').on('change', function(event) {
        checkProductType(event.target.value)
      })
      checkProductType(jQuery('#productTypeSelector').value);
    })
    </script>



</section>

<section class="view-main__section">

  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Currency, weight Settings</h3>
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr {{ $errors->has('weight') ? ' has-error' : ''}}">
      <label for="ef-number">Weight (g)</label>
      <input name="weight" id="ef-number" type="number" placeholder="Weight" value="{{ old('weight') ? old('weight') : $product->weight }}" step="0.01" required/>
    </div>

    <div class="input--text flex-2-fr {{ $errors->has('stock') ? ' has-error' : ''}}">
      <label for="ef-number">Stock Level</label>
      <input name="stockCount" id="ef-number" type="number" placeholder="Stock Level" value="{{ old('stock') ? old('stock') : $product->stockCount }}" required/>
    </div>

    {{-- <div class="input--toggle">
      <input name="inStock" type="checkbox" class="input--toggle__checkbox" id="toggle-instock-toggle" @if($product->inStock) checked @endif/>
      <label class="input--toggle__label" for="toggle-instock-toggle">Product In stock</label>
    </div> --}}

  </div>

  <div class="notif-card full-width" data-notif-type="neutral" id="voucher-notice" style="display: none;">
    <header class="notif-card__header">
      <svg class="notif-card__icon" width="18" height="18" viewbox="0 0 18 18"><use xlink:href="#icon-info"></use></svg>
      <h3 class="notif-card__title">Vouchers will only be available for purchase for users who sets the currency to one that the voucher supports. To make sure your voucher is available and can be applied to orders of any currency, fill in the price fields for all currencies.</h3>
    </header>
  </div>

  <div class="flex-row">

    @foreach(app('currency')->options() as $option)
      <div class="input--text flex-2-fr">
        <label for="ef-number">Price ({{ $option->label }})</label>
        <input name="price.{{ $option->value }}" id="ef-number" type="number" placeholder="Price" value="{{ old('price_' . $option->value) ? old('price_' . $option->value) : $product->displayPriceValue($option->value) }}" step="0.01" min="0.00" max="10000.00"/>
      </div>
    @endforeach

  </div>

  <script>

    // $.urlParam = function(name){
    //   var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
    //   if (results==null) return null;
    //   else return results[1] || 0;
    // }
    // var typeMap = [
    //   {id: 1, name: 'Watches'},
    //   {id: 2, name: 'Straps'},
    //   {id: 3, name: 'Bands'},
    //   {id: 4, name: 'Buckles'},
    //   {id: 5, name: 'Accessories'},
    //   {id: 6, name: 'Vouchers'},
    // ]
    // var selectedType = typeMap.find(type => type.name === $.urlParam('type'));
    // console.log($.urlParam('type'));
    // if (selectedType) $('#productTypeSelector').val(selectedType.id);
    //
    // if ($.urlParam('type') === 'Vouchers') {
    //   $('#voucher-notice').show();
    // }

  </script>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/product') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>

  <br />
  <br />
</div>

</form>
@endsection

@section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/product', $product) }}" method="post" onsubmit="return confirm_delete();">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this product</button></li>
      </form>
    </ul>
  </section>

@endsection
