@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Social Media settings</h1>
@endsection

@section('view-main__body')
  @foreach ($socialMedia as $social)
    <section class="view-main__section">
    <form class="form--standard is-flex-grid" action="{{ url('admin/social', $social) }}" method="POST">
      <input type="hidden" name="_method" value="PUT">
      {{ csrf_field() }}
        <header class="view-main__section__header">
          <h3 class="view-main__section__title">{{ $social->name }}</h3>
        </header>
        <div class="flex-row">
          <div class="input--toggle">
            <input name="show" type="checkbox" class="input--toggle__checkbox" id="{{ $social->name }}-toggle-show" @if($social->show || old('show')) checked @endif/>
              <label class="input--toggle__label" for="{{ $social->name }}-toggle-show"></label>
            </div>
          <div class="input--text flex-3-row {{ $errors->has('link') ? ' has-error' : ''}}">
            <label for="{{ $social->name }}-link">Link</label>
            <input name="link" id="{{ $social->name }}-link" type="text" placeholder="Link" value="{{ old('link') ? old('link') : $social->link }}"/>
          </div>
          <div class="input--text flex-3-row {{ $errors->has('header') ? ' has-error' : ''}}">
            <label for="{{ $social->name }}-token">Token</label>
            <input name="token" id="{{ $social->name }}-token" type="text" placeholder="Token" value="{{ old('token') ? old('token') : $social->token }}"/>
          </div>
          <div class="input--text flex-3-row {{ $errors->has('username') ? ' has-error' : ''}}">
            <label for="{{ $social->name }}-username">Username</label>
            <input name="username" id="{{ $social->name }}-username" type="text" placeholder="Username" value="{{ old('username') ? old('username') : $social->username }}"/>
          </div>
          <div class="input--text flex-1-row {{ $errors->has('count') ? ' has-error' : ''}}">
            <label for="{{ $social->name }}-username">Count</label>
            <input name="count" id="{{ $social->name }}-count" type="text" placeholder="Follower Count" value="{{ old('count') ? old('count') : $social->count }}"/>
          </div>
          <div class="button-group">
            <button type="submit" class="btn--std--primary">
              <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
              <span class="button-label">Save</span>
            </button>
          </div>
        </div>
        @if (is_null($social->token_description))
          <p>
            An Access Token is not currently required for this app.
          </p>
        @else
          <p>For accessing a new <em>{{ $social->name }}</em>  token, <a target="_blank" class="form__link" href="{{ $social->token_description }}">click here</a></p>
        @endif
    </form>
    </section>
  @endforeach
@endsection
