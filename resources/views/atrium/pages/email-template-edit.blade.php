@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit Email template: {{ $emailTemplate->name }}</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ route('email-template.update', $emailTemplate) }}" method="POST">
  <input type="hidden" name="_method" value="PUT">
  {{ csrf_field() }}
<section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Email template</h3>
    <p>Available variables to use:</p>
    <table class="view-main__section__content variables__list">
      <tbody>
        <tr class="variables__list-header">
          <th>Variable Name</th>
          <th>Variable Description</th>
        </tr>
        <tr>
          <td><strong>{customer.name}</strong></td>
          <td>- Prints customer's name in template.</td>
        </tr>
        <tr>
          <td><strong>{customer.email}</strong></td>
          <td>- Prints customer's email in template.</td>
        </tr>
        <tr>
          <td><strong>{order.number}</strong></td>
          <td>- Prints order number.</td>
        </tr>
          @if($emailTemplate->id == 1)
            <tr>
              <td><strong>{order.tracking_number}</strong></td>
              <td>- Prints tracking number in template.</td>
            </tr>
            <tr>
              <td><strong>{order.vouchers}</strong></td>
              <td>- Includes list of voucher codes (if purchased by user).</td>
            </tr>
          @elseif ($emailTemplate->id == 2 || $emailTemplate->id == 3)
            <tr>
              <td><strong>{order.products}</strong></td>
              <td>- Prints a simple list with products.</td>
            </tr>
            <tr>
              <td><strong>{order.products.withImages}</strong></td>
              <td>- Prints simple list with products and images.</td>
            </tr>
            <tr>
              <td><strong>{order.products.details}</strong></td>
              <td>- Prints detailed list of products for admin use.</td>
            </tr>
            <tr>
              <td><strong>{order.products.details.withImages}</strong></td>
              <td>- Prints Simple list with products and images.</td>
            </tr>
            <tr>
              <td><strong>{order.addresses}</strong></td>
              <td>- Prints shipping and billing addresses.</td>
            </tr>
          @endif
      </tbody>
    </table>
  </header>


  <div class="flex-row">
    <div class="flex-row">
      <div class="input--textarea flex-6-fr {{ $errors->has('content') ? ' has-error' : ''}}">
        <label for="ckeditor">Content</label>
        <textarea name="body" id="ckeditor" rows="15">{!! old('body') ? old('body') : $emailTemplate->body !!}</textarea>
      </div>
    </div>
  </div>

</section>

{{-- <section class="view-main__section">
  <header class="view-main__section__header">
    <h3 class="view-main__section__title">Email template options</h3>
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr">
      <label for="ef-post-title">Send email copy to this email</label>
      <input name="ccTo" id="ef-post-titletag" type="text" placeholder="" value="{{ (old('ccTo')) ? old('ccTo') : $emailTemplate->ccTo }}"/>
    </div>

    <div class="input--toggle">
      <input name="ccCopy" type="checkbox" class="input--toggle__checkbox" id="toggle-second-toggle" @if($emailTemplate->ccCopy) checked @endif/>
      <label class="input--toggle__label" for="toggle-second-toggle">Get copy of this email</label>
    </div>

  </div>

</section> --}}

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/email-templates') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

{{-- @section('widget-panel__content')

  <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/page', $emailTemplate) }}" method="post" onsubmit="return confirm_delete();">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this page</button></li>
      </form>
    </ul>
  </section>

@endsection --}}
