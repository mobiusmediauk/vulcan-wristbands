@extends('atrium.layouts.app')

@section('view-header__content')
  <h1 class="view-title">Edit your profile</h1>
@endsection

@section('view-main__body')

<form class="view-main__content__editor-form form--standard is-flex-grid" action="{{ url('admin/update-profile', $user) }}" method="POST">
  {{ csrf_field() }}
  
<section class="view-main__section">
  <header class="view-main__section__header">
    {{-- <h3 class="view-main__section__title">Page Contents</h3> --}}
  </header>

  <div class="flex-row">

    <div class="input--text flex-2-fr {{ $errors->has('name') ? ' has-error' : ''}}">
      <label for="change-user-name">Name</label>
      <input name="name" id="change-user-name" type="text" placeholder="Your Name" value="{{ old('name') ? old('name') : $user->name }}" required/>
    </div>
    <div class="input--text flex-2-fr {{ $errors->has('email') ? ' has-error' : ''}}">
      <label for="change-user-email">Email</label>
      <input name="email" id="change-user-email" type="text" placeholder="Your email address" value="{{ old('email') ? old('email') : $user->email }}" required/>
    </div>
  </div>

</section>

<div class="button-group">
  <button type="submit" class="btn--std--primary">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-check"></use></svg>
    <span class="button-label">Save</span>
  </button>
  <a href="{{ url('admin/page') }}" class="btn--std">
    <svg width="16" height="16" viewbox="0 0 16 16"><use xlink:href="#icon-close"></use></svg>
    <span class="button-label">Cancel</span>
  </a>
</div>

</form>
@endsection

@section('widget-panel__content')

  {{-- <section class="widget-panel__section" data-section-name="actions">
    <h3 class="widget-panel__section__title">Actions</h3>
    <ul class="widget-panel__link-group">
      <form class="" action="{{ url('admin/page', $user) }}" method="post" onsubmit="return confirm_delete();">
        <input type="hidden" name="_method" value="DELETE">
        {{ csrf_field() }}
        <li><button type="submit" class="widget-panel__link" href="">Delete this page</button></li>
      </form>
    </ul>
  </section> --}}

@endsection
