<form class="shaded-form padded-form" id="login-form" action="{{ route('login') }}" method="POST">
  {{ csrf_field() }}
  <h3>Registered Customer</h3>
  <div class="input--text">
    <input name="email" placeholder="Email" type="email" required/>
  </div>
  <div class="input--text">
    <input name="password" placeholder="Password" type="password"/>
  </div>
  <div class="shaded-form-submit-row">
    <button type="submit" class="button button-inverted">Login</button>
    @if(!Request::is('join'))
    <a style="margin-left: 15px;" href="{{ url('join') }}">Register</a>
    @endif
  </div>
</form>
