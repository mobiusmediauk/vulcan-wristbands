@foreach (App\SocialMedia::where('id', '!=', 2)->where('id', '!=', 4)->get() as $media)
  @if($media->show)
    @if ($media->link)
      <li><a href="{{ route('blog.share', [$post, $media->name]) }}" target="_blank"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#{{ $media->icon }}"></use></svg></a></li>
    @endif
  @endif
@endforeach
