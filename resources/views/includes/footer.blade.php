<section class="page-section ship-globally-section" style="background-image: url({{asset('images/ship-globally-background.jpg')}})">
  <div class="section-container">
    <img src="{{asset('images/dhl-logo.png')}}" />
    <h2>We Ship Globally</h2>
  </div>
</section>

<!-- TrustPilot -->
<section class="footer__trustpilot">
    <div class="section-container">
        <script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
        <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="5b5f41c84a82540001fc52a5" data-style-height="24" data-style-width="100%" data-theme="light">
            <a href="https://uk.trustpilot.com/review/vulcanwatchstraps.com" target="_blank">Trustpilot</a>
        </div>
    </div>
</section>

<footer class="site-footer page-section">
  <div class="section-container">
    <div class="footer-logo-container">
      <svg width="192" height="192" viewBox="0 0 192 192"><use xlink:href="#vulcan-logo-full"></use></svg>
    </div>
    <div class="footer-newsletter-signup-form">
      <h3>Sign up to our newsletter</h3>
      <form action="{{ route('newsletter.subscribe') }}" method="post">
        {{ csrf_field() }}
        <input name="email" placeholder="Email"/>
        <button type="submit" class="button">Sign up</button>
      </form>
      <div class="footer-social-links">
        {{-- <h3>Stay tuned</h3> --}}
        <ul class="social-links-list">
          @include('includes.social-media-list-partial')
        </ul>
        @php
          $infoEmail = App\Settings::find(1)->infoEmail;
        @endphp
        @isset($infoEmail)
          <p class="footer-email-link"><a href="mailto:{{ $infoEmail }}">{{ $infoEmail }}</a></p>
        @endisset
      </div>
    </div>
    <div class="footer-payment-icons-column">
      <a class="payment-icon-worldpay" href="https://www.worldpay.com/" target="_blank"><img src="{{ asset('images/payment-icons/powered-by-worldpay.png') }}" /></a>
      <div class="payment-methods-icon-list">
        <img src="{{ asset('images/payment-icons/card-visa.png') }}"/>
        <img src="{{ asset('images/payment-icons/card-mastercard.png') }}"/>
        <img src="{{ asset('images/payment-icons/amex.png') }}"/>
        {{-- <img src="{{ asset('images/payment-icons/card-maestro.png') }}"/>
        <img src="{{ asset('images/payment-icons/card-jcb.gif') }}"/> --}}
      </div>
    </div>
  </div>
  <div class="copyright__footer">
    <p>Vulcan Watch Straps are an independent brand are not affiliated in any way Rolex SA.</p>
    <p>All watches, likenesses and logos are trademarks of Rolex SA.</p>
  </div>
  <div class="footer-of-footer">
    <ul class="footer-links">
      @foreach (\App\Page::where('position', true)->get() as $page)
        <li><a href={{ url($page->slug) }}>{{ $page->linkName }}</a></li>
      @endforeach
      <li>
      @if(auth()->check())
          <a href="{{ route('logout') }}"
             onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              {{ __('Log out') }}
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
      @else
        <a href="{{ url('join') }}">Login</a>
      @endif
    </ul>
    <p class="footer-credit">&copy; <?php echo date('Y'); ?> Vulcan watch straps. <a href="http://mushdigital.co.uk/">Designed by m.</a></p>
  </div>

</footer>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>

function shareOverrideOGMeta(overrideLink, overrideTitle, overrideDescription, overrideImage)
{
	FB.ui({
		method: 'share_open_graph',
		action_type: 'og.shares',
		action_properties: JSON.stringify({
			object: {
				'og:url': overrideLink,
				'og:title': overrideTitle,
				'og:description': overrideDescription,
				'og:image': overrideImage
			}
		})
	},
	function (response) {
	// Action after response
	});
}

</script>
<script type="text/javascript" language="javascript">

function TwitterLink() {
  var linkToTweet = document.getElementById('linkToShare').innerHTML;
  var twit =  'https://twitter.com/intent/tweet?url=';
  var twitLink = twit.linkToTweet;
  document.getElementById("dynamicButton").innerHTML = twitLink;
  console.log(document.getElementById("dynamicButton").innerHTML = twitLink);
}

        //Not Jquery !!! just a helper function to return an object
        function $(i_obj) {
            return document.getElementById(i_obj);
        }

        //Function displays the Feed Dialog
        function LaunchFeedDialog() {

          // setTimeout(function() {
            let link = document.getElementById('linkToShare').innerHTML;
            console.log(link);

            //Create an object with the below properties.
            //There are a lot more parameters than what I have written below. Will explain each one of them in coming posts.
            var obj = {
              method: 'feed',
              link: link,
              caption: 'content sample',
            };

            //Calling the Facebook API : Important
            FB.ui(obj, callback);

          // },100);

        }

        function callback(response) {
            //Do anything you want here :)
            //document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
            //alert(response['post_id']); Some diagnostics lol :)
        }

        // Load the SDK Asynchronously. This is a very important part. It loads the Facebook javascript SDK automatically.
        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        } (document));
    </script>
    <script>
    jQuery(document).ready(function(){
    jQuery(".close_modal").click(function(){
        jQuery("#sharing__modal").toggleClass("is-closed");
        jQuery("#sharing__modal").removeClass("is-open");
    });
  });
  jQuery(document).ready(function(){
  jQuery(".open_modal").click(function(){
      jQuery("#sharing__modal").toggleClass("is-open");
      jQuery("#sharing__modal").removeClass("is-closed");
    });
  });
  jQuery(document).ready(function(){
  jQuery("#email-share-button").click(function(){
      jQuery("#sharing__modal").toggleClass("is-closed");
      jQuery("#sharing__modal").removeClass("is-open");
    });
  });

    </script>
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>