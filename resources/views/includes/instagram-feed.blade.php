@php

$get = App\SocialMedia::where('name', 'instagram')->first();

if(isset($get->token)){
  $instagram = new Vinkla\Instagram\Instagram($get->token);
}

// dump($instagram->get());

@endphp

@if($get->show)
  @if($get->token && $instagram)
    <section class="page-section section-simple-centered">
      <div class="section-container instagram-container">
        <h2 class="page-section__title">Find us on instagram</h2>
        {{-- <svg width="48" height="48" viewBox="0 0 48 48" style="margin-top: 20px;">
        <use xlink:href="#icon-gallery" fill-opacity="0.3"></use>
      </svg> --}}
      <h3 class="page-section__title-link"><a href="https://www.instagram.com/vulcanwatchstraps/" target="_blank">Please follow us to keep up to date</a></h3>
      <ul class="instagram-feed">
        @foreach ($instagram->get() as $key => $post)
          @if($key < 9)
            <li>
              <a href="{{ $post->link }}" target="_blank">
                <figure>
                  <img src="{{ $post->images->low_resolution->url }}">
                  <figcaption><div>{{ (isset($post->caption->text)) ? $post->caption->text : '' }}</div></figcaption>
                </figure>
              </a>
            </li>
          @endif
        @endforeach
      </ul>
      </div>
    </section>
  @endif
@endif
