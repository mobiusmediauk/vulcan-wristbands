{{-- <li><a href="#"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#icon-facebook"></use></svg></a></li>
<li><a href="#"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#icon-instagram"></use></svg></a></li>
<li><a href="#"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#icon-twitter"></use></svg></a></li>
<li><a href="#"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#icon-youtube"></use></svg></a></li>
<li><a href="#"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#icon-snap"></use></svg></a></li> --}}
<div id="wrapper" class="square">
@foreach (App\SocialMedia::all() as $media)
  @if($media->show)
    @if ($media->link)
      <li><a class="item {{ $media->name }}"href="{{ $media->link }}" target="_blank"><svg width="22" height="22" viewBox="0 0 22 22"><use xlink:href="#{{ $media->icon }}"></use></svg><i class="fa fa-twitter"></i><span class="count">{{ $media->count }}</span></a></li>
    @endif
  @endif
@endforeach
</div>
