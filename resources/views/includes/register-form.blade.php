<form class="shaded-form padded-form" id="register-form" action="{{ url('register') }}" method="post">
  {{ csrf_field() }}
  <h3>Create a new account</h3>
  <div class="input--text{{ $errors->has('name') ? ' is-invalid' : '' }}">
    <input name="name" placeholder="Name (Required)" type="text" value="{{ old('name') }}" required/>
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
  </div>
  <div class="input--text{{ $errors->has('email') ? ' is-invalid' : '' }}">
    <input name="email" placeholder="Email (Required)" type="email" value="{{ old('email') }}" required/>
    @if ($errors->has('email'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
  </div>
  <div class="input--text{{ $errors->has('password') ? ' is-invalid' : '' }}">
    <input name="password" placeholder="Password" type="password" value="{{ old('password') }}" required/>
    @if ($errors->has('password'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
  </div>
  <div class="input--text{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
    <input name="password_confirmation" placeholder="Confirm Password" type="password" value="{{ old('password_confirmation') }}" required/>
    @if ($errors->has('password_confirmation'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
    @endif
  </div>
  <h4>Newsletter</h4>
  <div class="checkbox">
    <input type="checkbox" name="receiveNewsletter" id="receiveNewsletter" checked/>
    <label for="receiveNewsletter">I want to receive the Vulcan Newsletter</label>
  </div>
  <p><small>
    Enter your email address below to be kept up to date with all of our latest news and offers.
  </p>
  <div class="shaded-form-submit-row">
    <button type="submit" class="button button-inverted">Proceed</button>
  </div>
</form>
