<header class="site-header page-section">
  <div class="section-container">
    <a class="site-logo" href="{{ route('home') }}">
      <svg width="38" height="38" viewBox="0 0 38 38">
        <use xlink:href="#vulcan-logo-icon"></use>
      </svg>
    </a>
    <nav class="site-nav" id="site-nav">
      <ul class="site-nav-major site-desktop-menu">
        <li><a href="{{ route('aboutus') }}">About us</a></li>
        <li><a href="{{ route('accessories') }}">Accessories</a></li>
        <li><a href="{{ route('differences') }}"><span>Our </span>Differences</a></li>
        <li><a href="{{ route('contact') }}">Contact<span> us</span></a></li>
        <li><a href="{{ route('blog') }}">Blog</a></li>
      </ul>
      <a href="{{ route('customise') }}" class="button header-customise-button">Buy Now</a>
      <ul class="site-nav-minor">
        {{-- @if(Auth::check())
          <li><a href="{{ route('logout') }}">{{ Auth::user()->name }}</a></li>
        @else
          <li><a href="{{ route('join') }}">Join</a></li>
        @endif --}}
        <li class="cart-button"><a href="{{ route('cart') }}"><svg width="18" height="18" viewBox="0 0 18 18"><use xlink:href="#icon-cart"></use></svg><span>Cart @if(\Cart::count() > 0){{ \Cart::count() }}@endif</span></a></li>
        <li>
          <a href="#">
          <div class="details site-nav-join">
          <span class="join-newsletter">Newsletter</span>
            <div class="page-section__pop-up newsletter__pop-up">
              <h2>Interested in our newsletter?</h2>
              <p>Enter your email address below to be kept up to date with all of our latest news and offers.</p>
              <form action="{{ route('newsletter.subscribe') }}" method="post">
                {{ csrf_field() }}
                <input type="email" name="email" placeholder="email">
                <button type="submit" class="button join-newsletter-button"> Sign up</button>
              </form>
              <button class="button close-newsletter-button">X</button>
            </div>
          </div>
          {{-- <span class="newsletter-signup-successful">Newsletter Sign up successful!</span> --}}
          </a>
        </li>
        <li class="desktop__login">
        @if(auth()->check())
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ __('Log out') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
        @else
          <a href="{{ url('join') }}">Login</a>
        @endif
        </li>
        <li>
          <div class="site-nav-minor-details details">
            <summary id="currency-value-holder" data-currency="{{ (session()->has('selectedCurrency')) ? session()->get('selectedCurrency') : app('currency')->get() }}"><div class="currency-holder"></div></summary>
              <ul>
                @foreach (app('currency')->options() as $option)
                  <li><a href="{{ route('cart.setCurrency', $option->value) }}">{{ $option->label }}</a></li>
                @endforeach
              </ul>
          </div>
        </li>
        <li>
          <div id="google_translate_element"></div>
        </li>
        {{-- <li>
          <details class="site-nav-minor-details">
            <summary>EN</summary>
            <ul>
              <li><a>English</a></li>
            </ul>
          </details>
        </li> --}}
      </ul>
      <div class="site-menu-button details" id="site-menu-button">
       <summary class="hamburger">
         <svg class="icon-menu" width="32" height="32" viewBox="0 0 32 32" fill="currentColor"><use xlink:href="#icon-menu"></use></svg>
       </summary>
        <ul class="site-nav-major">
          <li><a href="{{ route('aboutus') }}">About us</a></li>
          <li><a href="{{ route('accessories') }}">Accessories</a></li>
          <li><a href="{{ route('differences') }}"><span>Our </span>Differences</a></li>
          <li><a href="{{ route('contact') }}">Contact<span> us</span></a></li>
          <li><a href="{{ route('blog') }}">Blog</a></li>
          <li><a href="{{ route('customise') }}" class="mobile-customise-button">Buy Now</a></li>
          <li class="mobile__login">
          @if(auth()->check())
              <a href="{{ route('logout') }}"
                 onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  {{ __('Log out') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
          @else
            <a href="{{ url('join') }}">Login</a>
          @endif
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>
@if(session()->has('status'))
  <div class="session-status-notice" id="session-status-notice">
    <div class="section-container">
      <p>{{ session()->get('status') }}</p>
      <button onclick="var noticeDOM = document.getElementById('session-status-notice'); noticeDOM.parentNode.removeChild(noticeDOM);">Dismiss</button>
    </div>
  </div>
@endif
