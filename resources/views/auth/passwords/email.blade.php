@extends('layouts.app')
@section('content')
<header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
  <div class="section-container">
    <h1>Reset Password</h1>
  </div>
</header>
<div class="page-paper-wrapper">
  <div class="page-paper-wrapper__container">
    <section class="page-section login-forms-section">
      <div class="section-container">
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <form class="shaded-form padded-form" role="form" method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}
          <div class="input--text{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button type="submit" class="button">
              Send Password Reset Link
              </button>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>
@endsection
