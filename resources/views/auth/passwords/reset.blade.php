@extends('layouts.app')
@section('content')
<header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
  <div class="section-container">
    <h1>Reset Password</h1>
  </div>
</header>
<div class="page-paper-wrapper">
  <div class="page-paper-wrapper__container">
    <section class="page-section login-forms-section">
      <div class="section-container">
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        <form class="shaded-form padded-form" role="form" method="POST" action="{{ route('password.request') }}">
          {{ csrf_field() }}
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="input--text{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
            @if ($errors->has('email'))
            <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
          </div>
          <div class="input--text{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
            @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>
          <div class="input--text{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm" class="control-label">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
            <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
          </div>
          <div class="form-group">
            <button type="submit" class="button">
            Reset Password
            </button>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>
@endsection
