@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-image-accessories.jpg')">
    <div class="section-container">
      <h1>Welcome</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">

      <section class="page-section login-forms-section">

        <div class="section-container">

          @include('includes.login-form')

          @include('includes.register-form')
          
        </div>

      </section>

    </div>
  </div>

@endsection
