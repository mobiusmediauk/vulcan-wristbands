@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="background-image: url('images/vulcan-banner-people.jpg')">
    <div class="section-container">
      <h1>@yield('header')</h1>
    </div>
  </header>

  <div class="page-paper-wrapper">
    <div class="page-paper-wrapper__container">
      <section class="page-section template-page-content-section">
        <div class="section-container">
          <article class="page-section__copy-article">
            @yield('body')
          </article>
        </div>
      </section>
    </div>
  </div>

@endsection
