@extends('layouts.app')
@section('content')

  <header class="page-section section-simple-header has-background" style="">
    <div class="section-container">
      <h1>@yield('header')</h1>
    </div>
  </header>

  <section class="page-section section-generic-banner-with-text" style="background:white">
    <div class="section-container">
      <div class="page-section__copy-article">
        @yield('body')
      </div>
    </div>
  </section>

@endsection
