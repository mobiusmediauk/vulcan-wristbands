var APP = (function(app){

  app.popup = {

    init : function() {

      jQuery('.popup-trigger').click(function(e){
        e.preventDefault();
        // console.log(e);
        var target = '#' + jQuery(this).attr('data-for');
        app.popup.summon(target);
      });

      jQuery('[data-action="dismiss-popup"], .popup-overlay').click(function(e){
        e.preventDefault();
        // console.log(e.target);
        var target = '#' + jQuery(this).parents('.popup-section').attr('id');
        var popup = target + ' .popup-wrapper';
        var overlay = target + ' .popup-overlay';
        var tl = new TimelineLite({
          onComplete: function(){
          }
        });
        tl.to(overlay, 0.5, {opacity: 0},0);
        tl.to(popup, 0.5, {
          ease: Power3.easeInOut,
          // transformOrigin: '0 0',
          opacity: 0,
          yPercent: '10%'
        },0);
        tl.set(target, {display: 'none'}, 0.5);
        if (s.ui.effectLevel === 'high' && !s.ui.isHighDensity) {
          tl.to('.view-trunk', 0.5, {filter: 'blur(0)'},0);
        }
        tl.set('.view-trunk', {filter: 'none'},0.5);
      });

    },

    summon : function(target) {
      var popup = target + ' .popup-wrapper';
      var overlay = target + ' .popup-overlay';
      var tl = new TimelineLite();
      tl.set(target, {display: 'block'});
      tl.fromTo(overlay, 0.5, {opacity: 0}, {opacity: 1}, 0);
      tl.fromTo(popup, 0.5, {
        ease: Power4.easeInOut,
        // transformOrigin: '0 0',
        yPercent: '10%',
        scale: 0.95,
        opacity: 0
      }, {
        yPercent: '0',
        scale: 1,
        opacity: 1
      },0);
      if (target === '#thought-catcher-popup') {
        jQuery('[name="thoughtcontent"]').focus();
      }
    },



  };

  return app;

}(APP || {}, jQuery));
