var APP = (function(app){

  app.notifCards = {

    init : function() {
      var notifStack = jQuery('#view-notif-card-stack');
      if ( notifStack.length < 1 ) {
        return;
      }
      notifStack.addClass('is-loaded');
      TweenMax.set('.notif-card',{opacity: 0});
      TweenMax.staggerFromTo('.notif-card',1,{x: -40, delay: 2},{opacity: 1, x: 0}, 0.38);
      $('[data-action="dismiss-notif"]').click(function(){
        var card = $(this).parents('.notif-card');
        TweenMax.to(card, 0.3, {scale: 0.95, opacity: 0, onComplete: function(){ card.remove(); }});
      });
    }

  };

  return app;

}(APP || {}, jQuery));
