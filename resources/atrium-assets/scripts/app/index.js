var s;
// var s is created as a shortcut for module.settings. will be linked up upon initialisation.

var APP = (function(app){

  // reference: http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html

  app.settings = {
    // set settings or variables here that's accessible to the whole module
    initialised : false,
    ui : {}
  };


  return app;

  // below is scripts to import to this module.
  // APP || {} enables the scripts from different augments to be loaded asynchonously, creating the APP if it does not already exist.
  // note that module properties will not be able to run before initialisation is complete.

}(APP || {}, $));

jQuery(function(){
  APP.init();
});
