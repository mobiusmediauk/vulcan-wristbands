<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@Home')->name('admin.home');
    Route::group(['middleware' => 'auth'], function () {
        Route::group(['middleware' => ['role:admin']], function () {
            Route::resource('page', 'PageController');
            Route::resource('product', 'ProductController');
            Route::post('product/reorder', 'ProductController@reorder')->name('product.reorder');
            Route::get('product/type/{type}', 'ProductController@index');
            Route::resource('order', 'OrderController');
            Route::resource('shipping', 'ShippingController');
            Route::resource('social', 'SocialMediaController');
            Route::resource('enquiry', 'EnquiryController');
            Route::resource('lead-conversion-types', 'LeadConversionTypeController');
            Route::resource('user', 'UserController');
            Route::resource('post', 'PostController');
            Route::resource('mainpage', 'MainPageController');
            Route::resource('voucher', 'VoucherController');
            Route::resource('inspireme', 'InspireMeController');
            Route::resource('settings', 'SettingsController');
            
            Route::get('/invoice/{order}', 'OrderController@invoice');

            # Slide controlls:
            Route::get('slider/slide/{slide}/remove-slide/', 'SliderController@removeSlide')->name('slider.remove-slide');
            Route::get('slider/slide/{slide}/remove-background/', 'SliderController@removeBackground')->name('slider.remove-slide-background');
            Route::get('slider/slide/{slide}/remove-item/', 'SliderController@removeItem')->name('slider.remove-slide-item');

            Route::get('email-templates', 'EmailTemplateController@index')->name('emailTemplates.index');
            Route::resource('email-template', 'EmailTemplateController');

            Route::get('video-post', 'PostController@vlogIndex'); 
            Route::post('video-post', 'PostController@storeVlog'); 
            Route::post('video-post/{postId}/delete', 'PostController@destroyVlog');
            Route::post('video-post/edit/{postId}', 'PostController@updateVlog');




            Route::get('video-post/{postId}/edit', 'PostController@editVlog');
            Route::get('video-post/create', 'PostController@createVlog');
            Route::get('mailing-list', 'EnquiryController@mailingListIndex')->name('mailinglist');


            Route::get('statistics', 'StatisticsController@getStatistics')->name('statistics');

            Route::get('profile', 'UserController@adminProfileIndex')->name('admin.profile');
            Route::post('update-profile/{user}', 'UserController@adminProfileUpdate');
            Route::get('logout', 'Auth\LoginController@logOut')->name('login.logout');

            Route::get('/module-sheet', 'AdminController@moduleSheet')->name('admin.module-sheet');
        });
    });
});
