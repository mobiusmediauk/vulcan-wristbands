<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*
*
*/

Route::post('/email-share', 'SocialMediaController@emailShare')->name('email.share');
Route::get('products/json','ProductsContoller@getProductsJson')->name('products.getJson');
Route::get('/shipping-json', 'ShippingController@getCountryJson')->name('shipping.getCountryJson');
Route::get('/share/{id}', 'MainPageController@share')->name('mainpage.share');
Route::get('/', 'MainPageController@home')->middleware('set.ip.currency')->name('home');
Route::get('home/', 'MainPageController@homeRedirect')->middleware('set.ip.currency')->name('homeRedirect');

Auth::routes();

Route::any('image-generate', 'ImageController@imageGenerate')->name('image.generate');

route::post('share-count', 'SocialMediaController@incrementShareCount')->name('socialmedia.incrementShare');
Route::get('join', 'Auth\LoginController@join')->name('login.join');
Route::get('signed-in/', 'Auth\LoginController@signedIn')->name('login.signedIn');
Route::get('accessories', 'ProductController@indexAccessories')->name('accessories');
Route::get('aboutus', 'MainPageController@aboutUs')->name('aboutus');
Route::get('differences', 'MainPageController@differences' )->name('differences');
Route::get('contact', 'MainPageController@contact')->name('contact');
Route::get('contact-success', 'MainPageController@contactSuccess')->name('contact-success');
// Route::get('customise', 'CustomiserController@index')->name('customise');
Route::get('our-straps', 'CustomiserController@index')->name('customise');

//TODO: CART DISABLED FOR NOW
Route::get('cart', 'CartController@cart')->name('cart');

Route::post('apply-voucher', 'VoucherController@apply')->name('voucher.apply');
Route::get('remove-voucher', 'VoucherController@remove')->name('voucher.remove');
// Route::get('cart', function () {
//     return redirect()->back();
// })->name('cart');

Route::post('checkout', 'CheckoutController@post')->name('checkout');
Route::get('checkout-login', 'CheckoutController@checkoutLogin')->name('checkout-login');
Route::post('checkout/guest', 'CheckoutController@guest')->name('checkout.guest');

Route::post('checkout/complete', 'CheckoutController@checkout')->name('checkout.complete');
Route::get('completed/', 'CheckoutController@completed')->name('checkout.completed');

Route::get('checkout/failed', 'CheckoutController@checkoutFailed')->name('checkout.failed');

Route::get('order-success', 'OrderController@orderSuccess')->name('order-success');

Route::post('enquiry/send', 'EnquiryController@send')->name('enquiry.send');
Route::post('newsletter/subscribe', 'EnquiryController@signupForNewsletter')->name('newsletter.subscribe');

Route::get('unsubscribe/{email}', 'EnquiryController@unsubscribeFromNewsletter');


Route::post('cart/remove', 'CartController@remove')->name('cart.remove');
Route::post('cart/qty', 'CartController@qty')->name('cart.qty');
Route::post('cart/{product}', 'CartController@add')->name('cart.add');
Route::get('cart/destroy', 'CartController@destroy')->name('cart.destroy');
Route::get('cart/set/currency/{currency}', 'CartController@setCurrency')->name('cart.setCurrency');

Route::post('customiser/assemble', 'CustomiserController@assemble')->name('customiser.assemble');

Route::resource('customiser', 'CustomiserController');


Route::get('blog', 'PostController@allPosts')->name('blog');
Route::get('blog/{post}', 'PostController@show')->name('blog.show');
Route::get('blog/{post}/share/{socialMedia}', 'PostController@share')->name('blog.share');

Route::get('mail-test/order/{order}/','CheckoutController@mailTest')->name('checkout.mailTest');
Route::get('mail-test/order-admin/{order}/', 'CheckoutController@adminMailTest')->name('checkout.adminMailTest');
// Pages Controller to control user defined pages:
Route::get('{slug}', 'PageController@show');
