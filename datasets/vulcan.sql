# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.20-0ubuntu0.16.04.1)
# Database: vulcan
# Generation Time: 2018-04-13 14:07:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table shippingOptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shippingOptions`;

CREATE TABLE `shippingOptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flatFee` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `isUKOnly` tinyint(1) NOT NULL DEFAULT '0',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `groupedBy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'world',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `shippingOptions` WRITE;
/*!40000 ALTER TABLE `shippingOptions` DISABLE KEYS */;

INSERT INTO `shippingOptions` (`id`, `name`, `flatFee`, `description`, `isUKOnly`, `country`, `created_at`, `updated_at`, `groupedBy`)
VALUES
	(1,'Royal Mail First Class',3.40,'<p>Your Vulcan product will arrive within 2-3 working days so, if time is not of the essence, this is the option for you.</p>',1,NULL,NULL,'2018-04-13 08:10:51','gb'),
	(2,'Royal Mail Signed For First Class',4.40,'<p>Your order will arrive within 2 working days. Please note that a signature will be required when your Vulcan watch strap arrives so, if you are unable to sign, please make arrangements via the tracking website.</p>',1,NULL,NULL,'2018-04-13 08:11:22','gb'),
	(3,'Royal Mail Special Delivery Guaranteed by 1pm',6.50,'<p>Place your order before 12pm and you&rsquo;ll receive it the next working day. That&rsquo;s style with speed. Orders placed on a Friday will be delivered the following Monday. Please note that a signature will be required when your Vulcan watch strap arrives so, if you are unable to sign, please make arrangements via the tracking website.</p>',1,NULL,NULL,'2018-04-13 08:11:58','gb'),
	(9,'USA - 2 Days',25.00,'<p>Vulcan Watch Straps deliver internationally. However, please bare in mind that all our products are all shipped from the UK so delivery times will vary by country. Broadly speaking we endeavor to deliver all orders within 5-10 working days. For more specific timings please contact hello@vulcanwatchstraps.com.</p>',0,NULL,'2018-04-09 11:39:48','2018-04-09 11:41:35','usa'),
	(8,'International (Europe)',22.50,'<p>..</p>',0,NULL,'2018-04-06 18:28:56','2018-04-09 11:36:18','europe'),
	(5,'Royal Mail Signed For Second class',3.85,'<p>Give yourself something to look forward to over the weekend. Orders placed before 12pm on Friday will arrive on Saturday.</p>',1,NULL,NULL,'2018-04-13 08:12:32','gb'),
	(6,'International Delivery (World Wide)',35.00,'<p>Vulcan Watch Straps deliver internationally. However, please bare in mind that all our products are all shipped from the UK so delivery times will vary by country. Broadly speaking we endeavor to deliver all orders within 5-10 working days. For more specific timings please contact hello@vulcanwatchstraps.com.</p>',0,NULL,NULL,'2018-04-09 11:38:12','world'),
	(7,'Click & Collect',0.00,'<p>Order online and collect your package from one of our designated pick-up points. When your item is ready for collection we&rsquo;ll send you an e-mail to let you know. This convenient service is complimentary for all Vulcan customers.</p>',0,'UK',NULL,'2018-04-12 09:29:27','gb');

/*!40000 ALTER TABLE `shippingOptions` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
