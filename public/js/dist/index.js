"use strict";

var s;
// var s is created as a shortcut for module.settings. will be linked up upon initialisation.

var app = function (app) {

  // reference: http://www.adequatelygood.com/JavaScript-Module-Pattern-In-Depth.html

  app.settings = {
    // add settings or variables here that's accessible to the whole module
    initialised: false,
    ui: {}
  };

  // return the module
  return app;

  // below is scripts to import to this module.
  // MODULE || {} enables the scripts from different augments to be loaded asynchonously, creating the MODULE if it does not already exist.
  // note that module properties will not be able to run before initialisation is complete.
}(app || {}, jQuery);

jQuery(function () {
  app.init();
});