'use strict';

var app = function (app) {

  app.popup = {

    init: function init() {

      jQuery('.popup-trigger, [data-action="popup"]').click(function (e) {
        e.preventDefault();
        var target = '#' + jQuery(this).attr('data-target');
        app.popup.summon(target);
      });

      jQuery('[data-action="dismiss-popup"], .popup-overlay').click(function (e) {
        // e.preventDefault();
        var popup = jQuery(this).parents('.popup-section');
        popup.addClass('is-closing');
        setTimeout(function () {
          popup.removeClass('is-opened is-closing');
        }, 618);
      });
    },

    summon: function summon(target) {
      var popup = jQuery(target);
      popup.addClass('is-opened is-opening');
      setTimeout(function () {
        popup.removeClass('is-opening');
      }, 618);
    }

  };

  return app;
}(app || {}, jQuery);