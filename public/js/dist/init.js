'use strict';

var app = function (app) {

  app.init = function () {

    s = app.settings;

    app.ui.init();

    jQuery('html').attr('data-js', 'on');

    s.initialised = true;
  };

  return app;
}(app || {}, jQuery);