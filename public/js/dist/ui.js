'use strict';

/* jshint esversion: 6 */

var app = function (app) {

  app.ui = {

    init: function init() {

      app.ui.measureUnit();

      var onResize = function onResize() {
        jQuery(document.body).trigger("sticky_kit:recalc");
        app.ui.measureUnit();
      };

      onResize();

      var onScroll = function onScroll() {};

      // jQuery(window).on('load', onLoad);
      jQuery(window).on('resize', onResize);
      jQuery(window).on('scroll', onScroll);
    },

    measureUnit: function measureUnit() {

      s.ui.vw = document.documentElement.clientWidth;
      s.ui.vh = document.documentElement.clientHeight;

      if (s.ui.vw < 768) {
        s.ui.layoutMode = 'phone';
      } else if (s.ui.vw < 1024) {
        s.ui.layoutMode = 'tablet';
      } else {
        s.ui.layoutMode = 'desktop';
      }

      s.ui.isHighDensity = window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches) || window.devicePixelRatio && window.devicePixelRatio > 1.3;

      if (window.devicePixelRatio && devicePixelRatio >= 2) {
        var testElem = document.createElement('div');
        testElem.style.border = '.5px solid transparent';
        document.body.appendChild(testElem);
        if (testElem.offsetHeight == 1) {
          document.querySelector('html').classList.add('hairlines');
        }
        document.body.removeChild(testElem);
      }
    }

  };

  return app;
}(app || {}, jQuery);