'use strict';

var APP = function (app) {

  app.init = function () {
    s = app.settings;

    app.ui.init();
    app.popup.init();

    jQuery('html').attr('data-js', 'on');
    s.initialised = true;

    console.log('Atrium initiated');
  };

  return app;
}(APP || {}, jQuery);
//# sourceMappingURL=init.js.map
