'use strict';

var APP = function (app) {

  app.ui = {

    init: function init() {

      // app.ui.stickyElements();
      app.ui.redDots();
      app.ui.navController();
      app.ui.sidebarController();
      app.popup.init();
      app.notifCards.init();

      // var onLoad = function() {
      // };

      var onResize = function onResize() {
        app.ui.measureUnit();
        $(document.body).trigger("sticky_kit:recalc");
      };

      var onScroll = function onScroll() {};

      onResize();

      // $(window).on('load', onLoad);
      $(window).on('resize', onResize);
      $(window).on('scroll', onScroll);

      console.log('UI controls initiated');
    },

    measureUnit: function measureUnit() {

      // s.ui.unit = document.getElementById('site-header').clientHeight;

      s.ui.vw = document.documentElement.clientWidth;
      s.ui.vh = document.documentElement.clientHeight;

      if (s.ui.vw < 768) {
        s.ui.layoutMode = 'phone';
      } else if (s.ui.vw < 1024) {
        s.ui.layoutMode = 'tablet';
      } else {
        s.ui.layoutMode = 'desktop';
      }

      // s.ui.scrollTop = jQuery(window).scrollTop();

      s.ui.isHighDensity = window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches) || window.devicePixelRatio && window.devicePixelRatio > 1.3;

      if (window.devicePixelRatio && devicePixelRatio >= 2) {
        var testElem = document.createElement('div');
        testElem.style.border = '.5px solid transparent';
        document.body.appendChild(testElem);
        if (testElem.offsetHeight == 1) {
          document.querySelector('html').classList.add('hairlines');
        }
        document.body.removeChild(testElem);
      }
    },

    stickyElements: function stickyElements() {
      $('#view-nav, #widget-panel__content').stick_in_parent({
        offset_top: $('.view-header').outerHeight()
      });
    },

    redDots: function redDots() {
      $('.view-header__control').mouseenter(function () {
        var scaler = 2;
        var dot = jQuery(this).find('.alert-dot');
        if (jQuery(this).find('svg').hasClass('is-big-dot')) {
          scaler = 2.5;
        }
        if (s.ui.vw >= 990) {
          TweenMax.to(dot, 0.4, { scale: scaler, transformOrigin: '50% 50%', ease: Back.easeOut.config(1.618) });
          $(this).mouseleave(function () {
            TweenMax.to(dot, 0.2, { scale: 1, transformOrigin: '50% 50%' });
            $(this).off('mouseleave');
          });
        }
      });
    },

    tabGroups: function tabGroups() {
      // todo: js scrolling function to hide scrollbar
    },

    navController: function navController() {
      $('#site-nav-toggle, .view-nav-sidebar__overlay').click(function () {
        var sidebar = $('#view-nav-sidebar');
        if (sidebar.hasClass('is-opened')) {
          sidebar.addClass('is-closing');
          setTimeout(function () {
            sidebar.removeClass('is-opened is-closing');
          }, 618);
        } else {
          sidebar.addClass('is-opened is-opening');
          setTimeout(function () {
            sidebar.removeClass('is-opening');
          }, 618);
        }
      });
    },

    sidebarController: function sidebarController() {
      $('#widget-panel-toggle, .widget-panel__overlay').click(function () {
        var sidebar = $('#widget-panel');
        if (sidebar.hasClass('is-opened')) {
          sidebar.addClass('is-closing');
          setTimeout(function () {
            sidebar.removeClass('is-opened is-closing');
          }, 618);
        } else {
          sidebar.addClass('is-opened is-opening');
          setTimeout(function () {
            sidebar.removeClass('is-opening');
          }, 618);
        }
      });
    }

  };

  return app;
}(APP || {}, $);
//# sourceMappingURL=ui.js.map
