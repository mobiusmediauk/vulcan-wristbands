<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderCustomStrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderCustomStraps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId');
            $table->string('name');
            $table->string('color');
            $table->string('image');
            $table->integer('qty');
            $table->decimal('unitPrice', 10, 2);
            $table->integer('bandId');
            $table->integer('strapId');
            $table->integer('buckleId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderCustomStraps');
    }
}
