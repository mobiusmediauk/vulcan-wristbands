<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emailTemplates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('body')->nullable();
            $table->integer('ccCopy')->default(0);
            $table->string('ccTo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emailTemplates');
    }
}
