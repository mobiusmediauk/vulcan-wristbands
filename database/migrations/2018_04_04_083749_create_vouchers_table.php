<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->float('amount', 8, 2);
            $table->string('currency');
            $table->integer('quantity')->nullable();
            $table->integer('timesUsed')->default(0);
            $table->enum('type', ['fixed', 'percent']);
            $table->date('expiryDate')->nullable();
            $table->integer('orderId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
