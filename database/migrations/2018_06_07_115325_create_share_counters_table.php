<?php

use App\ShareCounter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shareCounter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('socialMedia');
            $table->integer('count')->default(0);
            $table->timestamps();
        });

        DB::table('shareCounter')->insert(['socialMedia' => 'facebook']);
        DB::table('shareCounter')->insert(['socialMedia' => 'twitter']);
        DB::table('shareCounter')->insert(['socialMedia' => 'pinterest']);
        DB::table('shareCounter')->insert(['socialMedia' => 'email']);
        DB::table('shareCounter')->insert(['socialMedia' => 'download']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareCounter');
    }
}
