<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mainPages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('text1')->nullable();
            $table->text('text2')->nullable();
            $table->text('body1')->nullable();
            $table->text('body2')->nullable();
            $table->text('body3')->nullable();
            $table->text('body4')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('image5')->nullable();
            $table->string('image6')->nullable();
            $table->string('image7')->nullable();
            $table->string('caption1')->nullable();
            $table->string('caption2')->nullable();
            $table->string('caption3')->nullable();
            $table->string('caption4')->nullable();
            $table->string('caption5')->nullable();
            $table->string('caption6')->nullable();
            $table->string('titleTag')->nullable();
            $table->string('metaDescription')->nullable();
            $table->string('altTag1')->nullable();
            $table->string('altTag2')->nullable();
            $table->string('altTag3')->nullable();
            $table->string('altTag4')->nullable();
            $table->string('altTag5')->nullable();
            $table->string('altTag6')->nullable();
            $table->string('altTag7')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mainPages');
    }
}
