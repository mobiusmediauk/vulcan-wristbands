<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressesTableModifierAddMobileField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('addresses', function (Blueprint $table) {
           $table->string('mobile')->nullable()->after('countryCode');
           $table->string('sms')->nullable()->after('mobile');
       });
     }
}
