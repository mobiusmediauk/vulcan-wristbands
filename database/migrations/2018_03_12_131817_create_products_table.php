<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id'); // Standard ID
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');
            $table->string('customiserImage')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('typeId')->references('id')->on('productTypes');
            $table->float('weight', 8, 2)->nullable();
            $table->string('rgb')->nullable();
            $table->string('strapImage')->nullable(); // Strap Product Only
            $table->string('strapAppliedTopImage')->nullable(); // Strap Product Only
            $table->string('strapAppliedBottomImage')->nullable(); // Strap Product Only
            $table->integer('stockCount')->default(0);
            $table->boolean('inStock')->default(0);
            $table->boolean('availableToBuy')->default(1);
            $table->integer('orderId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
