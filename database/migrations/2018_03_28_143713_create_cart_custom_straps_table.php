<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartCustomStrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cartCustomStraps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->integer('strapId');
            $table->integer('bandId');
            $table->integer('buckleId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cartCustomStraps');
    }
}
