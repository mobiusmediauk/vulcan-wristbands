<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status', ['pending', 'dispatched', 'completed', 'cancelled'])->default('pending');
            $table->string('orderCode');
            // $table->integer('voucherId')->references('id')->on('vouchers')->nullable();
            $table->string('orderNumber');
            $table->string('trackingNumber')->nullable();
            $table->integer('shippingOption');
            $table->decimal('shippingPrice', 10, 2);
            $table->decimal('subtotal', 10, 2);
            $table->decimal('discount', 10, 2);
            $table->decimal('total', 10, 2);
            $table->string('currency')->nullable();
            $table->integer('voucherId')->nullable();
            // $table->integer('billingAddressId')->refernces('id')->on('addresses');
            // $table->integer('shippingAddressId')->refernces('id')->on('addresses');
            $table->string('addressId')->nullable();
            $table->string('email')->nullable();
            $table->integer('userId')->nullable();
            // $table->string('note')->nullable();
            // $table->string('method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
