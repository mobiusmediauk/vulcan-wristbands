<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ProductsDatabaseSeeder::class);
         $this->call(ProductTypeDatabaseSeeder::class);
         $this->call(PagesTableSeeder::class);
         $this->call(ShippingOptionsTableSeeder::class);
         $this->call(ProductPricesTableSeeder::class);
         $this->call(SocialMediaTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(MainPagesTableSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(EmailTemplatesTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(LeadConversionTypesTableSeeder::class);
    }
}
