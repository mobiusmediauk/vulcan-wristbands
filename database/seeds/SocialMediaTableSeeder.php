<?php

use Illuminate\Database\Seeder;

class SocialMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('socialMedia')->insert([
          ['name' => 'facebook', 'icon' => 'icon-facebook', 'link' => 'https://www.facebook.com/vulcanwatchstraps/', 'token' => null, 'show' => 1],
          ['name' => 'instagram', 'icon' => 'icon-instagram', 'link' => 'https://www.instagram.com/vulcanwatchstraps/', 'token' => '5874839843.1677ed0.46073d9bc95e4f3abb2be1a029ec7560', 'show' => 1],
          ['name' => 'twitter', 'icon' => 'icon-twitter', 'link' => 'https://twitter.com/VulcanStraps', 'token' => null, 'show' => 1],
          ['name' => 'youtube', 'icon' => 'icon-youtube', 'link' => 'https://www.youtube.com/channel/UCYtv2FAwetYIsNqD3pEJAwQ', 'token' => null, 'show' => 1],
          ['name' => 'pinterest', 'icon' => 'icon-pinterest', 'link' => 'https://www.pinterest.co.uk/vulcanwatch/', 'token' => null, 'show' => 1],
        ]);
    }
}
