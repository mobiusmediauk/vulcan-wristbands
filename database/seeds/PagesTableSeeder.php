<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
        [
        'title' => 'Company Information',
        'header' => 'Company Information',
        'slug' => 'company-information',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Company Information',
        'body' => '<h2>Company Information</h2>
          <p><strong>Postal Address:</strong></br>
          Kemp House</br>
          152-160 City Road</br>
          London</br>
          EC1V 2NX</br></br>
          <strong>Contact telephone number:</strong></br>
          0203 195 3822</br></br>
          <strong>Email Address:</strong></br>
          hello@vulcanwatchstraps.com</br></br>
          <strong>Registration Name:</strong></br>
          Vulcan Watch Straps Ltd</br>
          Place Of Registration:</br>
          Companies House, Cardiff, United Kingdom</br></br>
          <strong>Registered Office Address:</strong></br>
          Kemp House</br>
          152-160 City Road</br>
          London</br>
          EC1V 2NX</br></br>
          <strong>Registered number:</strong></br>
          10907934</br>
          VAT registration Number:</br>
          283 5512 94</br>
          </p>',
        ],
//         [
//         'title' => 'FAQ',
//         'header' => 'FAQ',
//         'slug' => 'faq',
//         'image' => '',
//         'template' => 'default',
//         'position' => '1',
//         'linkName' => 'FAQ',
//         'body' => '<h2>Lorem Ipsum</h2>
// <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
//         ],
//         [
//         'title' => 'Retailer',
//         'header' => 'Retailer',
//         'slug' => 'retailer',
//         'image' => '',
//         'template' => 'default',
//         'position' => '1',
//         'linkName' => 'Retailer',
//         'body' => '<h2>Lorem Ipsum</h2>
// <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
//         ],
//         [
//         'title' => 'Terms of use',
//         'header' => 'Terms Of Use',
//         'slug' => 'terms-of-use',
//         'image' => '',
//         'template' => 'default',
//         'position' => '1',
//         'linkName' => 'Terms of use',
//         'body' => '<h2>Lorem Ipsum</h2>
// <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
//         ],
        [
        'title' => 'Privacy',
        'header' => 'Privacy Policy',
        'slug' => 'privacy',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Privacy',
        'body' => '<h2>PRIVACY POLICY</h2>
<p>At Vulcan Watch Straps we take your privacy and security very seriously. All the data you provide remains completely confidential. However, it should be noted that every now and then we may update our policy, so please occasionally checkback for changes. </p>
<h2>OUR COMMITMENT TO VULCAN CUSTOMERS</h2>
<p>Vulcan are committed to keeping your details safe and only use your personal information to ensure that your shopping experience is of the highest quality.</p>
<p>We strictly comply with the Data Protection Act 1998 and other applicable laws. If you have any questions regarding this, or would like further details, please do not hesitate to contact us at hello@vuclanwatchstraps.com.</p>

<h2>YOUR INFORMATION</h2>
<p>When shopping on our website you will be asked for certain information such as your name, e-mail, billing address and delivery address. We keep a record of these, but rest assured all the information will remain secure. </p>
<p>Vulcan Watch Straps will never store any details of your payment method so youcan shop securely and with peace of mind. All banking details are verified through our card merchant, Worldpay, and will never be saved to our system. Furthermore, Secure Socket Layer (DzSSLdz) encryption technology is used for the protection of all payment transactions. </p>
<p>It should be noted that we keep a record of purchases made through our website and all e-mail correspondences. These records help us to deal with any queries you may have further down the line, whilst also enabling us to tailor the information we send you in the future.</p>
<p>If you would like to revise or delete any of the details you have supplied you can do so by contacting hello@vulcanwatchstraps.com.</p>

<h2>HOW WE USE YOUR PERSONAL DATA</h2>
<p>All the information we collect helps us to improve our services.</p>
<li>We pass your address on to our despatch team, who are often sub-contracted, so they can deliver your order.</li>
<li>We may analyse your shopping habits, e-mails and style preferences. This helps us to tailor our correspondences with you. </li>
<li>Your data helps us to look after our website and ensure that everything is running smoothly.</li>
<li>It enables us to provide you with relevant information about our latest products/news. </li>
<li>We verify your payment method and credit/debit card details using this information.</li>

<h2>SHARING DATA</h2>
<p>We will share data if one of the following scenarios arises:</p>
<li>In the unlikely event that Vulcan Watch Straps is purchased by another company, our data records will also become the property of the new owner. In such an event you will receive a notification and we will do our upmost to ensure that the current privacy policy stays in place. </li>
<li>If requested by police or any other regulatory or government authoritywho are investigating illegal activity we will provide the personal information they require.</li>

<h2>WHAT YOU CAN DO TO STAY SECURE</h2>
<p>When using a public computer always log out once you have finished shopping.This will ensure no-one else can access your personal information or correspondences.</p>
<p>Vulcan also recommend that you do not use any personal information in your passwords.</p>

<h2>CONSENT</h2>
<p>Use of the Vulcan Watch Straps website and disclosure of any personal details marks your consent to us collecting and using this information, as per the policy. Whilst we can not ensure the security of information you transmit to us through the internet, we follow all Data Protection laws and will do our best to ensure your information is safe once it is with us.</p>

<h2>UNSUBSCRIBE FROM MARKETING COMMUNICATIONS</h2>
<p>If you no longer wish to receive updates and news from Vulcan Watch Straps you can Ǯunsubscribeǯ by following the link provided on all e-mails. Weǯll be sad to see you go, so please let us know if thereǯs something we could have done better during your time with us.</p>',
        ],
        [
        'title' => 'Copyright policy',
        'header' => 'Copyright Policy',
        'slug' => 'copyright-policy',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Copyright Policy',
        'body' => '<h2>Copyright Policy</h2><p><strong>Copyright</strong><br>
Please read the following section before using this website<br>
Purpose of the website<br>
All information, images and text presented on this website are for the sole purpose of providing information for promoting the products and services of Vulcan Watch Straps. The information presented does not constitute offers for products and or services of Vulcan Watch Straps or associated retailers.
On rare occasions our website may present information on a product or service which is not currently available in your location. Any reference to such product does not imply that the said product or service is currently or will be in the future available in your location.
<br><br><strong>User Content</strong><br>
As with any information posted on the internet, if you engage with our website using comments in the blog section or forum this information will be accessible by other users. Please take care when posting to ensure you are happy for other people to see what you post.
Vulcan Watch Straps does not claim to have any ownership or any rights to any text, images, video or sound that you post as content on any part of Vulcan Watch Straps website or social media platforms. If you choose to post content on any of our platforms, you will retain full ownership of all rights to the content, and you have the right to use the content in any way you choose.
By posting any content on or through our website, you hereby grant us non-exclusive, transferable, free and worldwide licence to use, modify, publicly perform, publicly display, reproduce, and distribute your user content on and through the website or any social media platform which Vulcan Watch Straps operates, until you remove your content from the platform.
You are solely responsible for the content which you generate and post on the website. You may not post, transmit or share any user content on our website that you did not create or that you do not have permission to post. No user content is necessarily reviewed by Vulcan Watch straps at the time of posting and does not necessarily represent the opinions of Vulcan Watch Straps in any way.
You are prohibited from posting or transmitting any unlawful, libellous, obscene, inflammatory, and threatening, scandalous, pornographic or profane material on our website or any of our platforms.
Vulcan Watch Straps reserves the right to remove any information or materials posted or transmitted by anyone in breach of these rules.<br>
<br><br><strong>Intellectual Property Rights</strong><br>
The website together with any information, text, images, photographs, video clips, audio clips, logos, graphics, symbols, names, and products, as well as al software contained within the website are subject to legal protection, in particular copyright. No website content may be modified, copied or distributed, reproduced, republished or transmitted in part or in whole without prior written consent from Vulcan Watch Straps.
Provided you are eligible for use of the website, you are granted a limited licence to access and use the website and the website content to download or print a copy of any any of the information contained within the website for your personal use only.
<br><br>
Vulcan Watch Straps and other trademarks, logos, fonts, characters, page headers, buttons, icons and other marks (collectively trademarks) displayed on the website are subject to trademark and other rights of Vulcan Watch Straps.
Our trademarks may not be used, including as part of trademarks and or as part of domain names in connection with any product or service that may cause confusion.
Vulcan Watch Straps product designs are also subject to the copyrights outlined above.<br>
If you have any questions about copyright please send us an email to hello@vulcanwatchstraps.com</p>',
        ],
        [
        'title' => 'Return Policy',
        'header' => 'Cancellations, Returns And Refunds',
        'slug' => 'return-policy',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Return Policy',
        'body' => '<h2>HOW TO CANCEL AN ORDER</h2>
<p>If you wish to cancel an order please contact hello@vulcanwatchstraps before12pm on the day of purchase. If it’s after this time your strap may have already been dispatched.</p>
<p>Once your order has been cancelled we will refund you within 14 calendar days.</p>
<p>If your card issuer has not refunded you within 30 days please let us know using the above e-mail address and we will ensure the issue is resolved as quickly as possible. </p>

<h2>RETURNS</h2>
<p>Making a return is simple. And, whilst we’re sorry you weren’t 100% satisfied with your Vulcan product, we understand that everyone has different tastes. </p>
<p>If you decide to return you item(s) you’ll have 14 days to notify us, starting from the day you received the order. </p>
<p>Your return should be sent back to us in the post and we’ll let you know as soon as it has been received. Please note that you will be responsible for the cost of return postage. We also advise that you use recorded delivery because, unfortunately, we can not be held accountable for any goods that go missing in transit.</p>
<p>To make a return please use the following steps: </p>
<li>Pack your Vulcan item(s) in their original packaging and enclose a completed returns slip. Our address is XXX.</li>
<li>Post the package at any Post Office. We recommend you obtain proof of postage.</li>
<li>Keep your proof of postage until you have received a confirmation e-mail from one of the Vulcan team, who will let you know that your refund is being processed.</li>

<h2>FAULTY ITEMS</h2>
<p>We do our best to ensure that all our products meet the highest standards. Each and every item is checked and quality controlled before being dispatched. However, if for any reason you believe your order is faulty or damaged, please speak to our customer service team at hello@vulcanwatchstrap.com and they’ll be happy to help.</p>
<p>In the case of a faulty item you can either:</p>
<li>Receive a full refund. This will be credited to the account that was used to purchase the product.</li>
<li>Receive a replacement. This will be dispatched at the next available postage.</li>
<p>Please do not dispose of the faulty product before you have spoken to one of our client service team as it could affect your refund.</p>

<h2>EXCHANGES</h2>
<p>We understand that sometimes people change their minds so we’re happy to exchange your Vulcan watch strap for one that better suits your style.</p>
<p>As soon as we receive your request one of our team will contact you to arrangepayment for the postage and packaging of your new strap. Once this is completewe will dispatch your order on the same working day.</p>',
        ],
        [
        'title' => 'Warranty',
        'header' => 'Warranty',
        'slug' => 'warranty',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Warranty',
        'body' => '<h2>Warranty</h2><p>International Warranty<br/><br/>
Your new Strap is warranted to be free of manufacturing defects. Should such defects appear during 12 months (1 year) from the date of purchase we will replace the strap free of charge under the terms of this warranty.
<br/><br/>Vulcan Watch Straps will replace at no additional cost to our customer, any products purchased direct through Vulcan Watch Straps or through any authorized retail outlet which are deemed to have any manufacturing defect resulting in the product not operating as intended.
<br/><br/>All warranty cards must be filled out in full at point of purchase to validate this warranty by an authorised retailer of Vulcan Watch Straps.
<br/><br/>During the period of warranty and by presenting the validated warranty card supplied with your Vulcan Watch Strap, you will have the right to have your strap returned and replaced for a new strap free of charge should your strap have any manufacturing defects.
<br/><br/>This warranty does not cover any damage or general wear resulting from the use or misuse of the product, accidental damage or damage as a result of any use for which the product was not intended.
<br/><br/>This manufacturers’ warranty does not cover: wear and tear, any damage resulting from abnormal/abusive use, lack of care, negligence, accidents (knocks, dents, crushing etc.) incorrect use of the strap, negligent installation of strap by unqualified person, or any alterations or damage to the strap which are beyond Vulcan Watch Straps control.
<br/><br/>Our quality procedures ensure that our straps leave our factory in the condition they were intended but if for any reason you are unhappy in any way please contact us at hello@vulcanwatchstraps.com</p>',
        ],
        [
        'title' => 'Delivery',
        'header' => 'Delivery',
        'slug' => 'delivery',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Delivery',
        'body' => '<p>Vulcan offers a variety of delivery options both within the UK and worldwide.</p>
        <h2>Delivery Options</h2>
        <h3>Standard delivery (UK only), £2.50</h3>
        <p>Your Vulcan product will arrive within 2-3 working days
        so, if time is not of the essence, this is the option for you</p>

        <h3>TRACKED 48 DELIVERY (UK only), £3.50</h3>
        <p>Your order will arrive within 2 working days. Please note
        that a signature will be required when your Vulcan watch
        strap arrives so, if you are unable to sign, please make
        arrangements via the tracking website.</p>

        <h3>TRACKED 24 DELIVERY (UK only), £4.00</h3>
        <p>Place your order before 12pm and you’ll receive it the
        next working day. That’s style with speed. Orders placed
        on a Friday will be delivered the following Monday.
        Please note that a signature will be required when your
        Vulcan watch strap arrives so, if you are unable to sign,
        please make arrangements via the tracking website.</p>

        <h3>SPECIAL DELIVERY (UK only), £8.00</h3>
        <p>Place your order before 12pm and we’ll deliver it the next
        working day. Orders placed on a Friday will be delivered
        the following
        Monday.</p>

        <h3>SPECIAL DELIVERY – SATURDAY DELIVERY (UK only), £10.00</h3>
        <p>Give yourself something to look forward to over the
        weekend. Orders placed before 12pm on Friday will
        arrive on Saturday.</p>

        <h3>INTERNATIONAL DELIVERY, £10.00</h3>
        <p>Vulcan Watch Straps deliver internationally. However,
        please bare in mind that all our products are all shipped
        from the UK so delivery times will vary by country.
        Broadly speaking we endeavor to deliver all orders within
        5-10 working days. For more specific timings please
        contact hello@vulcanwatchstraps.com.</p>

        <p>Gift cards are available for free standard delivery across the UK.</p>

        <h2>DELIVERY TIMES</h2>
        <p>We guarantee that all our products will be despatched on the same day, if
        ordered before 12 noon.</p>
        <p>Any goods ordered after this time will be posted the following working day. In
        such cases we’ll send you a confirmation e-mail when your package is en route.</p>
        <p>At Vulcan we always keep to our word and dispatch your item(s) as specified on
        the order. However, occasionally factors out of our hands can cause delays. This
        includes bad weather, postal strikes and overlooked delivery notes. If one of
        these scenarios arises we ask for your understanding. Moreover, we give our
        word that we will do our upmost to rectify the situation as soon as possible.</p>
        <p>If you have any further questions regarding delivery times then do not hesitate
        to contact us at hello@vulcanwatchstraps.com. We are always more than happy
        to help.</p>

        <h2>DELIVERY AND PROMOTIONS</h2>
        <p>If you are taking advantage of a promotional code the specified reduction will
        apply to the price of the product and not the price of delivery (unless otherwise
        stated).</p>
        ',
        ],
        [
          'title' => 'Frequently Asked Questions',
          'header' => 'Frequently Asked Questions',
          'slug'  => 'frequently-asked-questions',
          'image' => '',
          'template' => 'default',
          'position' => '1',
          'linkName' => 'FAQ',
          'body' => '<h2>Frequently asked questions</h2>
<p>
<strong>Where are you straps manufactured?</strong><br>
All parts of our rubber watch straps are designed, manufactured and assembled in the UK using the very best aerospace grade tooling. Our straps are manufactured to the highest of tolerances and to the highest possible quality standards. This ensures that they fit the luxury timepieces they are intended for with the grace and accuracy they deserve. Our rubber straps are manufactured in the same factory where some of the top names in luxury are manufacturing their luxury rubber products, but shhhhh don’t tell anyone.
<br><br><strong>Will your straps fit my wrist size X?</strong><br>
Our straps are designed to fit wrist sizes 16.5cm to 21cm. And because we love our clients so much we have even developed a handy wrist size checker for you to print out and check your personal wrist size to ensure that the strap will fit as intended. Because hey we all got different size wrists – that’s a good thing right.
<br><br><strong>What kind of rubber are your straps made from?</strong><br>
Our rubber is formulated and vulcanised in the UK by leading British Engineers and rubber technologists. We have formulated our own blend of vulcanised rubber which is based on the hardest wearing, longest lasting, most skin friendly robust rubber available on Earth. The base formulation of our rubber is used in medical, petrochemical and pharmaceutical applications all over the world. We have made some tweaks to ensure that the properties of the rubber are reinforced in the areas we need them. Rest assured our materials are amongst the best on the market for watch straps. Clue: These straps are not silicon.
<br><br><strong>What is your warranty period?</strong><br>
Our warranty period is 12 months, you will receive a warranty card within the packaging when you purchase your watch strap, and you don’t need to activate the warranty. Please ensure that the warranty card is filled out at the point of purchase and you are good to go. For more details of our warranty policy please click here.
<br><br><strong>Will your strap fit my x brand watch?</strong><br>
Our watch straps are designed for Rolex watches, they are designed to precisely fit the Rolex models we specify when customising your watch straps during the purchasing process. All of the metallic integration within the watch strap is designed and manufactured to fit the exact Rolex model it was intended for. Our straps may fit other watches by coincidence but we do not recommend or advise you installing our straps on any other watches. You may risk damage to the strap by installing on a watch it was not intended to fit and this may also invalidate your warranty.
<br><br><strong>How long will it take for me to receive my order?</strong><br>
All of our rubber watch straps are shipped via DHL and the chosen delivery method will determine the amount of time it takes to receive your product. There will be tracking options on the DHL website where you can track progress of your strap delivery and we will advise of tracking numbers when the products have been shipped. All watch straps are shipped on the same day if ordered before 12 noon. For more information on shipping click here.
<br><br><strong>Can you accommodate special requests?</strong><br>
We love each and every one of our clients, so if you have a special request we will of course endeavour to do whatever we can to make your buying experience from us be the luxury experience we are intending it to be. While in normal circumstances we cannot have someone like Pamela Anderson hand-deliver your product we will of course do what we can to accommodate and small requests which will make your buying experience a little more special for your or the person receiving the product. If you do have any special requests for your order please add a note to your order during the checkout process or drop us an email to hello@vulcanwatchstraps.com
Can we buy your product to sell on Amazon or other generic auction or sale sites?
We are a luxury retailer and want to ensure that anyone buying our products enjoy the experience which buying a luxury product brings. We carefully vet and consider all of our retailers to ensure they meet the highest standards we set for delivering our clients the absolute best experience they deserve.  We do not knowingly or intentionally sell our products to anyone we know will be selling our products on Amazon or other generic auction or sale sites, in fact we actually do research into our retailers websites too so that we ensure our products are only being sold on the best platforms by the best retailers. And we also carefully only select 1 retailer in every major town or city around the world so that we can ensure the highest of standards throughout our retail network.
<br><br><strong>Are your 18k buckles solid gold or gold plated?</strong><br>
Vulcan Watch Straps provide their clients with the absolute best quality products down to parts which aren’t even seen, all of our 18k gold buckles are solid gold and contain only pure 18k gold therefore they are worth their weight in gold or so the saying goes.
<br><br><strong>Are Vulcan Watch Straps endorsed by or do they work in partnership with any Swiss watch manufacturers?</strong><br>
Vulcan Watch Straps are in no way affiliated with, endorsed by or in partnership with any of the watch manufacturers we manufacture watch straps for. Our products are intended to be an aftermarket product.
<br><br><strong>What is your refund policy?</strong><br>
Full details of our refund policy can be found here.
<br><br><strong>What is return policy?</strong><br>
Full details of our return policy can be found here.
<br><br><strong>What colours do you offer?</strong><br>
You will be surprised at the number of colours we offer our clients. Our base colours include Ice White, Onyx Black, Stone Grey, Sunset Orange, Crimson Red, Emerald Green and Azure Blue but various customisation options are available with the retaining loops. See our customisation page here to customise your colours.
<br><br><strong>Do you manufacture straps for Audermars Piguet, Tudor, Patek Phillipe Panerai etc.?</strong><br>
We are looking at developing a number of straps for a number of manufactures of our favourite watches, the design and development of a new strap is a long and expensive process so while we have a few new straps in mind, they may not  come as fast as you would like. Please bear with us and the more requests we get for a certain strap the quicker they will move up the list. Want to know a secret? Our next 2 products will be the deployment strap for Rolex then a buckle version strap for the Tudor Black Bay Herritage.
<br><br><strong>Do I need an account to shop?</strong><br>
No you do not need an account to shop, if you register for an account and sign up the mailing list it helps us to better serve you as a client, and saves you some time filling in forms. If you do register it helps us to know what you like and your shopping habits so we can try to show you the information which we feel is best suited to you, but it’s not mandatory, just personal preference, you’re welcome to shop with us any way you feel comfortable, it’s your experience.
<br><br><strong>How can we get in touch?</strong><br>
We love to hear from our clients and as such you can get in touch with us in many different ways. The easiest is to go to our contact us page here and fill in the standard form and we will get back to you as soon as we possibly can. Or you can get in touch via all of the normal social media channels such as Facebook, twitter, Instagram and YouTube.
<br><br><strong>Can we tag you / share our purchases on Social media?</strong><br>
Of course yes, in fact we encourage it, we love it when our clients tag us in their images with their new straps on their amazing watches in amazing locations, just use the hashtag #vulcanwatchstraps and be sure to tag us on the various social platforms, you may even feature on our page or website once in a while if you are lucky.
<br><br><strong>Can we buy the strap loop retainers separately?</strong><br>
Yes you can, our brand is all about premium meets personality. We would love you to, but don’t expect you to buy a new complete strap each time you want to change your look. So we have given our clients the ability to be able to purchase the loop retainers separately to give them a few different looks.
<br><br><strong>Can I purchase Vulcan Watch Straps locally in the country I live in?</strong><br>
If we currently have an authorised retailer in your town / city / country then yes of course you can buy the watch straps locally. A list of our available retailers worldwide can be found here.
<br><br><strong>Do Vulcan Watch Straps ship worldwide?</strong><br>
Yes we do, all of our shipments are made by DHL and area available for delivery all over the world, all of our straps are in stock waiting to be customised and shipped to you wherever you are in the world. We would love to see your strap on your watch in your country so be sure to tag us in those social media posts.
<br><br><strong>Do you offer a version of your straps which utilises the deployment clasp which came with my Rolex?</strong><br>
We are currently developing a deployment clasp version of the Vulcan Watch Straps, all of our new products take a while to design, develop and formulate to ensure that we are providing the absolute best quality on the market, we need to manufacture the aerospace grade tooling too so it’s a lengthy process but please feel free to sign up to our mailing list and we can keep you informed of any new products which will be released, and if you have any requests for new products please drop us a line at hello@vulcanwatchstraps.com we would love to hear from you.
<br><br><strong>How do I install my new strap to my watch?</strong><br>
The straps are designed to be easily interchangeable using a basic set of tools. We recommend using our specially designed tool kit and cleaning cloth for changing the straps on your Rolex. Full details of how to change the strap on your Rolex watch are available here.
If you have any questions which are not listed here please feel free to drop us an email to ask anything you want at hello@vulcanwatchstraps.com and we will do our best to answer any questions you may have.
Thank you for your interest in our products and continued support we are truly blessed with the best clients in the world.
<br/>Thank you</p>',
],
        [
        'title' => 'Cookie Policy',
        'header' => 'Cookie Policy',
        'slug' => 'cookie-policy',
        'image' => '',
        'template' => 'default',
        'position' => '1',
        'linkName' => 'Cookies',
        'body' => '<h2>COOKIES</h2>
        <p>We think it’s important that we’re transparent with each and every one of our
        customers. So it’s important that we explain exactly where and how your data is
        stored.</p>

        <h2>WHAT IS A COOKIE?</h2>
        <p>Cookies are pieces of data that are stored by internet browsers. They are used by
        many websites, including ours, to tailor online experiences. Browsers can only
        access these files when users visit the websites that generated them. So,
        in short, cookies are great for remembering preferences and shopping baskets.
        Moreover, they help to deliver luxury user experiences.</p>

        <h2>OUR POLICY</h2>
        <p>The data we collect through cookies is solely to enhance your user experience
        whilst using our website. Vulcan Watch Straps will never sell this data to any
        third parties. We’re not that way inclined.</p>
        <p>By visiting our website, with the correct browser settings, you are consenting to
        us using cookies to enhance your experience whilst on our site. Unfortunately
        our website, like many other retail websites, will not work when cookies
        are blocked.</p>

        <h2>MANAGING COOKIES</h2>
        <p>If cookies aren’t enabled on your computer your shopping expereince will be
        limited to browsing and researching. To enable them please click the “help”
        button on your browser and search for cookie preferences.</p>
        <p>Our website is compatible with the following browsers: Google Chrome, Internet
        Explorer, Mozilla Firefox, Safari, Opera and Google Analytics Opt Out.</p>

        <h2>TYPES OF COOKIES WE USE</h2>
        <p>Session (or transient) cookies
        We use these cookies so that you can switch between websites without being
        logged out or losing what you’re doing on vulcanwatchstraps.com. These cookies
        are stored in your computer’s memory for as long as your browsing session
        lasts. After a period of inactivity, or when the browser is closed, they will become
        inaccessible.</p>
        <p>Persistent cookies
        These help us to remember your preferences so that we’re always providing the
        best service possible. They are stored in your computers memory. They also help us to understand how you&#39;re
        using the site, as we’re continually looking for ways to optimise your shopping
        experience.</p>
        <p>Flash cookies
        These are supported by Adobe Flash. They can store small files on your
        computer that are used in a similar way to internet cookies. They back up data so
        that our website will still recognise you, even if you decide to have a spring
        clean and delete your cookie history.</p>

        <h2>SOCIAL NETWORKS</h2>
        <p>If you share any of our products or content across social networks, such as
        Twitter and Facebook, they may set a cookie on your computer memory.</p>',
        ],
      ]);
    }
}
