<?php

use Illuminate\Database\Seeder;

use App\Media\Slider;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::create([
          'name' => 'Homepage Slider',
          'pageId' => 5,
        ]);
    }
}
