<?php

use Illuminate\Database\Seeder;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emailTemplates')->insert([
          [
            'name' => 'Shipped Order',
            'body' => '<h1>Hi {customer.name},</h1>
                        <p>Your order <strong>{order.number}</strong> has been shipped!</p>
                        <p>Your tracking code <strong>{order.tracking_number}</strong></p>
                        <p>{order.vouchers}</p>
                        <p>Thanks,</p>
                        <p>Vulcan Watch Straps</p>',
          ],
          [
            'name' => 'Received Order',
            'body' => '<h1>Thanks {customer.name} for your order</h1>
                        <p>Thank you for the recent pre-order purchase. The product will be shipped to you June 2018.</p>
                        <p>Your order number <strong>{order.number}</strong></p>
                        <p>We will email you when the shipment has been dispatched.</p>
                        <p>Thanks,</p>
                        <p>Vulcan Watch Straps</p>',
          ],
          [
            'name' => 'Admin - Received Order',
            'body' => '<h1>New Order {order.number}</h1>
                        <p>Received new order&nbsp;from customer <strong>{customer.name}</strong> ({customer.email})</p>
                        <p>------------------------</p>
                        <p><strong>Products without images </strong>order.products<strong>:</strong> {order.products}</p>
                        <p>------------------------</p>
                        <p><strong>Products with images </strong>order.products.withImages<strong>:</strong> {order.products.withImages}</p>
                        <p>------------------------</p>
                        <p><strong>Products list for administrators </strong>order.products.details<strong>:</strong> {order.products.details}</p>
                        <p>------------------------</p>
                        <p><strong>Products list for administrators with images&nbsp;</strong>order.products.details.withImages<strong>:&nbsp;</strong>{order.products.details.withImages}</p>
                        <p>------------------------</p>
                        <p>Customer Information: {order.addresses}</p>',
          ],
        ]);
    }
}
