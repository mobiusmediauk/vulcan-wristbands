<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
          [
            'ordersEmail' => 'orders@vulcanwatchstraps.com',
            'infoEmail' => 'hello@vulcanwatchstraps.com',
          ],
        ]);
    }
}
