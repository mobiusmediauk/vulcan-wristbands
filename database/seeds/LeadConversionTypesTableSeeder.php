<?php

use Illuminate\Database\Seeder;
use App\LeadConversionType;

class LeadConversionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeadConversionType::create([
        'name' => 'Online',
        'slug' => 'online'
      ]);
        LeadConversionType::create([
        'name' => 'From a Friend',
        'slug' => 'from-a-friend'
      ]);
        LeadConversionType::create([
        'name' => 'Featured in magazine',
        'slug' => 'featured-in-magazine'
      ]);
        LeadConversionType::create([
        'name' => 'Featured on blog',
        'slug' => 'featured-on-blog'
      ]);
        LeadConversionType::create([
        'name' => 'Rather not say',
        'slug' => 'rather-not-say'
      ]);
    }
}
