<?php

use Illuminate\Database\Seeder;

class ShippingOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shippingOptions')->insert([
          ['name' => 'Standard Delivery',
            'flatFee' => 2.5,
            'description' => 'Your Vulcan product will arrive within 2-3 working days so, if time is not of the essence, this is the option for you.',
            'isUKOnly' => true],

          ['name' => 'Tracked 48 Delivery',
            'flatFee' => 3.5,
            'description' => 'Your order will arrive within 2 working days. Please note that a signature will be required when your Vulcan watch strap arrives so, if you are unable to sign, please make arrangements via the tracking website.',
            'isUKOnly' => true],

          ['name' => 'Tracked 24 Delivery',
            'flatFee' => 4,
            'description' => 'Place your order before 12pm and you’ll receive it the next working day. That’s style with speed. Orders placed on a Friday will be delivered the following Monday. Please note that a signature will be required when your Vulcan watch strap arrives so, if you are unable to sign, please make arrangements via the tracking website.',
            'isUKOnly' => true],

          ['name' => 'Special Delivery',
            'flatFee' => 8,
            'description' => 'Place your order before 12pm and we’ll deliver it the next working day. Orders placed on a Friday will be delivered the following
Monday.',
            'isUKOnly' => true],

          ['name' => 'Special Delivery - Saturday Delivery',
            'flatFee' => 10,
            'description' => 'Give yourself something to look forward to over the weekend. Orders placed before 12pm on Friday will arrive on Saturday.',
            'isUKOnly' => true],

          ['name' => 'International Delivery',
            'flatFee' => 10,
            'description' => 'Vulcan Watch Straps deliver internationally. However, please bare in mind that all our products are all shipped from the UK so delivery times will vary by country. Broadly speaking we endeavor to deliver all orders within 5-10 working days. For more specific timings please contact hello@vulcanwatchstraps.com.',
            'isUKOnly' => false],

          ['name' => 'Click & Collect',
            'flatFee' => 0,
            'description' => 'Order online and collect your package from one of our designated pick-up points. When your item is ready for collection we’ll send
you an e-mail to let you know. This convenient service is complimentary for all Vulcan customers.',
            'isUKOnly' => false]
        ]);
    }
}
