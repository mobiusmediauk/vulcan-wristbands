<?php

use Illuminate\Database\Seeder;

class ProductTypeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productTypes')->insert([
        [
          'name' => 'Watch',
        ],
        [
          'name' => 'Strap'
        ],
        [
          'name' => 'Band'
        ],
        [
          'name' => 'Buckle'
        ],
        [
          'name' => 'Accessory'
        ],
        [
          'name' => 'Voucher'
        ],
      ]);
    }
}
