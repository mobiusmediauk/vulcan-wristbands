<?php

use Illuminate\Database\Seeder;

class MainPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mainPages')->insert([
          [
            'name' => 'Our Straps',
            'text1' => null,
            'text2' => null,
            'body1' => '<h2>A Strap For Every Passion</h2>
            <p>Sometimes you want to escape city life and push your limits, so we designed a rubber watch strap that can accompany you, whatever the adventure.</p>
            <p>The Vulcan band has been designed with the highest quality materials and tested in the most extreme conditions, so whether your passion is skiing, diving or hiking, the Vulcan Watch Strap won’t fail you.</p>',
            'body2' => '<h2>A Strap For Every Place</h2>
            <p>However, our all-new rubber watchband can go further than just the average ski slope. It’s been designed to withstand the most extreme conditions on this planet: from the hottest dessert valleys to the coldest corners of the Arctic.</p>
            <p>The straps have been developed by British engineers and stress-tested in a lab to ensure they’re tough enough for the most extreme adventurers. The result? A superior material that is unique to Vulcan Watch Straps.</p>
            <p>So, however extreme your exploration, the innovative Vulcan band will retain its properties. And, most importantly, it will always remain comfortable.</p>
            <p>Why? Because we believe the best deserves the best.</p>',
            'body3' => '<h2>Our Guarantee</h2>
            <p>We guarantee that wherever you end up, whether it’s at the top of Everest or the bottom of the Blue Hole, this custom strap will be with you every step of the way.</p>
            <p>It’s British made luxury for every type of explorer.</p>',
            'body4' => null,
            'image1' => 'our-strap-red.jpg',
            'image2' => 'vulcan-red-strap-homepage.png',
            'image3' => 'our-strap-banner.jpg',
            'image4' => 'our-strap-bottom.jpg',
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Our Diffecences',
            'text1' => 'You Will Never Influence The World By Trying To Be Like It.',
            'text2' => 'Sean McCabe',
            'body1' => '<h2>The Material</h2>
            <p>Vulcan Watch Straps are different from anything else on the market: not only do we create them to withstand any condition on the planet, we engineer out any impurities, making them friendly to wear (even for those with sensitive skin).</p>
            <p>The bands have been designed by some of the best engineers in London, alongside some of the worlds most renowned rubber manufactures. But it doesn’t stop there.</p>
            <p>Our Rolex replacement watch bands are manufactured using UK aerospace M1 grade tooling. This ensures the tightest tolerances and the best finishes, meaning style and substance are always met in equal measure.</p>',
            'body2' => '<h2>The Buckle</h2>
            <p>A unique feature on the Vulcan Watch Strap is the metallic mould at the buckle end. This mould ensures the pin is secured through metallic housing at both ends of the strap, rather than just relying on the rubber itself, so you can be assured your Rolex is always in safe hands.</p>
            <p>We also offer premium materials for the buckles themselves including 18k gold, brushed or polished, and black ceramic. Moreover, our buckles are manufactured from a metal injection moulding process, which creates a thicker and more durable buckle. The combination of higher quality materials and higher quality processes means we have created a buckle for your Rolex that will stand the test of time.</p>
            <p>Both the buckles and the retaining loops can be bought separately so customisation is never more than a click away.</p>',
            'body3' => '<h2>The Design</h2>
            <p>We understand that when you’re buying a Rolex replacement watchband you want to retain some of the original design. So we have created a strap with 2 groves to mimic the original Rolex Oyster strap.</p>
            <p>Furthermore, the curved underside of the Vulcan Watch Strap has been uniquely engineered to increase air flow, subsequently minimising skin contact and allowing sweat to escape. Ultimately this design ensures both comfort and style are maintained 24/7.</p>',
            'body4' => null,
            'image1' => 'our-difference-image-2.jpg',
            'image2' => 'our-difference-image-3.jpg',
            'image3' => null,
            'image4' => null,
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Contact Us',
            'text1' => null,
            'text2' => null,
            'body1' => '<p>If you have any questions we’re here to help.</p>
            <p>E-mail hello@vulcanwatchstraps.com and one of our dedicated team will get back to you as soon as possible.</p>
            <p>Otherwise, if you’d like to speak directly to our founder Lee Scott, you can contact him at lee@vulcanwatchstraps.com.</p>',
            'body2' => null,
            'body3' => null,
            'body4' => null,
            'image1' => null,
            'image2' => null,
            'image3' => null,
            'image4' => null,
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Customise Now',
            'text1' => null,
            'text2' => null,
            'body1' => '<h1>Tailor your watch strap</h1>
            <p>The world is your Rolex Oyster! It’s time to show off your style and combine your iconic watch with your own unique design. To make yours simply choose from the options below and see your vision become a reality on the right-hand side.</p>',
            'body2' => null,
            'body3' => null,
            'body4' => null,
            'image1' => null,
            'image2' => null,
            'image3' => null,
            'image4' => null,
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Homepage',
            'text1' => null,
            'text2' => null,
            'body1' => '<h2>Premium Meets<br />Personality</h2>
            <p>Stand out from the crowd with Vulcan’s customisable rubber watch straps. We use the most advanced materials to combine science and style because we understand your watch should be as unique as the life you lead. We’ve engineered it, you customise it.</p>',
            'body2' => '<h2>Match your style</h2>
            <p>Vulcan Watch Straps are uniquely engineered for luxury watches including the Rolex Submariner and Rolex Daytona.</p>
            <p>With our customisable designs you can create numerous colour combinations to best suit your style.</p>
            <p>Start by selecting 1 of 7 base colours for the strap, next pick 1 of these 7 colours for the retaining band and finally choose 1 of 3 premium materials for the buckle, with either a brushed or polished finish.</p>
            <p>So whether your style is a loud and proud combination of Sunset Orange and Azure Blue with an 18k gold polished buckle, or a more subtle Onyx Black complete with a stainless steel buckle, we’ve got something for you.</p>',
            'body3' => '<h2>Innovative materials</h2>
            <p>Vulcan rubber straps have been innovated and manufactured in London. They use aerospace tooling from the highest quality materials to fit Swiss-made luxury watches. Currently, our straps have been specifically created for Rolex watches, but keep your eyes peeled because we’re in the process of expanding our range to cater for Tudor watches.</p>
            <p>A lot of the innovation is in the materials we use, which have been developed to withstand any environment on earth. So whether you’re diving 400 ft in the Blue Hole, trekking across ice caps in Antarctica or travelling through 134 degree heat in Death Valley, our strap will remain comfortable and in tact.</p>
            <p>We’re pushing the limits, so you can too!</p>',
            'body4' => '<h2>Our Story</h2>
            <p>The idea for Vulcan Watch Straps was born by Lee Scott, a qualified mechanical engineer and product designer. Having spent a number of years designing luxury products, one of which can be found at the Burj Al Arab in Dubai, Lee decided it was time to focus on his true passion: watches.</p>
            <p>During the 25 years Lee has been collecting watches he has developed a strong understanding of both the aesthetics and engineering that create timeless perfection.</p>
            <p>If you, like Lee, have an appreciation for luxury but have been patiently waiting for a product that will let you put your own unique touch on your Rolex Sports model then you’ll love Vulcan Watch Straps.</p>',
            'image1' => 'Watch-home-banner.png',
            'image2' => 'vulcan-watch-blue.jpg',
            'image3' => 'vulcan-strap-blue.jpg',
            'image4' => 'vulcan-straps-blue.png',
            'image5' => 'vulcan-watch-floating-emerald-green-right.png',
            'image6' => 'vulcan-strap-green.jpg',
            'image7' => 'vulcan-emerald-green-straps.png',
            'caption1' => 'Sunset Orange',
            'caption2' => 'Azure Blue',
            'caption3' => 'Azure Blue',
            'caption4' => 'Emerald Green',
            'caption5' => 'Emerald Green',
            'caption6' => 'https://www.youtube.com/embed/Pg8hrWT8cxs?rel=0&amp;controls=0&amp;showinfo=0',
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Accessories',
            'text1' => null,
            'text2' => null,
            'body1' => null,
            'body2' => null,
            'body3' => null,
            'body4' => null,
            'image1' => null,
            'image2' => null,
            'image3' => null,
            'image4' => null,
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
          [
            'name' => 'Blog',
            'text1' => null,
            'text2' => null,
            'body1' => null,
            'body2' => null,
            'body3' => null,
            'body4' => null,
            'image1' => null,
            'image2' => null,
            'image3' => null,
            'image4' => null,
            'image5' => null,
            'image6' => null,
            'image7' => null,
            'caption1' => null,
            'caption2' => null,
            'caption3' => null,
            'caption4' => null,
            'caption5' => null,
            'caption6' => null,
            'titleTag' => null,
            'metaDescription' => null,
            'altTag1' => null,
            'altTag2' => null,
            'altTag3' => null,
            'altTag4' => null,
            'altTag5' => null,
            'altTag6' => null,
            'altTag7' => null,
          ],
        ]);
    }
}
